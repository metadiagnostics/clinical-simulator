/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.flowsheet.model;

import com.cognitivemedicine.common.gson.RuntimeTypeAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.InputStreamReader;
import java.io.Reader;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class SmartFormSerializationTest {
    
    private Gson gson;
    
    @Before
    public void doBefore(){
        RuntimeTypeAdapterFactory<SmartFormDefinitionDTO.ItemType> typeFactory = RuntimeTypeAdapterFactory  
        .of(SmartFormDefinitionDTO.ItemType.class, "type") 
        .registerSubtype(SmartFormDefinitionDTO.ItemTypeChoice.class, "choice")
        .registerSubtype(SmartFormDefinitionDTO.ItemTypeComputed.class, "computed");
        
        gson = new GsonBuilder().registerTypeAdapterFactory(typeFactory).create();
    }
    
    @Test
    public void doSmartFormDefinitionDTODeserializationTest(){
        
        Reader json = new InputStreamReader(SmartFormSerializationTest.class.getResourceAsStream("/serialized-dtos/smartFormDefinitionDTO.json"));
        
        SmartFormDefinitionDTO dto = gson.fromJson(json, SmartFormDefinitionDTO.class);
        
        System.out.println("");
        
    }
    
    @Test
    public void doSmartFormResponsesDTODeserializationTest(){
        Reader json = new InputStreamReader(SmartFormSerializationTest.class.getResourceAsStream("/serialized-dtos/smartFormResponsesDTO-1.json"));
        
        SmartFormResponsesDTO dto = gson.fromJson(json, SmartFormResponsesDTO.class);
        
        System.out.println("");
    }
}
