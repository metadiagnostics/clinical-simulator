package com.cognitivemedicine.common.note;

public class ClinicalNoteType {
    public static final String CREATED_DATE_TIME = "createdDateTime";
    public static final String TITLE = "title";
    public static final String LOCATION = "location";
    public static final String DOCUMENT_TYPE = "documentType";
    public static final String LAST_UPDATED = "lastUpdated";
    public static final String AUTHOR = "author";
    public static final String SPECIALTY = "specialty";
    public static final String AUTHOR_TYPE = "authorType";
    public static final String STATUS = "status";
    public static final String ID = "id";
}
