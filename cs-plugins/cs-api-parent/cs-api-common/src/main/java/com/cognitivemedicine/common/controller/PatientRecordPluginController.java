package com.cognitivemedicine.common.controller;

import com.cognitivemedicine.common.service.DataSubscriptionService;
import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import com.cognitivemedicine.common.util.Utilities;
import org.apache.commons.lang.StringUtils;
import org.carewebframework.api.event.IGenericEvent;
import org.carewebframework.shell.elements.ElementPlugin;
import org.carewebframework.shell.elements.ElementTabPane;
import org.carewebframework.shell.plugins.PluginController;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.fhirsub.ISubscriptionCallback;
import org.hspconsortium.cwf.api.fhirsub.SubscriptionWrapper;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class PatientRecordPluginController extends PluginController {
    @Autowired
    private DataSubscriptionService dataSubscriptionService;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private static final String CLIENT_CALLBACK_ID = "_callback";
    private static final String CLIENT_UUID = "_uuid";
    private List<SubscriptionWrapper> subscriptions;
    private boolean dataChanged = true;

    private final IGenericEvent<?> patientChangeListener = (eventName, eventData) -> {
        this.setVisibility();
    };

    private final ISubscriptionCallback dataChangeListener = (eventName, resource) -> {
        if (resource.getMeta() != null && resource.getMeta().getVersionId() != null && this.isActive()) {
            this.publishSingleModelUpdate(resource);
            this.dataChanged = false; // We are updating the UI on the fly so it will already be up-to-date
        } else {
            this.dataChanged = true;
        }
    };

    // This works in combination with the patient search plugin to handle switching tabs
    private void setVisibility() {
        if (PatientContext.getActivePatient() == null) {
            this.unsubscribeFromUpdates();
            this.dataChanged = true;
            this.getPlugin().setVisible(false);
        } else {
            this.getPlugin().setVisible(true);
        }
    }

    /**
     * This will clean up existing data subscriptions on the server for this controller
     */
    @Override
    protected void cleanup() {
        super.cleanup();
        this.unsubscribeFromUpdates();
    }

    @Override
    public void onLoad(ElementPlugin plugin) {
        super.onLoad(plugin);
        this.setVisibility();
        getEventManager().subscribe(Utilities.PATIENT_CONTEXT_EVENT, this.patientChangeListener);
    }

    @Override
    public void onUnload() {
        super.onUnload();
        getEventManager().unsubscribe(Utilities.PATIENT_CONTEXT_EVENT, this.patientChangeListener);
    }

    @EventHandler(value="openDrawer")
    public void openDrawer(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        getEventManager().fireLocalEvent(Utilities.EVENT_OPEN_DRAWER, params);
    }

    @Override
    public void onInactivate() {
        super.onInactivate();
    }

    @Override
    public void onActivate() {
        super.onActivate();
        Map<String, Object> response = new HashMap<>();
        boolean flag = this.getDataSubscriptionConfiguration() == null || this.dataChanged;
        response.put("dataChanged", flag);

        this.ngInvoke("activatePlugin", response);
    }

    protected Date convertToDate(String date) {
        Date jDate = null;
        try {
            jDate = this.dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jDate;
    }

    protected void addMetadata(Map<String, Object> data, String uuid, String message) {
        if (data != null) {
            Map<String, Object> metadata = new HashMap<>();
            metadata.put("_uuid", uuid);

            if (message != null) {
                metadata.put("message", message);
            }
            data.put("metadata", metadata);
        }
    }

    protected void ngInvoke(Map<String, Object> params, Object payload) {

        String callback = (String) params.get(CLIENT_CALLBACK_ID);
        String uuid = (String) params.get(CLIENT_UUID);

        if (!StringUtils.isBlank(callback) && this.getPlugin().getInnerComponent() != null) {
            AngularComponent component = this.getPlugin().getInnerComponent().getChild(AngularComponent.class);

            if (component != null) {
                AngularCallbackResponse response = new AngularCallbackResponse(uuid, payload);
                component.ngInvoke(callback, response);
            }
        }
    }

    protected void ngInvoke(String functionName, Object payload) {
        if (this.getPlugin().getInnerComponent() != null) {
            AngularComponent component = this.getPlugin().getInnerComponent().getChild(AngularComponent.class);

            if (component != null) {
                AngularCallbackResponse response = new AngularCallbackResponse(null, payload);
                component.ngInvoke(functionName, response);
            }
        }
    }

    protected void subscribeToUpdates() {
        unsubscribeFromUpdates();

        if (this.getDataSubscriptionConfiguration() != null) {
            this.subscriptions = this.dataSubscriptionService.subscribeToUpdates(PatientContext.getActivePatient(), this.getDataSubscriptionConfiguration());

            if (this.subscriptions != null) {
                for (SubscriptionWrapper subscription : this.subscriptions) {
                    getEventManager().subscribe(subscription.getEventName(), this.dataChangeListener);
                }
            }
        }
    }

    protected void setDataChanged(boolean dataChanged) {
        this.dataChanged = dataChanged;
    }

    protected void unsubscribeFromUpdates() {
        if (this.subscriptions != null) {
            this.dataSubscriptionService.unsubscribeFromUpdates(this.subscriptions);

            for (SubscriptionWrapper subscription : this.subscriptions) {
                getEventManager().unsubscribe(subscription.getEventName(), this.dataChangeListener);
            }

            this.subscriptions = null;
        }
    }

    protected abstract DataSubscriptionConfiguration getDataSubscriptionConfiguration();
    protected abstract void publishSingleModelUpdate(IBaseResource resource);
}
