package com.cognitivemedicine.common.flowsheet;

public enum FlowsheetType {
    I_O, MEDS, VITALS
}
