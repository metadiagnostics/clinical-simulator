/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.flowsheet.mar;

import com.cognitivemedicine.common.flowsheet.FlowSheetConstants;
import com.cognitivemedicine.common.flowsheet.FlowsheetCellValue;
import com.cognitivemedicine.common.flowsheet.FlowsheetRowModel;
import com.cognitivemedicine.common.util.Utilities;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;

import java.util.*;

/**
 * Contains a data structure representation of the UI flowsheet grid model from a FHIR bundle for IO
 */
public class FlowsheetModel {

    private final List<Date> headerNames;

    private final List<FlowsheetRowModel> rowNames;

    private final List<List<FlowsheetCellValue>> rows;

    private final Date userStartTime;

    private final Date userEndTime;

    private final List<MedicationAdministration> medicationBundle;

    private final Map<String, List<MedicationAdministration>> medicationAdministrationsByRequestMap;
    private final Map<String, MedicationRequest> medicationRequestMap;

    private static final Log log = LogFactory.getLog(FlowsheetModel.class);

    /**
     * The constructor create the grid automatically
     *
     * @param medicationBundle
     * @param userStartTime
     * @param userEndTime
     */
    public FlowsheetModel(List<MedicationAdministration> medicationBundle, Date userStartTime, Date userEndTime) {
        headerNames = new LinkedList<>();
        rowNames = new LinkedList<>();
        rows = new ArrayList<>();
        medicationAdministrationsByRequestMap = new HashMap<>();
        medicationRequestMap = new HashMap<>();

        this.medicationBundle = medicationBundle;
        this.userStartTime = userStartTime;
        this.userEndTime = userEndTime;

        processBundle();
    }

    private List<MedicationAdministration> createMedicalAdministrations(){
        return this.medicationBundle;
    }

    /**
     * Returns {@link Medication} from {@link MedicationRequest}
     *
     * @param medicationRequest
     * @return Medication from request.
     */
    private Medication getMedication(MedicationRequest medicationRequest) {
        if (medicationRequest.getMedication() != null && medicationRequest.getMedication() instanceof CodeableConcept) {
            try {
                CodeableConcept codeableConcept = (CodeableConcept) medicationRequest.getMedication();
                Medication medication = new Medication();
                medication.setCode(codeableConcept);
                return medication;
            } catch(Exception e) {
                return null;
            }
        } else {
            try {
                if (medicationRequest.getMedicationReference() != null
                        && medicationRequest.getMedicationReference().getResource() != null) {
                    return (Medication) medicationRequest.getMedicationReference().getResource();
                }
                throw new RuntimeException("Medication reference or concept are missing from the medication request");
            } catch (FHIRException e) {
                throw new RuntimeException("Error getting the medication from the medication request");
            }
        }
    }
    /**
     * Creates headerNames, rowNames and rows
     */
    private void processBundle() {
        if (medicationBundle != null) {
            createMedicationRequestMap();

            Medication medication;
            MedicationRequest medicationRequest;
            List<MedicationAdministration> medicationAdministrations;
            for (Map.Entry<String, List<MedicationAdministration>> entry : medicationAdministrationsByRequestMap.entrySet()) {
                medicationRequest = medicationRequestMap.get(entry.getKey());
                medicationAdministrations = entry.getValue();
                medication = getMedication(medicationRequest);

                if (medication != null) {
                    Timing timing = medicationRequest.getDosageInstruction().get(0).getTiming();
                    Date medicationStartDate = getStartDate(timing);
                    if (medicationStartDate != null) {
                        Date medicationEndDate = getEndDate(timing);
                        long frequency = getFrequency(timing);
                        long tolerance = getTolerance(medicationRequest);
                        // TODO finalize this when we have the final requirements.
                        String resourceID = medicationRequest.getIdElement().getIdPart();

                        //create the medication request row
                        FlowsheetRowModel rm = new FlowsheetRowModel();
                        rm.setDisplayName(getMedicationNameWithDosage(medication, medicationRequest));
                        rm.setCode(medication.getCode().getCoding().get(0).getCode());
                        rm.setCodeSystem(medication.getCode().getCoding().get(0).getSystem());
                        rm.setType(FlowSheetConstants.Type.NONE);
                        rm.setResourceID(resourceID);
                        String confidentiality = "N";
                        if (medication.getMeta().getSecurity() != null) {
                            List<Coding> codes = medication.getMeta().getSecurity();
                            Iterator iter = codes.iterator();
                            while (iter.hasNext()) {
                                Coding code = (Coding) iter.next();
                                if (code.getSystem().equals("http://hl7.org/fhir/v3/Confidentiality")) {
                                    confidentiality = code.getCode();
                                    break;
                                }
                            }
                        }
                        rm.setConfidentialy(confidentiality);
                        rowNames.add(rm);
                        List<FlowsheetCellValue> row = addRow();

                        Set<Date> tempHeaders = getTempHeaders(medicationAdministrations, medicationStartDate, medicationEndDate,
                                frequency);
                        setRowValues(medicationAdministrations, tempHeaders, row, tolerance);
                    }
                }
            }
        }
    }

    /**
     * Adds all MedicationAdministrations to a map of MedicationRequests
     */
    private void createMedicationRequestMap() {
        for (MedicationAdministration medicationAdministration : medicationBundle) {
            addToMedicationRequestMap(medicationAdministration);
        }
    }

    /**
     * Addes MedicationAdministrations to a keyed MedicationRequest
     *
     * @param medicationAdministration
     */
    private void addToMedicationRequestMap(MedicationAdministration medicationAdministration) {
        MedicationRequest medicationRequest = (MedicationRequest) medicationAdministration.getPrescription().getResource();
        List<MedicationAdministration> medicationAdministrations = new ArrayList<>();

        if (medicationRequest != null) {
            String medicationRequestId = medicationRequest.getIdElement().getIdPart();
            medicationRequestMap.put(medicationRequestId, medicationRequest);
            if (medicationAdministrationsByRequestMap.containsKey(medicationRequestId)) {
                medicationAdministrations = medicationAdministrationsByRequestMap.get(medicationRequestId);
            } else {
                medicationAdministrationsByRequestMap.put(medicationRequestId, medicationAdministrations);
            }
        }

        medicationAdministrations.add(medicationAdministration);
    }

    /**
     * Get the start date from timing. Throwing a runtime exception instead.
     *
     * @param timing
     * @return
     */
    private Date getStartDate(Timing timing) {
        try {
            Timing.TimingRepeatComponent repeatComponent = timing.getRepeat();
            if (repeatComponent.getBounds() == null) {
                return null;
            }

            Period boundsPeriod = repeatComponent.getBoundsPeriod();
            return timing.getRepeat().getBoundsPeriod().getStart();
        } catch (FHIRException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Get the end date from timing. Throwing a runtime exception instead.
     *
     * @param timing
     * @return
     */
    private Date getEndDate(Timing timing) {
        try {
            return timing.getRepeat().getBoundsPeriod().getEnd();
        } catch (FHIRException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Create the data row for medication request based on the medication administrations - INPUT
     * DATA
     *
     * @param medicationAdministrations
     * @param tempHeaders
     * @param row
     * @param tolerance
     */
    private void setRowValues(List<MedicationAdministration> medicationAdministrations, Set<Date> tempHeaders,
                              List<FlowsheetCellValue> row, long tolerance) {
        List<Date> dates = getDates(medicationAdministrations);
        Collections.sort(dates);
        long currentTimeLong = new Date().getTime();

        //Create the headers based on the dosage instructions to get the DUE and OVERDUE
        for (Date tempHeader : tempHeaders) {
            MedicationAdministration medicationAdministration = contains(medicationAdministrations, tempHeader, tolerance);
            if (medicationAdministration != null) {
                Date header = getMedicationAdministrationDate(medicationAdministration);
                createHeader(header);

                int index = headerNames.indexOf(header);

                SimpleQuantity simpleQuantity = medicationAdministration.getDosage().getDose();
                String id = medicationAdministration.getIdElement().getIdPart();
                row.set(index, new FlowsheetCellValue(
                        simpleQuantity.getValue().doubleValue(),
                        simpleQuantity.getUnit(),
                        FlowsheetCellValue.Info.NONE,
                        FlowSheetConstants.Type.NONE, id));
            } else {
//                createHeader(tempHeader);
//                int index = headerNames.indexOf(tempHeader);
//
//                //check to see if the time that is was expected to be administered is past the allowed tolerance of the current time
//                if (tempHeader.getTime() <= currentTimeLong - tolerance) {
//                    row.set(index, new FlowsheetCellValue("OVERDUE"));
//                } else {
//                    //todo should all future ones be DUE or just the ones within the 30 minute time slot, currently shows DUE for all future ones
//                    row.set(index, new FlowsheetCellValue("DUE"));
//                }
            }
        }

        //Create headers for any medication administrations that are outside of the dosage instruction times
        for (MedicationAdministration medicationAdministration : medicationAdministrations) {
            Date medAdminDate = getMedicationAdministrationDate(medicationAdministration);
            if (!headerNames.contains(medAdminDate)) {
                medAdminDate = Utilities.trimSeconds(medAdminDate);
                createHeader(medAdminDate);

                int index = headerNames.indexOf(medAdminDate);
                SimpleQuantity simpleQuantity = medicationAdministration.getDosage().getDose();
                String id = medicationAdministration.getIdElement().getIdPart();
                row.set(index, new FlowsheetCellValue(
                        simpleQuantity.getValue().doubleValue(),
                        simpleQuantity.getUnit(),
                        FlowsheetCellValue.Info.NONE,
                        FlowSheetConstants.Type.NONE, id));
            }
        }
    }

    /**
     * Get the medication administration that matches the date and tolerance
     *
     * @param medicationAdministrations
     * @param createdHeader
     * @param tolerance
     * @return
     */
    private MedicationAdministration contains(List<MedicationAdministration> medicationAdministrations, Date createdHeader,
                                              long tolerance) {
        long headerTime = createdHeader.getTime();
        long leftTime = headerTime - tolerance;
        long rightTime = headerTime + tolerance;

        Date medAdminDate;
        for (MedicationAdministration medicationAdministration : medicationAdministrations) {
            medAdminDate = getMedicationAdministrationDate(medicationAdministration);

            if (leftTime <= medAdminDate.getTime() && medAdminDate.getTime() <= rightTime) {
                return medicationAdministration;
            }
        }

        return null;
    }

    /**
     * Return effective date from {@link MedicationAdministration}
     *
     * @param medicationAdministration
     * @return
     */
    private Date getMedicationAdministrationDate(MedicationAdministration medicationAdministration) {
        return ((DateTimeType) medicationAdministration.getEffective()).getValue();
    }

    /**
     * Create the column headers for a particular medication request
     *
     * @param startDate
     * @param frequency
     * @return
     */
    private Set<Date> getTempHeaders(List<MedicationAdministration> medicationAdministrations, Date startDate, Date endDate,
                                     long frequency) {
        Set<Date> setDates = new HashSet<>();
        List<Date> dates = getDates(medicationAdministrations);
        Collections.sort(dates);
        Date leftDate = dates.get(0);

        int i = -1;
        long startTime = startDate.getTime();
        while (true) {
            i++;
            long time = startTime + (frequency * i);

            if (time > userEndTime.getTime()) {
                break;
            }
            if (endDate != null && time > endDate.getTime()) {
                break;
            }
            if (userStartTime.getTime() <= time && leftDate.getTime() <= time) {
                Date date = new Date(time);
                setDates.add(date);
            }
        }

        return setDates;
    }

    /**
     * Adds a header to the headerNames List
     *
     * @param date
     */
    private void createHeader(Date date) {
        //check to see if we already have the time
        if (!headerNames.contains(date)) {
            long startTime = date.getTime();
            int index = -1;

            for (int i = 0; i < headerNames.size(); i++) {
                long time = headerNames.get(i).getTime();
                if (startTime < time) {
                    index = i;
                    break;
                }
            }

            //check the index for the time
            if (index == -1) {
                appendColumn(date);
            } else {
                insertColumn(date, index);
            }
        }
    }

    /**
     * Get all the medication administrations dates for a particular medication request
     *
     * @param medicationAdministrations
     * @return
     */
    private List<Date> getDates(List<MedicationAdministration> medicationAdministrations) {
        List<Date> dates = new ArrayList<>();

        for (MedicationAdministration medicationAdministration : medicationAdministrations) {
            Date date = ((DateTimeType) medicationAdministration.getEffective()).getValue();
            //trim the seconds for I&O
            //date = Utilities.trimSeconds(date);
            if (!dates.contains(date)) {
                dates.add(date);
            }
        }

        return dates;
    }

    /**
     * Append a column header
     *
     * @param startDate
     */
    private void appendColumn(Date startDate) {
        headerNames.add(startDate);
        for (List<FlowsheetCellValue> row : rows) {
            row.add(null);
        }
    }

    /**
     * Insert a column header
     *
     * @param startDate
     * @param index
     */
    private void insertColumn(Date startDate, int index) {
        headerNames.add(index, startDate);
        for (List<FlowsheetCellValue> row : rows) {
            row.add(index, null);
        }
    }

    /**
     * Makes a call to determine the tolerance of dosages such as must be within 15 minutes of the
     * schedule time Currently hard coded to 15 minutes. Plan to make a call to another service
     *
     * @param medicationRequest
     * @return
     */
    private long getTolerance(MedicationRequest medicationRequest) {
        return 1000 * 60 * 15; // 15 minutes
    }

    /**
     * Returns the time in milliseconds between dosages Currently only implemented for times per day
     *
     * @param timing
     * @return Time in milliseconds between dosages.
     */
    private long getFrequency(Timing timing) {
        Timing.TimingRepeatComponent repeat = timing.getRepeat();
        Timing.UnitsOfTime unitOfTime = repeat.getPeriodUnit();

        switch (unitOfTime) {
            case D:
                return Utilities.HOUR * (24 / repeat.getFrequency());
            default:
                throw new RuntimeException("Frequency of " + unitOfTime + " not implemented yet");
        }
    }

    /**
     * Gets the medication name with the dosage instructions
     *
     * @param medication
     * @param medicationRequest
     * @return Medication name with dosage.
     */
    private String getMedicationNameWithDosage(Medication medication, MedicationRequest medicationRequest) {
        Dosage dosage = medicationRequest.getDosageInstruction().get(0);
        String dosageText = dosage.getText();

        if (dosageText != null && !dosageText.trim().isEmpty()) {
            String medicationName = medication.getCode().getCoding().get(0).getDisplay();
            medicationName = Utilities.stripAfterOpenParentheses(medicationName);
            return medicationName + " (" + dosageText + ")";
        } else {
            SimpleQuantity quantityObj = (SimpleQuantity) dosage.getDose();
            String display = medication.getCode().getCoding().get(0).getDisplay();
            String quantity = quantityObj.getValue() + " " + quantityObj.getUnit();

            return display + " (" + quantity + ")";
        }
    }

    /**
     * Returns {@link Medication} from {@link MedicationRequest}
     *
     * @param medicationAdministration
     * @return Medication from MedicationAdministration.
     */
    private CodeableConcept getCodeableConcept(MedicationAdministration medicationAdministration) {
        try {
            CodeableConcept codeableConcept = medicationAdministration.getMedicationCodeableConcept();
            return codeableConcept;
//            if (medicationAdministration.getMedicationCodeableConcept() != null
//                    && medicationAdministration.getMedicationReference().getResource() != null) {
//                return (Medication) medicationAdministration.getMedicationReference().getResource();
//            }
//                throw new RuntimeException("Medication reference or concept are missing from the medication request");
        } catch (FHIRException e) {
//                throw new RuntimeException("Error getting the medication from the medication administration");
        }
        return null;
    }

    /**
     * Creates a row for the grid
     *
     * @return List of cells belonging to the added row.
     */
    private List<FlowsheetCellValue> addRow() {
        List<FlowsheetCellValue> cells = new LinkedList<>();

        for (int i = 0; i < headerNames.size(); i++) {
            cells.add(null);
        }

        rows.add(cells);

        return cells;
    }

    /**
     * Get grid date headers
     *
     * @return List of date headers.
     */
    public List<Date> getHeaderNames() {
        return headerNames;
    }

    /**
     * Get data rows
     *
     * @return List of cells within their respective rows.
     */
    public List<List<FlowsheetCellValue>> getRows() {
        return rows;
    }

    /**
     * Get names column
     *
     * @return List of row names.
     */
    public List<FlowsheetRowModel> getRowNames() {
        return rowNames;
    }

}