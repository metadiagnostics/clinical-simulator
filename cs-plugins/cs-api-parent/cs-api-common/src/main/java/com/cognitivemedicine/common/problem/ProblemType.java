package com.cognitivemedicine.common.problem;

public class ProblemType {
    public static final String PROBLEM = "problem";
    public static final String ONSET_DATE = "onsetDate";
    public static final String ACUITY = "acuity";
    public static final String FACILITY = "facility";
    public static final String PROVIDER = "provider";
    public static final String REPORTED_DATE = "reportedDate";
    public static final String RESOLVED_DATE = "resolvedDate";
    public static final String FACTOR_NAME = "factorName";
    public static final String CONTRIBUTION = "contribution";
    public static final String NAME = "name";
    public static final String DATE_TIME = "dateTime";
    public static final String ASSESSMENT = "assessment";
    public static final String ID = "id";
    public static final String BAR = "bar";
}
