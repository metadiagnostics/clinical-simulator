package com.cognitivemedicine.common.diagnosticreport;

public class DiagnosticReportType {
    public static final String ID = "id";
    public static final String PANEL = "panel";
    public static final String STUDY = "study";
    public static final String PERFORMER = "performer";
    public static final String ORDERING_PROVIDER = "orderingProvider";
    public static final String STATUS = "status";
    public static final String FACILITY = "facility";
    public static final String COLLECTION_DATE_TIME = "collectionDateTime";
    public static final String RESULT_DATE_TIME = "resultDateTime";
    public static final String LAST_UPDATED = "lastUpdated";
    public static final String COMPONENT = "component";
    public static final String RESULT_VALUE = "resultValue";
    public static final String UNITS = "units";
    public static final String REFERENCE_RANGE = "referenceRange";
    public static final String INTERPRETATION = "interpretation";
    public static final String QUERY_PENDING = "pending";
}
