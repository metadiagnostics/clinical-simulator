/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author esteban
 */
public class MimeTypeUtils {
    
    private final static String MIME_TYPE_FILE = "/mime-types/mime.types";
    
    private static Map<String, String> extensionByMimeType = new HashMap<>();
    
    static {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(MimeTypeUtils.class.getResourceAsStream(MIME_TYPE_FILE)));
            
            String line;
            while ((line = reader.readLine()) != null){
                String[] parts = line.split("=");
                String extension = parts[1];
                
                if (extension.contains(",")){
                    extension = extension.split(",")[0];
                }
                
                extensionByMimeType.put(parts[0], extension);
            }
        } catch (IOException ex) {
            throw new IllegalStateException("Exceptions loading mime types map", ex);
        }
    }
    
    public static String getExtensionFromMimeType(String mimeType, String defaultExtension){
        return extensionByMimeType.getOrDefault(mimeType, defaultExtension);
    }
    
}
