/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.controller;

/**
 *
 * @author esteban
 */
public class AngularCallbackResponse<T> {
    
    private String _uuid;
    private boolean error;
    private String errorMessage;
    private T payload;

    public AngularCallbackResponse(String uuid) {
        this._uuid = uuid;
    }

    public AngularCallbackResponse(String uuid, boolean error, String errorMessage, T payload) {
        this._uuid = uuid;
        this.error = error;
        this.errorMessage = errorMessage;
        this.payload = payload;
    }

    public AngularCallbackResponse(String uuid, T payload) {
        this._uuid = uuid;
        this.payload = payload;
    }

    public String get_uuid() {
        return _uuid;
    }

    public void set_uuid(String _uuid) {
        this._uuid = _uuid;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
    
    
    
    
}
