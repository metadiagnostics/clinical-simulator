/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.flowsheet.vitals;

import com.cognitivemedicine.common.flowsheet.FlowSheetConstants;
import com.cognitivemedicine.common.flowsheet.FlowsheetCellValue;
import com.cognitivemedicine.common.flowsheet.FlowsheetRowModel;
import com.cognitivemedicine.common.util.Utilities;
import com.cognitivemedicine.cs.client.ClientUtilities;
import com.cognitivemedicine.cs.models.FormResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Contains a data structure representation of the UI flowsheet grid model from a FHIR bundle for IO
 */
public class FlowsheetModel {
    private final List<Date> headerNames;
    private final List<FlowsheetRowModel> rowNames;
    private final List<List<FlowsheetCellValue>> rows;
    private final Date userStartTime;
    private final Date userEndTime;
    private final List<Observation> observationBundle;
    private final FormResponse[] formResponses;
    private final Map<String, List<Observation>> outputMap;
    private List<Observation> observations;

    private static final Log log = LogFactory.getLog(FlowsheetModel.class);

    /**
     * The constructor create the grid automatically
     *
     * @param observationBundle
     * @param userStartTime
     * @param userEndTime
     */
    public FlowsheetModel(List<Observation> observationBundle, FormResponse[] formResponses, Date userStartTime, Date userEndTime) {
        headerNames = new LinkedList<>();
        rowNames = new LinkedList<>();
        rows = new ArrayList<>();
        outputMap = new HashMap<>();


        this.observationBundle = observationBundle;
        this.formResponses = formResponses;
        this.userStartTime = userStartTime;
        this.userEndTime = userEndTime;

        processBundle();
    }

    private void createOutputMap() {
        for (Observation observation : observationBundle) {
            if (observation.hasCategory() && observation.getCategory().get(0).hasCoding() &&
                    observation.getCategory().get(0).getCoding().get(0).hasCode() &&
                    observation.getCategory().get(0).getCoding().get(0).getCode().equals("vital-signs")) {
                addToObservationMap(observation);
            }
        }
    }
    private void addToObservationMap(Observation observation) {
        String display = observation.getCode().getCoding().get(0).getDisplay();
        display = Utilities.stripAfterOpenParentheses(display);

        try {
            if (observation.hasValueQuantity() && observation.getValueQuantity().getUnit() != null
                    && !observation.getValueQuantity().getUnit().isEmpty()) {
                String unit = observation.getValueQuantity().getUnit();
                display = display + " (" + unit + ")";
            }
        } catch (FHIRException e) {
            log.info("No units found");
        }

        observations = new ArrayList<>();

        if (outputMap.containsKey(display)) {
            observations = outputMap.get(display);
        } else {
            observations = new ArrayList<>();
            outputMap.put(display, observations);
        }

        observations.add(observation);
    }

    private void processBundle() {
        if (observationBundle == null && this.formResponses == null) {
            return;
        }

        if (this.observationBundle != null) {
            createOutputMap();
            for (Map.Entry<String, List<Observation>> entry : outputMap.entrySet()) {

                Observation obj = entry.getValue().get(0);
                FlowsheetRowModel rm = new FlowsheetRowModel();
                rm.setDisplayName(entry.getKey());
                rm.setCode(obj.getCode().getCoding().get(0).getCode());
                rm.setCodeSystem(obj.getCode().getCoding().get(0).getSystem());
                rm.setResourceID(obj.getIdElement().getIdPart());
                String confidentiality = "N";
                if (obj.getMeta().getSecurity() != null) {
                    List<Coding> codes = obj.getMeta().getSecurity();
                    Iterator iter = codes.iterator();
                    while (iter.hasNext()) {
                        Coding code = (Coding) iter.next();
                        if (code.getSystem().equals("http://hl7.org/fhir/v3/Confidentiality")) {
                            confidentiality = code.getCode();
                            break;
                        }
                    }
                }
                rm.setConfidentialy(confidentiality);

                rowNames.add(rm);
                List<FlowsheetCellValue> row = addRow();
                setRowValues(entry.getValue(), row);
            }
        }

        // This only handle GutCheckNEC forms for now
        if (this.formResponses != null && this.formResponses.length > 0) {
            FlowsheetRowModel rowModel = new FlowsheetRowModel();
            rowModel.setDisplayName("GutCheck NEC");
            this.rowNames.add(rowModel);

            List<FlowsheetCellValue> row = addRow();
            try {
                setRowValues(this.formResponses, row);
            } catch(ParseException pe) {
                throw new RuntimeException("Failed to parse dates for GutCheckNEC form responses");
            }
        }
    }

    /**
     * Get the start date from timing. Throwing a runtime exception instead.
     *
     * @param timing
     * @return
     */
    private Date getStartDate(Timing timing) {
        try {
            Timing.TimingRepeatComponent repeatComponent = timing.getRepeat();
            Period boundsPeriod = repeatComponent.getBoundsPeriod();
            return timing.getRepeat().getBoundsPeriod().getStart();
        } catch (FHIRException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Get the end date from timing. Throwing a runtime exception instead.
     *
     * @param timing
     * @return
     */
    private Date getEndDate(Timing timing) {
        try {
            return timing.getRepeat().getBoundsPeriod().getEnd();
        } catch (FHIRException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * For output headers, we will reuse an existing header if it is within the same HH:MM (not
     * including seconds). However for input header, we only reuse headers if it has the same
     * HH:MM:SS An exception is if there are 2 urine observations during the same minute, then we
     * need to not reuse the existing header
     *
     * @param outputDate
     * @param forceNewHeader
     * @return
     */
    private Date createOutputHeader(Date outputDate, boolean forceNewHeader) {
        Date responseHeader = null;

        if (!forceNewHeader) {
            //check to reuse an existing header if the HH:MM is the same.
            for (Date header : headerNames) {
                if (Utilities.equalsByMinute(outputDate, header)) {
                    responseHeader = header;
                    break;
                }
            }
        }

        if (responseHeader == null) {
            createHeader(outputDate);
            responseHeader = outputDate;
        }

        return responseHeader;
    }

    /**
     * Create the data row for Observations-vital
     * DATA
     *
     * @param observations
     * @param row
     */
    private void setRowValues(List<Observation> observations, List<FlowsheetCellValue> row) {
        Map<Long, Long> previousOutputTimeInMs = new HashMap<>();

        for (Observation observation : observations) {
            DateTimeType effective = (DateTimeType) observation.getEffective();
            Date effectiveDate = effective.getValue();
            Date dateWithoutSeconds = Utilities.trimSeconds(effectiveDate);
            Long dateWithoutSecondsLong = dateWithoutSeconds.getTime();
            Date headerDate;

            if (previousOutputTimeInMs.containsKey(dateWithoutSecondsLong)) {
                headerDate = createOutputHeader(effectiveDate, true);
            } else {
                headerDate = createOutputHeader(effectiveDate, false);
                previousOutputTimeInMs.put(dateWithoutSecondsLong, dateWithoutSecondsLong);
            }

            createHeader(effectiveDate);

            int index = headerNames.indexOf(headerDate);

            Quantity quantity;
            try {
                quantity = observation.getValueQuantity();
            } catch (FHIRException e) {
                throw new RuntimeException(e);
            }

            String code = observation.getCode().getCoding().get(0).getCode();
            String id = observation.getIdElement().getIdPart();
            FlowsheetCellValue cell = row.get(index);
            if (cell == null) {
                row.set(index,
                        new FlowsheetCellValue(quantity.getValue().doubleValue(),
                                quantity.getUnit(),
                                FlowsheetCellValue.Info.NONE,
                                FlowSheetConstants.Type.NONE, id));
            } else {
                cell.addValue(quantity.getValue().doubleValue());
            }
        }
    }

    private void setRowValues(FormResponse[] formResponses, List<FlowsheetCellValue> row) throws ParseException {
        Map<Long, Long> previousOutputTimeInMs = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");

        for (FormResponse response : formResponses) {

            Date effectiveDate = sdf.parse(response.getLastUpdateDateTime());
            Date dateWithoutSeconds = Utilities.trimSeconds(effectiveDate);
            Long dateWithoutSecondsLong = dateWithoutSeconds.getTime();
            Date headerDate;

            if (previousOutputTimeInMs.containsKey(dateWithoutSecondsLong)) {
                headerDate = createOutputHeader(effectiveDate, true);
            } else {
                headerDate = createOutputHeader(effectiveDate, false);
                previousOutputTimeInMs.put(dateWithoutSecondsLong, dateWithoutSecondsLong);
            }

            createHeader(effectiveDate);

            int index = headerNames.indexOf(headerDate);

            String id = response.getId();
            FlowsheetCellValue cell = row.get(index);
            if (cell == null) {
                row.set(index,
                        new FlowsheetCellValue(Double.valueOf(ClientUtilities.calculateGutCheckNecScore(response)),
                                "",
                                FlowsheetCellValue.Info.NONE,
                                FlowSheetConstants.Type.NONE, id));
            } else {
                cell.addValue(Double.valueOf(ClientUtilities.calculateGutCheckNecScore(response)));
            }
        }
    }

    /**
     * Adds a header to the headerNames List
     *
     * @param date
     */
    private void createHeader(Date date) {
        //check to see if we already have the time
        if (!headerNames.contains(date)) {
            long startTime = date.getTime();
            int index = -1;

            for (int i = 0; i < headerNames.size(); i++) {
                long time = headerNames.get(i).getTime();
                if (startTime < time) {
                    index = i;
                    break;
                }
            }

            //check the index for the time
            if (index == -1) {
                appendColumn(date);
            } else {
                insertColumn(date, index);
            }
        }
    }

    /**
     * Append a column header
     *
     * @param startDate
     */
    private void appendColumn(Date startDate) {
        headerNames.add(startDate);
        for (List<FlowsheetCellValue> row : rows) {
            row.add(null);
        }
    }

    /**
     * Insert a column header
     *
     * @param startDate
     * @param index
     */
    private void insertColumn(Date startDate, int index) {
        headerNames.add(index, startDate);
        for (List<FlowsheetCellValue> row : rows) {
            row.add(index, null);
        }
    }

    /**
     * Creates a row for the grid
     *
     * @return List of cells belonging to the added row.
     */
    private List<FlowsheetCellValue> addRow() {
        List<FlowsheetCellValue> cells = new LinkedList<>();

        for (int i = 0; i < headerNames.size(); i++) {
            cells.add(null);
        }

        rows.add(cells);
        return cells;
    }

    /**
     * Get grid date headers
     *
     * @return List of date headers.
     */
    public List<Date> getHeaderNames() {
        return headerNames;
    }

    /**
     * Get data rows
     *
     * @return List of cells within their respective rows.
     */
    public List<List<FlowsheetCellValue>> getRows() {
        return rows;
    }

    /**
     * Get names column
     *
     * @return List of row names.
     */
    public List<FlowsheetRowModel> getRowNames() {
        return rowNames;
    }

}