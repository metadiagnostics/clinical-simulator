package com.cognitivemedicine.common.util;

import java.util.Date;
import java.util.Map;

public class DataSubscriptionConfiguration {
    public enum Domain {
        I_O, MEDICATION, PROBLEM,
        ORDER, DIAGNOSTIC_REPORT, VITAL,
        DEMOGRAPHICS
    }

    public enum Change {
        ADD, UPDATE
    }

    private Date startDate;
    private Date endDate;
    private Domain domain;
    private String _liveUpdateCallback;
    private Map<String, String> additionalParams;

    public DataSubscriptionConfiguration(Domain domain) {
        this.domain = domain;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public String getLiveUpdateCallback() {
        return _liveUpdateCallback;
    }

    public void setLiveUpdateCallback(String liveUpdateCallback) {
        this._liveUpdateCallback = liveUpdateCallback;
    }

    public Map<String, String> getAdditionalParams() {
        return additionalParams;
    }

    public void setAdditionalParams(Map<String, String> additionalParams) {
        this.additionalParams = additionalParams;
    }


    public void setDateRange(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
