package com.cognitivemedicine.common.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LineChartDetail {
    private String title;
    private List<LineChartPoint> data;

    public LineChartDetail() {
        this.data = new ArrayList<>();
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addEntry(String x, String y) {
        this.data.add(new LineChartPoint(x,y));
    }

    public List<LineChartPoint> getData() {
        return this.data;
    }

    public void setData(List<LineChartPoint> data) {
        this.data = data;
    }
}