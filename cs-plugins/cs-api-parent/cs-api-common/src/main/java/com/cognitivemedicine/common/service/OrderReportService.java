/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognitivemedicine.common.service;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.client.IGenericClient;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.uhn.fhir.rest.gclient.TokenClientParam;
import com.cognitivemedicine.common.model.GridCell;
import com.cognitivemedicine.common.model.GridModel;
import com.cognitivemedicine.common.model.GridRow;
import com.cognitivemedicine.common.orders.OrdersType;
import com.cognitivemedicine.common.util.Codes;
import java.util.ArrayList;

import com.cognitivemedicine.common.util.FhirServiceUtil;
import com.cognitivemedicine.common.util.Utilities;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.fhir.common.BaseService;

/**
 *
 * @author duanedecouteau
 */
public class OrderReportService extends BaseService {
    private DateFormat queryParamSdf = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat orderDateSdf = new SimpleDateFormat("yyyyMMddHHmm");

    private static final String MEDICATION_REQUEST_QUERY = "MedicationRequest?_count=50"
            + "&_include=MedicationRequest:context"
            + "&_include=MedicationRequest:requester"
            + "&_include=MedicationRequest:medication";
    
    private static final String NUTRITION_ORDER_QUERY = "NutritionOrder?_count=50"
            + "&_include=NutritionOrder:encounter"
            + "&_include=NutritionOrder:provider";
    
    private static final String PROCEDURE_REQUEST_QUERY = "ProcedureRequest?_count=50"
            + "&_include=ProcedureRequest:encounter"
            + "&_include=ProcedureRequest:performer";
    
    private static final String REFERRAL_REQUEST_QUERY = "ReferralRequest?_count=50"
            + "&_include=ReferralRequest:encounter"
            + "&_include=ReferralRequest:requester";
    

    public OrderReportService(IGenericClient client) {
        super(client);
    }

    public Map<String, Object> retrieveOrderData(Patient patient, Date startDate, Date endDate) {
        Map<String, Object> data = new HashMap<>();
        
        GridModel model = new GridModel();
        
        List<GridRow> rows = new ArrayList<>();
        rows.addAll(this.retrieveMedicationRequests(patient, startDate, endDate));
        rows.addAll(this.retrieveNutritionOrders(patient, startDate, endDate));
        rows.addAll(this.retrieveProcedureRequests(patient, startDate, endDate));
        rows.addAll(this.retrieveReferralRequests(patient, startDate, endDate));
        
        rows.forEach(model::addRow);
        
        data.put("data", model.convertToSerializeableModel());
        data.put("type", OrdersType.ORDER_DATE_TIME);
        data.put("dataType", Utilities.DATA_TYPE_DATE_TIME);
        return data;
    }

    public Map<String, Object> retrieveOrderResourceById(String resourceType, String id) {
        Map<String, Object> data = new HashMap<>();
        Bundle bundle  = null;
        GridModel model = new GridModel();

        switch(resourceType) {
            case "MedicationRequest":
                bundle = getClient().search().forResource(MedicationRequest.class)
                        .include(new Include("MedicationRequest:context"))
                        .include(new Include("MedicationRequest:requester"))
                        .include(new Include("MedicationRequest:medication"))
                        .where(new TokenClientParam("_id").exactly().code(id))
                        .returnBundle(Bundle.class)
                        .execute();
                break;

            case "NutritionOrder":
                bundle = getClient().search().forResource(NutritionOrder.class)
                        .include(new Include("NutritionOrder:encounter"))
                        .include(new Include("NutritionOrder:provider"))
                        .where(new TokenClientParam("_id").exactly().code(id))
                        .returnBundle(Bundle.class)
                        .execute();
                break;

            case "ProcedureRequest":
                bundle = getClient().search().forResource(ProcedureRequest.class)
                        .include(new Include("ProcedureRequest:encounter"))
                        .include(new Include("ProcedureRequest:performer"))
                        .where(new TokenClientParam("_id").exactly().code(id))
                        .returnBundle(Bundle.class)
                        .execute();
                break;

            case "ReferralRequest":
                bundle = getClient().search().forResource(ReferralRequest.class)
                        .include(new Include("ReferralRequest:encounter"))
                        .include(new Include("ReferralRequest:requester"))
                        .where(new TokenClientParam("_id").exactly().code(id))
                        .returnBundle(Bundle.class)
                        .execute();
                break;
        }

        for (Bundle.BundleEntryComponent component : bundle.getEntry()) {
            Resource resource = component.getResource();
            if (resource instanceof MedicationRequest) {
                model.addRow(this.buildMedicationRow((MedicationRequest) resource));
            } else if (resource instanceof NutritionOrder) {
                model.addRow(this.buildNutritionOrderRow((NutritionOrder) resource));
            } else if (resource instanceof ProcedureRequest) {
                model.addRow(this.buildProcedureRequestRow((ProcedureRequest) resource));
            } else if (resource instanceof ReferralRequest) {
                model.addRow(this.buildReferralRequestRow((ReferralRequest) resource));
            }
        }

        data.put("data", model.convertToSerializeableModel());
        data.put("type", OrdersType.ORDER_DATE_TIME);
        data.put("dataType", Utilities.DATA_TYPE_DATE_TIME);

        return data;
    }

    
    public List<GridRow> retrieveMedicationRequests(Patient patient, Date startDate, Date endDate) {
        String query = this.createQuery(MEDICATION_REQUEST_QUERY, "subject", patient, startDate, endDate, "authoredon");

        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();
        List<MedicationRequest> medicationRequests = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, MedicationRequest.class);
        List<GridRow> rows = new ArrayList<>();

        for(MedicationRequest medicationRequest : medicationRequests){
            rows.add(this.buildMedicationRow(medicationRequest));
        }

        return rows;
    }

    private GridRow buildMedicationRow(MedicationRequest request) {
        String orderDateTime = "";
        if (request.hasAuthoredOn()) {
            orderDateTime = this.orderDateSdf.format(request.getAuthoredOn());
        }

        GridRow row = new GridRow(orderDateTime);
        row.addCell(new GridCell(OrdersType.CLASS_TYPE, "MedicationRequest"));

        if (request.hasMedication()) {
            String name = null;

            try {
                if (request.hasMedicationReference() && request.getMedicationReference().getResource() != null) {
                    name = ((Medication) request.getMedicationReference().getResource()).getCode().getCodingFirstRep().getDisplay();
                } else if (request.hasMedicationCodeableConcept() && request.getMedicationCodeableConcept().hasCoding()){
                    name = request.getMedicationCodeableConcept().getCodingFirstRep().getDisplay();
                }
            } catch (FHIRException ex) {
                ex.printStackTrace();
                name = "ERROR";
            }
            row.addCell(new GridCell(OrdersType.NAME, name));
        }

        if (request.hasStatus()) {
            row.addCell(new GridCell(OrdersType.STATUS, request.getStatus().getDisplay()));
        }

        //TODO: Verify
        if (request.hasCategory()) {
            //row.addCell(new GridCell(OrdersType.TYPE, request.getCategory().getText()));
        }

        row.addCell(new GridCell(OrdersType.TYPE, "Medication Request"));


        //TODO: Verify
        if (request.hasRequester() && request.getRequester().hasOnBehalfOf()) {
            Organization departmentOrganization = this.getOrganization(request.getRequester().getOnBehalfOf().getResource(), Codes.DEPARTMENT_CODE);

            if (departmentOrganization != null) {
                row.addCell(new GridCell(OrdersType.FACILITY, departmentOrganization.getName()));
            }
        }

        //TODO: Verify
        if (request.hasRequester() && request.getRequester().hasAgent()) {
            IBaseResource requesterResource = request.getRequester().getAgent().getResource();

            if (requesterResource instanceof Practitioner && ((Practitioner) requesterResource).getName() != null) {
                row.addCell(new GridCell(OrdersType.ORDERING_PROVIDER, ((Practitioner) requesterResource).getName().get(0).getText()));
            }
        }

        if (request.hasPriority()) {
            row.addCell(new GridCell(OrdersType.PRIORITY, request.getPriority().toString()));
        }

        row.addCell(new GridCell(OrdersType.ID, request.getIdElement().getIdPart()));

        return row;
    }
    
    public List<GridRow> retrieveNutritionOrders(Patient patient, Date startDate, Date endDate) {
        String query = this.createQuery(NUTRITION_ORDER_QUERY, "patient", patient, startDate, endDate, "datetime");

        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();
        List<NutritionOrder> nutritionOrders = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, NutritionOrder.class);
        List<GridRow> rows = new ArrayList<>();

        for(NutritionOrder nutritionOrder : nutritionOrders){
            rows.add(this.buildNutritionOrderRow(nutritionOrder));
        }

        return rows;
    }

    private GridRow buildNutritionOrderRow(NutritionOrder order) {
        String orderDateTime = "";
        if (order.hasDateTime()) {
            orderDateTime = this.orderDateSdf.format(order.getDateTime());
        }

        GridRow row = new GridRow(orderDateTime);
        row.addCell(new GridCell(OrdersType.CLASS_TYPE, "NutritionOrder"));

        //if (order.hasCode()) {
        //TODO
        //row.addCell(new GridCell(OrdersType.NAME, "TO-DO"));
        //}

        if (order.hasStatus()) {
            row.addCell(new GridCell(OrdersType.STATUS, order.getStatus().getDisplay()));
        }

        //TODO: Verify
        //if (order.hasCategory()) {
        row.addCell(new GridCell(OrdersType.TYPE, "Nutrition Order"));
        //}

        //TODO: Verify
        if (order.hasOrderer()) {
            //TODO
            //row.addCell(new GridCell(OrdersType.FACILITY, "TO-DO"));
        }

        //TODO: Verify
        if (order.hasOrderer() && order.getOrderer().getResource() instanceof Practitioner && ((Practitioner)  order.getOrderer().getResource()).getName() != null) {
            row.addCell(new GridCell(OrdersType.ORDERING_PROVIDER, ((Practitioner)  order.getOrderer().getResource()).getName().get(0).getText()));
        }

        //if (order.hasPriority()) {
        //TODO
        //row.addCell(new GridCell(OrdersType.PRIORITY, "TO-DO"));
        //}

        row.addCell(new GridCell(OrdersType.ID, order.getIdElement().getIdPart()));
        return row;
    }
    
    public List<GridRow> retrieveProcedureRequests(Patient patient, Date startDate, Date endDate) {
        String query = this.createQuery(PROCEDURE_REQUEST_QUERY, "subject", patient, startDate, endDate, "authored");

        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();
        List<ProcedureRequest> procedureRequests = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, ProcedureRequest.class);
        List<GridRow> rows = new ArrayList<>();

        for(ProcedureRequest procedureRequest : procedureRequests){
            rows.add(this.buildProcedureRequestRow(procedureRequest));
        }

        return rows;
    }

    private GridRow buildProcedureRequestRow(ProcedureRequest request) {
        String orderDateTime = "";
        if (request.hasAuthoredOn()) {
            orderDateTime = this.orderDateSdf.format(request.getAuthoredOn());
        }

        GridRow row = new GridRow(orderDateTime);
        row.addCell(new GridCell(OrdersType.CLASS_TYPE, "ProcedureRequest"));

        if (request.hasCode()) {
            row.addCell(new GridCell(OrdersType.NAME, request.getCode().getText()));
        }

        if (request.hasStatus()) {
            row.addCell(new GridCell(OrdersType.STATUS, request.getStatus().getDisplay()));
        }

        //TODO: Verify
        if (request.hasCategory() && request.getCategory().size() > 0) {
            //row.addCell(new GridCell(OrdersType.TYPE, request.getCategory().get(0).getText()));
        }

        row.addCell(new GridCell(OrdersType.TYPE, "Procedure Request"));

        //TODO: Verify
        if (request.hasRequester() && request.getRequester().hasAgent()) {
            Organization departmentOrganization = this.getOrganization(request.getRequester().getAgent().getResource(), Codes.DEPARTMENT_CODE);

            if (departmentOrganization != null) {
                row.addCell(new GridCell(OrdersType.FACILITY, departmentOrganization.getName()));
            }
        }

        //TODO: Verify
        if (request.hasRequester() && request.getRequester().hasAgent()) {
            IBaseResource requesterResource = request.getRequester().getAgent().getResource();

            if (requesterResource instanceof Practitioner && ((Practitioner) requesterResource).getName() != null) {
                row.addCell(new GridCell(OrdersType.ORDERING_PROVIDER, ((Practitioner) requesterResource).getName().get(0).getText()));
            }
        }

        if (request.hasPriority()) {
            row.addCell(new GridCell(OrdersType.PRIORITY, request.getPriority().toString()));
        }

        row.addCell(new GridCell(OrdersType.ID, request.getIdElement().getIdPart()));

        return row;
    }
    
    public List<GridRow> retrieveReferralRequests(Patient patient, Date startDate, Date endDate) {
        String query = this.createQuery(REFERRAL_REQUEST_QUERY, "subject", patient, startDate, endDate, "authored-on");

        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();
        List<ReferralRequest> referralRequests = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, ReferralRequest.class);
        List<GridRow> rows = new ArrayList<>();

        for(ReferralRequest referralRequest : referralRequests){
            rows.add(this.buildReferralRequestRow(referralRequest));
        }

        return rows;
    }

    private GridRow buildReferralRequestRow(ReferralRequest request) {
        String orderDateTime = "";
        if (request.hasAuthoredOn()) {
            orderDateTime = this.orderDateSdf.format(request.getAuthoredOn());
        }

        GridRow row = new GridRow(orderDateTime);
        row.addCell(new GridCell(OrdersType.CLASS_TYPE, "ReferralRequest"));

        //if (request.hasCode()) {
        //row.addCell(new GridCell(OrdersType.NAME, "TO-DO"));
        //}

        if (request.hasStatus()) {
            row.addCell(new GridCell(OrdersType.STATUS, request.getStatus().getDisplay()));
        }

        //TODO: Verify
        //if (request.hasCategory()) {
        row.addCell(new GridCell(OrdersType.TYPE, "Referral Request"));
        //}

        //TODO: Verify
        if (request.hasRequester() && request.getRequester().hasAgent()) {
            Organization departmentOrganization = this.getOrganization(request.getRequester().getAgent().getResource(), Codes.DEPARTMENT_CODE);

            if (departmentOrganization != null) {
                row.addCell(new GridCell(OrdersType.FACILITY, departmentOrganization.getName()));
            }
        }

        //TODO: Verify
        if (request.hasRequester() && request.getRequester().hasAgent()) {
            IBaseResource requesterResource = request.getRequester().getAgent().getResource();

            if (requesterResource instanceof Practitioner && ((Practitioner) requesterResource).getName() != null) {
                row.addCell(new GridCell(OrdersType.ORDERING_PROVIDER, ((Practitioner) requesterResource).getName().get(0).getText()));
            }
        }

        if (request.hasPriority()) {
            row.addCell(new GridCell(OrdersType.PRIORITY, request.getPriority().toString()));
        }

        row.addCell(new GridCell(OrdersType.ID, request.getIdElement().getIdPart()));
        return row;
    }
    
    private String createQuery(String baseQuery, String patientAttribute, Patient patient, Date startDate, Date endDate, String dateAttribute){
        String query = baseQuery + "&"+patientAttribute+"=Patient/" + patient.getIdElement().getIdPart();

        if (startDate != null) {
            String formattedStartDate = this.queryParamSdf.format(startDate);
            query += "&"+dateAttribute+"=%3E" + formattedStartDate;
        }
        if (endDate != null) {
            String formattedEndDate = this.queryParamSdf.format(endDate);
            query += "&"+dateAttribute+"=%3C" + formattedEndDate;
        }
        
        return query;
    }

    //TODO: This should be made into a common function that can be used for all services
    private Organization getOrganization(IBaseResource resource, String organizationType) {
        if (resource instanceof Organization) {
            Organization organization = (Organization) resource;
            List<CodeableConcept> concepts = organization.getType();
            if (concepts != null && !concepts.isEmpty()) {
                if (organizationType.equals(concepts.get(0).getCoding().get(0).getCode())) {
                    return organization;
                }
            }
        }

        return null;
    }
}