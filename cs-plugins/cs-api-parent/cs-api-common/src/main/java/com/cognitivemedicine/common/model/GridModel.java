package com.cognitivemedicine.common.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridModel {
    private List<GridRow> rows;

    public GridModel() {
        this.rows = new ArrayList<>();
    }

    public void addRow(GridRow row) {
        this.rows.add(row);
    }

    public List<Map> convertToSerializeableModel() {
        List<Map> output = new ArrayList<>();

        for(GridRow row : this.rows) {
            Map<String, List> rowEntry = new HashMap<>();
            List<Map> cellList = new ArrayList<>();

            for(GridCell cell : row.getCells()) {
                Map<String, String> cellEntry = new HashMap<>();
                cellEntry.put("type", cell.getType());
                cellEntry.put("value", cell.getValue());
                cellEntry.put("dataType", cell.getDataType());
                cellList.add(cellEntry);
            }

            rowEntry.put(row.getKey(), cellList);
            output.add(rowEntry);
        }

        return output;
    }
}
