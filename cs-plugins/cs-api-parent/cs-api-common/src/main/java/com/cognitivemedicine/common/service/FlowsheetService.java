package com.cognitivemedicine.common.service;

import java.text.DateFormat;
import java.util.*;
import java.util.stream.Stream;

import com.cognitivemedicine.common.util.*;
import com.cognitivemedicine.common.flowsheet.FlowsheetType;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.cs.models.FormResponse;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.fhir.common.BaseService;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.model.base.composite.BaseIdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.DateClientParam;
import ca.uhn.fhir.rest.gclient.ReferenceClientParam;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

public class FlowsheetService extends BaseService {
    private DataServiceRestClient client;
    private static final Logger LOG = LoggerFactory.getLogger(FlowsheetService.class);

    public FlowsheetService(IGenericClient client) {
        super(client);
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        String baseUrl = config.getString(ConfigUtilConstants.KEY_SIMULATOR_DATA_SERVICE_URL);
        this.client = new DataServiceRestClient(baseUrl);
    }

    public Map<String, Object> getFlowsheetDataMap(FlowsheetType type, Patient patient, Date startDate, Date endDate, String formType, String formVersion) {
        Map<String, Object> results = null;

        if (patient != null && type != null) {
            switch (type) {
                case I_O:
                    results = getIOData(patient, startDate, endDate);
                    break;
                
                case MEDS:
                    results = getMARData(patient, startDate, endDate);
                    break;

                case VITALS:
                    results = getVitalsData(patient, startDate, endDate, formType, formVersion);
                    break;

                default:
                    break;
            }
        }
        
        if (results != null) {
            results.put("type", type);
        }

        return results;
    }

    public Map<String, Object> retrieveFlowsheetResourceById(IBaseResource resource, DataSubscriptionConfiguration config) {
        Bundle medicationBundle = null;
        Bundle observationBundle = null;
        Map<String, Object> data = null;

        if (resource instanceof Observation) {
            observationBundle = getClient().search().forResource(Observation.class)
                    .where(new TokenClientParam("_id").exactly().code(resource.getIdElement().getIdPart())).returnBundle(Bundle.class)
                    .execute();
        } else if (resource instanceof MedicationAdministration) {
            medicationBundle = getClient().search().forResource(MedicationAdministration.class)
                    .include(new Include("MedicationAdministration:prescription"))
                    .include(new Include("MedicationAdministration:medication"))
                    .where(new TokenClientParam("_id").exactly().code(resource.getIdElement().getIdPart()))
                    .returnBundle(Bundle.class)
                    .execute();
        }
        
        if (config.getDomain() == DataSubscriptionConfiguration.Domain.I_O) {
            return getIOData(config, medicationBundle, observationBundle);
        }
        if (config.getDomain() == DataSubscriptionConfiguration.Domain.MEDICATION) {
            return getMedicationsData(config, medicationBundle);
        }

        if (config.getDomain() == DataSubscriptionConfiguration.Domain.VITAL) {
            return getVitalsData(config, observationBundle);
        }

        return data;

    }

    private Map<String, Object> getIOData(Patient patient, Date startDate, Date endDate) {
        DateFormat queryDateFormat = Utilities.getFhirDateFormat();
        String patientId = patient.getIdElement().getIdPart();

        long startTime = System.currentTimeMillis();

        Bundle medicationBundle = getClient().search().forResource(MedicationAdministration.class)
                .where(new DateClientParam("effective-time").afterOrEquals().second(queryDateFormat.format(startDate)))
                .where(new DateClientParam("effective-time").beforeOrEquals().second(queryDateFormat.format(endDate)))
                .where(new ReferenceClientParam("patient").hasId(patientId))
                //Currently unable to search for multiple system|codes.
                //Can only search by a single system with multiple code or codes without regard to their system.
                .where(new TokenClientParam("code").exactly().identifiers(
                            formatCodes(Codes.BABY_FORMULA, Codes.BREAST_MILK, Codes.TPN)))
                .include(new Include("MedicationAdministration:prescription"))
                .count(10000).returnBundle(Bundle.class).execute();

        List<MedicationAdministration> medList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), medicationBundle, MedicationAdministration.class);

        Bundle observationBundle = getClient().search().forResource(Observation.class)
                .where(new DateClientParam("date").afterOrEquals().second(queryDateFormat.format(startDate)))
                .where(new DateClientParam("date").beforeOrEquals().second(queryDateFormat.format(endDate)))
                .where(new ReferenceClientParam("patient").hasId(patientId))
                .where(new TokenClientParam("code").exactly()
                        .identifiers(formatCodes(Codes.FECES, Codes.URINE, Codes.GASTRIC_FLUID)))
                .count(10000).sort().ascending("_id")
                .returnBundle(Bundle.class).execute();

        List<Observation> observationList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), observationBundle, Observation.class);

        long endTime = System.currentTimeMillis();
        System.out.println("RxR getIOData " + (endTime - startTime) + " milliseconds");

        com.cognitivemedicine.common.flowsheet.io.FlowsheetModel model =
                new com.cognitivemedicine.common.flowsheet.io.FlowsheetModel(medList, startDate, endDate, observationList);
        com.cognitivemedicine.common.flowsheet.io.FlowsheetConverter flowsheetConverter =
                new com.cognitivemedicine.common.flowsheet.io.FlowsheetConverter(model);

        return flowsheetConverter.getFlowsheetGridModel();
    }

    private Map<String, Object> getMARData(Patient patient, Date startDate, Date endDate) {
        //TODO Currently gets all Medication Administrations in the specified time period and their associated Medication Requests
        //However it currently won't show any DUE or OVERDUE medications if there are no Medication Administrations the time period
        //Needs to change to get all valid medication requests that are still active.  Then search for Medication Administrations

        DateFormat queryDateFormat = Utilities.getFhirDateFormat();
        String patientId = patient.getIdElement().getIdPart();

        long startTime = System.currentTimeMillis();

        Bundle medicationBundle = getClient().search().forResource(MedicationAdministration.class)
                .where(new DateClientParam("effective-time").afterOrEquals().second(queryDateFormat.format(startDate)))
                .where(new DateClientParam("effective-time").beforeOrEquals().second(queryDateFormat.format(endDate)))
                .where(new ReferenceClientParam("patient").hasId(patientId))
                .include(new Include("MedicationAdministration:prescription"))
                .include(new Include("MedicationAdministration:medication"))
                .count(10000).sort().ascending("_id")
                .returnBundle(Bundle.class).execute();

        List<MedicationAdministration> medicationList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), medicationBundle, MedicationAdministration.class);

        long endTime = System.currentTimeMillis();
        System.out.println("RxR getMARData " + (endTime - startTime) + " milliseconds");

        // TODO RxR complete this when you can test...
        medicationList = filterOutNonSubstances(medicationList);

        com.cognitivemedicine.common.flowsheet.mar.FlowsheetModel model =
                new com.cognitivemedicine.common.flowsheet.mar.FlowsheetModel(medicationList, startDate, endDate);
        com.cognitivemedicine.common.flowsheet.mar.FlowsheetConverter flowsheetConverter =
                new com.cognitivemedicine.common.flowsheet.mar.FlowsheetConverter(model);

        return flowsheetConverter.getFlowsheetGridModel();
    }

    private boolean isI_O(MedicationAdministration ma) {
        boolean retVal = false;

        try {
            if (ma.hasMedicationCodeableConcept() &&
                    ma.hasMedicationCodeableConcept() &&
                    ma.getMedicationCodeableConcept().hasCoding()) {
                String snowMedCode = ma.getMedicationCodeableConcept().getCoding().get(0).getCode();
                retVal = snowMedCode != null &&
                        !snowMedCode.isEmpty() &&
                        !snowMedCode.equals(Codes.BABY_FORMULA) &&
                        !snowMedCode.equals(Codes.BREAST_MILK) &&
                        !snowMedCode.equals(Codes.TPN) &&
                        !snowMedCode.equals(Codes.FECES) &&
                        !snowMedCode.equals(Codes.GASTRIC_FLUID);
                System.out.println("isIO is: " + retVal);
            }

        } catch (FHIRException e) {
            e.printStackTrace();
            System.out.println("RR " + e.getMessage());
        }

        return retVal;
    }

    // did not want to see the mentioned snomed codes in the I&O flowsheet
    private List<MedicationAdministration> filterOutNonSubstances(List<MedicationAdministration> input) {

        List<MedicationAdministration> retVal = input.stream().filter(p -> !this.isI_O(p)).collect(Collectors.toList());
        return retVal;
    }

    private Map<String, Object> getVitalsData(Patient patient, Date startDate, Date endDate, String formType, String formVersion) {
        long startTime = System.currentTimeMillis();

        DateFormat queryDateFormat = Utilities.getFhirDateFormat();
        String patientId = patient.getIdElement().getIdPart();

        Bundle vitalsBundle = getClient().search().forResource(Observation.class)
                .where(new DateClientParam("date").afterOrEquals().second(queryDateFormat.format(startDate)))
                .where(new DateClientParam("date").beforeOrEquals().second(queryDateFormat.format(endDate)))
                .where(new ReferenceClientParam("patient").hasId(patientId))
                .count(10000).sort().ascending("_id")
                .returnBundle(Bundle.class).execute();

        List<Observation> observationBundle = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), vitalsBundle, Observation.class);

        long endTime = System.currentTimeMillis();

        System.out.println("RxR getVitalsData " + (endTime - startTime) + " milliseconds");


        FormResponse[] responses = null;

        try {
            responses = this.client.retrieveFormResponses(patient.getIdElement().getIdPart(), formType, formVersion, startDate, endDate);
        } catch (Exception e) {
            // We don't want to kill the plugin if note retrieval fails
            System.out.println("Failed to retrieve form responses for vitals");
            e.printStackTrace();
        }


        com.cognitivemedicine.common.flowsheet.vitals.FlowsheetModel model =
                new com.cognitivemedicine.common.flowsheet.vitals.FlowsheetModel(observationBundle, responses, startDate, endDate);
        com.cognitivemedicine.common.flowsheet.vitals.FlowsheetConverter flowsheetConverter =
                new com.cognitivemedicine.common.flowsheet.vitals.FlowsheetConverter(model);

        return flowsheetConverter.getFlowsheetGridModel();
    }

    private List<BaseIdentifierDt> formatCodes(Coding... codes) {
        List<BaseIdentifierDt> result = new ArrayList<>();

        for (Coding code : codes) {
            result.add(new IdentifierDt(code.getSystem(), code.getCode()));
        }

        return result;
    }

    private Map<String, Object> getVitalsData(DataSubscriptionConfiguration config, Bundle observationBundle) {
        // TODO: This may need to be updated to pull GutCheckNEC data if necessary
        Map<String, Object> data;

        List<Observation> observationList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), observationBundle, Observation.class);
        com.cognitivemedicine.common.flowsheet.vitals.FlowsheetModel model =
                new com.cognitivemedicine.common.flowsheet.vitals.FlowsheetModel(observationList, null, config.getStartDate(), config.getEndDate());
        com.cognitivemedicine.common.flowsheet.vitals.FlowsheetConverter flowsheetConverter =
                new com.cognitivemedicine.common.flowsheet.vitals.FlowsheetConverter(model);

        data = flowsheetConverter.getFlowsheetGridModel();
        return data;
    }

    private Map<String, Object> getMedicationsData(DataSubscriptionConfiguration config, Bundle medicationBundle) {
        Map<String, Object> data;
        List<MedicationAdministration> medicationList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), medicationBundle, MedicationAdministration.class);
        com.cognitivemedicine.common.flowsheet.mar.FlowsheetModel model =
                new com.cognitivemedicine.common.flowsheet.mar.FlowsheetModel(medicationList, config.getStartDate(), config.getEndDate());
        com.cognitivemedicine.common.flowsheet.mar.FlowsheetConverter flowsheetConverter =
                new com.cognitivemedicine.common.flowsheet.mar.FlowsheetConverter(model);

        data = flowsheetConverter.getFlowsheetGridModel();
        return data;
    }

    private Map<String, Object> getIOData(DataSubscriptionConfiguration config, Bundle medicationBundle, Bundle observationBundle) {
        Map<String, Object> data;

        List<MedicationAdministration> medicationList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), medicationBundle, MedicationAdministration.class);
        List<Observation> observationList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), observationBundle, Observation.class);

        com.cognitivemedicine.common.flowsheet.io.FlowsheetModel model =
                new com.cognitivemedicine.common.flowsheet.io.FlowsheetModel(medicationList, config.getStartDate(), config.getEndDate(), observationList);
        com.cognitivemedicine.common.flowsheet.io.FlowsheetConverter flowsheetConverter =
                new com.cognitivemedicine.common.flowsheet.io.FlowsheetConverter(model);

        data = flowsheetConverter.getFlowsheetGridModel();
        return data;
    }
}
