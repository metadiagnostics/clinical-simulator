package com.cognitivemedicine.common.model;

import java.util.List;
import java.util.Map;

public class DrawerDetailModel {
    private Map<String, Object> riskFactors;
    private List<ProcessAdherenceModel> processAdherence;
    private LineChartModel lineChartModel;

    public Map<String, Object> getRiskFactors() {
        return riskFactors;
    }

    public void setRiskFactors(Map<String, Object> riskFactors) {
        this.riskFactors = riskFactors;
    }

    public List<ProcessAdherenceModel> getProcessAdherence() {
        return processAdherence;
    }

    public void setProcessAdherence(List<ProcessAdherenceModel> processAdherence) {
        this.processAdherence = processAdherence;
    }
    
    public LineChartModel getLineChartModel() {
        return lineChartModel;
    }

    public void setLineChartModel(LineChartModel lineChartModel) {
        this.lineChartModel = lineChartModel;
    }
}
