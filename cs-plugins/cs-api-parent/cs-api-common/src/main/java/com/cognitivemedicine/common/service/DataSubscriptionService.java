package com.cognitivemedicine.common.service;

import com.cognitivemedicine.common.util.Codes;
import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Patient;
import org.hspconsortium.cwf.api.fhirsub.ResourceSubscriptionService;
import org.hspconsortium.cwf.api.fhirsub.SubscriptionWrapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataSubscriptionService {
    private final ResourceSubscriptionService subscriptionService;

    private DateFormat queryParamSdf = new SimpleDateFormat("yyyy-MM-dd");

    public DataSubscriptionService(ResourceSubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    public List<SubscriptionWrapper> subscribeToUpdates(Patient patient, DataSubscriptionConfiguration config) {
        List<SubscriptionWrapper> subscriptions = null;

        if (patient != null && config.getDomain() != null) {
            subscriptions = new ArrayList<>();

            for (String criterion : getCriteria(patient, config)) {
                SubscriptionWrapper subscription = this.subscriptionService.subscribe(criterion, ResourceSubscriptionService.PayloadType.JSON);

                if (subscription != null) {
                    subscriptions.add(subscription);
                }
            }
        }

        return subscriptions;
    }

    public void unsubscribeFromUpdates(List<SubscriptionWrapper> subscriptions) {
        this.subscriptionService.unsubscribe(subscriptions);
    }

    private List<String> getCriteria(Patient patient, DataSubscriptionConfiguration config) {
        String patientId = patient.getIdElement().getIdPart();
        List<String> criteria = new ArrayList<>();
        String criterion;

        switch (config.getDomain()) {
            case I_O:
                Map<String, String> additionalParams = new HashMap<>();
                additionalParams.put("code", this.convertCodesToQueryString(Codes.BABY_FORMULA, Codes.BREAST_MILK));
                config.setAdditionalParams(additionalParams);

                criterion = getCriterion(patientId, "MedicationAdministration", "subject","effective-time", config);
                criteria.add(criterion);

                additionalParams = new HashMap<>();
                additionalParams.put("code", this.convertCodesToQueryString(Codes.FECES, Codes.URINE));
                config.setAdditionalParams(additionalParams);

                criterion = getCriterion(patientId, "Observation", "subject","date", config);
                criteria.add(criterion);
                break;

            case MEDICATION:
                criterion = getCriterion(patientId, "MedicationAdministration", "subject","effective-time", config);
                criteria.add(criterion);
                break;

            case VITAL:
                criterion = getCriterion(patientId, "Observation", "subject","date", config);
                criteria.add(criterion);
                break;

            case DIAGNOSTIC_REPORT:
                criterion = getCriterion(patientId, "DiagnosticReport", "subject","date", config);
                criteria.add(criterion);
                break;

            case PROBLEM:
                criterion = getCriterion(patientId, "Condition", "subject","onset-date", config);
                criteria.add(criterion);
                break;

            case ORDER:
                criterion = getCriterion(patientId, "MedicationRequest", "subject","authoredon", config);
                criteria.add(criterion);

                criterion = getCriterion(patientId, "NutritionOrder", "patient","datetime", config);
                criteria.add(criterion);

                criterion = getCriterion(patientId, "ProcedureRequest", "subject","authored", config);
                criteria.add(criterion);

                criterion = getCriterion(patientId, "ReferralRequest", "subject","authored-on", config);
                criteria.add(criterion);
                break;
            case DEMOGRAPHICS:
                criterion = "Patient?_id=" + patientId;
                criteria.add(criterion);

                criterion = getCriterion(patientId, "Observation", "subject","date", config);
                criteria.add(criterion);
                break;
        }

        return criteria;
    }

    private String getCriterion(String patientId, String resourceType, String patientAttribute, String dateParam, DataSubscriptionConfiguration config) {
        String criterion = resourceType + "?" + patientAttribute + "=Patient/" + patientId;

        if (config.getStartDate() != null && config.getEndDate() != null) {
            String formattedStartDate = this.queryParamSdf.format(config.getStartDate());
            String formattedEndDate = this.queryParamSdf.format(config.getEndDate());
            criterion += "&" + dateParam + "=%3E" + formattedStartDate + "&" + dateParam + "=%3C" + formattedEndDate;
        }

        if (config.getAdditionalParams() != null) {
            for (Map.Entry<String, String> param : config.getAdditionalParams().entrySet()) {
                criterion += "&" + param.getKey() + "=" + param.getValue();
            }
        }

        return criterion;
    }

    private String convertCodesToQueryString(Coding... codes) {
        List<String> items = new ArrayList<>();
        if (codes != null) {
            for (Coding code : codes) {
                items.add(code.getSystem() + "|" + code.getCode());
            }
        }

        return StringUtils.join(items, ",");
    }
}
