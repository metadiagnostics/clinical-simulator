/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.flowsheet;

import com.cognitivemedicine.common.flowsheet.FlowSheetConstants.Type;

/**
 * This class represent a cell for the flowsheet grid
 */
public class FlowsheetCellValue {

    /**
     * @return the info
     */
    public Info getInfo() {
        return info;
    }


    /**
     * Info enum for flag settings
     */
    public enum Info {
        NONE, INFORMATIONAL, ALERT_MINOR, ALERT_MAJOR;
    }

    /**
     * Cell type to calculate totals
     */

    private String description;
    private Double value;
    private String unit;
    private Info info;
    private Type type;
    private String id;

    public FlowsheetCellValue(String description) {
        this.description = description;
        this.type = Type.DESCRIPTION;
    }

    public FlowsheetCellValue(Double value, String unit, Info info, Type type, String id) {
        if (Type.DESCRIPTION.equals(type)) {
            throw new IllegalArgumentException("Invalid type");
        }

        this.value = value;
        this.unit = unit;
        this.type = type;
        this.info = info;
        this.id = id;
    }

    public String getDescription() {
        if (Type.DESCRIPTION.equals(type)) {
            try {
                double doubleDescription = Double.parseDouble(description);
                return ((long) Math.ceil(doubleDescription)) + "";
            } catch (NumberFormatException e) {
                return description;
            }
        } else {
            //commented out to not show units
            //return value.toString() + unit.toString();

            //round the value up
            return ((long) Math.ceil(value)) + "";
        }
    }

    public Double getValue() {
        return value;
    }

    public void addValue(Double value) {
        if (type.equals(Type.DESCRIPTION)) {
            if (this.description == null) {
                this.description = "0";
            }

            double description = Double.parseDouble(this.description);
            this.description = (description + value) + "";
        } else {
            if (this.value == null) {
                this.value = 0d;
            }

            this.value = this.value + value;
        }
    }

    public String getUnit() {
        return unit;
    }

    public Type getType() {
        return type;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
}
