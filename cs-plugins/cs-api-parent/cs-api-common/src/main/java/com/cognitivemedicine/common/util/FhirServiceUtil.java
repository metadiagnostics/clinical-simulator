package com.cognitivemedicine.common.util;

import ca.uhn.fhir.rest.client.IGenericClient;
import org.hl7.fhir.dstu3.model.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FhirServiceUtil {
    public static <T> List<T> buildAndRetrieveAllResources(IGenericClient client, Bundle initialBundle, Class<T> resourceType) {
        if (initialBundle.isEmpty()){
            return Collections.EMPTY_LIST;
        }

        List<T> resourceList = new ArrayList<>(getResourceFromBundle(initialBundle, resourceType));

        boolean hasLink = initialBundle.hasLink() && initialBundle.getLink("next") != null;
        while (hasLink) {
            initialBundle = fetchNextPage(client, initialBundle.getLink("next").getUrl());
            resourceList.addAll(getResourceFromBundle(initialBundle, resourceType));
            hasLink = initialBundle.hasLink() && initialBundle.getLink("next") != null;
        }

        return resourceList;
    }

    private static Bundle fetchNextPage(IGenericClient client, String url) {
        return client.search().byUrl(url)
                .returnBundle(Bundle.class)
                .execute();
    }

    public static <T> List<T> getResourceFromBundle(Bundle bundle, Class<T> resourceType) {
        return bundle.getEntry().stream()
                .map(e -> e.getResource())
                .filter(resourceType::isInstance)
                .map(resourceType::cast)
                .collect(toList());
    }
}
