/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.flowsheet.model;

/**
 *
 * @author esteban
 */
public class CodingDTO {
    
    private String code;
    private String system;
    private String display;
    private String version;

    public CodingDTO() {
    }

    public CodingDTO(String code, String system) {
        this.code = code;
        this.system = system;
    }

    public CodingDTO(String code, String system, String display, String version) {
        this.code = code;
        this.system = system;
        this.display = display;
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }
    
    
}
