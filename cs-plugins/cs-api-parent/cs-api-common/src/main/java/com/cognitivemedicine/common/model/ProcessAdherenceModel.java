package com.cognitivemedicine.common.model;

import java.util.Map;

public class ProcessAdherenceModel {
    private String label;
    private String status;
    private Map<String, Object> criteria;

    public ProcessAdherenceModel(String label, String status, Map<String, Object> criteria) {
        this.label = label;
        this.status = status;
        this.criteria = criteria;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, Object> getCriteria() {
        return criteria;
    }

    public void setCriteria(Map<String, Object> criteria) {
        this.criteria = criteria;
    }
}
