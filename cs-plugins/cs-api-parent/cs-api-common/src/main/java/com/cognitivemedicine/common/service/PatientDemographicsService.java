package com.cognitivemedicine.common.service;

import ca.uhn.fhir.rest.client.IGenericClient;
import com.cognitivemedicine.common.patient.PatientDemographics;
import com.cognitivemedicine.common.patient.PatientUtil;
import com.cognitivemedicine.common.util.Codes;
import com.cognitivemedicine.common.util.FhirServiceUtil;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hspconsortium.cwf.fhir.common.BaseService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class PatientDemographicsService extends BaseService {
    private final String QUERY = "Observation?_count=50&";

    public PatientDemographicsService(IGenericClient client) {
        super(client);
    }

    public PatientDemographics retrievePatientDemographics(Patient patient) {

        String url = QUERY + "subject=Patient/" + patient.getIdElement().getIdPart();
        url += "&" + this.buildDemographicsCodeParameters();
        url += "&_sort=date";

        Bundle bundle = getClient().search().byUrl(url).returnBundle(Bundle.class).execute();

        List<Observation> observations = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, Observation.class);
        return this.buildDemographicsFromBundle(patient, observations);
    }

    public Patient findPatientById(String patientId) {
        String query = "Patient?_id=" + patientId;
        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();

        if (bundle != null && bundle.hasEntry() && bundle.getEntryFirstRep().getResource() instanceof Patient) {
            return (Patient) bundle.getEntryFirstRep().getResource();
        }

        return null;
    }

    private String buildDemographicsCodeParameters() {
        return "code=" + this.getDemographicsCodeParamValue();
    }

    public String getDemographicsCodeParamValue() {
        String codeParam = Codes.SNOMED_CODESYSTEM + "|" + Codes.ESTIMATED_GESTATIONAL_AGE_CODE + ",";
        codeParam += Codes.SNOMED_CODESYSTEM + "|" + Codes.BIRTH_WEIGHT_CODE + ",";
        codeParam += Codes.SNOMED_CODESYSTEM + "|" + Codes.BODY_WEIGHT_CODE + ",";
        codeParam += Codes.SNOMED_CODESYSTEM + "|" + Codes.CALCULATED_WEIGHT_CODE;
        return codeParam;
    }

    private PatientDemographics buildDemographicsFromBundle(Patient patient, List<Observation> observations) {
        PatientDemographics demographics = new PatientDemographics();
        demographics.setName(PatientUtil.getPatientName(patient));
        demographics.setMedicalRecordNumber(PatientUtil.getMedicalRecordNumber(patient));

        Integer daysofLife = PatientUtil.getDaysOfLife(patient);
        demographics.setDaysOfLife(daysofLife);

        if (patient.hasGender()) {
            demographics.setGender(patient.getGender().getDisplay());
        }

        BigDecimal egaDays = null;
        for (Observation observation : observations) {
            if (observation.hasCode() &&
                    observation.getCode().hasCoding() &&
                    observation.getCode().getCodingFirstRep() != null &&
                    observation.getCode().getCodingFirstRep().hasCode()) {

                try {
                    String code = observation.getCode().getCodingFirstRep().getCode();
                    if (code.equals(Codes.BIRTH_WEIGHT_CODE) && observation.hasValueQuantity()) {
                        demographics.setBirthWeight(this.getWeightString(observation.getValueQuantity()));
                    } else if (code.equals(Codes.CALCULATED_WEIGHT_CODE) && observation.hasValueQuantity()) {
                        demographics.setCalculatedWeight(this.getWeightString(observation.getValueQuantity()));
                    } else if (code.equals(Codes.BODY_WEIGHT_CODE) && observation.hasValueQuantity()) {
                        demographics.setTodayWeight(this.getWeightString(observation.getValueQuantity()));
                    } else if (code.equals(Codes.ESTIMATED_GESTATIONAL_AGE_CODE) && observation.hasValueQuantity() &&
                            observation.getValueQuantity().hasValue() && observation.getValueQuantity().hasUnit() &&
                            (observation.getValueQuantity().getCode().equals("d") || observation.getValueQuantity().getCode().equals("wk"))) {
                        if (observation.getValueQuantity().getCode().equals("wk")) {
                            egaDays = observation.getValueQuantity().getValue().multiply(BigDecimal.valueOf(7));
                        } else {
                            egaDays = observation.getValueQuantity().getValue();
                        }
                        demographics.setEstimatedGestationalAge(this.getGestationalAgeString(egaDays));
                    }
                } catch(FHIRException e) {
                    // We don't want to skip all resources if one observation doesn't have a value quantity
                }
            }
        }

        BigDecimal cga = this.calculateCGA(egaDays, daysofLife);

        if (cga != null) {
            demographics.setCorrectedGestationalAge(this.getGestationalAgeString(cga));
        }

        return demographics;
    }

    private BigDecimal calculateCGA(BigDecimal egaDays, Integer daysOfLife) {
        BigDecimal cga = null;

        if (egaDays != null && daysOfLife != null) {
            cga = egaDays.add(BigDecimal.valueOf(daysOfLife));
        }

        return cga;
    }

    private String getWeightString(Quantity quantity) {
        BigDecimal weightNum = quantity.getValue().setScale(0, RoundingMode.HALF_UP);
        String weight = weightNum.toBigInteger().toString();

        if (quantity.getCode() != null) {
            weight += quantity.getCode();
        }
        return weight;
    }

    private String getGestationalAgeString(BigDecimal value) {
        String output = value.divide(BigDecimal.valueOf(7), RoundingMode.DOWN).toBigInteger().toString();
        output += "+";
        output += value.remainder(BigDecimal.valueOf(7)).toBigInteger().toString();
        output += "wks";

        return output;
    }

}
