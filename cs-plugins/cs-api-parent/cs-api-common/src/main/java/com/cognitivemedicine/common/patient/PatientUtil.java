package com.cognitivemedicine.common.patient;

import com.cognitivemedicine.common.util.Codes;
import org.hl7.fhir.dstu3.model.Identifier;
import org.hl7.fhir.dstu3.model.Patient;
import org.hspconsortium.cwf.fhir.common.FhirUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PatientUtil {

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

    public static String getPatientBirthdate(Patient patient) {
        Date birthDate = patient.getBirthDate();
        if (birthDate != null) {
            return dateFormat.format(birthDate);
        }
        return "";
    }

    public static String getPatientName(Patient patient) {
        String name = "";
        if (patient.getName() != null && patient.getName().size() > 0) {
            name = FhirUtil.formatName(patient.getName());
        }

        return name;
    }

    public static String getMedicalRecordNumber(Patient patient) {
        String medicalRecordNumber = null;

        if (patient.hasIdentifier()) {
            for (Identifier id : patient.getIdentifier()) {
                if (id.getSystem() != null && id.getSystem().equals(Codes.MR_SYSTEM)) {
                    medicalRecordNumber = id.getValue();
                    break;
                }
            }
        }

        return medicalRecordNumber;
    }

    public static Integer getDaysOfLife(Patient patient) {
        Integer value = null;

        if (patient.hasBirthDate()) {
            Date today = new Date();
            value = (int)( (today.getTime() - patient.getBirthDate().getTime()) / (1000 * 60 * 60 * 24));
        }

        return value;
    }
}
