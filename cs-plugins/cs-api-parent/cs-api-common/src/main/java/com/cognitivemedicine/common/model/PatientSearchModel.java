package com.cognitivemedicine.common.model;

import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class PatientSearchModel {
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private Date dob;
    private String medicalRecordNumber;
    private String address;
    private String city;
    private String state;
    private String zip;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    public PatientSearchModel(Map<String, Object> params) {
        this.parsePatientModel(params);
    }

    private void parsePatientModel(Map<String, Object> params) {
        this.setFirstName((String) params.get("firstName"));
        this.setMiddleName((String) params.get("middleName"));
        this.setLastName((String) params.get("lastName"));
        this.setGender((String) params.get("gender"));
        this.setMedicalRecordNumber((String) params.get("medicalRecord"));

        String address = (String) params.get("addressOne");
        String addressTwo = (String) params.get("addressTwo");
        String addressString = "";

        if (!StringUtils.isEmpty(address) || !StringUtils.isEmpty(addressTwo)) {

            if(!StringUtils.isEmpty(address)) {
                addressString += address;
            }

            if (!StringUtils.isEmpty(address) && !StringUtils.isEmpty(addressTwo)) {
                addressString += ",";
            }

            if (!StringUtils.isEmpty(addressTwo)) {
                addressString += addressTwo;
            }
        }
        this.setAddress(addressString);

        this.setCity((String) params.get("city"));
        this.setState((String) params.get("state"));
        this.setZip((String) params.get("zip"));

        String dobString = (String) params.get("dateOfBirth");

        if (dobString != null && !StringUtils.isEmpty(dobString)) {
            try {
                this.setDob(sdf.parse(dobString));
            } catch (ParseException e) {
                // Just don't set this property
            }
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
