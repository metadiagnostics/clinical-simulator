package com.cognitivemedicine.common.service;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import com.cognitivemedicine.common.diagnosticreport.DiagnosticReportType;
import com.cognitivemedicine.common.model.GridCell;
import com.cognitivemedicine.common.model.GridModel;
import com.cognitivemedicine.common.model.GridRow;
import com.cognitivemedicine.common.util.Codes;
import com.cognitivemedicine.common.util.FhirServiceUtil;
import com.cognitivemedicine.common.util.MimeTypeUtils;
import com.cognitivemedicine.common.util.Utilities;
import com.cognitivemedicine.flowsheet.model.AttachmentDTO;
import com.cognitivemedicine.flowsheet.model.CodingDTO;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.fhir.common.BaseService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class DiagnosticReportService extends BaseService {

    private DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
    private DateFormat queryParamSdf = new SimpleDateFormat("yyyy-MM-dd");
    
    private static final String QUERY = "DiagnosticReport?"
            + "&_include=DiagnosticReport:encounter"
            + "&_include=DiagnosticReport:performer";
    
    public DiagnosticReportService(IGenericClient client) {
        super(client);
    }

    public Map<String, Object> retrieveIndividualReport(String id) {
        Map<String, Object> data = new HashMap<>();

        Bundle bundle = getClient().search().forResource(DiagnosticReport.class)
                .include(new Include("DiagnosticReport:specimen"))
                .include(new Include("DiagnosticReport:result"))
                .where(new TokenClientParam("_id").exactly().code(id)).returnBundle(Bundle.class)
                .execute();
        
        Specimen firstSpecimen = null; 
        List<CodingDTO> diagnosticCodes = new ArrayList<>();
        
        GridModel model = new GridModel();

        if (bundle.getEntry() != null && bundle.getEntry().size() > 0) {
            Resource resource = bundle.getEntry().get(0).getResource();
            if (resource instanceof DiagnosticReport) {
                DiagnosticReport diagnosticReport = (DiagnosticReport) resource;

                if (diagnosticReport.hasResult()) {
                    for (Reference ref : diagnosticReport.getResult()) {
                        if (ref.getResource() != null) {
                            if (ref.getResource() instanceof Observation) {
                                model.addRow(this.buildLabDetailRow((Observation) ref.getResource()));
                            } else if (ref.getResource() instanceof Specimen) {
                                firstSpecimen = (Specimen) ref.getResource();
                            } else if (ref.getResource() instanceof DiagnosticReport) {
                                List<GridRow> panelDetailRows = this.buildLabPanelDetailRows((DiagnosticReport) ref.getResource());
                                panelDetailRows.forEach(r -> model.addRow(r));
                            }
                        }
                    }
                } else if (diagnosticReport.getContained() != null && !diagnosticReport.getContained().isEmpty()) {
                    for (Resource containedResource : diagnosticReport.getContained()) {
                        if (containedResource instanceof Observation) {
                            model.addRow(this.buildLabDetailRow((Observation) containedResource));
                        } else if (containedResource instanceof Specimen && firstSpecimen == null){
                            firstSpecimen = (Specimen) containedResource;
                        }
                    }
                }


                List<CodeableConcept> codedDiagnosis = diagnosticReport.getCodedDiagnosis();
                if (codedDiagnosis != null){
                    for (CodeableConcept cd : codedDiagnosis) {
                        if (cd.hasCoding()){
                            diagnosticCodes.add(new CodingDTO(
                                cd.getCodingFirstRep().getCode(), 
                                cd.getCodingFirstRep().getSystem(), 
                                cd.getCodingFirstRep().getDisplay(), 
                                cd.getCodingFirstRep().getVersion())
                            );
                        }
                    }
                }
            }
        }

        data.put("data", model.convertToSerializeableModel());
        data.put("type", DiagnosticReportType.RESULT_DATE_TIME);
        data.put("dataType", Utilities.DATA_TYPE_DATE_TIME);
        data.put("accessionNumber", firstSpecimen == null? "<Unknown>" : firstSpecimen.getAccessionIdentifier().getValue());
        data.put("accessionDateTime", firstSpecimen == null || firstSpecimen.getReceivedTime() == null ? "" : this.formatDate(firstSpecimen.getReceivedTime()));
        data.put("diagnosticCodes", diagnosticCodes);
        
        return data;
    }

    public Map<String, Object> retrieveDiagnosticReportData(Patient patient, Date startDate, Date endDate, String status, String categories, Integer count, Integer pageOffset) {
        String newQuery = QUERY + "&subject=Patient/" + patient.getIdElement().getIdPart();

        if (status != null && status.equals(DiagnosticReportType.QUERY_PENDING)) {
            newQuery += "&status=" + DiagnosticReport.DiagnosticReportStatus.REGISTERED.toCode();
        }

        if (startDate != null && endDate != null) {
            String formattedStartDate = this.queryParamSdf.format(startDate);
            String formattedEndDate = this.queryParamSdf.format(endDate);
            newQuery += "&date=%3E" + formattedStartDate + "&date=%3C" + formattedEndDate;
        }

        if (categories != null) {
            newQuery += "&category=" + categories;
        }

        if (count == null) {
            newQuery += "&_count=50";
        } else {
            newQuery += "&_count=" + count.toString();

            if (pageOffset != null) {
                newQuery += "&_getpagesoffset=" + pageOffset.toString();
            }
        }

        Bundle bundle = getClient().search().byUrl(newQuery).returnBundle(Bundle.class).execute();

        List<DiagnosticReport> diagnosticReportList;
        if (count == null) {
            diagnosticReportList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, DiagnosticReport.class);
        } else {
            diagnosticReportList = FhirServiceUtil.getResourceFromBundle(bundle, DiagnosticReport.class);
        }

        Map<String, Object> data = this.buildDiagnosticReportGridData(diagnosticReportList);
        data.put("totalRows", bundle.getTotal());

        System.out.println("RxR query: " + newQuery + " total rows: " + bundle.getTotal());

        return data;
    }

    private Map<String, Object> buildDiagnosticReportGridData(List<DiagnosticReport> diagnosticReportList) {
        Map<String, Object> data = new HashMap<>();

        GridModel model = new GridModel();

        for (DiagnosticReport diagnosticReport : diagnosticReportList) {
            GridRow row = new GridRow(getEffective(diagnosticReport));

            Practitioner practitioner = this.getPractitionerPerformer(diagnosticReport.getPerformer());
            Organization providerOrganization = this.getOrganization(diagnosticReport.getPerformer(), Codes.PROVIDER_CODE);
            Organization departmentOrganization = this.getOrganization(diagnosticReport.getPerformer(), Codes.DEPARTMENT_CODE);

            row.addCell(new GridCell(DiagnosticReportType.STUDY, diagnosticReport.getCode().getText()));

            if (diagnosticReport.getStatus() != null) {
                row.addCell(new GridCell(DiagnosticReportType.STATUS, diagnosticReport.getStatus().getDisplay()));
            }

            if (departmentOrganization != null) {
                row.addCell(new GridCell(DiagnosticReportType.PERFORMER, departmentOrganization.getName()));
            }

            if (practitioner != null && practitioner.getName() != null && practitioner.getName().get(0) != null) {
                row.addCell(new GridCell(DiagnosticReportType.ORDERING_PROVIDER, practitioner.getName().get(0).getText()));
            }

            if (providerOrganization != null) {
                row.addCell(new GridCell(DiagnosticReportType.FACILITY, providerOrganization.getName()));
            }

            row.addCell(new GridCell(DiagnosticReportType.ID, diagnosticReport.getIdElement().getIdPart()));
            model.addRow(row);
        }

        data.put("data", model.convertToSerializeableModel());
        data.put("type", DiagnosticReportType.COLLECTION_DATE_TIME);
        data.put("dataType", Utilities.DATA_TYPE_DATE_TIME);

        return data;
    }

    public Map<String, Object> retrieveGridRowReportById(String id) {
        String newQuery = QUERY + "&_id=" + id;
        Bundle bundle = getClient().search().byUrl(newQuery).returnBundle(Bundle.class).execute();
        List<DiagnosticReport> diagnosticReportList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, DiagnosticReport.class);
        return this.buildDiagnosticReportGridData(diagnosticReportList);
    }

    /**
     * Returns all the elements considered to be attachments of a Diagnostic
     * Report. 
     * The elements this method considers an attachment are:
     * <ul>
     *  <li>Any element present in the `image` (Media) attribute of the Diagnostic Report</li>
     *  <li>Any attachment in the `presentedForm` attribute that is not of type 'text/plain'</li>
     * </ul>
     * @param id
     * @return 
     */
    public List<AttachmentDTO> retrieveDiagnosticReportAttachments(String id){
        
        DiagnosticReport report = getClient().read(DiagnosticReport.class, id);
        
        if (report == null){
            throw new IllegalArgumentException("Unknown Diagnostic Report with id "+id);
        }
        
        List<AttachmentDTO> result = new ArrayList<>();
        
        if (report.getPresentedForm() != null){
            result.addAll(report.getPresentedForm().stream()
                .filter(pf -> !("text/plain".equalsIgnoreCase(pf.getContentType())))
                .map(pf -> new AttachmentDTO(pf.getContentType(), MimeTypeUtils.getExtensionFromMimeType(pf.getContentType(), "bin"), pf.getCreation(), pf.getTitle(), pf.getDataElement().asStringValue()))
                .collect(Collectors.toList()));
        }
        
        if (report.getImage() != null){
            result.addAll(report.getImage().stream()
                .map(im -> ((Media)im.getLink().getResource()).getContent())
                .map(im -> new AttachmentDTO(im.getContentType(), MimeTypeUtils.getExtensionFromMimeType(im.getContentType(), "bin"), im.getCreation(), im.getTitle(), im.getDataElement().asStringValue()))
                .collect(Collectors.toList()));
        }
        

        
        return result;
    }
    
    /**
     * Returns all the `presentedForm` of a DiagnosticReport whose content-type
     * is 'text/plain'.
     * @param id
     * @return 
     */
    public List<AttachmentDTO> retrieveDiagnosticReportTextAttachments(String id){
        DiagnosticReport report = getClient().read(DiagnosticReport.class, id);
        
        if (report == null){
            throw new IllegalArgumentException("Unknown Diagnostic Report with id "+id);
        }
        
        List<AttachmentDTO> result = new ArrayList<>();
        
        if (report.getPresentedForm() != null){
            result.addAll(report.getPresentedForm().stream()
                .filter(pf -> ("text/plain".equalsIgnoreCase(pf.getContentType())))
                .map(pf -> new AttachmentDTO(pf.getContentType(), MimeTypeUtils.getExtensionFromMimeType(pf.getContentType(), "bin"), pf.getCreation(), pf.getTitle(), pf.getDataElement().asStringValue()))
                .collect(Collectors.toList()));
        }
        
        return result;
    }

    public Map<String, String> buildSubscriptionParams(String status, String categories) {
        Map<String, String> params = new HashMap<>();

        if (status != null && status.equals(DiagnosticReportType.QUERY_PENDING)) {
            params.put("status", DiagnosticReport.DiagnosticReportStatus.REGISTERED.toCode());
        }

        if (categories != null) {
            params.put("category", categories);
        }

        return params;
    }

    private GridRow buildLabDetailRow(Observation observation) {
        String resultDateTime = "";
        if (observation.getIssued() != null) {
            resultDateTime = this.formatDate(observation.getIssued());
        }

        GridRow row = new GridRow(resultDateTime);

        if (observation.getIssued() != null) {
            row.addCell(new GridCell(DiagnosticReportType.LAST_UPDATED, this.formatDate(observation.getIssued()), Utilities.DATA_TYPE_DATE_TIME));
        }

        row.addCell(new GridCell(DiagnosticReportType.COMPONENT, observation.getCode().getText()));
        row.addCell(new GridCell(DiagnosticReportType.RESULT_VALUE, this.getResultValue(observation)));
        row.addCell(new GridCell(DiagnosticReportType.UNITS, this.getUnit(observation)));
        row.addCell(new GridCell(DiagnosticReportType.REFERENCE_RANGE, this.getReferenceRange(observation)));
        row.addCell(new GridCell(DiagnosticReportType.INTERPRETATION, this.getInterpretation(observation)));
        row.addCell(new GridCell(DiagnosticReportType.ID, observation.getId()));

        return row;
    }

    private List<GridRow> buildLabPanelDetailRows(DiagnosticReport report) {
        List<GridRow> rows = new ArrayList<>();
        if (report.hasResult()) {
            for (Reference ref : report.getResult()) {
                if (ref.getResource() != null && ref.getResource() instanceof Observation) {
                    GridRow row = this.buildLabDetailRow((Observation) ref.getResource());
                    row.addCell(new GridCell(DiagnosticReportType.PANEL, report.getCode().getText()));
                    rows.add(row);
                }
            }
        }

        return rows;
    }
    
    private String formatDate(Date date) {
        return this.sdf.format(date);
    }

    /**
     * Returns the value quantity
     * @param observation
     * @return
     */
    private String getResultValue(Observation observation) {
        try {
            if (observation.hasValueQuantity() && observation.getValueQuantity().hasValue()) {
                return observation.getValueQuantity().getValue().toString();
            } else if (observation.hasValueStringType()) {
                return observation.getValueStringType().getValue();
            }
        } catch (FHIRException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns unit from value quantity
     * @param observation
     * @return
     */
    private String getUnit(Observation observation) {
        try {
            if (observation.hasValueQuantity() && observation.getValueQuantity().hasUnit()) {
                return observation.getValueQuantity().getUnit();
            }
        } catch (FHIRException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the reference range as String
     * @param observation
     * @return
     */
    private String getReferenceRange(Observation observation) {
        if (observation.getReferenceRange() != null && !observation.getReferenceRange().isEmpty()) {
            SimpleQuantity lowQuantity = observation.getReferenceRange().get(0).getLow();
            SimpleQuantity highQuantity = observation.getReferenceRange().get(0).getHigh();
            String low = null;
            String high = null;
            if (lowQuantity != null) {
                if (lowQuantity.getValue() != null) {
                    low = lowQuantity.getValue().toString();
                }
            }
            if (highQuantity != null) {
                if (highQuantity.getValue() != null) {
                    high = highQuantity.getValue().toString();
                }
            }

            if (low != null && high != null) {
                return low + "-" + high;
            } else {
                if (low != null) {
                    return low;
                }
                if (high != null) {
                    return high;
                }
            }
        }

        return null;
    }

    /**
     * Returns the interpretation code
     * @param observation
     * @return
     */
    private String getInterpretation(Observation observation) {
        //should we use observation.getInterpretation().getText()?  Currently the example FHIR data does not have this.
        CodeableConcept codeableConcept = observation.getInterpretation();
        if (codeableConcept != null && !codeableConcept.getCoding().isEmpty()) {
            return codeableConcept.getCoding().get(0).getDisplay();
        }

        return null;
    }

    /**
     * Returns and organization by code
     * @param performer
     * @param organizationType
     * @return
     */
    private Organization getOrganization(List<DiagnosticReport.DiagnosticReportPerformerComponent> performer, String organizationType) {
        IBaseResource resource;
        for (DiagnosticReport.DiagnosticReportPerformerComponent component : performer) {
            resource = component.getActor().getResource();
            if (resource instanceof Organization) {
                Organization organization = (Organization) resource;
                List<CodeableConcept> concepts = organization.getType();
                if (concepts != null && !concepts.isEmpty()) {
                    if (organizationType.equals(concepts.get(0).getCoding().get(0).getCode())) {
                        return organization;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Returns a practitioner
     * @param performer
     * @return
     */
    private Practitioner getPractitionerPerformer(List<DiagnosticReport.DiagnosticReportPerformerComponent> performer) {
        IBaseResource resource;
        for (DiagnosticReport.DiagnosticReportPerformerComponent component : performer) {
            resource = component.getActor().getResource();
            if (resource instanceof Practitioner) {
                return (Practitioner) resource;
            }
        }

        return null;
    }

    /**
     * Returns effective date as String
     * @param diagnosticReport
     * @return
     */
    private String getEffective(DiagnosticReport diagnosticReport) {
        try {
            return this.formatDate(diagnosticReport.getEffectiveDateTimeType().getValue());
        } catch (FHIRException e) {
            try {
                return this.formatDate(diagnosticReport.getEffectivePeriod().getEnd());
            } catch (FHIRException e1) {
                e1.printStackTrace();
            }
        }

        return null;
    }
}
