package com.cognitivemedicine.common.service;

import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import com.cognitivemedicine.common.model.GridCell;
import com.cognitivemedicine.common.model.GridModel;
import com.cognitivemedicine.common.model.GridRow;
import com.cognitivemedicine.common.model.PatientSearchModel;
import com.cognitivemedicine.common.patient.PatientType;
import com.cognitivemedicine.common.patient.PatientUtil;
import com.cognitivemedicine.common.util.Codes;
import com.cognitivemedicine.common.util.FhirServiceUtil;
import org.hl7.fhir.dstu3.model.*;
import org.hspconsortium.cwf.fhir.common.BaseService;
import org.thymeleaf.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

public class PatientSearchService extends BaseService {
    private SimpleDateFormat dobFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final String UTF_8 = "UTF-8";
    private final String QUERY = "Patient?";

    public PatientSearchService(IGenericClient client) {
        super(client);
    }

    public Map<String, Object> findPatientByCriteria(PatientSearchModel model) throws UnsupportedEncodingException {
        String query = QUERY;

        if (!StringUtils.isEmpty(model.getGender())) {
            query += "gender=" + model.getGender() + "&";
        }

        if (!StringUtils.isEmpty(model.getLastName())) {
            query += "family=" + URLEncoder.encode(model.getLastName(), UTF_8) + "&";
        }

        if (!StringUtils.isEmpty(model.getFirstName()) || !StringUtils.isEmpty(model.getMiddleName())) {
            String given = "";

            if (!StringUtils.isEmpty(model.getFirstName())) {
                given += URLEncoder.encode(model.getFirstName(), UTF_8);
            }

            if (!StringUtils.isEmpty(model.getFirstName()) && !StringUtils.isEmpty(model.getMiddleName())) {
                given += ",";
            }

            if (!StringUtils.isEmpty(model.getMiddleName())) {
                given += URLEncoder.encode(model.getMiddleName(), UTF_8);
            }

            query += "given=" + given + "&";
        }

        if (!StringUtils.isEmpty(model.getAddress())) {
            query += "address=" + URLEncoder.encode(model.getAddress(), UTF_8) + "&";
        }

        if (!StringUtils.isEmpty(model.getCity())) {
            query += "address-city=" + URLEncoder.encode(model.getCity(), UTF_8) + "&";
        }

        if (!StringUtils.isEmpty(model.getState())) {
            query += "address-state=" + URLEncoder.encode(model.getState(), UTF_8) + "&";
        }

        if (!StringUtils.isEmpty(model.getZip())) {
            query += "address-postalcode=" + URLEncoder.encode(model.getZip(), UTF_8) + "&";
        }

        if (model.getDob() != null) {
            String dobString = dobFormat.format(model.getDob());

            if (dobString != null) {
                query += "birthdate=" + dobString + "&";
            }
        }

        if (!StringUtils.isEmpty(model.getMedicalRecordNumber())) {
            String formattedNumber = Codes.MR_SYSTEM + "|" + model.getMedicalRecordNumber();
            query += "identifier=" + formattedNumber;
        }

        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();
        List<Patient> patientList = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, Patient.class);
        return this.buildPatientResponse(patientList);
    }

    private Map<String, Object> buildPatientResponse(List<Patient> patientList) {
        Map<String, Object> data = new HashMap<>();
        GridModel model = new GridModel();

        for (Patient patient : patientList) {
            String lastName = "";
            String firstName = "";
            String middleName = "";

            if (patient.hasName()) {
                HumanName name = patient.getNameFirstRep();
                lastName = name.getFamily();

                if (name.hasGiven()) {
                    firstName = name.getGiven().get(0).getValueAsString();

                    if (name.getGiven().size() > 1) {
                        for (int i = 1; i < name.getGiven().size(); i++) {
                            middleName += name.getGiven().get(i).getValueAsString();

                            if (i != name.getGiven().size() - 1) {
                                middleName += " ";
                            }
                        }
                    }
                }
            }

            GridRow row = new GridRow(lastName);
            row.addCell(new GridCell(PatientType.FIRST_NAME, firstName));
            row.addCell(new GridCell(PatientType.MIDDLE_NAME, middleName));
            row.addCell(new GridCell(PatientType.BIRTHDATE, PatientUtil.getPatientBirthdate(patient)));
            row.addCell(new GridCell(PatientType.ID, patient.getId()));

            if (patient.hasGender()) {
                row.addCell(new GridCell(PatientType.GENDER, patient.getGender().getDisplay()));
            }

            if (patient.hasAddress()) {
                Address address = patient.getAddressFirstRep();
                row.addCell(new GridCell(PatientType.CITY, address.getCity()));
                row.addCell(new GridCell(PatientType.ZIP, address.getPostalCode()));
            }

            String mrNumber = PatientUtil.getMedicalRecordNumber(patient);
            if (mrNumber != null) {
                row.addCell(new GridCell(PatientType.MEDICAL_RECORD_NUMBER, mrNumber));
            }

            model.addRow(row);
        }

        data.put("data", model.convertToSerializeableModel());
        data.put("type", PatientType.LAST_NAME);

        return data;
    }
}
