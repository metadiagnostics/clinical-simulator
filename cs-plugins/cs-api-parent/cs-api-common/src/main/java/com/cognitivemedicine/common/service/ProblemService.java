package com.cognitivemedicine.common.service;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.TokenClientParam;

import com.cognitivemedicine.common.model.*;
import com.cognitivemedicine.common.problem.ProblemType;
import com.cognitivemedicine.common.util.ConfigUtilConstants;
import com.cognitivemedicine.common.util.FhirServiceUtil;
import com.cognitivemedicine.common.util.Utilities;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.abstractreasoning.data.*;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hspconsortium.cwf.fhir.common.BaseService;
import org.thymeleaf.util.StringUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProblemService extends BaseService {
    private static final String QUERY = "Condition?_count=50"
            + "&_include=Condition:encounter&_include=Condition:asserter";

    private DateFormat queryParamSdf = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat outputSdf = new SimpleDateFormat("yyyyMMddHHmm");
    private RestClient abstractClient;

    public ProblemService(IGenericClient client) {
        super(client);
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        String baseUrl = config.getString(ConfigUtilConstants.KEY_DECISIONING_SERVICE_URL);
        this.abstractClient = new RestClient(baseUrl);
    }


    private String getFacility(String provider) {
        String newQuery = "PractitionerRole?_count=50&_include=PractitionerRole:organization&practitioner=Practitioner/"+ provider;
        Bundle bundle = getClient().search().byUrl(newQuery).returnBundle(Bundle.class).execute();

        List<PractitionerRole> practitionerRoles = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, PractitionerRole.class);
        for (PractitionerRole role : practitionerRoles) {
            if (role.hasOrganization() && role.getOrganization().hasReference()
                    && role.getOrganization().getResource() != null
                    && role.getOrganization().getResource() instanceof Organization) {
                return ((Organization) role.getOrganization().getResource()).getName();

            }
        }
        return "N/A";
    }


    public Map<String, Object> retrieveProblemData(Patient patient, Date startDate, Date endDate, boolean includeAlertData) {
        Map<String, Object> data = new HashMap<>();
        String newQuery = QUERY + "&subject=Patient/" + patient.getIdElement().getIdPart();

        if (startDate != null && endDate != null) {
            String formattedStartDate = this.queryParamSdf.format(startDate);
            String formattedEndDate = this.queryParamSdf.format(endDate);
            newQuery += "&onset-date=%3E" + formattedStartDate + "&onset-date=%3C" + formattedEndDate;
        }

        Bundle bundle = getClient().search().byUrl(newQuery).returnBundle(Bundle.class).execute();

        List<Condition> conditions = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, Condition.class);
        GridModel model = this.buildGridModel(conditions, patient.getIdElement().getIdPart(), includeAlertData);

        data.put("data", model.convertToSerializeableModel());
        data.put("type", ProblemType.PROBLEM);
        return data;
    }

    public List<Condition> retrieveProblemsByPatientId(String patientId) {
        String newQuery = QUERY + "&subject=Patient/" + patientId;
        Bundle bundle = getClient().search().byUrl(newQuery).returnBundle(Bundle.class).execute();

        return FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, Condition.class);
    }

    public Map<String, Object> retrieveProblemResourceById(String id) {
        Map<String, Object> data = new HashMap<>();

        Bundle bundle = getClient().search().forResource(Condition.class)
                .include(new Include("Condition:encounter"))
                .include(new Include("Condition:subject"))
                .include(new Include("Condition:asserter"))
                .where(new TokenClientParam("_id").exactly().code(id))
                .returnBundle(Bundle.class)
                .execute();

        String patientId = null;
        if (bundle != null && bundle.getEntryFirstRep() != null && bundle.getEntryFirstRep().getResource() instanceof Condition) {
            Condition condition = (Condition) bundle.getEntryFirstRep().getResource();

            if (condition.hasSubject() && condition.getSubject().getResource() instanceof Patient) {
                patientId = ((Patient) condition.getSubject().getResource()).getIdElement().getIdPart();
            }
        }

        List<Condition> conditions = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, Condition.class);
        GridModel model = this.buildGridModel(conditions, patientId,false);
        data.put("data", model.convertToSerializeableModel());
        data.put("type", ProblemType.PROBLEM);

        return data;
    }

    public Map<String, Object> retrieveDrawerData(Patient patient, String userId) {
        Map<String, Object> data = new HashMap<>();
        String patientId = patient.getIdElement().getIdPart();

        List<Condition> problems = this.retrieveProblemsByPatientId(patientId);

        GridModel model = this.buildDrawerGridModel(problems, patient.getIdElement().getIdPart(), userId);
        data.put("data", model.convertToSerializeableModel());
        data.put("type", "problem");

        return data;
    }

    public DrawerDetailModel retrieveDrawerDetailData(Patient patient, String code, String codeSystem) {
        DrawerDetailModel model = new DrawerDetailModel();
        NECWorkProductModel serviceModel = this.retrieveWorkProduct(patient.getIdElement().getIdPart(), code, codeSystem);

        if (serviceModel != null) {
            List<RiskFactors> riskFactors = serviceModel.getRiskFactors();

            if (riskFactors != null) {
                GridModel riskFactorGridModel = new GridModel();

                for (RiskFactors factor : riskFactors) {
                    GridRow gridRow = new GridRow(factor.getName());
                    gridRow.addCell(new GridCell(ProblemType.CONTRIBUTION, factor.getContribution()));
                    riskFactorGridModel.addRow(gridRow);
                }

                Map<String, Object> risks = new HashMap<>();
                risks.put("data", riskFactorGridModel.convertToSerializeableModel());
                risks.put("type", ProblemType.FACTOR_NAME);
                model.setRiskFactors(risks);
            }

            LineChartServiceModel lineChartServiceModel = serviceModel.getLineChartModel();

            if (lineChartServiceModel != null && lineChartServiceModel.getData() != null) {
                LineChartServiceDetail lineChartServiceDetail = lineChartServiceModel.getData();
                List<LineChartServicePoint> pointList = lineChartServiceDetail.getData();

                LineChartDetail detail = new LineChartDetail();
                detail.setTitle(lineChartServiceModel.getData().getTitle());

                for (LineChartServicePoint point : pointList) {
                    detail.addEntry(point.getX(), point.getY());
                }

                LineChartModel lineChartDisplayModel = new LineChartModel();
                lineChartDisplayModel.setData(detail);

                model.setLineChartModel(lineChartDisplayModel);
            }

            List<ProcessAdherenceModel> processModelList = new ArrayList<>();
            List<ProcessAdherenceServiceModel> serviceModelList = serviceModel.getProcessAdherence();

            if (serviceModelList != null) {
                for (ProcessAdherenceServiceModel processModel : serviceModelList) {
                    List<Criteria> criteriaList = processModel.getCriteria();
                    Map<String, Object> criteriaMap = new HashMap<>();

                    if (criteriaList != null) {
                        GridModel gridModel = new GridModel();
                        for (Criteria criteria : criteriaList) {
                            List<Assessment> assessList = criteria.getData();

                            for (Assessment assessment : assessList) {
                                GridRow gridRow = new GridRow(this.getDrawerDateString(criteria.getDate()));
                                GridCell gridCellName = new GridCell(ProblemType.NAME, assessment.getName());
                                GridCell gridCellAssessment = new GridCell(ProblemType.ASSESSMENT, assessment.getCompliancy());
                                gridRow.addCell(gridCellName);
                                gridRow.addCell(gridCellAssessment);
                                gridModel.addRow(gridRow);
                            }

                        }
                        criteriaMap.put("data", gridModel.convertToSerializeableModel());
                        criteriaMap.put("type", ProblemType.DATE_TIME);
                        criteriaMap.put("dataType", Utilities.DATA_TYPE_DATE_TIME);
                    }

                    processModelList.add(new ProcessAdherenceModel(processModel.getLabel(), processModel.getStatus(), criteriaMap));
                }
            }

            model.setProcessAdherence(processModelList);
        }

        return model;
    }

    public void updateAcknowledgement(String resourceId, String userId) {
        Acknowledgements ack = new Acknowledgements();
        ack.setDateTimeViewed(new Date());
        ack.setResourceId(resourceId);
        ack.setResourceType("Condition");
        ack.setUserId(userId);
        ack.setViewed(1);
        this.abstractClient.addAcknowledgement(userId, ack);
    }

    private GridModel buildGridModel(List<Condition> conditions, String patientId, boolean includeAlertData) {
        GridModel model = new GridModel();

        for (Condition problem : conditions) {
            String problemName = "";
            String code = null;
            String codeSystem = null;

            if (problem.getCode() != null
                    && problem.getCode().getCoding() != null
                    && problem.getCode().getCoding().size() > 0) {
                Coding coding = problem.getCode().getCodingFirstRep();
                problemName = coding.getDisplay();
                code = coding.getCode();
                codeSystem = coding.getSystem();
            }

            String acuity = "N/A";
            CodeableConcept severity = problem.getSeverity();

            if (severity != null) {
                if (severity.getCoding() != null && severity.getCoding().size() > 0) {
                    acuity = problem.getSeverity().getCoding().get(0).getDisplay();
                } else {
                    acuity = severity.getText();
                }
            }

            String providerName = "N/A";
            String providerId = null;
            Reference asserter = problem.getAsserter();
            if (asserter != null && asserter.getReference() != null
                    && asserter.getResource() != null
                    && asserter.getResource() instanceof Practitioner) {
                Practitioner practitioner = (Practitioner) asserter.getResource();
                providerId = practitioner.getIdElement().getIdPart();

                if (practitioner.hasName()) {
                    providerName = practitioner.getNameFirstRep().getFamily();

                    if (practitioner.getNameFirstRep().hasGiven()) {
                        providerName += ", " + practitioner.getNameFirstRep().getGiven().get(0).getValue();
                    }
                }
            }

            String facility = this.getFacility(providerId);

            GridRow row = new GridRow(problemName);

            String onsetDate = "N/A";
            if (problem.getOnset() != null) {
                onsetDate = problem.getOnset().toString();
            }

            String assertedDate = "N/A";
            if (problem.getAssertedDateElement() != null) {
                assertedDate = problem.getAssertedDateElement().getValueAsString();
            }

            String resolvedDate = "N/A";
            boolean abatementDateTime = false;
            if (problem.hasAbatementDateTimeType()) {
                try {
                    resolvedDate = problem.getAbatementDateTimeType().getValueAsString();
                    abatementDateTime = true;
                } catch (FHIRException e) {
                    e.printStackTrace();
                }
            }

            row.addCell(new GridCell(ProblemType.ONSET_DATE, onsetDate, Utilities.DATA_TYPE_DATE_TIME));
            row.addCell(new GridCell(ProblemType.REPORTED_DATE, assertedDate, Utilities.DATA_TYPE_DATE));

            if (abatementDateTime) {
                row.addCell(new GridCell(ProblemType.RESOLVED_DATE, resolvedDate, Utilities.DATA_TYPE_DATE));
            } else {
                row.addCell(new GridCell(ProblemType.RESOLVED_DATE, resolvedDate));
            }

            row.addCell(new GridCell(ProblemType.PROVIDER, providerName));
            row.addCell(new GridCell(ProblemType.ACUITY, acuity));
            row.addCell(new GridCell(ProblemType.FACILITY, facility));
            row.addCell(new GridCell(ProblemType.ID, problem.getIdElement().getIdPart()));

            if (includeAlertData && code != null && codeSystem != null) {
                NECWorkProductModel workProductModel = this.retrieveWorkProduct(patientId, code, codeSystem);

                if (workProductModel != null) {
                    row.addCell(new GridCell(ProblemType.BAR, this.getColorFromWorkProduct(workProductModel)));
                }
            }

            model.addRow(row);
        }

        return model;
    }

    private GridModel buildDrawerGridModel(List<Condition> problems, String patientId, String userId){
        GridModel model = new GridModel();
        for(Condition problem : problems) {
            String code = null;
            String codeSystem = null;
            String display = null;

            if (problem.hasCode() && problem.getCode().hasCoding()) {
                Coding coding = problem.getCode().getCodingFirstRep();
                code = coding.getCode();
                display = coding.getDisplay();
                codeSystem = coding.getSystem();
            }

            if (code != null && codeSystem != null) {
                CdsWpOutcomes outcome = this.abstractClient.getDecisioningOutcomeByPatientIdAndCoding(patientId, code, codeSystem);

                if (outcome != null) {
                    String problemId = problem.getIdElement().getIdPart();
                    GridRow row = new GridRow(display);
                    row.addCell(new GridCell("flagSetDateTime", this.outputSdf.format(outcome.getDateProcessed()), "date"));
                    row.addCell(new GridCell("id", problemId));
                    row.addCell(new GridCell("code", code));
                    row.addCell(new GridCell("codeSystem", codeSystem));

                    NECWorkProductModel workProduct = this.getWorkProduct(outcome);

                    if (workProduct != null) {
                        row.addCell(new GridCell("bar", this.getColorFromWorkProduct(workProduct)));
                    }

                    List<Acknowledgements> acks = this.getAcknowledgements(userId, problemId);
                    for (Acknowledgements ack : acks) {
                        if (ack.getResourceId() != null && ack.getResourceId().equals(problemId)
                                && ack.getResourceType() != null && ack.getResourceType().equals("Condition")) {
                            // If the row exists in the DB table, then the user has already acknowledged it
                            row.addCell(new GridCell("ack", "1"));
                            break;
                        }
                    }

                    model.addRow(row);
                }
            }
        }

        return model;
    }

    private List<Acknowledgements> getAcknowledgements(String userId, String resourceId) {
        // by user id for now
        List<Acknowledgements> list = new ArrayList<>();
        try {
            list = abstractClient.getAcknowledgements(userId, resourceId);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    private NECWorkProductModel retrieveWorkProduct(String patientId, String code, String codeSystem) {
        CdsWpOutcomes outcome = this.abstractClient.getDecisioningOutcomeByPatientIdAndCoding(patientId, code, codeSystem);
        return this.getWorkProduct(outcome);
    }

    private NECWorkProductModel getWorkProduct(CdsWpOutcomes outcome) {
        NECWorkProductModel model = null;

        if (outcome != null && !StringUtils.isEmpty(outcome.getWorkProduct())) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            try {
                model = mapper.readValue(outcome.getWorkProduct(), NECWorkProductModel.class);
            } catch (IOException io) {
                // Just return null for the model
                io.printStackTrace();
            }
        }

        return model;
    }

    private String getColorFromWorkProduct(NECWorkProductModel model) {
        String color = "";

        if (model.getProcessAdherence() != null && model.getProcessAdherence().size() > 0) {
            int redCount = 0;
            int yellowCount = 0;
            int greenCount = 0;

            for (ProcessAdherenceServiceModel pa : model.getProcessAdherence()) {
                switch (pa.getStatus()) {
                    case "ok":
                        greenCount++;
                        break;
                    case "caution":
                        yellowCount++;
                        break;
                    case "fail":
                        redCount++;
                        break;
                }
            }

            if (redCount > 0) {
                color = "red";
            } else if (yellowCount > 0) {
                color = "yellow";
            } else if (greenCount > 0) {
                color = "green";
            }
        }

        return color;
    }

    private String getDrawerDateString(Date date) {
        String res = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
            res = sdf.format(date);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return res;
    }
}
