/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.orders;

public class OrdersType {
    /**
     * This represent the concrete class of a particular Row
     */
    public static final String CLASS_TYPE = "classType";
    public static final String ORDER_DATE_TIME = "orderDateTime";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String STATUS = "status";
    public static final String PRIORITY = "priority";
    public static final String ORDERING_PROVIDER = "orderingProvider";
    public static final String FACILITY = "facility";
    public static final String ID = "id";
}
