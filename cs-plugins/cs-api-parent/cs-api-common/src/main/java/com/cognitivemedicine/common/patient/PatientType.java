package com.cognitivemedicine.common.patient;

public class PatientType {
    public static String FIRST_NAME = "firstName";
    public static String LAST_NAME = "lastName";
    public static String MIDDLE_NAME = "middleName";
    public static String GENDER = "gender";
    public static String MEDICAL_RECORD_NUMBER = "medicalRecordNumber";
    public static String CITY = "city";
    public static String ZIP = "zip";
    public static String ID = "id";
    public static String BIRTHDATE = "birthdate";
}
