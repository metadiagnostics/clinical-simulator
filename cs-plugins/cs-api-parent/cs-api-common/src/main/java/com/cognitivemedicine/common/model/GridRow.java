package com.cognitivemedicine.common.model;

import java.util.ArrayList;
import java.util.List;

public class GridRow {
    private String key;
    private List<GridCell> cells;

    public GridRow(String key) {
        this.key = key;
        this.cells = new ArrayList<>();
    }

    public List<GridCell> getCells() {
        return cells;
    }

    public void setCells(List<GridCell> cells) {
        this.cells = cells;
    }

    public void addCell(GridCell cell) {
        this.cells.add(cell);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
