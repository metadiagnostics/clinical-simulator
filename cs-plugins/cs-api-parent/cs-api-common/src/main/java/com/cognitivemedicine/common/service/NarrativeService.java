/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.service;

import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import com.cognitivemedicine.common.model.NarrativeModel;
import org.hl7.fhir.dstu3.model.*;
import org.hspconsortium.cwf.fhir.common.BaseService;

public class NarrativeService extends BaseService {
    public NarrativeService(IGenericClient client) {
        super(client);
    }

    public NarrativeModel retrieveNarrative(String resourceId, String resourceType) {
        Bundle bundle = getClient().search().forResource(resourceType)
                .where(new TokenClientParam("_id").exactly().code(resourceId)).returnBundle(Bundle.class)
                .execute();
        NarrativeModel model = new NarrativeModel();
        if (bundle.getEntry() != null && bundle.getEntry().size() > 0) {

            Resource resource = bundle.getEntry().get(0).getResource();
            if(resource instanceof MedicationRequest) {
                MedicationRequest request = (MedicationRequest) resource;
                model.setTitle("Medication Request");

                if (request.getText() != null) {
                    model.setNarrative(request.getText().getDivAsString());
                    model.setContentType("HTML");
                }
            } else if(resource instanceof ProcedureRequest) {
                ProcedureRequest request = (ProcedureRequest) resource;

                model.setTitle("Procedure Request");
                model.setNarrative("");
                model.setContentType("text");
            } else if(resource instanceof NutritionOrder) {
                NutritionOrder order = (NutritionOrder) resource;

                model.setTitle("Nutrition Order");
                model.setNarrative("");
                model.setContentType("text");
            } else if(resource instanceof ReferralRequest) {
                ReferralRequest request = (ReferralRequest) resource;
                model.setTitle("Referral Request");
                model.setNarrative("");
                model.setContentType("text");
            }
        }
        return model;
    }
}
