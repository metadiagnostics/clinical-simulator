/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.flowsheet.model;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author esteban
 */
public class SmartFormResponsesDTO {

    public static class Metadata{
        private String id;
        private String smartform_id;
        private String smartform_version;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSmartform_id() {
            return smartform_id;
        }

        public void setSmartform_id(String smartform_id) {
            this.smartform_id = smartform_id;
        }

        public String getSmartform_version() {
            return smartform_version;
        }

        public void setSmartform_version(String smartform_version) {
            this.smartform_version = smartform_version;
        }
    }
    
    public static class Subject {
        private String id;
        private String name;
        private int dob;  //in yyyyMMdd format  
        private String gender;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getDob() {
            return dob;
        }

        public void setDob(int dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
        
    }
    
    public static class Author {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    
    public static class IdValue {
        private String id;
        private String value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        
    }
    
    private Metadata metadata;
    private int date_created; //in yyyyMMdd format  
    private Integer date_signed; //in yyyyMMdd format  
    private String status;

    private boolean _final;
    private Map<String, IdValue> responses = new HashMap<>();

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public int getDate_created() {
        return date_created;
    }

    public void setDate_created(int date_created) {
        this.date_created = date_created;
    }

    public Integer getDate_signed() {
        return date_signed;
    }

    public void setDate_signed(Integer date_signed) {
        this.date_signed = date_signed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean is_final() {
        return _final;
    }

    public void set_final(boolean _final) {
        this._final = _final;
    }

    public Map<String, IdValue> getResponses() {
        return responses;
    }

    public void setResponses(Map<String, IdValue> responses) {
        this.responses = responses;
    }
    
    
}
