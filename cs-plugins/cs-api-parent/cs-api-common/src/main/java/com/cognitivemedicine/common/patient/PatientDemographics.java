package com.cognitivemedicine.common.patient;

public class PatientDemographics {
    private String name;
    private String gender;
    private String medicalRecordNumber;
    private String estimatedGestationalAge;
    private String correctedGestationalAge;
    private Integer daysOfLife;
    private String birthWeight;
    private String todayWeight;
    private String calculatedWeight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public String getEstimatedGestationalAge() {
        return estimatedGestationalAge;
    }

    public void setEstimatedGestationalAge(String estimatedGestationalAge) {
        this.estimatedGestationalAge = estimatedGestationalAge;
    }

    public String getCorrectedGestationalAge() {
        return correctedGestationalAge;
    }

    public void setCorrectedGestationalAge(String correctedGestationalAge) {
        this.correctedGestationalAge = correctedGestationalAge;
    }

    public Integer getDaysOfLife() {
        return daysOfLife;
    }

    public void setDaysOfLife(Integer daysOfLife) {
        this.daysOfLife = daysOfLife;
    }

    public String getBirthWeight() {
        return birthWeight;
    }

    public void setBirthWeight(String birthWeight) {
        this.birthWeight = birthWeight;
    }

    public String getTodayWeight() {
        return todayWeight;
    }

    public void setTodayWeight(String todayWeight) {
        this.todayWeight = todayWeight;
    }

    public String getCalculatedWeight() {
        return calculatedWeight;
    }

    public void setCalculatedWeight(String calculatedWeight) {
        this.calculatedWeight = calculatedWeight;
    }
}
