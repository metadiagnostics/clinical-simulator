/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.util;

import org.hl7.fhir.dstu3.model.Coding;

/**
 * Created by Jeff on 6/14/2017.
 */
public class Codes {

    public static final String SNOMED_CODESYSTEM = "http://snomed.info/sct";

    public static final String LOINC_CODESYSTEM = "http://loinc.org";

    public static final String NDS_NHRC_CODESYSTEM = "http://hl7.org/fhir/sid/ndc";

    public static final String MR_SYSTEM = "urn:oid:1.2.3.4";

    public static final Coding BABY_FORMULA = new Coding(NDS_NHRC_CODESYSTEM, "70074-0562-70", "Baby formula");

    public static final Coding BREAST_MILK = new Coding(SNOMED_CODESYSTEM, "226790003", "Breast milk");

    public static final Coding TPN = new Coding(SNOMED_CODESYSTEM, "427324005", "Total parenteral nutrition");
    
    public static final Coding URINE = new Coding(SNOMED_CODESYSTEM, "364202003", "Urine");
    
    public static final Coding FECES = new Coding(SNOMED_CODESYSTEM, "167621006", "Feces");
    
    public static final Coding GASTRIC_FLUID = new Coding(SNOMED_CODESYSTEM, "258459007", "Gastric fluid");

    public static final String PROVIDER_CODE = "prov";

    public static final String DEPARTMENT_CODE = "dept";

    public static final String ESTIMATED_GESTATIONAL_AGE_CODE = "444135009";

    public static final String BIRTH_WEIGHT_CODE = "364589006";

    public static final String BODY_WEIGHT_CODE = "27113001";

    public static final String CALCULATED_WEIGHT_CODE = "9999999";
}
