package com.cognitivemedicine.common.service;

import ca.uhn.fhir.rest.client.IGenericClient;
import com.cognitivemedicine.common.model.GridCell;
import com.cognitivemedicine.common.model.GridModel;
import com.cognitivemedicine.common.model.GridRow;
import com.cognitivemedicine.common.note.ClinicalNoteType;
import com.cognitivemedicine.common.util.ConfigUtilConstants;
import com.cognitivemedicine.common.util.Utilities;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.client.ClientUtilities;
import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.cs.models.FormDefinition;
import com.cognitivemedicine.cs.models.FormResponse;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.hl7.fhir.dstu3.model.*;
import org.hspconsortium.cwf.fhir.common.BaseService;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ClinicalNoteService extends BaseService {

    private DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
    private DateFormat queryParamSdf = new SimpleDateFormat("yyyy-MM-dd");
    private DataServiceRestClient restClient;
    
    public ClinicalNoteService(IGenericClient client) {
        super(client);
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        String baseUrl = config.getString(ConfigUtilConstants.KEY_SIMULATOR_DATA_SERVICE_URL);
        this.restClient = new DataServiceRestClient(baseUrl);
    }

    /**
     * This method returns all the Clinical Notes (Smart Forms) instances (that
     * were responded) for a patient.
     * 
     * @param patient
     * @param startDate
     * @param endDate
     * @return 
     */
    public Map<String, Object> retrieveClinicalNotesInstancesData(Patient patient, Date startDate, Date endDate, String formType, String formVersion) {
        FormResponse[] responses = this.restClient.retrieveFormResponses(patient.getIdElement().getIdPart(), formType, formVersion, startDate, endDate);
        return this.buildGridModel(responses);
    }

    public FormDefinition retrieveClinicalNoteDefinition(String type, String version){
        return this.restClient.retrieveFormDefinition(type, version);
    }
    
    public FormResponse retrieveClinicalNoteResponse(String id){
        return this.restClient.retrieveFormResponseById(id);
    }

    public Integer getCurrentGutCheckNecScore(Patient patient, String formType, String formVersion) throws ParseException {
        FormResponse[] responses = this.restClient.retrieveFormResponses(patient.getIdElement().getIdPart(), formType, formVersion);

        Integer latestScore = null;
        Date latestDate = null;
        if (responses != null && responses.length > 0) {
            for (FormResponse response : responses) {
                if (latestDate == null) {
                    latestDate = sdf.parse(response.getLastUpdateDateTime());
                    latestScore = ClientUtilities.calculateGutCheckNecScore(response);
                } else {
                    Date responseDate = sdf.parse(response.getLastUpdateDateTime());

                    if (latestDate.before(responseDate)) {
                        latestDate = responseDate;
                        latestScore = ClientUtilities.calculateGutCheckNecScore(response);
                    }
                }
            }
        }

        return latestScore;
    }

    public Map<String, Object> saveForm(Map<String, Object> formData) throws IOException {
        String id = this.restClient.addFormResponse(getJsonFromFormResponseData(formData));
        return this.buildSingleResponseGridModel(id);
    }

    public Map<String, Object> updateForm(Map<String, Object> formData) throws IOException {
        String id = this.restClient.updateFormResponse(getJsonFromFormResponseData(formData));
        return this.buildSingleResponseGridModel(id);
    }

    private String getJsonFromFormResponseData(Map<String, Object> formResponseData) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        FormResponse response = mapper.convertValue(formResponseData, FormResponse.class);

       return mapper.writeValueAsString(response);

    }

    private Map<String, Object> buildSingleResponseGridModel(String responseId) {
        FormResponse savedResponse = this.retrieveClinicalNoteResponse(responseId);
        FormResponse[] responses = new FormResponse[1];
        responses[0] = savedResponse;

        return this.buildGridModel(responses);
    }

    private Map<String, Object> buildGridModel(FormResponse[] responses) {
        Map<String, Object> data = new HashMap<>();

        GridModel model = new GridModel();
        if (responses != null && responses.length > 0) {
            for (FormResponse response : responses) {
                GridRow row = new GridRow(response.getCreatedDateTime());
                row.addCell(new GridCell(ClinicalNoteType.TITLE, response.getTitle()));
                row.addCell(new GridCell(ClinicalNoteType.LOCATION, response.getLocation()));
                row.addCell(new GridCell(ClinicalNoteType.DOCUMENT_TYPE, response.getDocumentType()));
                row.addCell(new GridCell(ClinicalNoteType.LAST_UPDATED, response.getLastUpdateDateTime(), Utilities.DATA_TYPE_DATE_TIME));
                row.addCell(new GridCell(ClinicalNoteType.AUTHOR, response.getAuthorName()));
                row.addCell(new GridCell(ClinicalNoteType.SPECIALTY, response.getSpecialty()));
                row.addCell(new GridCell(ClinicalNoteType.AUTHOR_TYPE, response.getNoteType()));
                row.addCell(new GridCell(ClinicalNoteType.STATUS, response.getStatus()));
                row.addCell(new GridCell(ClinicalNoteType.ID, response.getId()));
                model.addRow(row);
            }
        }

        data.put("data", model.convertToSerializeableModel());
        data.put("type", ClinicalNoteType.CREATED_DATE_TIME);
        data.put("dataType", Utilities.DATA_TYPE_DATE_TIME);

        return data;
    }
    
}
