package com.cognitivemedicine.common.util;

public class ConfigUtilConstants {
    public static final String CONTEXT_NAME = "cs.common.api.config";
    public static final String KEY_SIMULATOR_DATA_SERVICE_URL = "data.service.url";
    public static final String KEY_DECISIONING_SERVICE_URL = "decisioning.service.url";
}
