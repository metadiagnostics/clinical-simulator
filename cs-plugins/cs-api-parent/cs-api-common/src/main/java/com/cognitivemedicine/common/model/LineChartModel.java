package com.cognitivemedicine.common.model;

public class LineChartModel {
    private LineChartDetail data;

    public LineChartDetail getData() {
        return data;
    }

    public void setData(LineChartDetail data) {
        this.data = data;
    }
}
