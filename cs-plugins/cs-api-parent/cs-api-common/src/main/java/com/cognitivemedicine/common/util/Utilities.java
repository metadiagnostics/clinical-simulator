/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.cs.models.FormResponseItem;
import org.hl7.fhir.instance.model.api.IBaseCoding;
import org.hl7.fhir.instance.model.api.IBaseMetaType;
import org.hl7.fhir.instance.model.api.IBaseResource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utilities {
    
    private static final String TMINUS = "Tminus";
    
    public static final long SECOND = 1000;
    
    public static final long MINUTE = SECOND * 60;
    
    public static final long HOUR = MINUTE * 60;
    
    public static final long DAY = HOUR * 24;
    
    public static final long WEEK = DAY * 7;
    
    private static final String CHAR_WEEK = "w";
    
    private static final String CHAR_DAY = "d";
    
    private static final String CHAR_HOUR = "h";
    
    private static final String CHAR_MINUTE = "m";
    
    private static final String CHAR_SECOND = "s";
    
    private static final Pattern TMINUS_PATTERN_REGEX = Pattern.compile("Tminus([0-9]+)([wdhms])");
    
    public static final ObjectMapper jsonMapper = new ObjectMapper();
    
    public static final String FHIR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    
    public static final String FHIR_DATE_FORMAT_WITHOUT_MILLIS = "yyyy-MM-dd'T'HH:mm:ssXXX";
    
    public static final String CMS_CODESYSTEM = "http://cognitivemedicalsystems.com";
    
    public static final String CMS_CODE = "CMS-FHIR";

    public static final String DATA_TYPE_DATE_TIME = "datetime";

    public static final String DATA_TYPE_DATE = "date";

    public static final String EVENT_OPEN_DRAWER = "DRAWER.OPEN";

    public static final String EVENT_DRAWER_VISIBLE = "DRAWER.VISIBLE";

    public static final String PATIENT_CONTEXT_EVENT = "CONTEXT.CHANGED.Patient";
    
    /**
     * Get Tminus as long
     * 
     * @param tMinus
     * @return
     */
    public static long getTMinusValueAsLong(String tMinus) {
        Matcher matcher = TMINUS_PATTERN_REGEX.matcher(tMinus);
        
        if (matcher.find()) {
            String metaValue = tMinus.substring(TMINUS.length());
            String tMinusValue = metaValue.substring(0, metaValue.length() - 1);
            String tMinusPeriod = metaValue.substring(metaValue.length() - 1);
            
            return getTMinusValueAsLong(tMinusValue, tMinusPeriod);
        } else {
            throw new IllegalArgumentException("Invalid Tminus value");
        }
    }
    
    /**
     * Get Tminus as long
     * 
     * @param tMinusValue
     * @param tMinusPeriod
     * @return
     */
    public static long getTMinusValueAsLong(String tMinusValue, String tMinusPeriod) {
        long tMinusLongValue = Long.parseLong(tMinusValue);
        long tMinusMillis;
        
        if (CHAR_WEEK.equals(tMinusPeriod)) {
            tMinusMillis = WEEK * tMinusLongValue;
        } else if (CHAR_DAY.equals(tMinusPeriod)) {
            tMinusMillis = DAY * tMinusLongValue;
        } else if (CHAR_HOUR.equals(tMinusPeriod)) {
            tMinusMillis = HOUR * tMinusLongValue;
        } else if (CHAR_MINUTE.equals(tMinusPeriod)) {
            tMinusMillis = MINUTE * tMinusLongValue;
        } else if (CHAR_SECOND.equals(tMinusPeriod)) {
            tMinusMillis = 1000 * tMinusLongValue;
        } else {
            throw new IllegalArgumentException("Period not recognized: " + tMinusPeriod);
        }
        
        return tMinusMillis;
    }
    
    /**
     * Convert a java object to string json
     * 
     * @param object
     * @return
     */
    public static String getAsJson(Object object) {
        try {
            return jsonMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Server error");
        }
    }
    
    /**
     * Returns a date format used by the flowsheet with this pattern yyyyMMddHHmmss
     * 
     * @return
     */
    public static DateFormat getFlowsheetDateFormat() {
        return new SimpleDateFormat("yyyyMMddHHmmss");
    }
    
    /**
     * Returns a date without any seconds or milliseconds which have been set to 0
     * 
     * @param date
     * @return
     */
    public static Date trimSeconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }
    
    public static boolean equalsByMinute(Date date1, Date date2) {
        date1 = trimSeconds(date1);
        date2 = trimSeconds(date2);
        
        return date1.equals(date2);
    }
    
    /**
     * Returns a DateFormat for FHIR resources with this pattern yyyy-MM-dd'T'HH:mm:ss
     * 
     * @return
     */
    public static DateFormat getFhirDateFormat() {
        //        return new SimpleDateFormat(FHIR_DATE_FORMAT);
        return new SimpleDateFormat(FHIR_DATE_FORMAT_WITHOUT_MILLIS);
    }
    
    public static String stripAfterOpenParentheses(String content) {
        if (content.contains("(")) {
            int index = content.indexOf("(");
            String cut = content.substring(0, index - 1);
            content = cut.trim();
        }
        return content;
    }
    
    /**
     * Set the CMS FHIR tag
     *
     * @param resource
     */
    public static void setCmsTag(IBaseResource resource) {
        IBaseMetaType meta = resource.getMeta();
        IBaseCoding tag = meta.addTag();
        tag.setCode(CMS_CODE);
        tag.setSystem(CMS_CODESYSTEM);
    }
}
