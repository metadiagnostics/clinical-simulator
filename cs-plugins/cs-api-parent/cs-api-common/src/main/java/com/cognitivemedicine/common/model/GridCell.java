package com.cognitivemedicine.common.model;

public class GridCell {
    private String value;
    private String type; //Corresponds to the column name
    private String dataType;

    public GridCell(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public GridCell(String type, String value, String dataType){
        this.type = type;
        this.value = value;
        this.dataType = dataType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
