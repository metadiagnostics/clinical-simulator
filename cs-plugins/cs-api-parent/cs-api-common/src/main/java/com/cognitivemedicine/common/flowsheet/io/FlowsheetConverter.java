/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.common.flowsheet.io;

import com.cognitivemedicine.common.flowsheet.FlowSheetConstants;
import com.cognitivemedicine.common.flowsheet.FlowsheetCellValue;
import com.cognitivemedicine.common.flowsheet.FlowsheetRowModel;
import com.cognitivemedicine.common.util.Utilities;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Converts the FlowsheetModel into a map of the flowsheet model
 */
public class FlowsheetConverter {
    
    private final FlowsheetModel ioGridModel;
    
    private Map<String, Object> data;
    
    public FlowsheetConverter(FlowsheetModel ioGridModel) {
        this.ioGridModel = ioGridModel;
        
        convertToFlowsheetGridModel();
    }
    
    /**
     * Convert {@link FlowsheetModel} to Flowsheet grid format
     */
    private void convertToFlowsheetGridModel() {
        List<Date> headers = ioGridModel.getHeaderNames();
        List<FlowsheetRowModel> rowNames = ioGridModel.getRowNames();
        List<List<FlowsheetCellValue>> rows = ioGridModel.getRows();
        DateFormat dateFormat = Utilities.getFlowsheetDateFormat();
        
        List<Map> rowList = new ArrayList<>();
        for (int i = 0; i < rowNames.size(); i++) {
            List<Map> cellList = new ArrayList<>();
            List<FlowsheetCellValue> row = rows.get(i);
            for (int j = 0; j < row.size(); j++) {
                if (row.get(j) != null) {
                    FlowsheetCellValue cellValue = (FlowsheetCellValue)row.get(j);
                    String value = "UNK";
                    if (cellValue.getValue() != null) {
                        Integer cValue = cellValue.getValue().intValue();
                        value = cValue.toString();
                    }
                    else {
                        value = cellValue.getDescription();
                    }
                    Map<String, String> cellEntry = new HashMap<>();
                    cellEntry.put("value", value);
                    cellEntry.put("timestamp", dateFormat.format(headers.get(j)));
                    String info = null;
                    if (cellValue.getInfo() != null) {
                        info = cellValue.getInfo().name();
                    }
                    cellEntry.put("info", info);
                    cellList.add(cellEntry);
                }
            }
            
            FlowsheetRowModel rowName = rowNames.get(i);
            Map<String, Object> rowvalues = new HashMap<>();
            rowvalues.put(FlowSheetConstants.NAME, rowName.getDisplayName());
            //rowvalues.put(FlowSheetConstants.CONFIDENTIALITY, rowName.getConfidentialy());
            if (rowName.getType().equals(FlowSheetConstants.Type.INPUT)) {
                rowvalues.put(FlowSheetConstants.TYPE, 1);
            }
            else if (rowName.getType().equals(FlowSheetConstants.Type.OUTPUT)) {
                rowvalues.put(FlowSheetConstants.TYPE, -1);
            }
            else {
                rowvalues.put(FlowSheetConstants.TYPE, 0);
            }
            rowvalues.put(FlowSheetConstants.VALUES, cellList);
            
            rowList.add(rowvalues);
        }
        
        data = new HashMap<>();
        data.put("data", rowList);
        data.put("type", "I_O");
        System.out.println("Done with the IO convertToFlowsheetGridModel");
    }
    
    /**
     * Returns the flowsheet grid model as a map for conversion to JSON
     * 
     * @return
     */
    public Map<String, Object> getFlowsheetGridModel() {
        return data;
    }
}
