#!/bin/sh

if [ $# -eq 0 ]
  then
    echo "Please pass in the branch name to check out for the submodules"
    exit 1
fi

branchName=$1
echo "Submodule branch to check out: $branchName"

paths=$( git submodule | awk '{print $2}')
currentDir=$(pwd)
echo "Current directory is $currentDir"

IFS=$'\n'
pathsArray=($paths)

for path in "${pathsArray[@]}"
do
  echo "Updating submodule: $path"

  # run git commands
  cd $path
  git fetch
  git checkout $branchName
  git pull origin $branchName
  cd $currentDir
done

echo "Finished updating all submodules"
exit 0