/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.ProblemService;
import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import com.cognitivemedicine.common.model.DrawerDetailModel;
import com.cognitivemedicine.common.util.Utilities;
import org.carewebframework.api.context.UserContext;
import org.carewebframework.api.event.IGenericEvent;
import org.carewebframework.shell.elements.ElementPlugin;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.*;

import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;


public class DrawerPluginController extends PatientRecordPluginController {

    @WiredComponent
    private AngularComponent drawerPlugin;

    @Autowired
    private ProblemService problemService;

    private final IGenericEvent<?> drawerOpenListener = (eventName, eventData) -> {
        Map<String, String> payload = new HashMap<>();

        if (eventData != null) {
            Map<String, Object> eventDataMap = (Map<String, Object>)eventData;
            String resourceId = (String) eventDataMap.get("resourceId");
            payload.put("resourceId", resourceId);

            String callback = (String) eventDataMap.get("_callback");
            this.drawerPlugin.ngInvoke(callback, payload);
        }
    };
    private final IGenericEvent<?> changeDrawerVisibleListener = (eventName, eventData) -> {
        if (eventData != null) {
            Map<String, Object> eventDataMap = (Map<String, Object>)eventData;
            Boolean visible = (Boolean) eventDataMap.get("visible");

            if (visible != null) {
                this.getPlugin().getParent().setVisible(visible.booleanValue());
            }
        }
    };

    @Override
    public void onLoad(ElementPlugin plugin) {
        super.onLoad(plugin);
        getEventManager().subscribe(Utilities.EVENT_OPEN_DRAWER, drawerOpenListener);
        getEventManager().subscribe(Utilities.EVENT_DRAWER_VISIBLE, changeDrawerVisibleListener);

    }

    @Override
    public void onUnload() {
        super.onUnload();
        getEventManager().unsubscribe(Utilities.EVENT_OPEN_DRAWER, drawerOpenListener);
        getEventManager().unsubscribe(Utilities.EVENT_DRAWER_VISIBLE, changeDrawerVisibleListener);
    }

    @EventHandler(value = "retrieveData", target = "drawerPlugin")
    private void onRetrieveData$drawerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        Map<String, Object> data = this.problemService.retrieveDrawerData(PatientContext.getActivePatient(), UserContext.getActiveUser().getLoginName());

        this.addMetadata(data, (String)params.get("_uuid"), null);
        String callback = (String)params.get("_callback");

        this.drawerPlugin.ngInvoke(callback, data);
    }

    @EventHandler(value = "retrieveDetailData", target = "drawerPlugin")
    private void onRetrieveDetailData$drawerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String code = (String) params.get("code");
        String codeSystem = (String) params.get("codeSystem");
        DrawerDetailModel model = this.problemService.retrieveDrawerDetailData(PatientContext.getActivePatient(), code, codeSystem);

        Map<String, Object> data = new HashMap<>();
        data.put("data", model);

        this.addMetadata(data, (String)params.get("_uuid"), null);
        String callback = (String) params.get("_callback");
        this.drawerPlugin.ngInvoke(callback, data);
    }

    @EventHandler(value = "updateAcknowledgement", target = "drawerPlugin")
    private void updateAcknowledgement$drawerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        String resourceId = (String) params.get("resourceId");
        String userId = UserContext.getActiveUser().getLoginName();
        this.problemService.updateAcknowledgement(resourceId, userId);
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return null;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
    }
    
}
