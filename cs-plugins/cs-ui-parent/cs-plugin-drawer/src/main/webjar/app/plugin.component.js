"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var platform_browser_1 = require("@angular/platform-browser");
var button_1 = require("primeng/components/button/button");
var accordion_1 = require("primeng/components/accordion/accordion");
var sidebar_1 = require("primeng/components/sidebar/sidebar");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var $ = require("jquery");
var _ = require("lodash");
var moment = require("moment");
var drawer_table_model_processor_1 = require("./drawer.table.model.processor");
var drawer_table_grid_renderer_1 = require("./drawer.table.grid.renderer");
var cs_plugin_common_1 = require("cs-plugin-common");
var PluginComponent = /** @class */ (function () {
    function PluginComponent(backendService) {
        this.backendService = backendService;
        this.display = false;
        this.accordionIndex = -1;
        this.primaryModel = null;
        this.detailsHeader = {};
        this.adherenceButtons = [];
        this.selectedCriteriaButtonId = null;
        this.detailsLoading = false;
        this.detailsLoaded = false;
    }
    PluginComponent.prototype.ngOnInit = function () {
        this.accordionIndex = 0;
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializePrimaryTable();
        this.initializeSecondaryTable();
        this.initializeAdherenceCriteriaTable();
    };
    PluginComponent.prototype.ngAfterViewInit = function () {
        // Cannot remove the default close button by configuration so this removes it from the DOM after rendering the view
        $('.ui-sidebar-close').remove();
    };
    PluginComponent.prototype.requestPrimaryData = function () {
        this.primaryTable.setModel({ data: [] });
        this.primaryTable.showLoadingIndicator();
        var params = {
            _uuid: this.primaryTable.getUUID(),
            endpoint: 'drawer_p',
            category: 'primary',
            _callback: 'updatePrimaryModel'
        };
        this.backendService.sendBackendEvent("retrieveData", params);
    };
    PluginComponent.prototype.requestSecondaryData = function (code, codeSystem) {
        this.detailsLoading = true;
        var params = {
            _uuid: this.riskFactors.getUUID(),
            endpoint: 'drawer_s',
            code: code,
            codeSystem: codeSystem,
            _callback: 'updateSecondaryModel'
        };
        this.backendService.sendBackendEvent("retrieveDetailData", params);
    };
    PluginComponent.prototype.initializePrimaryTable = function () {
        var _this = this;
        var gridOptions = {
            onGridReady: function () {
                if (cs_plugin_common_1.Utilities.isRunningStandalone()) {
                    _this.requestPrimaryData();
                }
            },
            onSelectionChanged: function ($event) {
                var selectedRows = $event.api.getSelectedRows();
                if (_.isArray(selectedRows) && selectedRows.length > 0) {
                    var x = selectedRows[0];
                    _this.detailsHeader = {
                        colorClass: 'header-color cds-' + x.bar,
                        text: x.problem
                    };
                    _this.requestSecondaryData(x.code, x.codeSystem);
                }
            },
            getRowNodeId: function (data) {
                return data.id;
            },
            onCellClicked: function (event) {
                if (event.column.colId === 'ack' &&
                    !_.get(event, 'event.target.disabled') &&
                    _.get(event, 'event.target.checked') === true) {
                    event.event.target.disabled = true;
                    var data = {
                        resourceId: _.get(event, 'data.id')
                    };
                    _this.backendService.sendBackendEvent('updateAcknowledgement', data);
                }
            }
        };
        var renderer = new drawer_table_grid_renderer_1.DrawerPrimaryTableGrid();
        var modelProcessor = new drawer_table_model_processor_1.DrawerTableModelProcessor([{
                "field": "icon",
                "headerName": " ",
                "renderer": "bar"
            }, {
                "field": "problem",
                "headerName": "Problem"
            }, {
                "field": "flagSetDateTime",
                "headerName": "Flag Set"
            }, {
                "field": "ack",
                "headerName": "Ack",
                "renderer": "checkbox"
            }]);
        this.primaryTable.initialize(modelProcessor, renderer, gridOptions);
    };
    PluginComponent.prototype.initializeSecondaryTable = function () {
        var gridOptions = {
            emptyHeight: '100px'
        };
        var renderer = new cs_plugin_common_1.TableGrid(true);
        var modelProcessor = new drawer_table_model_processor_1.DrawerTableModelProcessor([{
                "field": "factorName",
                "headerName": "Risk Factor"
            }, {
                "field": "contribution",
                "headerName": "Relative Contribution"
            }]);
        this.riskFactors.initialize(modelProcessor, renderer, gridOptions);
    };
    PluginComponent.prototype.initializeAdherenceCriteriaTable = function () {
        var gridOptions = {};
        var renderer = new cs_plugin_common_1.TableGrid(true);
        var modelProcessor = new drawer_table_model_processor_1.DrawerTableModelProcessor([{
                "field": "dateTime",
                "headerName": "Date/Time"
            }, {
                "field": "name",
                "headerName": "Adherence Criteria"
            }, {
                "field": "assessment",
                "headerName": "Assessment"
            }]);
        this.adherenceCriteriaTable.initialize(modelProcessor, renderer, gridOptions);
    };
    PluginComponent.prototype.setAdherenceButtons = function (model) {
        var buttons = _.get(model, 'data.processAdherence', []);
        _.each(buttons, function (button) {
            button.id = cs_plugin_common_1.Guid.newGuid();
            button.class = 'status-' + button.status;
        });
        this.adherenceButtons = buttons;
    };
    PluginComponent.prototype.onButtonClicked = function (model) {
        this.adherenceCriteriaTable.setModel(model.criteria);
        _.each(this.adherenceButtons, function (btn) {
            if (btn.id !== model.id) {
                btn.selected = '';
            }
        });
        model.selected = 'selected';
        this.selectedCriteriaButtonId = model.id;
    };
    PluginComponent.prototype.getDataToPlot = function (values) {
        var retVal = [];
        _.forEach(values, function (value) {
            var x = moment(value.x, 'YYYYMMDDHHmm').format('MM-DD-YYYY HH:mm');
            retVal.push(new cs_plugin_common_1.LineChartElement(x, value.y));
        });
        return retVal;
    };
    PluginComponent.prototype.updateGutCheckNECTrend = function (model) {
        if (_.isNull(model) || _.isEmpty(model.data) || _.isEmpty(model.data.title)) {
            console.error('invalid plot data');
            return;
        }
        var data = this.getDataToPlot(model.data.data);
        this.gutCheckNECLineGraph.draw(model.data.title, data);
    };
    // start of fujion callbacks...
    PluginComponent.prototype.activatePlugin = function () {
        // DO NOT REMOVE - will cause errors
    };
    PluginComponent.prototype.openDrawer = function (model) {
        this.selectedProblemId = _.get(model, 'resourceId', '');
        this.display = true;
        this.detailsLoading = false;
        this.detailsLoaded = false;
        this.requestPrimaryData();
    };
    PluginComponent.prototype.updatePrimaryModel = function (model) {
        this.primaryModel = model;
        this.primaryTable.setModel(model);
        var row = this.primaryTable.findRowById(this.selectedProblemId);
        if (_.isEmpty(row)) {
            this.primaryTable.selectRow(0);
        }
        else {
            this.primaryTable.selectRow(row.rowIndex);
        }
    };
    PluginComponent.prototype.updateSecondaryModel = function (model) {
        this.detailsLoading = false;
        this.detailsLoaded = true;
        this.riskFactors.setModel(_.get(model, 'data.riskFactors', []));
        this.setAdherenceButtons(model);
        this.updateGutCheckNECTrend(_.get(model, 'data.lineChartModel', null));
    };
    __decorate([
        core_1.ViewChild('drawerPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('drawerPrimaryTable'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "primaryTable", void 0);
    __decorate([
        core_1.ViewChild('riskFactors'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "riskFactors", void 0);
    __decorate([
        core_1.ViewChild('adherenceCriteria'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "adherenceCriteriaTable", void 0);
    __decorate([
        core_1.ViewChild('accordion'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "accordion", void 0);
    __decorate([
        core_1.ViewChild('gutCheckNEC'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "gutCheckNECLineGraph", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./css/plugin.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}());
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        animations_1.BrowserAnimationsModule,
        button_1.ButtonModule,
        sidebar_1.SidebarModule,
        cs_plugin_common_1.TableModule,
        ng2_split_pane_1.SplitPaneModule,
        accordion_1.AccordionModule,
        cs_plugin_common_1.ChartsModule
    ],
    declarations: [
        PluginComponent
    ],
    entryComponents: [
        PluginComponent
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map