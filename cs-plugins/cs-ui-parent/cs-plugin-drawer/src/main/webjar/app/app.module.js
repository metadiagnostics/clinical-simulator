"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var platform_browser_1 = require("@angular/platform-browser");
var button_1 = require("primeng/components/button/button");
var sidebar_1 = require("primeng/components/sidebar/sidebar");
var accordion_1 = require("primeng/components/accordion/accordion");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var cs_plugin_common_1 = require("cs-plugin-common");
var plugin_component_1 = require("./plugin.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                animations_1.BrowserAnimationsModule,
                button_1.ButtonModule,
                sidebar_1.SidebarModule,
                cs_plugin_common_1.TableModule,
                ng2_split_pane_1.SplitPaneModule,
                accordion_1.AccordionModule,
                cs_plugin_common_1.ChartsModule
            ],
            declarations: [
                plugin_component_1.PluginComponent
            ],
            entryComponents: [
                plugin_component_1.PluginComponent
            ],
            providers: [
                cs_plugin_common_1.ibackendProvider
            ],
            bootstrap: [plugin_component_1.PluginComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map