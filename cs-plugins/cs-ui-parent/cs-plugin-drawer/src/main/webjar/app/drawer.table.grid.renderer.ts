/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as _ from 'lodash';
import { Grid } from 'cs-plugin-common';

export class DrawerPrimaryTableGrid extends Grid {
    constructor() {
        super();
    }

    initialize(gridOptions: any, elementId: string) {
        super.initialize(gridOptions, elementId);

        gridOptions.enableSorting = false;
        gridOptions.suppressColumnVirtualisation = true;
        gridOptions.rowSelection = 'single';
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.enableColResize = true;
        gridOptions.emptyHeight = '100px';
    }


    protected configureColumns(columns) : Array<any> {
        let index = 0;
        _.forEach(columns, function(column) {
            column.editable = false;
            if (index > 0) {
                column.editable = false;
                column.headerClass = 'small-padding-left';
                column.cellClass = 'small-padding-left';
            }
            index ++;
        });

        columns[0].width = 4;
        columns[1].width = 175;
        columns[2].width = 175;
        columns[3].width = 25;

        return columns;
    }

    protected configureRows(rows) {
        return rows;
    }
}
