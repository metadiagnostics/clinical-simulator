"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var cs_plugin_common_1 = require("cs-plugin-common");
var DrawerPrimaryTableGrid = /** @class */ (function (_super) {
    __extends(DrawerPrimaryTableGrid, _super);
    function DrawerPrimaryTableGrid() {
        return _super.call(this) || this;
    }
    DrawerPrimaryTableGrid.prototype.initialize = function (gridOptions, elementId) {
        _super.prototype.initialize.call(this, gridOptions, elementId);
        gridOptions.enableSorting = false;
        gridOptions.suppressColumnVirtualisation = true;
        gridOptions.rowSelection = 'single';
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.enableColResize = true;
        gridOptions.emptyHeight = '100px';
    };
    DrawerPrimaryTableGrid.prototype.configureColumns = function (columns) {
        var index = 0;
        _.forEach(columns, function (column) {
            column.editable = false;
            if (index > 0) {
                column.editable = false;
                column.headerClass = 'small-padding-left';
                column.cellClass = 'small-padding-left';
            }
            index++;
        });
        columns[0].width = 4;
        columns[1].width = 175;
        columns[2].width = 175;
        columns[3].width = 25;
        return columns;
    };
    DrawerPrimaryTableGrid.prototype.configureRows = function (rows) {
        return rows;
    };
    return DrawerPrimaryTableGrid;
}(cs_plugin_common_1.Grid));
exports.DrawerPrimaryTableGrid = DrawerPrimaryTableGrid;
//# sourceMappingURL=drawer.table.grid.renderer.js.map