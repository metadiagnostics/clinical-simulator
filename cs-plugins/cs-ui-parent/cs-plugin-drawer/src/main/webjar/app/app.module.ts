import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from 'primeng/components/button/button';
import { SidebarModule } from 'primeng/components/sidebar/sidebar';
import { AccordionModule } from 'primeng/components/accordion/accordion';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import { TableModule, ibackendProvider, ChartsModule} from 'cs-plugin-common';
import { PluginComponent } from './plugin.component';

@NgModule({
    imports: [
        BrowserModule
        , HttpModule
        , BrowserAnimationsModule
        , ButtonModule
        , SidebarModule
        , TableModule
        , SplitPaneModule
        , AccordionModule
        , ChartsModule
    ],
    declarations: [
        PluginComponent
    ],
    entryComponents: [
        PluginComponent
    ],
    providers: [
        ibackendProvider
    ],
    bootstrap : [ PluginComponent ]
})
export class AppModule{}