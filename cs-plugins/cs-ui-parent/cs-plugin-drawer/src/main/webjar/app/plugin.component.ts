import {OnInit, AfterViewInit, Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {ButtonModule} from 'primeng/components/button/button';
import {AccordionModule} from 'primeng/components/accordion/accordion';
import {SidebarModule} from 'primeng/components/sidebar/sidebar';
import {SplitPaneModule} from 'ng2-split-pane/lib/ng2-split-pane';

import * as $ from 'jquery';
import * as _ from 'lodash';
import * as moment from 'moment';

import {DrawerTableModelProcessor} from './drawer.table.model.processor';
import {DrawerPrimaryTableGrid} from './drawer.table.grid.renderer';
import {
    Guid, Utilities, TableModule,
    ChartsModule, ibackendProvider, IBackendService,
    TableGrid, LineChartElement
} from 'cs-plugin-common';


@Component({
   moduleId: module.id,
   selector: 'plugin',
   templateUrl: 'plugin.component.html',
   styleUrls: ['./css/plugin.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class PluginComponent implements OnInit, AfterViewInit {
    @ViewChild('drawerPlugin')
    private root;

    @ViewChild('drawerPrimaryTable')
    private primaryTable;

    @ViewChild('riskFactors')
    private riskFactors;

    @ViewChild('adherenceCriteria')
    private adherenceCriteriaTable;

    @ViewChild('accordion')
    private accordion;

    @ViewChild('gutCheckNEC')
    private gutCheckNECLineGraph;

    private display: boolean = false;
    private accordionIndex: number = -1;
    private primaryModel: any = null;
    private detailsHeader: any = {};
    private adherenceButtons: any = [];
    private selectedCriteriaButtonId: any = null;
    private selectedProblemId: string;
    private detailsLoading = false;
    private detailsLoaded = false;

    constructor(private backendService: IBackendService) {}

    ngOnInit() {
        this.accordionIndex = 0;
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializePrimaryTable();
        this.initializeSecondaryTable();
        this.initializeAdherenceCriteriaTable();
    }

    ngAfterViewInit(): void {
        // Cannot remove the default close button by configuration so this removes it from the DOM after rendering the view
        $('.ui-sidebar-close').remove();
    }

    private requestPrimaryData() {
        this.primaryTable.setModel({data: []});
        this.primaryTable.showLoadingIndicator();
        let params: any = {
            _uuid: this.primaryTable.getUUID()
            , endpoint: 'drawer_p'
            , category: 'primary'
            , _callback: 'updatePrimaryModel'
        };

        this.backendService.sendBackendEvent("retrieveData", params);
    }

    private requestSecondaryData(code: string, codeSystem: string) {
        this.detailsLoading = true;
        let params: any = {
            _uuid: this.riskFactors.getUUID()
            , endpoint: 'drawer_s'
            , code: code
            , codeSystem: codeSystem
            , _callback: 'updateSecondaryModel'
        };

        this.backendService.sendBackendEvent("retrieveDetailData", params);
    }

    private initializePrimaryTable(): void {
        let gridOptions = {
            onGridReady: () => {
              if (Utilities.isRunningStandalone()) {
                  this.requestPrimaryData();
              }
            },
            onSelectionChanged: ($event) => {
                let selectedRows = $event.api.getSelectedRows();

                if (_.isArray(selectedRows) && selectedRows.length > 0) {
                    let x = selectedRows[0];
                    this.detailsHeader = {
                        colorClass: 'header-color cds-' + x.bar,
                        text: x.problem
                    };
                    this.requestSecondaryData(x.code, x.codeSystem);
                }
            },
            getRowNodeId: (data) => {
                return data.id;
            },
            onCellClicked: (event) => {
                if (event.column.colId === 'ack' &&
                    !_.get(event, 'event.target.disabled') &&
                    _.get(event, 'event.target.checked') === true) {
                    event.event.target.disabled = true;
                    let data = {
                        resourceId: _.get(event, 'data.id')
                    };
                    this.backendService.sendBackendEvent('updateAcknowledgement', data);
                }
            }
        };

        let renderer = new DrawerPrimaryTableGrid();
        let modelProcessor = new DrawerTableModelProcessor([ {
            "field": "icon",
            "headerName": " ",
            "renderer": "bar"
        }, {
            "field": "problem",
            "headerName": "Problem"
        }, {
            "field": "flagSetDateTime",
            "headerName": "Flag Set"
        }, {
            "field": "ack",
            "headerName": "Ack",
            "renderer": "checkbox"
        }]);

        this.primaryTable.initialize(modelProcessor, renderer, gridOptions);
    }

    private initializeSecondaryTable(): void {
        let gridOptions = {
            emptyHeight: '100px'
        };

        let renderer = new TableGrid(true);
        let modelProcessor = new DrawerTableModelProcessor([{
            "field": "factorName",
            "headerName": "Risk Factor"
        }, {
            "field": "contribution",
            "headerName": "Relative Contribution"
        }]);

        this.riskFactors.initialize(modelProcessor, renderer, gridOptions);
    }

    private initializeAdherenceCriteriaTable(): void {
        let gridOptions = {};

        let renderer = new TableGrid(true);
        let modelProcessor = new DrawerTableModelProcessor([ {
            "field": "dateTime",
            "headerName": "Date/Time"
        }, {
            "field": "name",
            "headerName": "Adherence Criteria"
        }, {
            "field": "assessment",
            "headerName": "Assessment"
        }]);

        this.adherenceCriteriaTable.initialize(modelProcessor, renderer, gridOptions);
    }

    private setAdherenceButtons(model) {
        let buttons = _.get(model, 'data.processAdherence', []);

        _.each(buttons, (button) => {
            button.id = Guid.newGuid();
            button.class = 'status-' + button.status;
        });

        this.adherenceButtons = buttons;
    }

    private onButtonClicked(model: any) {
        this.adherenceCriteriaTable.setModel(model.criteria);

        _.each(this.adherenceButtons, (btn) => {
           if (btn.id !== model.id) {
               btn.selected = '';
           }
        });
        model.selected = 'selected';
        this.selectedCriteriaButtonId = model.id;
    }

    private getDataToPlot(values: Array<any>) : Array<LineChartElement> {
        let retVal : Array<LineChartElement> = [];
        _.forEach(values, value => {
            let x = moment(value.x, 'YYYYMMDDHHmm').format('MM-DD-YYYY HH:mm');
            retVal.push(new LineChartElement(x, value.y));
        });

        return retVal;
    }

    private updateGutCheckNECTrend(model) {

        if (_.isNull(model) || _.isEmpty(model.data) || _.isEmpty(model.data.title)) {
            console.error('invalid plot data');
            return;
        }

        let data: Array<LineChartElement>  = this.getDataToPlot(model.data.data);

        this.gutCheckNECLineGraph.draw(model.data.title, data);
    }



    // start of fujion callbacks...
    public activatePlugin() {
        // DO NOT REMOVE - will cause errors
    }

    public openDrawer(model) {
        this.selectedProblemId = _.get(model, 'resourceId', '');
        this.display = true;
        this.detailsLoading = false;
        this.detailsLoaded = false;
        this.requestPrimaryData();
    }

    public updatePrimaryModel(model: any) {
        this.primaryModel = model;
        this.primaryTable.setModel(model);

        let row = this.primaryTable.findRowById(this.selectedProblemId);

        if (_.isEmpty(row)) {
            this.primaryTable.selectRow(0);
        } else {
            this.primaryTable.selectRow(row.rowIndex);
        }
    }

    public updateSecondaryModel(model: any) {
        this.detailsLoading = false;
        this.detailsLoaded = true;
        this.riskFactors.setModel(_.get(model, 'data.riskFactors', []));
        this.setAdherenceButtons(model);
        this.updateGutCheckNECTrend(_.get(model, 'data.lineChartModel', null));
    }

    // end of fujion callbacks/////
}

let ngModule = {
    imports: [
        BrowserModule
        , HttpModule
        , BrowserAnimationsModule
        , ButtonModule
        , SidebarModule
        , TableModule
        , SplitPaneModule
        , AccordionModule
        , ChartsModule
    ],
    declarations: [
        PluginComponent
    ],
    entryComponents: [
        PluginComponent
    ],
    providers: [
        ibackendProvider
    ]
};
export {PluginComponent as AngularComponent, ngModule};
