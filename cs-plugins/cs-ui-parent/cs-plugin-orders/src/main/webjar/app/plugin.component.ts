/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, ViewChild} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PluginSettings } from './plugin.settings';
import { MasterDetailDelegateCalllback, CWFPlugin, MasterDetailModule } from "cs-plugin-common";

@Component({
   moduleId   : module.id,
   selector   : 'plugin',
   templateUrl: 'plugin.component.html'
})
export class PluginComponent extends MasterDetailDelegateCalllback implements CWFPlugin{
  
    public static readonly PLUGIN_ID = "ORDERS";
  
    @ViewChild('ordersPlugin')
    private root;
    
    @ViewChild('md')
    private md;
    
    private masterDetailConfig: any;
    
    constructor(){
      super()
    }
    
    ngOnInit() {
      this.masterDetailConfig = PluginSettings.MASTER_DETAIL_CONFIG;
      this.masterDetailConfig.root = this.root;  
        
      this.setMasterDetailComponent(this.md);
    }
    
    getMasterDetailConfig(){
      return this.masterDetailConfig;
    }
    
    public getPluginId():string {
      return PluginComponent.PLUGIN_ID;
    }
    
}

let ngModule = {
    imports: [
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        MasterDetailModule
    ],
    declarations: [
      PluginComponent
    ]
};
export {PluginComponent as AngularComponent, ngModule};

