/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.model.NarrativeModel;
import com.cognitivemedicine.common.service.NarrativeService;
import com.cognitivemedicine.common.service.OrderReportService;
import java.util.Date;
import java.util.Map;

import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import org.fujion.annotation.EventHandler;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

public class OrdersPluginController extends PatientRecordPluginController {

    @Autowired
    private OrderReportService orderReportService;

    @Autowired
    private NarrativeService narrativeService;

    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.ORDER);
    
    @EventHandler(value = "retrieveData", target = "ordersPlugin")
    private void onRetrieveData$ordersPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        this.retrieveData(params);
    }

    @EventHandler(value = "retrieveNarrative", target = "ordersPlugin")
    private void onRetrieveNarrative$ordersPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        NarrativeModel narrative = this.narrativeService.retrieveNarrative((String)params.get("resourceId"), (String)params.get("resourceType"));
        this.ngInvoke(params, narrative);
    }
    
    private void retrieveData(Map<String, Object> params){
        Date startDate = this.convertToDate((String) params.get("startTime"));
        Date endDate = this.convertToDate((String) params.get("endTime"));

        this.config.setDateRange(startDate, endDate);
        this.config.setLiveUpdateCallback((String) params.get("_liveUpdateCallback"));
        this.subscribeToUpdates();

        Map<String, Object> reportData = this.orderReportService.retrieveOrderData(PatientContext.getActivePatient(), startDate, endDate);
        this.setDataChanged(false);
        this.addMetadata(reportData, (String)params.get("_uuid"), null);
        this.ngInvoke(params, reportData);
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        Map<String, Object> data = this.orderReportService.retrieveOrderResourceById(resource.getIdElement().getResourceType(), resource.getIdElement().getIdPart());

        if (data != null && this.config.getLiveUpdateCallback() != null) {
            this.addMetadata(data, null, null);
            this.ngInvoke(this.config.getLiveUpdateCallback(), data);
        }
    }
}
