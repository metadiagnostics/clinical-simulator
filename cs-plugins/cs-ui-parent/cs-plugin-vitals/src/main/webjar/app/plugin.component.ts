/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, ViewChild} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import {
    ChartsModule, DateSelectionModule,
    FlowsheetModule, SmartFormModule,
    ibackendProvider, IBackendService,
    VitalConstants, VitalsGrid,
    VitalsModelProcessor, LineChartElement,
    FlowsheetPluginHelper, Utilities,
    CreateResourceService
} from 'cs-plugin-common';

import { TabViewModule } from 'primeng/components/tabview/tabview';
import {VitalsFlowsheetPluginHelper} from "./vitals.flowsheet.plugin.helper";


import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
       moduleId: module.id,
       selector: 'plugin',
       templateUrl: 'plugin.component.html',
       styleUrls: ['./plugin.component.css']
})
export class PluginComponent extends VitalsFlowsheetPluginHelper{
    @ViewChild('cwfVitalsPlugin')
    private root;

    @ViewChild('vitals_flowsheet')
    private flowsheet;

    private selectedDate: Date;

    @ViewChild('dateSelection')
    private dateSelectionComponent;

    @ViewChild('linechart')
    private linechartComponent;

    @ViewChild('nec_smart_form')
    private necSmartForm;

    private grid: VitalsGrid = null;
    private modelProcessor : VitalsModelProcessor = null;
    private NECCellId: string = '';
    private formType: string = 'gutchecknec';
    private isNEC : boolean;
    private assessmentTabSelected: boolean = false;
    private activeIndex: number = 0;
    private selectedRow = null;
    private visibleColumns = [];

    constructor(private backendService: IBackendService) {
        super();
        this.resetDate();
    }

    public static getFormattedDateParam(date: Date, daysToAdd: number) : string {
        return moment(date).add(daysToAdd, 'day').format('YYYYMMDDHHmmss');
    }

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeFlowsheet();

        this.dateSelectionComponent.dateChanged$.subscribe(date => {
            if (date != null) {
                if (this.linechartComponent) {
                    this.linechartComponent.clear();
                }
                this.selectedDate = date;
                this.RequestVitalsData();
            }
        });

        this.flowsheet.getGrid().selectedRowChanged$.subscribe(row => {
            if (!_.isNull(row)) {
                this.selectedRow = row;
                this.updateLineChart(this.selectedRow, this.visibleColumns);
            }
        });

        this.flowsheet.getGrid().visibleColumnsChanged$.subscribe(columns => {
            if (! _.isEmpty(columns)) {
                this.visibleColumns = columns;
                this.updateLineChart(this.selectedRow, this.visibleColumns);
            }
        });
    }

    protected initializeFlowsheet() : void {
        this.modelProcessor = new VitalsModelProcessor();
        this.grid = new VitalsGrid();
        let gridOptions = {
            onGridReady: () => {
                if (Utilities.isRunningStandalone()) {
                    this.RequestVitalsData();
                }
            }, onCellClicked: ($event) => {
                this.processSelectedCell($event);
            }
        };

        this.flowsheet.setType(VitalConstants.TYPE);
        this.flowsheet.initialize(this.modelProcessor, this.grid, gridOptions, 30, true);
    }

    protected getModelQueryParams() : any {
        let retVal: any = {
            type: VitalConstants.TYPE
            , startTime: VitalsFlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, -4)
            , endTime: VitalsFlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, 4)
            , formType: this.formType
            , formVersion: "1.0"
            , uuid: this.flowsheet.getUUID()
        };

        return retVal;
    }

    private resetDate() {
        this.selectedDate = moment().toDate();
    }

    private mergeModels (modelToAdd: any) {
        this.flowsheet.updatedModel(modelToAdd);
    }

    private RequestVitalsData() : void {
        this.flowsheet.setModel({data: []});
        this.flowsheet.showLoadingIndicator();
        let params = this.getModelQueryParams();
        _.assign(params, {endpoint: 'vitals'});
        _.assign(params, {_callback: 'updateModel'});
        _.assign(params, {_liveUpdateCallback: 'liveUpdateModel'});

        this.backendService.sendBackendEvent("retrieveData", params);
    }

    private updateLineChart(row: any, columns: Array<any>) : void {
        if (_.isEmpty(row) || _.isEmpty(row.originalName)) {
            return;
        }

        let title = row.originalName;
        let data: Array<LineChartElement>  = VitalsFlowsheetPluginHelper.getDataToPlot(row, columns);
        this.linechartComponent.draw(title, data);
    }

    private processSelectedCell(event) {
        let necRowSelected = _.get(event, 'data.originalName', '') === 'GutCheck NEC';

        if (necRowSelected) {
            // Column property and timestamp are equivalent
            let column = event.colDef.field;
            let response = _.find(event.data.values, { timestamp: column});

            if (!_.isEmpty(response)) {
                this.displayAssessmentTab(true);
                this.NECCellId = response.id;
                this.requestNECDetailFormDefinition();
            } else {
                this.displayAssessmentTab(false);
            }
        } else {
            this.displayAssessmentTab(false);
        }
    }

    private onTabChange(index) {
        this.activeIndex = index;
    }

    private displayAssessmentTab(visible: boolean) {
        this.isNEC = visible;
        this.assessmentTabSelected = visible;
        this.activeIndex = visible ? 1 : 0;
    }

    private requestNECDetailFormDefinition() : void {
        let params: any = {
            uuid: this.flowsheet.getUUID()
            , formType: this.formType
            , formVersion: "1.0"
            , endpoint: 'vitals_nec_details_form_definition'
            , _callback: 'updateNECDetailFormDefinition'
        };

        this.backendService.sendBackendEvent("retrieveNECDetailFormDefinition", params);
    }

    private requestNECDetailFormResponses() : void {
        let params: any = {
            uuid: this.flowsheet.getUUID()
            , formResponseId: this.NECCellId
            , endpoint: 'vitals_nec_details_form_response'
            , _callback: 'updateNECDetailFormResponse'
        };

        this.backendService.sendBackendEvent("retrieveNECDetailFormResponse", params);
    }


    // start of fujion callbacks...
    public updateModel(model: any) {
        this.flowsheet.setModel(model);
    }

    public activatePlugin(response) {
        if (_.get(response, 'payload.dataChanged')) {
            this.flowsheet.setModel({data: []});
            this.resetDate();
            if (this.linechartComponent) {
                this.linechartComponent.clear();
            }
            this.RequestVitalsData();
        }
    }

    public liveUpdateModel(model : any) {
        this.mergeModels(model);
    }

    public updateNECDetailFormDefinition(response: any) {
        this.necSmartForm.smartForm = response.payload;
        this.requestNECDetailFormResponses();
    }

    updateNECDetailFormResponse(response: any){
        this.necSmartForm.responses = response.payload;
    }

    // end of fujion callbacks/////
}

let ngModule = {
    imports: [
        BrowserModule
        , HttpModule
        , FormsModule
        , BrowserAnimationsModule
        , SplitPaneModule
        , DateSelectionModule
        , FlowsheetModule
        , ChartsModule
        , TabViewModule
        , SmartFormModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        ibackendProvider,
        CreateResourceService
    ]
};
export {PluginComponent as AngularComponent, ngModule};

