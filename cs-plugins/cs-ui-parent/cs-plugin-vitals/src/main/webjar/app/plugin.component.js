"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var animations_1 = require("@angular/platform-browser/animations");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var cs_plugin_common_1 = require("cs-plugin-common");
var tabview_1 = require("primeng/components/tabview/tabview");
var vitals_flowsheet_plugin_helper_1 = require("./vitals.flowsheet.plugin.helper");
var moment = require("moment");
var _ = require("lodash");
var PluginComponent = /** @class */ (function (_super) {
    __extends(PluginComponent, _super);
    function PluginComponent(backendService) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        _this.grid = null;
        _this.modelProcessor = null;
        _this.NECCellId = '';
        _this.formType = 'gutchecknec';
        _this.assessmentTabSelected = false;
        _this.activeIndex = 0;
        _this.selectedRow = null;
        _this.visibleColumns = [];
        _this.resetDate();
        return _this;
    }
    PluginComponent.getFormattedDateParam = function (date, daysToAdd) {
        return moment(date).add(daysToAdd, 'day').format('YYYYMMDDHHmmss');
    };
    PluginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeFlowsheet();
        this.dateSelectionComponent.dateChanged$.subscribe(function (date) {
            if (date != null) {
                if (_this.linechartComponent) {
                    _this.linechartComponent.clear();
                }
                _this.selectedDate = date;
                _this.RequestVitalsData();
            }
        });
        this.flowsheet.getGrid().selectedRowChanged$.subscribe(function (row) {
            if (!_.isNull(row)) {
                _this.selectedRow = row;
                _this.updateLineChart(_this.selectedRow, _this.visibleColumns);
            }
        });
        this.flowsheet.getGrid().visibleColumnsChanged$.subscribe(function (columns) {
            if (!_.isEmpty(columns)) {
                _this.visibleColumns = columns;
                _this.updateLineChart(_this.selectedRow, _this.visibleColumns);
            }
        });
    };
    PluginComponent.prototype.initializeFlowsheet = function () {
        var _this = this;
        this.modelProcessor = new cs_plugin_common_1.VitalsModelProcessor();
        this.grid = new cs_plugin_common_1.VitalsGrid();
        var gridOptions = {
            onGridReady: function () {
                if (cs_plugin_common_1.Utilities.isRunningStandalone()) {
                    _this.RequestVitalsData();
                }
            }, onCellClicked: function ($event) {
                _this.processSelectedCell($event);
            }
        };
        this.flowsheet.setType(cs_plugin_common_1.VitalConstants.TYPE);
        this.flowsheet.initialize(this.modelProcessor, this.grid, gridOptions, 30, true);
    };
    PluginComponent.prototype.getModelQueryParams = function () {
        var retVal = {
            type: cs_plugin_common_1.VitalConstants.TYPE,
            startTime: vitals_flowsheet_plugin_helper_1.VitalsFlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, -4),
            endTime: vitals_flowsheet_plugin_helper_1.VitalsFlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, 4),
            formType: this.formType,
            formVersion: "1.0",
            uuid: this.flowsheet.getUUID()
        };
        return retVal;
    };
    PluginComponent.prototype.resetDate = function () {
        this.selectedDate = moment().toDate();
    };
    PluginComponent.prototype.mergeModels = function (modelToAdd) {
        this.flowsheet.updatedModel(modelToAdd);
    };
    PluginComponent.prototype.RequestVitalsData = function () {
        this.flowsheet.setModel({ data: [] });
        this.flowsheet.showLoadingIndicator();
        var params = this.getModelQueryParams();
        _.assign(params, { endpoint: 'vitals' });
        _.assign(params, { _callback: 'updateModel' });
        _.assign(params, { _liveUpdateCallback: 'liveUpdateModel' });
        this.backendService.sendBackendEvent("retrieveData", params);
    };
    PluginComponent.prototype.updateLineChart = function (row, columns) {
        if (_.isEmpty(row) || _.isEmpty(row.originalName)) {
            return;
        }
        var title = row.originalName;
        var data = vitals_flowsheet_plugin_helper_1.VitalsFlowsheetPluginHelper.getDataToPlot(row, columns);
        this.linechartComponent.draw(title, data);
    };
    PluginComponent.prototype.processSelectedCell = function (event) {
        var necRowSelected = _.get(event, 'data.originalName', '') === 'GutCheck NEC';
        if (necRowSelected) {
            // Column property and timestamp are equivalent
            var column = event.colDef.field;
            var response = _.find(event.data.values, { timestamp: column });
            if (!_.isEmpty(response)) {
                this.displayAssessmentTab(true);
                this.NECCellId = response.id;
                this.requestNECDetailFormDefinition();
            }
            else {
                this.displayAssessmentTab(false);
            }
        }
        else {
            this.displayAssessmentTab(false);
        }
    };
    PluginComponent.prototype.onTabChange = function (index) {
        this.activeIndex = index;
    };
    PluginComponent.prototype.displayAssessmentTab = function (visible) {
        this.isNEC = visible;
        this.assessmentTabSelected = visible;
        this.activeIndex = visible ? 1 : 0;
    };
    PluginComponent.prototype.requestNECDetailFormDefinition = function () {
        var params = {
            uuid: this.flowsheet.getUUID(),
            formType: this.formType,
            formVersion: "1.0",
            endpoint: 'vitals_nec_details_form_definition',
            _callback: 'updateNECDetailFormDefinition'
        };
        this.backendService.sendBackendEvent("retrieveNECDetailFormDefinition", params);
    };
    PluginComponent.prototype.requestNECDetailFormResponses = function () {
        var params = {
            uuid: this.flowsheet.getUUID(),
            formResponseId: this.NECCellId,
            endpoint: 'vitals_nec_details_form_response',
            _callback: 'updateNECDetailFormResponse'
        };
        this.backendService.sendBackendEvent("retrieveNECDetailFormResponse", params);
    };
    // start of fujion callbacks...
    PluginComponent.prototype.updateModel = function (model) {
        this.flowsheet.setModel(model);
    };
    PluginComponent.prototype.activatePlugin = function (response) {
        if (_.get(response, 'payload.dataChanged')) {
            this.flowsheet.setModel({ data: [] });
            this.resetDate();
            if (this.linechartComponent) {
                this.linechartComponent.clear();
            }
            this.RequestVitalsData();
        }
    };
    PluginComponent.prototype.liveUpdateModel = function (model) {
        this.mergeModels(model);
    };
    PluginComponent.prototype.updateNECDetailFormDefinition = function (response) {
        this.necSmartForm.smartForm = response.payload;
        this.requestNECDetailFormResponses();
    };
    PluginComponent.prototype.updateNECDetailFormResponse = function (response) {
        this.necSmartForm.responses = response.payload;
    };
    __decorate([
        core_1.ViewChild('cwfVitalsPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('vitals_flowsheet'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "flowsheet", void 0);
    __decorate([
        core_1.ViewChild('dateSelection'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "dateSelectionComponent", void 0);
    __decorate([
        core_1.ViewChild('linechart'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "linechartComponent", void 0);
    __decorate([
        core_1.ViewChild('nec_smart_form'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "necSmartForm", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./plugin.component.css']
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}(vitals_flowsheet_plugin_helper_1.VitalsFlowsheetPluginHelper));
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        forms_1.FormsModule,
        animations_1.BrowserAnimationsModule,
        ng2_split_pane_1.SplitPaneModule,
        cs_plugin_common_1.DateSelectionModule,
        cs_plugin_common_1.FlowsheetModule,
        cs_plugin_common_1.ChartsModule,
        tabview_1.TabViewModule,
        cs_plugin_common_1.SmartFormModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider,
        cs_plugin_common_1.CreateResourceService
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map