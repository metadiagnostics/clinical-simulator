/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.plugin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.ClinicalNoteService;
import com.cognitivemedicine.common.service.FlowsheetService;
import com.cognitivemedicine.common.flowsheet.FlowsheetType;

import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import com.cognitivemedicine.cs.models.FormDefinition;
import com.cognitivemedicine.cs.models.FormResponse;
import org.carewebframework.ui.dialog.DialogUtil;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

import org.hl7.fhir.dstu3.model.Patient;

/**
 * Simple component to display the current date and time.
 */
public class VitalsPluginController extends PatientRecordPluginController {

    @WiredComponent
    private AngularComponent vitalsPlugin;

    @Autowired
    private FlowsheetService flowsheetService;

    @Autowired
    private ClinicalNoteService clinicalNoteService;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.VITAL);

    @EventHandler(value = "retrieveData", target = "vitalsPlugin")
    private void onRetrieveData$VitalsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        Patient patient = PatientContext.getActivePatient();
        String formType = (String) params.get("formType");
        String formVersion = (String) params.get("formVersion");

        FlowsheetType type = FlowsheetType.valueOf((String) params.get("type"));
        Date startDate = this.convertDate((String) params.get("startTime"));
        Date endDate = this.convertDate((String) params.get("endTime"));
        this.config.setDateRange(startDate, endDate);
        this.config.setLiveUpdateCallback((String) params.get("_liveUpdateCallback"));
        this.subscribeToUpdates();

        Map<String, Object> flowsheetData = this.flowsheetService.getFlowsheetDataMap(type, patient, startDate, endDate, formType, formVersion);
        this.setDataChanged(false);

        this.addMetadata(flowsheetData, (String)params.get("uuid"), null);
        this.invoke("updateModel", flowsheetData);
    }

    @EventHandler(value = "retrieveNECDetailFormDefinition", target = "vitalsPlugin")
    private void onretrieveNECDetailFormDefinition$vitalsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        String formType = (String) params.get("formType");
        String formVersion = (String) params.get("formVersion");

        FormDefinition dto = this.clinicalNoteService.retrieveClinicalNoteDefinition(formType, formVersion);
        this.ngInvoke(params, dto);
    }

    @EventHandler(value = "retrieveNECDetailFormResponse", target = "vitalsPlugin")
    private void onretrieveNECDetailFormResponse$vitalsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String formResponseId = (String) params.get("formResponseId");
        if (formResponseId == null) {
            DialogUtil.showError("The id of nec detail Response is required");
            return;
        }

        FormResponse dto = this.clinicalNoteService.retrieveClinicalNoteResponse(formResponseId);
        this.ngInvoke(params, dto);
    }



    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        Map<String, Object> data = this.flowsheetService.retrieveFlowsheetResourceById(resource, this.config);

        if (data != null && this.config.getLiveUpdateCallback() != null) {
            this.addMetadata(data, null, null);
            this.invoke(this.config.getLiveUpdateCallback(), data);
        }
    }

    private void invoke(String functionName, Object... args) {
        if (this.vitalsPlugin != null) {
            this.vitalsPlugin.ngInvoke(functionName, args);
        }
    }

    private Date convertDate(String date) {
        Date jDate = null;
        try {
            jDate = this.dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jDate;
    }
}
