(function (global) {
    System.config({
            defaultJSExtensions: true,
            map: {
                'app': 'src/main/webjar/app',
                // angular bundles
                '@angular/animations': 'node_modules/@angular/animations/bundles/animations.umd.js',
                '@angular/core': 'node_modules/@angular/core/bundles/core.umd.js',
                '@angular/common': 'node_modules/@angular/common/bundles/common.umd.js',
                '@angular/compiler': 'node_modules/@angular/compiler/bundles/compiler.umd.js',
                '@angular/platform-browser': 'node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
                '@angular/platform-browser-dynamic': 'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
                '@angular/http': 'node_modules/@angular/http/bundles/http.umd.js',
                '@angular/router': 'node_modules/@angular/router/bundles/router.umd.js',
                '@angular/forms': 'node_modules/@angular/forms/bundles/forms.umd.js',
                '@angular/platform-browser/animations': 'node_modules/@angular/platform-browser/bundles/platform-browser-animations.umd.js',
                '@angular/animations/browser': 'node_modules/@angular/animations/bundles/animations-browser.umd.js',
                // other libraries
                'rxjs': 'node_modules/rxjs',
                'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
                // ag libraries
                'ag-grid-angular': 'node_modules/ag-grid-angular',
                'ag-grid': 'node_modules/ag-grid',
                'lodash': 'node_modules/lodash',
                'moment': 'node_modules/moment/moment.js',
                "primeng": "node_modules/primeng",
                'ng2-split-pane': 'node_modules/ng2-split-pane',
                'angular2-chartjs': 'node_modules/angular2-chartjs',
                'chart.js': 'node_modules/chart.js/dist/Chart.bundle.js',
                'jquery': 'node_modules/jquery/dist/jquery.js',
                'reflect-metadata': 'node_modules/reflect-metadata/Reflect.js',
                'mathjs': 'node_modules/mathjs/dist/math.js',
                'base64-js': 'node_modules/base64-js/index.js',
                'cs-plugin-common': 'node_modules/cs-plugin-common',
                'fujion-core': 'node_modules/jquery/dist/jquery.js' // This is a dummy import so the app can run locally
            },
            packages: {
                app: {
                    main: 'main.js'
                },
                'ag-grid': {
                    main: 'main.js'
                },
                'ag-grid-angular': {
                    main: 'main.js'
                },
                'rxjs': {
                    main: 'RxJs.js'
                },
                lodash: {
                    main: 'index.js'
                },
                'primeng': {
                    main: 'primeng.js'
                },
                'ng2-split-pane': {
                    main: 'ng2-split-pane.js'
                },
                'angular2-chartjs': {
                    main: './dist/index.js',
                    defaultExtension: 'js'
                },
                'cs-plugin-common': {
                    main: 'index.js',
                    defaultExtension: 'js'
                }
            }
        }
    );
})(this);

