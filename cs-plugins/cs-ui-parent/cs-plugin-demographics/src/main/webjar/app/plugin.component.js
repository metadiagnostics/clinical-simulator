"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var button_1 = require("primeng/components/button/button");
var _ = require("lodash");
var cs_plugin_common_1 = require("cs-plugin-common");
var PluginComponent = /** @class */ (function () {
    function PluginComponent(backendService) {
        this.backendService = backendService;
        this.patientSelected = false;
        this.model = {};
    }
    PluginComponent.prototype.ngOnInit = function () {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        if (!cs_plugin_common_1.Utilities.isRunningStandalone()) {
            this.backendService.sendBackendEvent('setPatientSelectedCallback', { _callback: 'updateSelectedPatient' });
        }
    };
    // fujion callbacks
    PluginComponent.prototype.updateSelectedPatient = function (model) {
        if (_.isEmpty(model.data)) {
            this.model = {};
            this.patientSelected = false;
        }
        else {
            this.model = model.data;
            this.patientSelected = true;
        }
    };
    PluginComponent.prototype.closeChart = function () {
        this.patientSelected = false;
        this.backendService.sendBackendEvent('closePatientChart', {});
    };
    PluginComponent.prototype.activatePlugin = function () { };
    __decorate([
        core_1.ViewChild('demographicsPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./css/plugin.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}());
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        animations_1.BrowserAnimationsModule,
        forms_1.FormsModule,
        button_1.ButtonModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map