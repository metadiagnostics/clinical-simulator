import {OnInit, Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {ButtonModule} from 'primeng/components/button/button';

import * as $ from 'jquery';
import * as _ from 'lodash';

import {ibackendProvider, IBackendService, Utilities} from 'cs-plugin-common';

@Component({
   moduleId: module.id,
   selector: 'plugin',
   templateUrl: 'plugin.component.html',
   styleUrls: ['./css/plugin.component.css'],
   encapsulation: ViewEncapsulation.None
})
export class PluginComponent implements OnInit {
    @ViewChild('demographicsPlugin')
    private root;

    private patientSelected: boolean = false;
    private model = {};

    constructor(private backendService: IBackendService) {}

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);

        if (!Utilities.isRunningStandalone()) {
            this.backendService.sendBackendEvent('setPatientSelectedCallback', { _callback: 'updateSelectedPatient'});
        }
    }

    // fujion callbacks
    private updateSelectedPatient(model: any) {
        if (_.isEmpty(model.data)) {
            this.model = {};
            this.patientSelected = false;
        } else {
            this.model = model.data;
            this.patientSelected = true;
        }
    }

    private closeChart() {
        this.patientSelected = false;
        this.backendService.sendBackendEvent('closePatientChart', {});
    }

    private activatePlugin() {}
}

let ngModule = {
    imports: [
        BrowserModule
        , HttpModule
        , BrowserAnimationsModule
        , FormsModule
        , ButtonModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        ibackendProvider
    ]
};
export {PluginComponent as AngularComponent, ngModule};
