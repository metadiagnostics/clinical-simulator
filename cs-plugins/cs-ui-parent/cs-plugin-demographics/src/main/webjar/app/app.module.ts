import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import { ButtonModule } from 'primeng/components/button/button';
import { PluginComponent } from './plugin.component';
import { ibackendProvider } from 'cs-plugin-common';

@NgModule({
    imports: [
        BrowserModule
        , HttpModule
        , BrowserAnimationsModule
        , FormsModule
        , ButtonModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        ibackendProvider
    ],
    bootstrap : [ PluginComponent ]
})
export class AppModule{}