/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.patient.PatientDemographics;
import com.cognitivemedicine.common.service.PatientDemographicsService;
import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import com.cognitivemedicine.common.util.Utilities;
import org.carewebframework.api.event.IGenericEvent;
import org.carewebframework.shell.elements.ElementPlugin;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.client.ExecutionContext;
import org.fujion.event.Event;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;


public class DemographicsPluginController extends PatientRecordPluginController {

    @WiredComponent
    private AngularComponent demographicsPlugin;

    @Autowired
    private PatientDemographicsService patientDemographicsService;

    private String _callback;
    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.DEMOGRAPHICS);

    private final IGenericEvent<?> patientChangeListener = (eventName, eventData) -> {
        Patient patient = PatientContext.getActivePatient();
        this.retrieveAndPushDemographics(patient, true);
    };

    private void retrieveAndPushDemographics(Patient patient, boolean subscribe) {
        if (_callback != null) {
            PatientDemographics demographics = null;

            if (patient != null) {
                demographics = this.patientDemographicsService.retrievePatientDemographics(patient);

                if (subscribe) {
                    this.subscribeToUpdates();
                }
            } else {
                this.unsubscribeFromUpdates();
            }

            Map<String, Object> data = new HashMap<>();
            data.put("data", demographics);
            this.demographicsPlugin.ngInvoke(_callback, data);
        }
    }

    @Override
    public void onLoad(ElementPlugin plugin) {
        Map<String, String> additionalParams = new HashMap<>();
        additionalParams.put("code", this.patientDemographicsService.getDemographicsCodeParamValue());
        config.setAdditionalParams(additionalParams);

        getEventManager().subscribe(Utilities.PATIENT_CONTEXT_EVENT, this.patientChangeListener);
    }

    @Override
    public void onUnload() {
        getEventManager().unsubscribe(Utilities.PATIENT_CONTEXT_EVENT, this.patientChangeListener);
    }

    @EventHandler( value = "setPatientSelectedCallback", target = "demographicsPlugin")
    private void setPatientSelectedCallback$demographicsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>)event.getData();

        if (params != null) {
            this._callback = (String) params.get("_callback");
        }
    }

    @EventHandler( value = "closePatientChart", target = "demographicsPlugin")
    private void closePatientChart$demographicsPlugin(Event event) {
        String pageId = ExecutionContext.getPage().getId();
        ExecutionContext.invoke(pageId, () -> {
            PatientContext.changePatient((Patient) null);
        });

        this.unsubscribeFromUpdates();
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        Patient patient = null;
        if (resource instanceof Patient) {
            patient = (Patient) resource;
        } else if (resource instanceof Observation && ((Observation) resource).hasSubject()) {
            String subjectReference = ((Observation) resource).getSubject().getReference();
            if (subjectReference != null) {
                String patientId;
                String[] referenceSplit = subjectReference.split("/");

                if (referenceSplit.length > 1) {
                    patientId = referenceSplit[1];
                } else {
                    patientId = referenceSplit[0];
                }

                patient = this.patientDemographicsService.findPatientById(patientId);
            }
        }

        if (patient != null && _callback != null) {
            this.retrieveAndPushDemographics(patient, false);
            Map<String, Object> data = new HashMap<>();
            data.put("data", this.patientDemographicsService.retrievePatientDemographics(patient));
            this.demographicsPlugin.ngInvoke(_callback, data);
        }
    }
}
