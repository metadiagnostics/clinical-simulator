"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var animations_1 = require("@angular/platform-browser/animations");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var main_1 = require("ag-grid-angular/main");
var cs_plugin_common_1 = require("cs-plugin-common");
var moment = require("moment");
var _ = require("lodash");
var medications_detail_component_1 = require("./medications-detail.component");
var PluginComponent = /** @class */ (function (_super) {
    __extends(PluginComponent, _super);
    function PluginComponent(backendService) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        _this.medicationsGrid = null;
        _this.medicationModelProcessor = null;
        _this.resetDate();
        return _this;
    }
    PluginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeFlowsheet();
        this.dateSelectionComponent.dateChanged$.subscribe(function (date) {
            if (date != null) {
                _this.selectedDate = date;
                _this.RequestMedsData();
            }
        });
        this.medicationsFlowsheet.getGrid().selectedRowChanged$.subscribe(function (row) {
            if (!_.isNull(row)) {
                console.log(row);
                _this.retrieveMedicationDetails(_this.medicationModelProcessor.getResourceID(row));
            }
        });
    };
    PluginComponent.prototype.initializeFlowsheet = function () {
        var _this = this;
        this.medicationModelProcessor = new cs_plugin_common_1.MedicationModelProcessor();
        this.medicationsGrid = new cs_plugin_common_1.MedicationsGrid();
        var gridOptions = {
            onGridReady: function () {
                if (cs_plugin_common_1.Utilities.isRunningStandalone()) {
                    _this.RequestMedsData();
                }
            }
        };
        this.medicationsFlowsheet.setType(cs_plugin_common_1.MedicationConstants.HEADER_TITLE);
        this.medicationsFlowsheet.initialize(this.medicationModelProcessor, this.medicationsGrid, gridOptions, 30, true);
    };
    PluginComponent.prototype.getModelQueryParams = function () {
        var retVal = {
            type: cs_plugin_common_1.MedicationConstants.TYPE,
            startTime: cs_plugin_common_1.FlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, -4),
            endTime: cs_plugin_common_1.FlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, 4),
            uuid: this.medicationsFlowsheet.getUUID()
        };
        return retVal;
    };
    PluginComponent.prototype.resetDate = function () {
        this.selectedDate = moment().toDate();
    };
    PluginComponent.prototype.mergeModels = function (modelToAdd) {
        this.medicationsFlowsheet.updatedModel(modelToAdd);
    };
    PluginComponent.prototype.RequestMedsData = function () {
        this.medicationsFlowsheet.setModel({ data: [] });
        this.medicationsFlowsheet.showLoadingIndicator();
        console.time('MAR data request');
        var params = this.getModelQueryParams();
        _.assign(params, { endpoint: 'medications' });
        _.assign(params, { _callback: 'updateModel' });
        _.assign(params, { _liveUpdateCallback: 'liveUpdateModel' });
        this.backendService.sendBackendEvent("retrieveData", params);
    };
    PluginComponent.prototype.retrieveMedicationDetails = function (resourceID) {
        if (_.isEmpty(resourceID)) {
            console.error('missing resourceID');
            return;
        }
        var params = {
            type: cs_plugin_common_1.MedicationConstants.TYPE,
            resourceID: resourceID,
            _uuid: 'unused',
            endpoint: 'medication_detail',
            _callback: "updateMedicationDetails"
        };
        var event = "requestMedicationDetails";
        this.backendService.sendBackendEvent(event, params);
    };
    // start of fujion callbacks...
    PluginComponent.prototype.updateModel = function (model) {
        console.timeEnd('MAR data request');
        this.medicationsFlowsheet.setModel(model);
        // TODO finalize when the server requirements are complete.
        // let firstResourceID = this.medicationModelProcessor.getResourceID();
        this.retrieveMedicationDetails('fake');
    };
    PluginComponent.prototype.activatePlugin = function (response) {
        if (_.get(response, 'payload.dataChanged')) {
            this.medicationsFlowsheet.setModel({ data: [] });
            this.resetDate();
            this.RequestMedsData();
        }
    };
    PluginComponent.prototype.liveUpdateModel = function (model) {
        this.mergeModels(model);
    };
    PluginComponent.prototype.updateMedicationDetails = function (model) {
        //this.detailsComponent.setModel(model.payload);
    };
    __decorate([
        core_1.ViewChild('cwfMedicationsPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('Medications_flowsheet'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "medicationsFlowsheet", void 0);
    __decorate([
        core_1.ViewChild('dateSelection'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "dateSelectionComponent", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./plugin.component.css']
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}(cs_plugin_common_1.FlowsheetPluginHelper));
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        forms_1.FormsModule,
        animations_1.BrowserAnimationsModule,
        cs_plugin_common_1.DateSelectionModule,
        ng2_split_pane_1.SplitPaneModule,
        cs_plugin_common_1.FlowsheetModule,
        cs_plugin_common_1.ChartsModule,
        main_1.AgGridModule.withComponents([])
    ],
    declarations: [
        PluginComponent,
        medications_detail_component_1.MedicationsDetailComponent
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map