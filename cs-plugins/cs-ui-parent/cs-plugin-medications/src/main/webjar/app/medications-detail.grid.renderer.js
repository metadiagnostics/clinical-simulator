"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var cs_plugin_common_1 = require("cs-plugin-common");
var MedicationsDetailGridRenderer = /** @class */ (function (_super) {
    __extends(MedicationsDetailGridRenderer, _super);
    function MedicationsDetailGridRenderer() {
        return _super.call(this) || this;
    }
    MedicationsDetailGridRenderer.prototype.initialize = function (gridOptions, elementId) {
        _super.prototype.initialize.call(this, gridOptions, elementId);
        gridOptions.enableSorting = false;
        gridOptions.rowSelection = 'none';
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressLoadingOverlay = false;
    };
    MedicationsDetailGridRenderer.prototype.configureColumns = function (columns) {
        _.forEach(columns, function (column) {
            column.editable = false;
            column.width = 100;
            column.headerClass = 'small-padding-left';
            column.cellClass = 'small-padding-left';
        });
        return columns;
    };
    MedicationsDetailGridRenderer.prototype.configureRows = function (rows) {
        return rows;
    };
    return MedicationsDetailGridRenderer;
}(cs_plugin_common_1.Grid));
exports.MedicationsDetailGridRenderer = MedicationsDetailGridRenderer;
//# sourceMappingURL=medications-detail.grid.renderer.js.map