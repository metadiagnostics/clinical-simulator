/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, ViewChild} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import { AgGridModule } from "ag-grid-angular/main";
import {
    ChartsModule, DateSelectionModule,
    ibackendProvider, IBackendService,
    MedicationConstants, MedicationsGrid,
    MedicationModelProcessor, FlowsheetPluginHelper,
    Utilities, FlowsheetModule
} from 'cs-plugin-common';
import * as moment from 'moment';

import * as _ from 'lodash';

import { MedicationsDetailComponent } from './medications-detail.component';

@Component({
       moduleId: module.id,
       selector: 'plugin',
       templateUrl: 'plugin.component.html',
       styleUrls: ['./plugin.component.css']
})
export class PluginComponent extends FlowsheetPluginHelper {
    @ViewChild('cwfMedicationsPlugin')
    private root;

    @ViewChild('Medications_flowsheet')
    private medicationsFlowsheet;

    // @ViewChild('Medications_details')
    // private detailsComponent;

    private selectedDate: Date;

    @ViewChild('dateSelection')
    private dateSelectionComponent;

    private medicationsGrid: MedicationsGrid = null;
    private medicationModelProcessor : MedicationModelProcessor = null;

    constructor(private backendService: IBackendService) {
        super();
        this.resetDate();
    }

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeFlowsheet();

        this.dateSelectionComponent.dateChanged$.subscribe(date => {
            if (date != null) {
                this.selectedDate = date;
                this.RequestMedsData();
            }
        });

        this.medicationsFlowsheet.getGrid().selectedRowChanged$.subscribe(row => {
            if (!_.isNull(row)) {
                console.log(row);

                this.retrieveMedicationDetails(this.medicationModelProcessor.getResourceID(row));
            }
        });
    }

    protected initializeFlowsheet() : void {
        this.medicationModelProcessor = new MedicationModelProcessor();
        this.medicationsGrid = new MedicationsGrid();
        let gridOptions = {
            onGridReady: () => {
                if (Utilities.isRunningStandalone()) {
                    this.RequestMedsData();
                }
            }
        };

        this.medicationsFlowsheet.setType(MedicationConstants.HEADER_TITLE);
        this.medicationsFlowsheet.initialize(this.medicationModelProcessor, this.medicationsGrid, gridOptions, 30, true);
    }

    protected getModelQueryParams() : any {
        let retVal: any = {
            type: MedicationConstants.TYPE
            , startTime: FlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, -4)
            , endTime: FlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, 4)
            , uuid: this.medicationsFlowsheet.getUUID()
        };

        return retVal;
    }

    private resetDate() {
        this.selectedDate = moment().toDate();
    }

    private mergeModels (modelToAdd: any) {
        this.medicationsFlowsheet.updatedModel(modelToAdd);
    }

    private RequestMedsData() : void {
        this.medicationsFlowsheet.setModel({data: []});
        this.medicationsFlowsheet.showLoadingIndicator();
        console.time('MAR data request');
        let params = this.getModelQueryParams();
        _.assign(params, {endpoint: 'medications'});
        _.assign(params, {_callback: 'updateModel'});
        _.assign(params, {_liveUpdateCallback: 'liveUpdateModel'});

        this.backendService.sendBackendEvent("retrieveData", params);
    }

    private retrieveMedicationDetails(resourceID: string){
        if (_.isEmpty(resourceID)) {
            console.error('missing resourceID');
            return;
        }

        let params: any = {
            type: MedicationConstants.TYPE,
            resourceID: resourceID,
            _uuid: 'unused',
            endpoint: 'medication_detail',
            _callback: "updateMedicationDetails"
        };

        let event: string = "requestMedicationDetails";
        this.backendService.sendBackendEvent(event, params);
    }

    // start of fujion callbacks...

    public updateModel(model: any) {
        console.timeEnd('MAR data request');
        this.medicationsFlowsheet.setModel(model);
        // TODO finalize when the server requirements are complete.
        // let firstResourceID = this.medicationModelProcessor.getResourceID();
        this.retrieveMedicationDetails('fake');
    }

    public activatePlugin(response) {
        if (_.get(response, 'payload.dataChanged')) {
            this.medicationsFlowsheet.setModel({data: []});
            this.resetDate();
            this.RequestMedsData();
        }
    }

    public liveUpdateModel(model : any) {
        this.mergeModels(model);
    }

    public updateMedicationDetails(model: any) {
        //this.detailsComponent.setModel(model.payload);
    }

    // end of fujion callbacks/////
}

let ngModule = {
    imports: [
        BrowserModule
        , HttpModule
        , FormsModule
        , BrowserAnimationsModule
        , DateSelectionModule
        , SplitPaneModule
        , FlowsheetModule
        , ChartsModule
        , AgGridModule.withComponents(
            [
            ])
    ],
    declarations: [
        PluginComponent
        , MedicationsDetailComponent
    ],
    providers: [
        ibackendProvider
    ]
};
export {PluginComponent as AngularComponent, ngModule};

