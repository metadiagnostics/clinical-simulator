"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var main_1 = require("ag-grid-angular/main");
var cs_plugin_common_1 = require("cs-plugin-common");
//components
var plugin_component_1 = require("./plugin.component");
var medications_detail_component_1 = require("./medications-detail.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                animations_1.BrowserAnimationsModule,
                ng2_split_pane_1.SplitPaneModule,
                cs_plugin_common_1.ChartsModule,
                cs_plugin_common_1.FlowsheetModule,
                cs_plugin_common_1.DateSelectionModule,
                main_1.AgGridModule.withComponents([])
            ],
            declarations: [
                plugin_component_1.PluginComponent,
                medications_detail_component_1.MedicationsDetailComponent
            ],
            providers: [
                cs_plugin_common_1.ibackendProvider
            ],
            bootstrap: [plugin_component_1.PluginComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map