"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var medications_detail_grid_renderer_1 = require("./medications-detail.grid.renderer");
var cs_plugin_common_1 = require("cs-plugin-common");
var columns = [
    {
        "field": "medicationDetails",
        "headerName": "Item"
    }, {
        "field": "value",
        "headerName": "Value"
    }
];
var MedicationsDetailComponent = /** @class */ (function () {
    function MedicationsDetailComponent() {
        this.gridOptions = {};
        this.modelProcessor = null;
        this.detailTable = null;
        this.uuid = null;
        this.uuid = cs_plugin_common_1.Guid.newGuid();
        this.modelProcessor = new cs_plugin_common_1.TableModelProcessor(columns);
        this.detailTable = new medications_detail_grid_renderer_1.MedicationsDetailGridRenderer();
        this.detailTable.initialize(this.gridOptions, this.uuid);
    }
    MedicationsDetailComponent.prototype.showLoadingIndicator = function () {
        this.gridOptions.api.showLoadingOverlay();
    };
    MedicationsDetailComponent.prototype.setModel = function (model) {
        this.modelProcessor.process(model);
        var rows = this.modelProcessor.getRows();
        if (!Array.isArray(rows)) {
            return;
        }
        var columns = this.modelProcessor.getColumns();
        if (!Array.isArray(columns) || columns.length === 0) {
            return;
        }
        this.detailTable.refreshGrid(this.gridOptions, rows, columns);
        this.gridOptions.api.sizeColumnsToFit();
    };
    MedicationsDetailComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'medication-detail',
            styleUrls: ['./medications-detail.component.css'],
            templateUrl: './medications-detail.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [])
    ], MedicationsDetailComponent);
    return MedicationsDetailComponent;
}());
exports.MedicationsDetailComponent = MedicationsDetailComponent;
//# sourceMappingURL=medications-detail.component.js.map