/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewEncapsulation } from '@angular/core';
import { GridOptions } from 'ag-grid/main';
import { MedicationsDetailGridRenderer } from './medications-detail.grid.renderer';
import { Guid, TableModelProcessor } from 'cs-plugin-common';

const columns : Array<any> = [
    {
        "field": "medicationDetails",
        "headerName": "Item"
    }, {
        "field": "value",
        "headerName": "Value"
    }];

@Component({
               moduleId: module.id,
               selector: 'medication-detail',
               styleUrls: ['./medications-detail.component.css'],
               templateUrl: './medications-detail.component.html',
               encapsulation: ViewEncapsulation.None
           })

export class MedicationsDetailComponent {
    private gridOptions: GridOptions = {};
    private modelProcessor: TableModelProcessor = null;
    private detailTable: MedicationsDetailGridRenderer = null;
    private uuid : any = null;


    constructor() {
        this.uuid = Guid.newGuid();
        this.modelProcessor = new TableModelProcessor(columns);
        this.detailTable = new MedicationsDetailGridRenderer();
        this.detailTable.initialize(this.gridOptions, this.uuid);
    }

    public showLoadingIndicator() {
        this.gridOptions.api.showLoadingOverlay();
    }

    public setModel(model) {
        this.modelProcessor.process(model);

        let rows = this.modelProcessor.getRows();
        if (!Array.isArray(rows)) {
            return;
        }

        let columns = this.modelProcessor.getColumns();
        if (!Array.isArray(columns) || columns.length === 0) {
            return;
        }

        this.detailTable.refreshGrid(this.gridOptions, rows, columns);
        this.gridOptions.api.sizeColumnsToFit();
    }
}

