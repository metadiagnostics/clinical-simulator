/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as _ from 'lodash';
import {Grid} from 'cs-plugin-common';

export class MedicationsDetailGridRenderer extends Grid {
    constructor() {
        super();
    }

    initialize(gridOptions: any, elementId: string) {
        super.initialize(gridOptions, elementId);

        gridOptions.enableSorting = false;
        gridOptions.rowSelection = 'none';
        gridOptions.suppressMovableColumns = true;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressLoadingOverlay = false;
    }


    protected configureColumns(columns): Array<any> {
        _.forEach(columns, function(column) {
            column.editable = false;
            column.width = 100;
            column.headerClass = 'small-padding-left';
            column.cellClass = 'small-padding-left';

        });

        return columns;
    }

    protected configureRows(rows) {
        return rows;
    }
}
