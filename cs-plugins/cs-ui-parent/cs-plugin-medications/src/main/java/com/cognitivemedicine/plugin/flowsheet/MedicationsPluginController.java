/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.plugin.flowsheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.flowsheet.FlowSheetConstants;
import com.cognitivemedicine.common.model.*;
import com.cognitivemedicine.common.service.FlowsheetService;
import com.cognitivemedicine.common.flowsheet.FlowsheetType;

import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import org.carewebframework.ui.dialog.DialogUtil;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

import org.hl7.fhir.dstu3.model.Patient;

/**
 * Simple component to display the current date and time.
 */
public class MedicationsPluginController extends PatientRecordPluginController {

    @WiredComponent
    private AngularComponent medicationsPlugin;

    @Autowired
    private FlowsheetService flowsheetService;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.MEDICATION);

    @EventHandler(value = "retrieveData", target = "medicationsPlugin")
    private void onRetrieveData$medsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        Patient patient = PatientContext.getActivePatient();

        FlowsheetType type = FlowsheetType.valueOf((String) params.get("type"));
        Date startDate = this.convertDate((String) params.get("startTime"));
        Date endDate = this.convertDate((String) params.get("endTime"));
        this.config.setDateRange(startDate, endDate);
        this.config.setLiveUpdateCallback((String) params.get("_liveUpdateCallback"));
        this.subscribeToUpdates();

        Map<String, Object> flowsheetData = this.flowsheetService.getFlowsheetDataMap(type, patient, startDate, endDate, null, null);
        this.setDataChanged(false);

        this.addMetadata(flowsheetData, (String)params.get("uuid"), null);
        this.invoke("updateModel", flowsheetData);
    }

    @EventHandler(value = "requestMedicationDetails", target = "medicationsPlugin")
    private void onRequestMedicationDetailsData$medsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String resourceID = (String) params.get("resourceID");
        if (resourceID == null) {
            DialogUtil.showError("The resource ID is required for a medication.");
            return;
        }

        Map<String, Object> medicationDetails = new HashMap<>();

        GridModel medicationsDetailModel = new GridModel();
        GridRow drugNameRow = new GridRow("Medication Name");
        drugNameRow.addCell(new GridCell(FlowSheetConstants.VALUE, "Asprin"));
        medicationsDetailModel.addRow(drugNameRow);
        GridRow doseRow = new GridRow("Dose");
        doseRow.addCell(new GridCell(FlowSheetConstants.VALUE, "2 tablets"));
        medicationsDetailModel.addRow(doseRow);
        GridRow routeRow = new GridRow("Route");
        routeRow.addCell(new GridCell(FlowSheetConstants.VALUE, "by mouth"));
        medicationsDetailModel.addRow(routeRow);
        GridRow frequencyRow = new GridRow("Frequency");
        frequencyRow.addCell(new GridCell(FlowSheetConstants.VALUE, "8 hours"));
        medicationsDetailModel.addRow(frequencyRow);
        GridRow durationRow = new GridRow("Duration");
        durationRow.addCell(new GridCell(FlowSheetConstants.VALUE, "10 days"));
        medicationsDetailModel.addRow(durationRow);

        medicationDetails.put("data", medicationsDetailModel.convertToSerializeableModel());
        medicationDetails.put("type", FlowSheetConstants.MEDICATION_DETAILS);
        this.addMetadata(medicationDetails, (String)params.get("_uuid"), null);
        String callback = (String) params.get("_callback");

        this.ngInvoke(callback, medicationDetails);
    }
    public class MedicationDetailModel {
        private Map<String, Object> medicationDetails;

        public Map<String, Object> getMedicationDetails() {
            return medicationDetails;
        }

        public void setMedicationDetails(Map<String, Object> medicationDetails) {
            this.medicationDetails = medicationDetails;
        }
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        Map<String, Object> data = this.flowsheetService.retrieveFlowsheetResourceById(resource, this.config);

        if (data != null && this.config.getLiveUpdateCallback() != null) {
            this.addMetadata(data, null, null);
            this.invoke(this.config.getLiveUpdateCallback(), data);
        }
    }

    private void invoke(String functionName, Object... args) {
        if (this.medicationsPlugin != null) {
            this.medicationsPlugin.ngInvoke(functionName, args);
        }
    }

    private Date convertDate(String date) {
        Date jDate = null;
        try {
            jDate = this.dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jDate;
    }
}
