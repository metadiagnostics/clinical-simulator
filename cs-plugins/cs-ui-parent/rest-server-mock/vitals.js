var _ = require('lodash');

var vitalsMock = {
    'data': [
        {
            'name': 'Heart Rate',
            'type': 0,
            'resourceID' : '32',
            'values': [
                {
                    'value': '8',
                    'timestamp': '20171107130100'
                },
                {
                    'value': '9',
                    'timestamp': '20171108010100'
                }
            ],
        }, {
            'name': 'Blood Pressure',
            'resourceID' : '34',
            'type': 0,
            'values': [
                {
                    'value': '2',
                    'timestamp': '20171108030100'
                },
                {
                    'value': '3',
                    'timestamp': '20171108060100'
                },
                {
                    'value': '4',
                    'timestamp': '20171108080100'
                }
            ]
        }, {
            'name': 'GutCheck NEC',
            'resourceID' : '33',
            'type': 1,
            'values': [
                {
                    'value': '12',
                    'timestamp': '20171107140100',
                    'id': 'mongodb-1234'
                },
                {
                    'value': '23',
                    'timestamp': '20171107150100',
                    'id': 'mongodb-4321'
                }
            ]
        }
    ]
};

    function processPrimary(req, res) {
    console.log('the post payload: ', req.body);
    _.assign(vitalsMock, {'noType': req.body.noType});
    _.assign(vitalsMock, {'uuid': req.body.uuid});

    return res.status(200).send(JSON.stringify(vitalsMock));
}

module.exports.processPrimary = processPrimary;
