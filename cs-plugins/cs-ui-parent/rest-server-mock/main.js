var fs = require('fs');
var path = require('path');
var http = require('http');
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var io = require('./i-o');
var scenarioSteps = require('./scenario-steps');
var medications = require('./medications');
var vitals = require('./vitals');
var drawer = require('./drawer');
var _ = require('lodash');


var port = process.env.PORT || '4000';
var RESOURCES_DIR = "resources";

var app = express();
app.set('port', port);

var server = http.createServer(app);
console.log('serving on port: ', port);
server.listen(port);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/**
 * Tells the requester that CORS is supported.
 */
app.options('/*', function(req, res) {
    console.log("OPT");
    return res.status(200).send("{}");
});

app.get('/', function (req, res) {
    res.status(200).send('success');
});

app.post('/:context/:event', function(req, res) {

    var context = req.params.context;
    var event = req.params.event;
    console.log("POST for : "+context+"/"+event);

    var filePath = path.join(path.join(path.join(__dirname, RESOURCES_DIR), context), event+".json");
    console.log("File to be served: "+filePath);

    if (fs.existsSync(filePath)){
        var content = fs.readFileSync(filePath, {encoding: 'utf-8'});

        if (req.body._uuid){
            var contentObj = JSON.parse(content);
            _.assign(contentObj, {'_uuid': req.body._uuid});
            content = JSON.stringify(contentObj);
        }

        return res.status(200).send(content);
    } else {
        return res.status(404).send(JSON.stringify({"error": "The file "+filePath+" doesn't exist"}));
    }
});

app.post('/i_o', function(req, res) {
    io.process(req, res);
});

app.post('/scenario_steps', function(req, res) {
    scenarioSteps.process(req, res);
});


app.post('/medications', function(req, res) {
    medications.processPrimary(req, res);
});

app.post('/medication_detail', function(req, res) {
    medications.processDetail(req, res);
});

app.post('/vitals', function(req, res) {
    vitals.processPrimary(req, res);
});

app.post('/vitals_nec_details_form_definition', function(req, res) {
    var filePath = path.join(path.join(path.join(__dirname, RESOURCES_DIR), "SMART_FORM/requestFormDefinition.json"));

    console.log("File to be served: "+filePath);

    if (fs.existsSync(filePath)){
        var content = fs.readFileSync(filePath, {encoding: 'utf-8'});

        if (req.body._uuid){
            var contentObj = JSON.parse(content);
            _.assign(contentObj, {'_uuid': req.body._uuid});
            content = JSON.stringify(contentObj);
        }

        return res.status(200).send(content);

    };
});

app.post('/vitals_nec_details_form_response', function(req, res) {
    var filePath = path.join(path.join(path.join(__dirname, RESOURCES_DIR), "SMART_FORM/requestFormResponses.json"));

    console.log("File to be served: "+filePath);

    if (fs.existsSync(filePath)){
        var content = fs.readFileSync(filePath, {encoding: 'utf-8'});

        if (req.body._uuid){
            var contentObj = JSON.parse(content);
            _.assign(contentObj, {'_uuid': req.body._uuid});
            content = JSON.stringify(contentObj);
        }

        return res.status(200).send(content);

    };
});

app.post('/drawer_p' , function(req, res) {
    drawer.processP(req, res);
});

app.post('/drawer_s' , function(req, res) {
    drawer.processS(req, res);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);

});

