var _ = require('lodash');

var i_o_mock = {
    'data': [
        {
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180115180000'
                }
            ]
        }, {
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180113000600'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180110060700'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180121145100'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180106180700'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180108120200'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180118000000'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180124205200'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180105210600'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180108210700'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180107205200'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180117120100'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180120150500'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180117055900'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180110085400'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180113180000'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180105090100'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180114060400'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180125025500'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180115085300'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '28',
                    'timestamp': '20180117025200'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '107',
                    'timestamp': '20180104205500'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '108',
                    'timestamp': '20180114210700'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '109',
                    'timestamp': '20180115205400'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '110',
                    'timestamp': '20180119205300'
                }
            ]
        },{
            'name': 'Maternal Breast Milk (active)',
            'type': 1,
            'values': [
                {
                    'info': '"INFORMATIONAL"',
                    'value': '111',
                    'timestamp': '20180113205900'
                }
            ]
        }
    ]
};

function process(req, res) {
    console.log('the post payload: ', req.body);
    _.assign(i_o_mock, {'type': req.body.type});
    _.assign(i_o_mock, {'uuid': req.body.uuid});

    return res.status(200).send(JSON.stringify(i_o_mock));
}

module.exports.process = process;