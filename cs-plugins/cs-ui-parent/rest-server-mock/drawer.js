var _ = require('lodash');

var primary = {
    "data":[
        {
            "NEC":[
                {
                    "type": "bar",
                    "value": "red"
                }, {
                    "type": "icon",
                    "value": ""
                }, {
                    "dataType": "date",
                    "type":"flagSetDateTime",
                    "value":"201711290800"
                }, {
                    "type":"ack",
                    "value":"0"
                }, {
                    "type": "id",
                    "value": "test123"
                }
            ]
        }
    ],
    "type":"problem"
};

var secondary = {
    "data": {
        "lineChartModel": {
            "data": {
                "title": "GutCheckNeck Trend",
                "data" : [
                    {"x" : "32",
                        "y" : "112"},
                    {"x" : "12",
                        "y" : "12"},

                ]
            }
        },
        "riskFactors": {
            "data": [
                {
                    "Steroid Exposure": [{
                        "type": "contribution",
                        "value": "High"
                    }]
                }, {
                    "Multiple Transfusions": [{
                        "type": "contribution",
                        "value": "Moderate"
                    }]
                }, {
                    "PDA": [{
                        "type": "contribution",
                        "value": "Minimal"
                    }]
                }
            ],
            "type": "factorName"
        },
        "processAdherence": [{
            "label": "Human Milk",
            "status": "ok",
            "criteria": {
                "data": [
                    {
                        "20171130": [{
                            "type": "name",
                            "value": "Being fed human milk"
                        }, {
                            "type": "assessment",
                            "value": "Compliant"
                        }]
                    }
                ],
                "type": "dateTime",
                "dataType": "date"
            }
        }, {
            "label": "Oral Care",
            "status": "caution",
            "criteria": {
                "data": [
                    {
                        "20171129": [{
                            "type": "name",
                            "value": "Clean teeth"
                        }, {
                            "type": "assessment",
                            "value": "Partially Compliant"
                        }]
                    }
                ],
                "type": "dateTime",
                "dataType": "date"
            }
        }, {
            "label": "Medication Stewardship",
            "status": "fail",
            "criteria": {
                "data": [
                    {
                        "20171030": [{
                            "type": "name",
                            "value": "H2-Blocker Prescribing"
                        }, {
                            "type": "assessment",
                            "value": "Non-Compliant"
                        }]
                    },
                    {
                        "20171030": [{
                            "type": "name",
                            "value": "Antibiotic Duration"
                        }, {
                            "type": "assessment",
                            "value": "Compliant"
                        }]
                    }
                ],
                "type": "dateTime",
                "dataType": "date"
            }
        }, {
            "label": "Feeding Protocol",
            "status": "ok",
            "criteria": {
                "data": [
                    {
                        "20171130": [{
                            "type": "name",
                            "value": "Feeding criteria 1"
                        }, {
                            "type": "assessment",
                            "value": "Compliant"
                        }]
                    },
                    {
                        "20171130": [{
                            "type": "name",
                            "value": "Feeding criteria 2"
                        }, {
                            "type": "assessment",
                            "value": "Compliant"
                        }]
                    }
                ],
                "type": "dateTime",
                "dataType": "date"
            }
        }]
    }
};


function processP(req, res) {
    console.log('the post payload: ', req.body);
    var retVal = primary;
//    _.assign(retVal, {'type': req.body.type});
    _.assign(retVal, {'uuid': req.body._uuid});



    return res.status(200).send(JSON.stringify(retVal));
}

function processS(req, res) {
    console.log('the post payload: ', req.body);
    var retVal = secondary;

    _.assign(retVal, {'uuid': req.body._uuid});



    return res.status(200).send(JSON.stringify(retVal));
}

module.exports.processS = processS;
module.exports.processP = processP;