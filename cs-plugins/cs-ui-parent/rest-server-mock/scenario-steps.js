var _ = require('lodash');

var mock = {
    'data': [
        {
            'name': 'Step Name',
            'index': '1',
            'time' : 'now'
        }, {
            'name': 'Step 2 name',
            'index': '2',
            'time' : 'now 2'
        }
    ]
};

function process(req, res) {
    console.log('the post payload: ', req.body);
    _.assign(mock, {'type': req.body.type});
    _.assign(mock, {'uuid': req.body.uuid});

    return res.status(200).send(JSON.stringify(mock));
}

module.exports.process = process;