var _ = require('lodash');

var medicationMock = {
    'data': [
        {
            'name': 'Some medication',
            'resourceID' : '32',
            'values': [{
                    'value': '32',
                    'timestamp': '20171107130100'
                }]
        }, {
            'name': 'Another  medication',
            'resourceID' : '12',
            'values': [{
                    'value': '12',
                    'timestamp': '20171107130100'
                }]
        }
    ]
};

var medicationDetailsMock = {
    "payload": {
        "type": "medicationDetails",
        "data": [
            {
                "Medication Name": [
                    {
                        "dataType": null,
                        "type": "value",
                        "value": "Asprin"
                    }
                ]
            }, {
                "Dose": [
                    {
                        "dataType": null,
                        "type": "value",
                        "value": "2 tablets"
                    }
                ]
            }, {
                "Route": [
                    {
                        "dataType": null,
                        "type": "value",
                        "value": "By mouth"
                    }
                ]
            }, {
                "Frequency": [
                    {
                        "dataType": null,
                        "type": "value",
                        "value": "8 hours"
                    }
                ]
            }, {
                "Duration": [
                    {
                        "dataType": null,
                        "type": "value",
                        "value": "10 days"
                    }
                ]
            }
        ]
    },

    "type": "medicationDetails"
}

function processPrimary(req, res) {
    console.log('the post payload: ', req.body);
    _.assign(medicationMock, {'uuid': req.body.uuid});

    return res.status(200).send(JSON.stringify(medicationMock));
}


function processDetail(req, res) {
    console.log('the post payload: ', req.body);
    _.assign(medicationDetailsMock, {'uuid': req.body.uuid});

    return res.status(200).send(JSON.stringify(medicationDetailsMock));
}

module.exports.processDetail = processDetail;
module.exports.processPrimary = processPrimary;
