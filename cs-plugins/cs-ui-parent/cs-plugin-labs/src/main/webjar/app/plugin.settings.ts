/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export class PluginSettings {

    public static readonly MASTER_DETAIL_CONFIG = {
        "root": "//REPLACE ME IN RUNTIME!",
        "type": "LABS",
        "pagination": {
            "strategy" : "server",
            "pageSize" : 30
        },
        "sections": {
            "header": {
                "title": "Labs",
                "dateSelection": true,
                "categories": "AU,BG,BLB,CG,CH,CP,GE,HM,IMM,LAB,MB,MCB,MYC,OSL,PAR,PAT,SP,SR,TX,URN,VR",
                "filterButtons": [{
                    "label": "All"
                },
                    {
                        "label": "Hematology",
                        "params": {
                            "categories": "HM"
                        }
                    },
                    {
                        "label": "Chemistry",
                        "params": {
                            "categories": "CH"
                        }
                    },
                    {
                        "label": "Microbiology",
                        "params": {
                            "categories": "MB"
                        }
                    },
                    {
                        "label": "Immunology/Serology",
                        "params": {
                            "categories": "IMM,SR"
                        }
                    },
                    {
                        "label": "Genetics",
                        "params": {
                            "categories": "GE"
                        }
                    },
                    {
                        "label": "Anatomic Path",
                        "params": {
                            "categories": "CH"
                        }
                    }
                ],
                "secondaryDropdownConfig": {
                    "label": "Status",
                    "options": [
                        {
                            "label": "All"
                        },
                        {
                            "label": "Pending",
                            "params": {
                                "status": "pending"
                            }
                        }
                    ]
                },
                "gridOptions": {
                    "enableColResize": true
                },
                "columns": [{
                    "field": "collectionDateTime",
                    "headerName": "Collection Date/Time"
                }, {
                    "field": "study",
                    "headerName": "Study"
                }, {
                    "field": "status",
                    "headerName": "Status"
                }, {
                    "field": "performer",
                    "headerName": "Performer"
                }, {
                    "field": "orderingProvider",
                    "headerName": "Ordering Provider"
                }, {
                    "field": "facility",
                    "headerName": "Facility"
                }]
            },
            "details": {
                "tabs": [{
                    "label": "Details",
                    "resourceType": "LABS",
                    "type": "DETAILS_TABLE",
                    "conditionalHeader": {
                        "field": "panel",
                        "headerName": "Panel"
                    },
                    "columns": [{
                        "field": "resultDateTime",
                        "headerName": "Result Date/Time"
                    }, {
                        "field": "lastUpdated",
                        "headerName": "Last Updated"
                    }, {
                        "field": "component",
                        "headerName": "Component"
                    }, {
                        "field": "resultValue",
                        "headerName": "Result Value"
                    }, {
                        "field": "units",
                        "headerName": "Units"
                    }, {
                        "field": "referenceRange",
                        "headerName": "Reference Range"
                    }, {
                        "field": "interpretation",
                        "headerName": "Interpretation"
                    }]
                }
                ]
            }
        }
    }
}
  