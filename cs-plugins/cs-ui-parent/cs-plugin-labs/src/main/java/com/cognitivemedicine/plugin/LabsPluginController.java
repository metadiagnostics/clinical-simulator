/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.DiagnosticReportService;
import java.util.Date;
import java.util.Map;

import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import org.fujion.annotation.EventHandler;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Simple component to display the current date and time.
 */
public class LabsPluginController extends PatientRecordPluginController {

    @Autowired
    private DiagnosticReportService diagnosticReportService;

    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.DIAGNOSTIC_REPORT);

    @EventHandler(value = "retrieveData", target = "labsPlugin")
    private void onRetrieveData$labsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        this.retrieveData(params);
    }
    
    @EventHandler(value = "retrieveDetailData", target = "labsPlugin")
    private void onRetrieveDetailData$labsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        this.retrieveData(params);
    }
    
    private void retrieveData(Map<String, Object> params){
        String reportId = (String) params.get("id");
        Map<String, Object> reportData;
        if (reportId == null) {
            Date startDate = this.convertToDate((String) params.get("startTime"));
            Date endDate = this.convertToDate((String) params.get("endTime"));
            String status = (String) params.get("status");
            String categories = (String) params.get("categories");
            Integer pageOffset= (Integer) params.get("pageOffset");
            Integer pageSize = (Integer) params.get("count");

            this.config.setDateRange(startDate, endDate);
            this.config.setLiveUpdateCallback((String) params.get("_liveUpdateCallback"));
            this.config.setAdditionalParams(this.diagnosticReportService.buildSubscriptionParams(status, categories));
            this.subscribeToUpdates();

            reportData = this.diagnosticReportService.retrieveDiagnosticReportData(PatientContext.getActivePatient(), startDate, endDate, status, categories, pageSize, pageOffset);
            this.setDataChanged(false);
        } else {
            reportData = this.diagnosticReportService.retrieveIndividualReport(reportId);
        }

        this.addMetadata(reportData, (String)params.get("_uuid"), null);
        this.ngInvoke(params, reportData);
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        String id = resource.getIdElement().getIdPart();
        Map<String, Object> data = this.diagnosticReportService.retrieveGridRowReportById(id);

        if (data != null) {
            this.addMetadata(data, null, null);
            this.ngInvoke(this.config.getLiveUpdateCallback(), data);
        }
    }
}
