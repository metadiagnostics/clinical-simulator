package com.cognitivemedicine.plugin;

import java.util.List;

public class RiskAnalysisPlot {

    private String title;
    private String lineColor;
    private boolean createAverageLine;
    private List<RiskAnalysisPoint> points;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    public boolean isCreateAverageLine() {
        return createAverageLine;
    }

    public void setCreateAverageLine(boolean createAverageLine) {
        this.createAverageLine = createAverageLine;
    }

    public List<RiskAnalysisPoint> getPoints() {
        return points;
    }

    public void setPoints(List<RiskAnalysisPoint> points) {
        this.points = points;
    }
}
