package com.cognitivemedicine.plugin;

import java.math.BigDecimal;

public class RiskAnalysisPoint {
    private String year;
    private BigDecimal percent;

    public RiskAnalysisPoint(String year, BigDecimal percent) {
        this.year = year;
        this.percent = percent;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }
}
