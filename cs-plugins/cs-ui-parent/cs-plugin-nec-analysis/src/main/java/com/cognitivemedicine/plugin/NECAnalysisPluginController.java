/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.ClinicalNoteService;
import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import org.fujion.annotation.EventHandler;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Simple component to display the current date and time.
 */
public class NECAnalysisPluginController extends PatientRecordPluginController {

    @Autowired
    private ClinicalNoteService clinicalNoteService;

    @EventHandler(value = "retrieveData", target = "necAnalysisPlugin")
    private void onRetrieveData$necAnalysisPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        String formType = (String) params.get("formType");
        String formVersion = (String) params.get("formVersion");

        Map<String, Object> results = new HashMap<>();

        Integer currentScore = null;

        try {
            currentScore = this.clinicalNoteService.getCurrentGutCheckNecScore(PatientContext.getActivePatient(), formType, formVersion);
        } catch(ParseException p) {
            // Just send back null
        }
        results.put("currentScore", currentScore);
        results.put("chartData", this.getRiskAnalysisData());

        this.ngInvoke(params, results);
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return null;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {}

    private List<RiskAnalysisPlot> getRiskAnalysisData() {
        List<RiskAnalysisPlot> plotList = new ArrayList<>();

        RiskAnalysisPlot lgmc = new RiskAnalysisPlot();
        lgmc.setCreateAverageLine(true);
        lgmc.setLineColor("#5E8BB3");
        lgmc.setTitle("LGMC NICU");

        List<RiskAnalysisPoint> lgmcPoints = new ArrayList<>();
        lgmcPoints.add(new RiskAnalysisPoint("2006", new BigDecimal(3.4)));
        lgmcPoints.add(new RiskAnalysisPoint("2007", new BigDecimal(2.1)));
        lgmcPoints.add(new RiskAnalysisPoint("2008", new BigDecimal(7.1)));
        lgmcPoints.add(new RiskAnalysisPoint("2009", new BigDecimal(2.6)));
        lgmcPoints.add(new RiskAnalysisPoint("2010", new BigDecimal(0.0)));
        lgmcPoints.add(new RiskAnalysisPoint("2011", new BigDecimal(0.0)));
        lgmcPoints.add(new RiskAnalysisPoint("2012", new BigDecimal(0.0)));
        lgmcPoints.add(new RiskAnalysisPoint("2013", new BigDecimal(0.0)));
        lgmcPoints.add(new RiskAnalysisPoint("2014", new BigDecimal(3.9)));
        lgmcPoints.add(new RiskAnalysisPoint("2015", new BigDecimal(2.0)));
        lgmcPoints.add(new RiskAnalysisPoint("2016", new BigDecimal(0.0)));
        lgmcPoints.add(new RiskAnalysisPoint("2017", new BigDecimal(0.0)));
        lgmc.setPoints(lgmcPoints);

        RiskAnalysisPlot von = new RiskAnalysisPlot();
        von.setLineColor("#C26967");
        von.setTitle("Vermont Oxford Network");

        List<RiskAnalysisPoint> vonPoints = new ArrayList<>();
        vonPoints.add(new RiskAnalysisPoint("2006", new BigDecimal(6.8)));
        vonPoints.add(new RiskAnalysisPoint("2007", new BigDecimal(7.4)));
        vonPoints.add(new RiskAnalysisPoint("2008", new BigDecimal(6.9)));
        vonPoints.add(new RiskAnalysisPoint("2009", new BigDecimal(5.9)));
        vonPoints.add(new RiskAnalysisPoint("2010", new BigDecimal(5.7)));
        vonPoints.add(new RiskAnalysisPoint("2011", new BigDecimal(5.2)));
        vonPoints.add(new RiskAnalysisPoint("2012", new BigDecimal(4.9)));
        vonPoints.add(new RiskAnalysisPoint("2013", new BigDecimal(4.6)));
        vonPoints.add(new RiskAnalysisPoint("2014", new BigDecimal(4.9)));
        vonPoints.add(new RiskAnalysisPoint("2015", new BigDecimal(4.7)));
        vonPoints.add(new RiskAnalysisPoint("2016", new BigDecimal(4.5)));
        vonPoints.add(new RiskAnalysisPoint("2017", null));
        von.setPoints(vonPoints);

        RiskAnalysisPlot aln = new RiskAnalysisPlot();
        aln.setLineColor("#AAC579");
        aln.setTitle("All Louisiana NICUs");

        List<RiskAnalysisPoint> alnPoints = new ArrayList<>();
        alnPoints.add(new RiskAnalysisPoint("2006", new BigDecimal(6.0)));
        alnPoints.add(new RiskAnalysisPoint("2007", new BigDecimal(5.8)));
        alnPoints.add(new RiskAnalysisPoint("2008", new BigDecimal(7.3)));
        alnPoints.add(new RiskAnalysisPoint("2009", new BigDecimal(5.0)));
        alnPoints.add(new RiskAnalysisPoint("2010", new BigDecimal(5.4)));
        alnPoints.add(new RiskAnalysisPoint("2011", new BigDecimal(4.9)));
        alnPoints.add(new RiskAnalysisPoint("2012", new BigDecimal(3.9)));
        alnPoints.add(new RiskAnalysisPoint("2013", new BigDecimal(4.4)));
        alnPoints.add(new RiskAnalysisPoint("2014", new BigDecimal(5.1)));
        alnPoints.add(new RiskAnalysisPoint("2015", new BigDecimal(4.9)));
        alnPoints.add(new RiskAnalysisPoint("2016", new BigDecimal(4.5)));
        alnPoints.add(new RiskAnalysisPoint("2017", null));
        aln.setPoints(alnPoints);

        plotList.add(lgmc);
        plotList.add(von);
        plotList.add(aln);

        return plotList;
    }
}
