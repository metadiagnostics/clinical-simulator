/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, ViewChild} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    Utilities, ChartsModule, LineChartElement, PlottableData, PlottableDataSetEntry,
    IBackendService, ibackendProvider
} from 'cs-plugin-common';

import * as _ from 'lodash';

@Component({
   moduleId   : module.id,
   selector   : 'plugin',
   templateUrl: 'plugin.component.html',
   styleUrls: ['./plugin.component.css']
})
export class PluginComponent {
  
    @ViewChild('naPlugin')
    private root;

    @ViewChild('line')
    private linechartComponent;

    private options;

    private currentScore: number;
    private currentScoreStyle: string;
    private imagePath: string;
    private chartData: any;
    private labels;

    constructor(private backendService: IBackendService) {
        this.options = this.getOptions();
    }

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);

        if (Utilities.isRunningStandalone()) {
            this.imagePath = 'src/main/webjar/app/images/nec-info-tree.png';
            this.requestData();
        } else {
            this.imagePath = '/cs-webapp/webjars/cwf-plugin-nec-analysis/app/images/nec-info-tree.png'
        }
    }

    public activatePlugin(response) : void {
        if (_.get(response, 'payload.dataChanged')) {
            this.requestData();
        }
    }

    private requestData() {
        let params = {
            _callback: 'updateModel',
            type: 'NEC_ANALYSIS',
            formType: 'gutchecknec',
            formVersion: '1.0'
        };

        this.backendService.sendBackendEvent("retrieveData", params);
    }

    private updateModel(model) {
        this.currentScore = _.get(model, 'payload.currentScore');
        this.currentScoreStyle = this.getPosition();
        this.drawLineChart(_.get(model, 'payload.chartData'));
        this.chartData = _.get(model, 'payload.chartData');
    }

    private getPosition() {
        if (_.isNumber(this.currentScore)) {
            let position = (100 / 60) * this.currentScore;
            let pixels = 59 - 15 + (this.currentScore / 2); // Half of div width - left margin + (current score / 2)

            // This doesn't line up for some reason, so special case
            if (this.currentScore === 60) {
                pixels = 38;
            }
            return 'calc(' + position + '% - ' + pixels + 'px)';
        }

        return null;
    }

    private drawLineChart(data: any) {
        let plottableData = {} as PlottableData;
        this.labels = [];
        let datasets = [];

        _.forEach(data, (facility) => {
            let dataSetEntry = {} as  PlottableDataSetEntry;
            dataSetEntry.label = facility.title;
            dataSetEntry.fill = false;
            dataSetEntry.borderColor = facility.lineColor;
            dataSetEntry.data = [];

            _.forEach(facility.points, (point) => {
                if (_.indexOf(this.labels, point.year) < 0) {
                    this.labels.push(point.year);
                }

                dataSetEntry.data.push(point.percent);
            });

            datasets.push(dataSetEntry);

            if (facility.createAverageLine) {
                let avgDataSetEntry = {} as  PlottableDataSetEntry;
                avgDataSetEntry.label = facility.title + ' Trend Line';
                avgDataSetEntry.fill = false;
                avgDataSetEntry.borderColor = 'black';
                avgDataSetEntry.borderDash = [5, 5];
                avgDataSetEntry.data = this.buildAverageDataPoints(dataSetEntry.data);
                datasets.push(avgDataSetEntry);
            }
        });

        plottableData.labels = this.labels;
        plottableData.datasets = datasets;

        this.linechartComponent.drawMultipleDataSets(plottableData, this.getOptions());
    }

    private getOptions() {
        return {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value) {
                            if (!_.isUndefined(value)) {
                                return value + '.0%';
                            }
                        }
                    }
                }]
            },
            elements: {
                line: {
                    tension: 0
                }
            }
        }
    }

    private buildAverageDataPoints(dataPoints) {
        let averageDataPoints = [];

        let m = (dataPoints[dataPoints.length - 1] - dataPoints[0]) / (dataPoints.length - 1);
        let b = dataPoints[0];

        for (let x = 0; x < dataPoints.length; x++) {
            let point = m * x + b;
            averageDataPoints.push(point);
        }

        return averageDataPoints;
    }

}

let ngModule = {
    imports: [
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        ChartsModule
    ],
    declarations: [
      PluginComponent
    ],
    providers: [
        ibackendProvider
    ]
};
export {PluginComponent as AngularComponent, ngModule};

