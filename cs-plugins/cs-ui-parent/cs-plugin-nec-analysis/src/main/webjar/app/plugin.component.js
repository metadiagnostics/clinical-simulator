"use strict";
/*
 *  Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var animations_1 = require("@angular/platform-browser/animations");
var cs_plugin_common_1 = require("cs-plugin-common");
var _ = require("lodash");
var PluginComponent = /** @class */ (function () {
    function PluginComponent(backendService) {
        this.backendService = backendService;
        this.options = this.getOptions();
    }
    PluginComponent.prototype.ngOnInit = function () {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        if (cs_plugin_common_1.Utilities.isRunningStandalone()) {
            this.imagePath = 'src/main/webjar/app/images/nec-info-tree.png';
            this.requestData();
        }
        else {
            this.imagePath = '/cs-webapp/webjars/cwf-plugin-nec-analysis/app/images/nec-info-tree.png';
        }
    };
    PluginComponent.prototype.activatePlugin = function (response) {
        if (_.get(response, 'payload.dataChanged')) {
            this.requestData();
        }
    };
    PluginComponent.prototype.requestData = function () {
        var params = {
            _callback: 'updateModel',
            type: 'NEC_ANALYSIS',
            formType: 'gutchecknec',
            formVersion: '1.0'
        };
        this.backendService.sendBackendEvent("retrieveData", params);
    };
    PluginComponent.prototype.updateModel = function (model) {
        this.currentScore = _.get(model, 'payload.currentScore');
        this.currentScoreStyle = this.getPosition();
        this.drawLineChart(_.get(model, 'payload.chartData'));
        this.chartData = _.get(model, 'payload.chartData');
    };
    PluginComponent.prototype.getPosition = function () {
        if (_.isNumber(this.currentScore)) {
            var position = (100 / 60) * this.currentScore;
            var pixels = 59 - 15 + (this.currentScore / 2); // Half of div width - left margin + (current score / 2)
            // This doesn't line up for some reason, so special case
            if (this.currentScore === 60) {
                pixels = 38;
            }
            return 'calc(' + position + '% - ' + pixels + 'px)';
        }
        return null;
    };
    PluginComponent.prototype.drawLineChart = function (data) {
        var _this = this;
        var plottableData = {};
        this.labels = [];
        var datasets = [];
        _.forEach(data, function (facility) {
            var dataSetEntry = {};
            dataSetEntry.label = facility.title;
            dataSetEntry.fill = false;
            dataSetEntry.borderColor = facility.lineColor;
            dataSetEntry.data = [];
            _.forEach(facility.points, function (point) {
                if (_.indexOf(_this.labels, point.year) < 0) {
                    _this.labels.push(point.year);
                }
                dataSetEntry.data.push(point.percent);
            });
            datasets.push(dataSetEntry);
            if (facility.createAverageLine) {
                var avgDataSetEntry = {};
                avgDataSetEntry.label = facility.title + ' Trend Line';
                avgDataSetEntry.fill = false;
                avgDataSetEntry.borderColor = 'black';
                avgDataSetEntry.borderDash = [5, 5];
                avgDataSetEntry.data = _this.buildAverageDataPoints(dataSetEntry.data);
                datasets.push(avgDataSetEntry);
            }
        });
        plottableData.labels = this.labels;
        plottableData.datasets = datasets;
        this.linechartComponent.drawMultipleDataSets(plottableData, this.getOptions());
    };
    PluginComponent.prototype.getOptions = function () {
        return {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function (value) {
                                if (!_.isUndefined(value)) {
                                    return value + '.0%';
                                }
                            }
                        }
                    }]
            },
            elements: {
                line: {
                    tension: 0
                }
            }
        };
    };
    PluginComponent.prototype.buildAverageDataPoints = function (dataPoints) {
        var averageDataPoints = [];
        var m = (dataPoints[dataPoints.length - 1] - dataPoints[0]) / (dataPoints.length - 1);
        var b = dataPoints[0];
        for (var x = 0; x < dataPoints.length; x++) {
            var point = m * x + b;
            averageDataPoints.push(point);
        }
        return averageDataPoints;
    };
    __decorate([
        core_1.ViewChild('naPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('line'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "linechartComponent", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./plugin.component.css']
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}());
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        animations_1.BrowserAnimationsModule,
        cs_plugin_common_1.ChartsModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map