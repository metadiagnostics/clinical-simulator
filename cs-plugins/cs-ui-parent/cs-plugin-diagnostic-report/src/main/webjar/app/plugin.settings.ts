/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export class PluginSettings {
    public static readonly MASTER_DETAIL_CONFIG = {
        "root": "//REPLACE ME IN RUNTIME!",
        "type": "DIAGNOSTIC_REPORT",
        "pagination": {
            "strategy" : "server",
            "pageSize" : 30
        },
        "sections": {
            "header": {
                "title": "Diagnostic Reports",
                "categories": "CT,CTH,CUS,EC,EN,IMG,NMR,NMS,OUS,PF,RAD,RC,RT,RUS,RX,VUS,XRC",
                "dateSelection": true,
                "filterButtons": [{
                    "label": "All"
                },
                    {
                        "label": "Radiology",
                        "params": {
                            "categories": "RAD"
                        }
                    },
                    {
                        "label": "ECG",
                        "params": {
                            "categories": "EC"
                        }
                    },
                    {
                        "label": "Endoscopy",
                        "params": {
                            "categories": "PAT,SP"
                        }
                    },
                    {
                        "label": "EEG",
                        "params": {
                            "categories": "EN"
                        }
                    },
                    {
                        "label": "Misc",
                        "params": {
                            "categories": "OTH"
                        }
                    }
                ],
                "secondaryDropdownConfig": {
                    "label": "Status",
                    "options": [
                        {
                            "label": "All"
                        },
                        {
                            "label": "Pending",
                            "params": {
                                "status": "pending"
                            }
                        }
                    ]
                },
                "gridOptions": {
                    "enableColResize": true
                },
                "columns": [{
                    "field": "collectionDateTime",
                    "headerName": "Collection Date/Time"
                }, {
                    "field": "study",
                    "headerName": "Study"
                }, {
                    "field": "status",
                    "headerName": "Status"
                }, {
                    "field": "performer",
                    "headerName": "Performer"
                }, {
                    "field": "orderingProvider",
                    "headerName": "Ordering Provider"
                }, {
                    "field": "facility",
                    "headerName": "Facility"
                }]
            },
            "details": {
                "tabs": [
                    {
                        "label": "Description",
                        "type": "ATTACHMENTS",
                        "mode": "single"
                    },
                    {
                        "label": "Attachments",
                        "type": "ATTACHMENTS",
                        "mode": "multi"
                    }
                ]
            }
        }
    }
}
  