/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import {
    ChartsModule, DateSelectionModule,
    ibackendProvider, IBackendService,
    IOConstants, LineChartElement,
    I_O_Grid, Utilities,
    IOModelProcessor, FlowsheetPluginHelper,
    FlowsheetModule
} from 'cs-plugin-common';

import * as moment from 'moment';


import * as _ from 'lodash';

import { PluginSettings } from './plugin.settings';

@Component({
       moduleId: module.id,
       selector: 'plugin',
       templateUrl: 'plugin.component.html',
       styleUrls: ['./plugin.component.css']
})
export class PluginComponent extends FlowsheetPluginHelper{
    @ViewChild('cwfIOPlugin')
    private root;

    @ViewChild('I_O_flowsheet')
    private i_o_flowsheet;

    private selectedDate: Date;

    @ViewChild('dateSelection')
    private dateSelectionComponent;

    @ViewChild('linechart')
    private linechartComponent;

    private grid: I_O_Grid = null;
    private modelProcessor : IOModelProcessor = null;
    private selectedRow = null;
    private visibleColumns = [];

    constructor(private backendService: IBackendService) {
        super();
        this.resetDate();
    }

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeFlowsheet();

        this.dateSelectionComponent.dateChanged$.subscribe(date => {
            if (date != null) {
                this.selectedDate = date;
                this.RequestI_OData();
            }
        });

        this.i_o_flowsheet.getGrid().selectedRowChanged$.subscribe(row => {
            if (!_.isNull(row)) {
                this.selectedRow = row;
                this.updateLineChart(this.selectedRow, this.visibleColumns);
            }
        });

        this.i_o_flowsheet.getGrid().visibleColumnsChanged$.subscribe(columns => {
            if (! _.isEmpty(columns)) {
                this.visibleColumns = columns;
                this.updateLineChart(this.selectedRow, this.visibleColumns);
            }
        });
    }

    private updateLineChart(row: any, columns: Array<any>) : void {
        if (_.isEmpty(row) || _.isEmpty(row.originalName)) {
            return;
        }

        let title = row.originalName;
        let data: Array<LineChartElement>  = FlowsheetPluginHelper.getDataToPlot(row, columns);
        this.linechartComponent.draw(title, data);
    }

    protected  initializeFlowsheet() : void {
        this.modelProcessor = new IOModelProcessor(PluginSettings.typeSpecifics.dailyTotal);
        this.grid = new I_O_Grid();
        let gridOptions = {
            onGridReady: () => {
                if (Utilities.isRunningStandalone()) {
                    this.RequestI_OData();
                }
            }
        };

        this.i_o_flowsheet.setType(IOConstants.I_O);
        this.i_o_flowsheet.initialize(this.modelProcessor, this.grid, gridOptions, 30, true);
    }

    protected getModelQueryParams() : any {
        let retVal: any = {
            type: IOConstants.I_O
            , startTime: FlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, -4)
            , endTime: FlowsheetPluginHelper.getFormattedDateParam(this.selectedDate, 4)
            , uuid: this.i_o_flowsheet.getUUID()
        };

        return retVal;
    }

    private resetDate() {
        this.selectedDate = moment().toDate();
    }

    private RequestI_OData() : void {
        this.i_o_flowsheet.setModel({data: []});
        this.i_o_flowsheet.showLoadingIndicator();
        console.time('I&O data request');
        let params = this.getModelQueryParams();
        _.assign(params, {endpoint: 'i_o'});
        _.assign(params, {_callback: 'updateModel'});
        _.assign(params, {_liveUpdateCallback: 'updateModel'});

        this.backendService.sendBackendEvent("retrieveData", params);
    }

    // start of fujion callbacks...
    public updateModel(model: any) {
        console.timeEnd('I&O data request');
        this.i_o_flowsheet.setModel(model);
    }

    public activatePlugin(response) : void {
        if (_.get(response, 'payload.dataChanged')) {
            this.i_o_flowsheet.setModel({data: []});
            this.resetDate();
            this.linechartComponent.clear();
            this.RequestI_OData();
        }
    }

    // end of fujion callbacks/////
}

let ngModule = {
    imports: [
        BrowserModule
        , HttpModule
        , FormsModule
        , BrowserAnimationsModule
        , DateSelectionModule
        , SplitPaneModule
        , ChartsModule
        , FlowsheetModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        ibackendProvider
    ]
};
export {PluginComponent as AngularComponent, ngModule};

