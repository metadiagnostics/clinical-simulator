/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.plugin.flowsheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.FlowsheetService;
import com.cognitivemedicine.common.flowsheet.FlowsheetType;

import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

import org.hl7.fhir.dstu3.model.Patient;

/**
 * Simple component to display the current date and time.
 */
public class IOPluginController extends PatientRecordPluginController {

    @WiredComponent
    private AngularComponent ioPlugin;

    @Autowired
    private FlowsheetService flowsheetService;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.I_O);

    private Patient currentPatient;

    @EventHandler(value = "retrieveData", target = "ioPlugin")
    private void onRetrieveData$ioPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        this.currentPatient = PatientContext.getActivePatient();

        FlowsheetType type = FlowsheetType.valueOf((String) params.get("type"));
        Date startDate = this.convertToIODate((String) params.get("startTime"));
        Date endDate = this.convertToIODate((String) params.get("endTime"));
        this.config.setDateRange(startDate, endDate);
        this.config.setLiveUpdateCallback((String) params.get("_liveUpdateCallback"));
        this.subscribeToUpdates();

        Map<String, Object> flowsheetData = this.flowsheetService.getFlowsheetDataMap(type, this.currentPatient, startDate, endDate, null, null);
        this.setDataChanged(false);

        this.addMetadata(flowsheetData, (String)params.get("uuid"), null);
        this.ioNgInvoke("updateModel", flowsheetData);
    }

    private void ioNgInvoke(String functionName, Object... args) {
        if (this.ioPlugin != null) {
            this.ioPlugin.ngInvoke(functionName, args);
        }
    }

    private Date convertToIODate(String date) {
        Date jDate = null;
        try {
            jDate = this.dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jDate;
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        Map<String, Object> data = this.flowsheetService.getFlowsheetDataMap(FlowsheetType.I_O, this.currentPatient, this.config.getStartDate(), this.config.getEndDate(), null, null);

        if (data != null && this.config.getLiveUpdateCallback() != null) {
            this.addMetadata(data, null, null);
            this.ioNgInvoke(this.config.getLiveUpdateCallback(), data);
        }
    }
}
