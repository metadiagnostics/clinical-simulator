/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.ProblemService;
import java.util.Date;
import java.util.Map;

import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import org.fujion.annotation.EventHandler;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;


public class ProblemsPluginController extends PatientRecordPluginController {

    @Autowired
    private ProblemService problemService;

    private DataSubscriptionConfiguration config = new DataSubscriptionConfiguration(DataSubscriptionConfiguration.Domain.PROBLEM);

    @EventHandler(value = "retrieveData", target = "problemsPlugin")
    private void onRetrieveData$problemsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        this.retrieveData(params);
    }

    private void retrieveData(Map<String, Object> params){
        this.config.setLiveUpdateCallback((String) params.get("_liveUpdateCallback"));
        this.subscribeToUpdates();

        Map<String, Object>reportData = this.problemService.retrieveProblemData(PatientContext.getActivePatient(), null, null, true);
        this.setDataChanged(false);

        this.addMetadata(reportData, (String)params.get("_uuid"), null);
        this.ngInvoke(params, reportData);
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return this.config;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {
        String id = resource.getIdElement().getIdPart();
        Map<String, Object> data = this.problemService.retrieveProblemResourceById(id);

        if (data != null) {
            this.addMetadata(data, null, null);
            this.ngInvoke(this.config.getLiveUpdateCallback(), data);
        }
    }
}
