/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export class PluginSettings {
  
  public static readonly MASTER_DETAIL_CONFIG = {
    "root": "//REPLACE ME IN RUNTIME!",
    "type": "PROBLEMS",
    "sections": {
      "header": {
        "title": "Problems",
        "dateSelection": false,
        "filterButtons": [{
            "label": 'All'
          }
        ],
        "gridOptions": {
          "enableColResize": true
        },
        "shouldOpenDrawer": function(data) {
          return data.bar ? true : false;
        },
        "columns": [{
            "field": "icon",
            "headerName": " ",
            "headerClass": "fa fa-check-circle-o problems-icon-header",
            "cellClass": "problem-icon-cell",
            "renderer": "bar",
            "width": 15
          }, {
            "field": 'problem',
            "headerName": 'Problem'
          }, {
            "field": 'onsetDate',
            "headerName": 'Onset'
          }, {
            "field": 'reportedDate',
            "headerName": 'Reported'
          }, {
            "field": 'resolvedDate',
            "headerName": 'Resolved'
          }, {
            "field": 'provider',
            "headerName": 'Provider'
          }, {
            "field": 'acuity',
            "headerName": 'Acuity'
          }, {
            "field": 'facility',
            "headerName": 'Facility'
          }]
      },
      "details": {}
    }
  }
}
  