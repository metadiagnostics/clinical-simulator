"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var animations_1 = require("@angular/platform-browser/animations");
var plugin_settings_1 = require("./plugin.settings");
var cs_plugin_common_1 = require("cs-plugin-common");
var PluginComponent = /** @class */ (function (_super) {
    __extends(PluginComponent, _super);
    function PluginComponent(backendService) {
        var _this = _super.call(this) || this;
        _this.backendService = backendService;
        return _this;
    }
    PluginComponent_1 = PluginComponent;
    PluginComponent.prototype.ngOnInit = function () {
        this.masterDetailConfig = plugin_settings_1.PluginSettings.MASTER_DETAIL_CONFIG;
        this.masterDetailConfig.root = this.root;
        this.setMasterDetailComponent(this.md);
    };
    PluginComponent.prototype.getMasterDetailConfig = function () {
        return this.masterDetailConfig;
    };
    PluginComponent.prototype.getPluginId = function () {
        return PluginComponent_1.PLUGIN_ID;
    };
    PluginComponent.PLUGIN_ID = "PROBLEMS";
    __decorate([
        core_1.ViewChild('problemsPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('md'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "md", void 0);
    PluginComponent = PluginComponent_1 = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./css/plugin.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
    var PluginComponent_1;
}(cs_plugin_common_1.MasterDetailDelegateCalllback));
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        animations_1.BrowserAnimationsModule,
        cs_plugin_common_1.MasterDetailModule
    ],
    declarations: [
        PluginComponent
    ],
    entryComponents: [
        PluginComponent
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map