/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.login;

import javax.servlet.http.HttpServletRequest;

import org.fujion.common.StrUtil;
import org.fujion.client.WebJarLocator;
import org.fujion.core.RequestUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller for login page.
 */
@Controller
public class CSLoginController {

    @GetMapping(path = "security/cs-login", produces = "text/html")
    public String login(ModelMap model, HttpServletRequest request) {
        model.addAttribute("baseUrl", RequestUtil.getBaseURL(request));
        model.addAttribute("webjarInit", WebJarLocator.getInstance().getWebJarInit());
        model.addAttribute("action", "security/login");
        
        String error = request.getParameter("error");
        model.addAttribute("error",
            error == null ? null : error.isEmpty() ? "Authentication failed, please try again." : error);
        return "classpath:/web/com/cognitivemedicine/login/login.htm";
    }

}
