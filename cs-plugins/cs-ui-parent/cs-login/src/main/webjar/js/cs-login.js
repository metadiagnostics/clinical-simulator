'use strict';

define('cs-login', ['jquery', 'lodash', 'cs-login-css', 'bootstrap-css'], function($) {
	return {
	
		init: function() {
			$('#cs-form').on('submit', this.submitHandler.bind(this));
			$('body').show();
		},
		submitHandler: function(event) {
			var username = $('#cs-username').val(),
				password = $('#cs-password').val();
				
			if (!username || !password) {
				return;
			}
			
			var domain = '3';
			username = domain ? domain + '\\' + username : username;
			$('#cs-username-real').val(username);
		},
		showError: function(message) {
			$('#cs-error').text(message);
		}
	};
});