/*
 * #%L
 * carewebframework
 * %%
 * Copyright (C) 2008 - 2016 Regenstrief Institute, Inc.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This Source Code Form is also subject to the terms of the Health-Related
 * Additional Disclaimer of Warranty and Limitation of Liability available at
 *
 *      http://www.carewebframework.org/licensing/disclaimer.
 *
 * #L%
 */
package com.cognitivemedicine.plugin;

import com.cognitivemedicine.common.controller.PatientRecordPluginController;
import com.cognitivemedicine.common.service.ClinicalNoteService;
import com.cognitivemedicine.common.util.DataSubscriptionConfiguration;
import com.cognitivemedicine.cs.models.FormDefinition;
import com.cognitivemedicine.cs.models.FormResponse;
import java.util.Date;
import java.util.Map;

import org.carewebframework.api.context.UserContext;
import org.carewebframework.ui.dialog.DialogUtil;
import org.fujion.annotation.EventHandler;
import org.fujion.event.Event;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Simple component to display the current date and time.
 */
public class ClinicalNotesPluginController extends PatientRecordPluginController {

    @Autowired
    private ClinicalNoteService clinicalNoteService;

    @EventHandler(value = "retrieveData", target = "clinicalNotesPlugin")
    private void onRetrieveData$clinicalNotesPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        Date startDate = this.convertToDate((String) params.get("startTime"));
        Date endDate = this.convertToDate((String) params.get("endTime"));
        String formType = (String) params.get("formType");
        String formVersion = (String) params.get("formVersion");

        Map<String, Object> reportData = this.clinicalNoteService.retrieveClinicalNotesInstancesData(PatientContext.getActivePatient(), startDate, endDate, formType, formVersion);

        this.addMetadata(reportData, (String) params.get("_uuid"), null);
        this.ngInvoke(params, reportData);
    }

    @EventHandler(value = "requestFormDefinition", target = "clinicalNotesPlugin")
    private void onRequestFormDefinitiona$clinicalNotesPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String formType = (String) params.get("formType");
        String formVersion = (String) params.get("formVersion");
        if (formType == null) {
            DialogUtil.showError("The formType and formVersion parameters are required");
            return;
        }
        FormDefinition dto = this.clinicalNoteService.retrieveClinicalNoteDefinition(formType, formVersion);
        this.ngInvoke(params, dto);
    }

    @EventHandler(value = "requestFormResponses", target = "clinicalNotesPlugin")
    private void onRequestFormResponses$clinicalNotesPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String formResponseId = (String) params.get("formResponseId");
        if (formResponseId == null) {
            DialogUtil.showError("The id of the Clinical Note Response is required");
            return;
        }

        FormResponse dto = clinicalNoteService.retrieveClinicalNoteResponse(formResponseId);
        this.ngInvoke(params, dto);
    }

    @EventHandler(value = "saveNewForm", target = "clinicalNotesPlugin")
    private void saveNewForm$clinicalNotesPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        params.put("patientId", PatientContext.getActivePatient().getIdElement().getIdPart());
        params.put("authorName", UserContext.getActiveUser().getFullName());
        try {
            Map<String, Object> responseData = this.clinicalNoteService.saveForm(params);
            this.ngInvoke(params, responseData);
        } catch (Exception e) {
            DialogUtil.showError("An unexpected error occurred while saving your form. Please try again later.");
        }
    }

    @EventHandler(value = "saveEditedForm", target = "clinicalNotesPlugin")
    private void saveEditedForm$clinicalNotesPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        try {
            Map<String, Object> responseData = this.clinicalNoteService.updateForm(params);
            this.ngInvoke(params, responseData);
        } catch (Exception e) {
            DialogUtil.showError("An unexpected error occurred while saving your form. Please try again later.");
        }
    }

    @Override
    protected DataSubscriptionConfiguration getDataSubscriptionConfiguration() {
        return null;
    }

    @Override
    protected void publishSingleModelUpdate(IBaseResource resource) {

    }
}
