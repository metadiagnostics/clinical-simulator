"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var PluginSettings = /** @class */ (function () {
    function PluginSettings() {
    }
    PluginSettings.MASTER_DETAIL_CONFIG = {
        "root": "//REPLACE ME IN RUNTIME!",
        "type": "CLINICAL_NOTES",
        "extraParams": {
            "formType": "gutchecknec",
            "formVersion": "1.0"
        },
        "sections": {
            "header": {
                "title": "Clinical Notes",
                "dateSelection": true,
                "filterButtons": [],
                "gridOptions": {
                    "enableColResize": true
                },
                "columns": [{
                        "field": "createdDateTime",
                        "headerName": "D/T Created"
                    }, {
                        "field": "title",
                        "headerName": "Title"
                    }, {
                        "field": "location",
                        "headerName": "Location"
                    }, {
                        "field": "documentType",
                        "headerName": "Document Type"
                    }, {
                        "field": "lastUpdated",
                        "headerName": "Last Updated"
                    }, {
                        "field": "author",
                        "headerName": "Author"
                    }, {
                        "field": "specialty",
                        "headerName": "Specialty"
                    }, {
                        "field": "authorType",
                        "headerName": "Type"
                    }, {
                        "field": "status",
                        "headerName": "Status"
                    }]
            },
            "details": {
                "tabs": [
                    {
                        "label": "Form",
                        "type": "SMART_FORM",
                        "writebackMode": 'edit'
                    }
                ]
            },
            createButton: {
                label: "New Note",
                domain: "note"
            }
        }
    };
    return PluginSettings;
}());
exports.PluginSettings = PluginSettings;
//# sourceMappingURL=plugin.settings.js.map