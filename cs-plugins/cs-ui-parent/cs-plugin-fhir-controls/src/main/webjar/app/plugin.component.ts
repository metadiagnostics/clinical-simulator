/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IBackendService, ibackendProvider, Guid } from 'cs-plugin-common';

import { ButtonModule } from 'primeng/components/button/button';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ConfirmDialogModule } from 'primeng/components/confirmdialog/confirmdialog';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { MessageService } from 'primeng/components/common/messageservice';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import {GrowlModule} from 'primeng/components/growl/growl';


@Component({
    moduleId: module.id,
    selector: 'plugin',
    templateUrl: 'plugin.component.html',
    styleUrls: ['./plugin.component.css'],
    encapsulation: ViewEncapsulation.None

})
export class PluginComponent {
    @ViewChild('fhirControlsPlugin')
    private root;
    
    private uuid: any = '';
    private cleanupResourcesDisabled: boolean = false;
    private cleanupResourcesButtonIcon: String;
    private cleanupResourcesButtonTooltip: String;
    
    constructor(
      private backendService: IBackendService,
      private messageService: MessageService,
      private confirmationService: ConfirmationService) {
        this.uuid = Guid.newGuid();
    }

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.checkCapability(); 
    }

    protected changeStatus(status:String){
      
      if (status == "DISABLED"){
        this.cleanupResourcesDisabled = true;
        this.cleanupResourcesButtonIcon = "fa fa-trash-o fa-lg";
        this.cleanupResourcesButtonTooltip = "The underlying FHIR Server does not support the Resources Cleanup Capability";
      } else if (status == "RUNNING"){
        this.cleanupResourcesDisabled = true;
        this.cleanupResourcesButtonIcon = "fa fa-cog fa-spin fa-lg";
        this.cleanupResourcesButtonTooltip = "The Cleanup Process is Running";
      } else if (status == "IDLE"){
        this.cleanupResourcesDisabled = false  ;
        this.cleanupResourcesButtonIcon = "fa fa-trash-o fa-lg";
        this.cleanupResourcesButtonTooltip = "Cleanup ALL Resources";
      }
      
    }
    
    // begin server commands
    public cleanupResources(){
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete ALL the resources from the FHIR Server?<br/><strong>This operation may take several minutes.</strong>',
            accept: () => {
              this.changeStatus("RUNNING"); 

              let params: any = {
                  "_uuid": this.uuid,
                  "_callback": "cleanupResourcesResponse",
                  "type": "FHIR_CONTROLS"
              };

              this.backendService.sendBackendEvent('cleanupResources', params);
            }
        });
    } 
    
    public checkCapability(){
      
      this.changeStatus("DISABLED"); 
      
      let params: any = {
          "_uuid": this.uuid,
          "_callback": "checkCapabilityResponse",
          "type": "FHIR_CONTROLS"
      };

      this.backendService.sendBackendEvent('checkCapability', params);
      
    }
    // end of server commands

    // start of fujion callbacks...
    public cleanupResourcesResponse(response: any) {
      
      if (!response._uuid || response._uuid != this.uuid){
        return;
      }
     
      try{ 
        if (response.error){
            this.messageService.add({severity:'error', summary:'Error', detail:response.errorMessage});
        } else {
          this.messageService.add({severity:'success', summary:'Success', detail:'Successfully deleted ALL Resources'});
        }
      } finally {
        this.changeStatus("IDLE");
      }
      
    }
    
    
    public checkCapabilityResponse(response: any) {
      
      if (!this.processResponse(response)){
        return;
      }
      
      this.changeStatus(response.payload.supported? "IDLE" : "DISABLED"); 
      
    }
    
    private processResponse(response:any){
      if (!response._uuid || response._uuid != this.uuid){
        return false;
      }
      
      if (response.error){
          this.messageService.add({severity:'error', summary:'Error', detail:response.errorMessage});
          return false;
      }
      
      return true;
    }
    // end of fujion callbacks...

}


let ngModule = {
    imports: [
        BrowserModule
        , HttpModule
        , FormsModule
        , BrowserAnimationsModule
        , ButtonModule
        , DropdownModule
        , DialogModule
        , ListboxModule
        , TooltipModule
        , ConfirmDialogModule
        , GrowlModule
    ],
    providers: [
      ibackendProvider,
      ConfirmationService,
      MessageService
    ],
    declarations: [
        PluginComponent
    ]
};
export {PluginComponent as AngularComponent, ngModule};

