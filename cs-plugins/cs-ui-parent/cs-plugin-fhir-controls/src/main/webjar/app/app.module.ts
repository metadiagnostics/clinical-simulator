/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule } from "@angular/forms";

import { PluginComponent } from './plugin.component';
import { ibackendProvider } from 'cs-plugin-common';


import { ButtonModule } from "primeng/components/button/button";
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import {TooltipModule} from "primeng/components/tooltip/tooltip";
import { ConfirmDialogModule } from 'primeng/components/confirmdialog/confirmdialog';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { MessageService } from 'primeng/components/common/messageservice';
import { GrowlModule } from 'primeng/components/growl/growl';


@NgModule({
    imports: [
        BrowserModule
        , HttpModule
        , FormsModule
        , BrowserAnimationsModule
        , ButtonModule
        , DropdownModule
        , DialogModule
        , ListboxModule
        , TooltipModule
        , ConfirmDialogModule
        , GrowlModule
    ],
    declarations: [
        PluginComponent
    ],
    providers: [
        ibackendProvider,
        ConfirmationService,
        MessageService
    ],
    bootstrap: [
        PluginComponent
    ]
})

export class AppModule {
}

