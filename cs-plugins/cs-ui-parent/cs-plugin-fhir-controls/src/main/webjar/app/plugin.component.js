"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var animations_1 = require("@angular/platform-browser/animations");
var cs_plugin_common_1 = require("cs-plugin-common");
var button_1 = require("primeng/components/button/button");
var dropdown_1 = require("primeng/components/dropdown/dropdown");
var dialog_1 = require("primeng/components/dialog/dialog");
var confirmdialog_1 = require("primeng/components/confirmdialog/confirmdialog");
var confirmationservice_1 = require("primeng/components/common/confirmationservice");
var messageservice_1 = require("primeng/components/common/messageservice");
var listbox_1 = require("primeng/components/listbox/listbox");
var tooltip_1 = require("primeng/components/tooltip/tooltip");
var growl_1 = require("primeng/components/growl/growl");
var PluginComponent = /** @class */ (function () {
    function PluginComponent(backendService, messageService, confirmationService) {
        this.backendService = backendService;
        this.messageService = messageService;
        this.confirmationService = confirmationService;
        this.uuid = '';
        this.cleanupResourcesDisabled = false;
        this.uuid = cs_plugin_common_1.Guid.newGuid();
    }
    PluginComponent.prototype.ngOnInit = function () {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.checkCapability();
    };
    PluginComponent.prototype.changeStatus = function (status) {
        if (status == "DISABLED") {
            this.cleanupResourcesDisabled = true;
            this.cleanupResourcesButtonIcon = "fa fa-trash-o fa-lg";
            this.cleanupResourcesButtonTooltip = "The underlying FHIR Server does not support the Resources Cleanup Capability";
        }
        else if (status == "RUNNING") {
            this.cleanupResourcesDisabled = true;
            this.cleanupResourcesButtonIcon = "fa fa-cog fa-spin fa-lg";
            this.cleanupResourcesButtonTooltip = "The Cleanup Process is Running";
        }
        else if (status == "IDLE") {
            this.cleanupResourcesDisabled = false;
            this.cleanupResourcesButtonIcon = "fa fa-trash-o fa-lg";
            this.cleanupResourcesButtonTooltip = "Cleanup ALL Resources";
        }
    };
    // begin server commands
    PluginComponent.prototype.cleanupResources = function () {
        var _this = this;
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete ALL the resources from the FHIR Server?<br/><strong>This operation may take several minutes.</strong>',
            accept: function () {
                _this.changeStatus("RUNNING");
                var params = {
                    "_uuid": _this.uuid,
                    "_callback": "cleanupResourcesResponse",
                    "type": "FHIR_CONTROLS"
                };
                _this.backendService.sendBackendEvent('cleanupResources', params);
            }
        });
    };
    PluginComponent.prototype.checkCapability = function () {
        this.changeStatus("DISABLED");
        var params = {
            "_uuid": this.uuid,
            "_callback": "checkCapabilityResponse",
            "type": "FHIR_CONTROLS"
        };
        this.backendService.sendBackendEvent('checkCapability', params);
    };
    // end of server commands
    // start of fujion callbacks...
    PluginComponent.prototype.cleanupResourcesResponse = function (response) {
        if (!response._uuid || response._uuid != this.uuid) {
            return;
        }
        try {
            if (response.error) {
                this.messageService.add({ severity: 'error', summary: 'Error', detail: response.errorMessage });
            }
            else {
                this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Successfully deleted ALL Resources' });
            }
        }
        finally {
            this.changeStatus("IDLE");
        }
    };
    PluginComponent.prototype.checkCapabilityResponse = function (response) {
        if (!this.processResponse(response)) {
            return;
        }
        this.changeStatus(response.payload.supported ? "IDLE" : "DISABLED");
    };
    PluginComponent.prototype.processResponse = function (response) {
        if (!response._uuid || response._uuid != this.uuid) {
            return false;
        }
        if (response.error) {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: response.errorMessage });
            return false;
        }
        return true;
    };
    __decorate([
        core_1.ViewChild('fhirControlsPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./plugin.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService,
            messageservice_1.MessageService,
            confirmationservice_1.ConfirmationService])
    ], PluginComponent);
    return PluginComponent;
}());
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        forms_1.FormsModule,
        animations_1.BrowserAnimationsModule,
        button_1.ButtonModule,
        dropdown_1.DropdownModule,
        dialog_1.DialogModule,
        listbox_1.ListboxModule,
        tooltip_1.TooltipModule,
        confirmdialog_1.ConfirmDialogModule,
        growl_1.GrowlModule
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider,
        confirmationservice_1.ConfirmationService,
        messageservice_1.MessageService
    ],
    declarations: [
        PluginComponent
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map