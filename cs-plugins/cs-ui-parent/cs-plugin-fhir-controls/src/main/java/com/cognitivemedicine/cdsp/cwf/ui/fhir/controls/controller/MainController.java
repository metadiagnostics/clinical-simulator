/** *****************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 ****************************************************************************** */
package com.cognitivemedicine.cdsp.cwf.ui.fhir.controls.controller;

import com.cognitivemedicine.cdsp.cwf.ui.fhir.controls.client.FhirClient;
import com.cognitivemedicine.common.controller.AngularCallbackResponse;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.carewebframework.shell.plugins.PluginController;
import org.carewebframework.ui.thread.ThreadEx;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.client.ExecutionContext;
import org.fujion.component.*;
import org.fujion.event.Event;
import org.hl7.fhir.dstu3.model.Patient;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MainController extends PluginController {

    @WiredComponent
    private AngularComponent fhirControlsPlugin;

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(MainController.class);

    private FhirClient fhirClient;

    public MainController() {
        super();

        fhirClient = new FhirClient();
    }

    /**
     * ****************************************************************
     * Overridden Supertype Methods
     * ****************************************************************
     */
    @Override
    public void afterInitialized(BaseComponent comp) {
        super.afterInitialized(comp);
    }

    @EventHandler(value = "cleanupResources", target = "fhirControlsPlugin")
    private void cleanupResources$fhirControlsPlugin(Event event) {

        // Cancel out any patient selection before removing resources
        String pageId = ExecutionContext.getPage().getId();
        ExecutionContext.invoke(pageId, () -> {
            PatientContext.changePatient((Patient) null);
        });

        Map<String, Object> params = (Map<String, Object>) event.getData();

        String uuid = (String) params.get("_uuid");
        String callback = ((String) params.get("_callback"));

        this.startBackgroundThread(new ThreadEx.IRunnable() {
            @Override
            public void run(ThreadEx thread) throws Exception {
                AngularCallbackResponse response;
                try {
                    boolean cleanupResourcesResult = fhirClient.cleanupResources();
                    if (cleanupResourcesResult){
                        response = new AngularCallbackResponse(uuid, null);
                    } else {
                        response = new AngularCallbackResponse(uuid, true, "The FHIR Server couldn't delete all the Resources", null);
                    }
                } catch (Exception e) {
                    log.error("Exception while cleaning up FHIR Resources", e);
                    response = new AngularCallbackResponse(uuid, true, e.getMessage(), null);
                }
                fhirControlsPlugin.ngInvoke(callback, response);

            }

            @Override
            public void abort() {
            }
        });
    }

    @EventHandler(value = "checkCapability", target = "fhirControlsPlugin")
    private void checkCapability$fhirControlsPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        AngularCallbackResponse response;
        String uuid = (String) params.get("_uuid");
        String callback = ((String) params.get("_callback"));
        try {

            boolean supported = fhirClient.isCleanupResourcesCapabilitySupported();

            Map<String, Boolean> payload = new HashMap<>();
            payload.put("supported", supported);
            response = new AngularCallbackResponse(uuid, payload);
        } catch (Exception e) {
            log.error("Exception while cleaning up FHIR Resources", e);
            response = new AngularCallbackResponse(uuid, true, e.getMessage(), null);
        }
        fhirControlsPlugin.ngInvoke(callback, response);
    }

}
