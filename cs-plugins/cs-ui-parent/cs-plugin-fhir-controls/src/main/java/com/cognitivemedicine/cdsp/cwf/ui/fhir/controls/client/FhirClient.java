package com.cognitivemedicine.cdsp.cwf.ui.fhir.controls.client;

import ca.uhn.fhir.rest.client.IGenericClient;
import ca.uhn.fhir.rest.gclient.IOperationUntypedWithInput;
import com.cognitivemedicine.cdsp.cwf.api.fhir.FhirConfiguratorFactory;
import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.cognitivemedicine.cdsp.fhir.client.FhirContext;
import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import java.util.List;
import org.hl7.fhir.dstu3.model.CapabilityStatement;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.StringType;

public class FhirClient {
    
    public final static String RESOURCES_CELANUP_OPERATION_CAPABILITY_NAME = "resources-cleanup";
    public final static String RESOURCES_CELANUP_OPERATION_NAME = "$resources-cleanup";
    
    private BaseService baseService;

    public FhirClient() {
        FhirConfigurator configurator = new FhirConfiguratorFactory().createInstance();
        
        FhirContext fhirContext = new FhirContext(configurator);
        fhirContext.getRestfulClientFactory().setConnectTimeout(Integer.MAX_VALUE);
        fhirContext.getRestfulClientFactory().setConnectionRequestTimeout(Integer.MAX_VALUE);
        fhirContext.getRestfulClientFactory().setSocketTimeout(Integer.MAX_VALUE);
        IGenericClient client = fhirContext.newRestfulGenericClient(configurator);
        
        baseService = new BaseService(client);
    }

    /**
     * Checks whether the FHIR Server contains a {@link CapabilityStatement}
     * with name {@link #RESOURCES_CELANUP_OPERATION_CAPABILITY_NAME}.
     * 
     * @return 
     */
    public boolean isCleanupResourcesCapabilitySupported(){
        List<CapabilityStatement> capabilities = this.baseService.searchResourcesByType(CapabilityStatement.class);
        
        if (capabilities == null){
            return false;
        }
        
        for (CapabilityStatement capability : capabilities) {
            if(RESOURCES_CELANUP_OPERATION_CAPABILITY_NAME.equals(capability.getName())){
                return true;
            }
        }
        return false;
        
    }
    
    public boolean cleanupResources(){
        
        IOperationUntypedWithInput<Parameters> operation = baseService.getClient()
            .operation()
            .onServer()
            .named(RESOURCES_CELANUP_OPERATION_NAME)
            .withNoParameters(Parameters.class);
        
        Parameters results = operation.useHttpGet().execute();
        for (Parameters.ParametersParameterComponent param : results.getParameter()) {
            if ("Status".equalsIgnoreCase(param.getName()) && param.getValue() instanceof StringType && "success".equalsIgnoreCase(((StringType)param.getValue()).getValue())){
                return true;
            }
        }
        
        return false;
    }
    
}

