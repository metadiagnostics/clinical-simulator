"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var ConfigSettings = /** @class */ (function () {
    function ConfigSettings() {
    }
    ConfigSettings.I_O = 'I_O';
    ConfigSettings.TYPE_SPECIFICS = 'typeSpecifics';
    ConfigSettings.DAILY_TOTAL = 'sameDayTotal';
    ConfigSettings._24_HOUR_TOTAL = '24HourTotal';
    ConfigSettings.MODEL_UPDATE_SCHEME = 'modelUpdateScheme';
    ConfigSettings.MODEL_UPDATE_SCHEDULED = 'scheduled';
    ConfigSettings.MODEL_UPDATE_REAL_TIME = 'realTime';
    ConfigSettings.MODEL_UPDATE_HYBRID = 'hybrid';
    ConfigSettings.SUBSCRIBE_RESOURCE = 'subscribeResource';
    ConfigSettings.SUBSCRIBE_RESOURCE_RESPONSE = 'subscribeResourceResponse';
    ConfigSettings.SUBSCRIBE_RESOURCE_SUCCESS = 'processed';
    ConfigSettings.NOTIFY_RESOURCE = 'notifyResource';
    ConfigSettings.CURRENT_PATIENT = 'currentPatient';
    ConfigSettings.SIMULATOR = 'simulator';
    ConfigSettings.PAUSED = 'paused';
    ConfigSettings.RESUMED = 'resumed';
    ConfigSettings.NO_PATIENT = 'none';
    ConfigSettings.I_O_TYPE = 'I_O_type';
    return ConfigSettings;
}());
exports.ConfigSettings = ConfigSettings;
//# sourceMappingURL=configuration.settings.js.map