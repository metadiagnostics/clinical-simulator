/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export class ConfigSettings {
    static readonly I_O = 'I_O';
    static readonly TYPE_SPECIFICS = 'typeSpecifics';
    static readonly DAILY_TOTAL= 'sameDayTotal';
    static readonly _24_HOUR_TOTAL = '24HourTotal';
    static readonly MODEL_UPDATE_SCHEME = 'modelUpdateScheme';
    static readonly MODEL_UPDATE_SCHEDULED = 'scheduled';
    static readonly MODEL_UPDATE_REAL_TIME = 'realTime';
    static readonly MODEL_UPDATE_HYBRID = 'hybrid';
    static readonly SUBSCRIBE_RESOURCE = 'subscribeResource';
    static readonly SUBSCRIBE_RESOURCE_RESPONSE = 'subscribeResourceResponse';
    static readonly SUBSCRIBE_RESOURCE_SUCCESS = 'processed';
    static readonly NOTIFY_RESOURCE = 'notifyResource';
    static readonly CURRENT_PATIENT = 'currentPatient';
    static readonly SIMULATOR = 'simulator';
    static readonly PAUSED = 'paused';
    static readonly RESUMED = 'resumed';
    static readonly NO_PATIENT = 'none';
    static readonly I_O_TYPE = 'I_O_type';
}

