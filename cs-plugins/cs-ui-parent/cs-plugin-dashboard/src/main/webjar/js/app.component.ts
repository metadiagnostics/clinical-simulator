/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import { BrowserModule } from '@angular/platform-browser';
import { AgGridModule } from "ag-grid-angular/main";
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { RouterModule } from '@angular/router';
// import { CommonModule} from "@angular/common";
import { Location, LocationStrategy, HashLocationStrategy } from "@angular/common";
// import { NgSwitchCase } from '@angular/common';
//import { routing } from './app.routing';
//
// // third party modules
import { InputTextModule }  from 'primeng/primeng';
import { ButtonModule }  from 'primeng/primeng';
import { TreeModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { TabViewModule } from 'primeng/primeng';
import { SelectButtonModule } from 'primeng/primeng';
import { MultiSelectModule } from 'primeng/primeng';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';

// services
import { MediatorService } from './shared/mediator.service';
import { ModelService } from './shared/model.service';


//components
import { DashboardComponent } from './dashboard.components' ;
import { LocationsComponent } from './query/locations.component';
import { UnitsComponent } from './query/units.component';
import { BundlesComponent } from './query/bundles.component';
import { TimeRangesComponent } from './query/time.range.component';
import { QueryRunnerComponent } from './query/query.runner.components';
import { PieChartComponent } from './results/piechart.component';
import { LineGraphComponent } from './results/linegraph.component';
import { MetricsTabComponent } from './metrics/metrics.component';
import { UnitsSummaryMetricsComponent } from './metrics/unit.summary.component'
import { IndividualsMetricsComponent } from './metrics/individuals.component';
import { UnitSummaryPrimaryGridComponent } from './results/unit.summary.grid.component';
import { IndividualsPrimaryGridComponent } from './results/individuals.grid.component';
import { Component, OnInit } from '@angular/core';

@Component({
    moduleId   : module.id,
    selector   : 'my-app',
    templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit{

    private isInitialized: boolean = false;
    private configurations : any;
    private roles : any;

    constructor(
        private location: Location,
        private service: MediatorService
    ) {
    }

    ngOnInit() {
        let location : string = this.location.path();
        if (location !== '') {
            location = location.substr(1, location.length);
            this.service.setServer(location);
        }

        console.log('location: ', location);
        this.service.getConfig()
            .do(config => {

                this.configurations = config;
                console.log('from configs:' , this.configurations);
                    this.isInitialized = true;
            })
                // will read later on by dashboard
            // .switchMap(roles => this.service.getRoles())
            // .do(roles => {
            //     this.roles = roles;
            //     console.log('from getTimeRanges roles:' , this.roles);

            // })
            .subscribe(
                event => {
                    // console.log(event)
                },
                // TODO in case of error we need to exit?
                err => console.error(err)
            );
    }

    let ngModule = {
        imports: [
            AgGridModule.withComponents([])
            , DropdownModule
            , BrowserModule
            , BrowserAnimationsModule
            , ReactiveFormsModule
            , HttpModule
            , FormsModule
            , InputTextModule
            , ButtonModule
            , TreeModule
            , TabViewModule
            , SelectButtonModule
            , SplitPaneModule
            , MultiSelectModule
        ],
        declarations: [
            AppComponent
            , DashboardComponent
            // query components
            , LocationsComponent
            , BundlesComponent
            , UnitsComponent
            , TimeRangesComponent
            , QueryRunnerComponent

            // metrics components
            , MetricsTabComponent
            , IndividualsMetricsComponent
            , UnitsSummaryMetricsComponent

            // query results components
            , UnitSummaryPrimaryGridComponent
            , IndividualsPrimaryGridComponent
            , PieChartComponent
            , LineGraphComponent
        ],
        providers: [
            MediatorService
            , ModelService,
            Location, {provide: LocationStrategy, useClass: HashLocationStrategy}
        ]
    };

    export { AppComponent as AngularComponent, ngModule }
}