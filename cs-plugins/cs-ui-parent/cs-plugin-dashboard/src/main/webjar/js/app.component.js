"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var platform_browser_1 = require("@angular/platform-browser");
var main_1 = require("ag-grid-angular/main");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var animations_1 = require("@angular/platform-browser/animations");
//import { RouterModule } from '@angular/router';
// import { CommonModule} from "@angular/common";
var common_1 = require("@angular/common");
// import { NgSwitchCase } from '@angular/common';
//import { routing } from './app.routing';
//
// // third party modules
var primeng_1 = require("primeng/primeng");
var primeng_2 = require("primeng/primeng");
var primeng_3 = require("primeng/primeng");
var primeng_4 = require("primeng/primeng");
var primeng_5 = require("primeng/primeng");
var primeng_6 = require("primeng/primeng");
var primeng_7 = require("primeng/primeng");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
// services
var mediator_service_1 = require("./shared/mediator.service");
var model_service_1 = require("./shared/model.service");
//components
var dashboard_components_1 = require("./dashboard.components");
var locations_component_1 = require("./query/locations.component");
var units_component_1 = require("./query/units.component");
var bundles_component_1 = require("./query/bundles.component");
var time_range_component_1 = require("./query/time.range.component");
var query_runner_components_1 = require("./query/query.runner.components");
var piechart_component_1 = require("./results/piechart.component");
var linegraph_component_1 = require("./results/linegraph.component");
var metrics_component_1 = require("./metrics/metrics.component");
var unit_summary_component_1 = require("./metrics/unit.summary.component");
var individuals_component_1 = require("./metrics/individuals.component");
var unit_summary_grid_component_1 = require("./results/unit.summary.grid.component");
var individuals_grid_component_1 = require("./results/individuals.grid.component");
var core_1 = require("@angular/core");
var AppComponent = /** @class */ (function () {
    function AppComponent(location, service) {
        this.location = location;
        this.service = service;
        this.isInitialized = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        var location = this.location.path();
        if (location !== '') {
            location = location.substr(1, location.length);
            this.service.setServer(location);
        }
        console.log('location: ', location);
        this.service.getConfig()
            .do(function (config) {
            _this.configurations = config;
            console.log('from configs:', _this.configurations);
            _this.isInitialized = true;
        })
            // will read later on by dashboard
            // .switchMap(roles => this.service.getRoles())
            // .do(roles => {
            //     this.roles = roles;
            //     console.log('from getTimeRanges roles:' , this.roles);
            // })
            .subscribe(function (event) {
            // console.log(event)
        }, 
        // TODO in case of error we need to exit?
        function (err) { return console.error(err); });
    };
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-app',
            templateUrl: 'app.component.html'
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof common_1.Location !== "undefined" && common_1.Location) === "function" && _a || Object, mediator_service_1.MediatorService])
    ], AppComponent);
    return AppComponent;
    var _a;
}());
exports.AppComponent = AppComponent;
exports.AngularComponent = AppComponent;
var ngModule = {
    imports: [
        main_1.AgGridModule.withComponents([]),
        primeng_4.DropdownModule,
        platform_browser_1.BrowserModule,
        animations_1.BrowserAnimationsModule,
        forms_1.ReactiveFormsModule,
        http_1.HttpModule,
        forms_1.FormsModule,
        primeng_1.InputTextModule,
        primeng_2.ButtonModule,
        primeng_3.TreeModule,
        primeng_5.TabViewModule,
        primeng_6.SelectButtonModule,
        ng2_split_pane_1.SplitPaneModule,
        primeng_7.MultiSelectModule
    ],
    declarations: [
        AppComponent,
        dashboard_components_1.DashboardComponent
        // query components
        ,
        locations_component_1.LocationsComponent,
        bundles_component_1.BundlesComponent,
        units_component_1.UnitsComponent,
        time_range_component_1.TimeRangesComponent,
        query_runner_components_1.QueryRunnerComponent
        // metrics components
        ,
        metrics_component_1.MetricsTabComponent,
        individuals_component_1.IndividualsMetricsComponent,
        unit_summary_component_1.UnitsSummaryMetricsComponent
        // query results components
        ,
        unit_summary_grid_component_1.UnitSummaryPrimaryGridComponent,
        individuals_grid_component_1.IndividualsPrimaryGridComponent,
        piechart_component_1.PieChartComponent,
        linegraph_component_1.LineGraphComponent
    ],
    providers: [
        mediator_service_1.MediatorService,
        model_service_1.ModelService,
        common_1.Location, { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=app.component.js.map