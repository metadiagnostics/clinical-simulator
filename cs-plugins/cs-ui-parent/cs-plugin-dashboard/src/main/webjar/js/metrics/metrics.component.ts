/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, OnInit } from '@angular/core';
import { MediatorService } from '../shared/mediator.service'
import { MetricsItem } from '../shared/utils';
import { Plugin } from '../plugin';

let plugin = Plugin.plugin;

@Component({
               moduleId: module.id,
               selector: 'metrics',
               templateUrl: './metrics.component.html',
           })

export class MetricsTabComponent implements OnInit {
    private metrics: Array<MetricsItem> = [];
    private selectedMetric: string;
    private roles : Array<string>;

    constructor(private service: MediatorService) {
        this.roles = [];
    }

    ngOnInit() {
        this.service.getMetrics()
            .do(metrics => {
                this.metrics = this.checkForAccess(metrics.data);
                this.selectedMetric = this.metrics[0].label;
                // this is to propagate to the metrics components to initialize
                // for the first time a query is selected...
                // this.service.setSelectedMetric(this.selectedMetric);
                console.log(this.metrics);
            })
            .subscribe(
                event => {
                    /*console.log(event);*/
                }
                , err => {
                    console.log(err);
                }
            );
    }

    private onChange(event) {
        console.log(event);
        this.selectedMetric = event.value;
        this.service.setSelectedMetric(this.selectedMetric);
    }

    private clear() {
        this.selectedMetric = null;
        // this.service.setMetric(this.selectedMetric);
    }

    // filter out the quality metrics that are not allowed
    // for the current logged on user
    private checkForAccess(input){
        return input;
    }
}