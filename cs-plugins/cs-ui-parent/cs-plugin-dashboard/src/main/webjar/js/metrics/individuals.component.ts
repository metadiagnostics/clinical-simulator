//our root app component
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { QueryModel } from '../shared/query.model';
import { MetricSettings} from "../shared/constants";
import { MediatorService } from '../shared/mediator.service';

@Component({
               moduleId: module.id
               , selector: 'individuals-metric'
               , styleUrls: ['./individuals.component.css']
               , templateUrl: './individuals.component.html'
           })

export class IndividualsMetricsComponent implements OnInit, OnDestroy {
    @Input() model: QueryModel = null;
    private subscription : Subscription;
    private secondaryQueryComponentTypeSubscription: Subscription;
    private active : boolean = false;
    readonly type : string = MetricSettings.INDIVIDUALS;
    private secondaryQueryComponentType : string = '';

    constructor(private mediatorService: MediatorService) {
    }

    subscribe() {
        this.secondaryQueryComponentTypeSubscription = this.mediatorService.secondaryQueryType$
            .subscribe(
                type => {
                    if (1 || this.active) {
                        this.secondaryQueryComponentType = type;
                        console.log('in IndividualsMetricsComponent secondaryQueryType$ is changed to', type);
                    }
                },
                // TODO more fatal
                err => console.error(err)
            );
        this.subscription = this.mediatorService.metricTypeSource$
            .subscribe(
                type => {
                    this.active = type === this.type;
                    console.log('Individual new metric type', type);
                },
                // TODO more fatal
                err => console.error(err)
            );
    }

    // if not active, do not react to the latest primary query results

    ngOnInit() {
        console.log('Indi initiated');
        this.subscribe();
    }

    ngOnDestroy() {
        console.log('Indi Destroyed');
        this.subscription.unsubscribe();
    }
}