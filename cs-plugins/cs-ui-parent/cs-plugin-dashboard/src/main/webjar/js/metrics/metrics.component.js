"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mediator_service_1 = require("../shared/mediator.service");
var plugin_1 = require("../plugin");
var plugin = plugin_1.Plugin.plugin;
var MetricsTabComponent = /** @class */ (function () {
    function MetricsTabComponent(service) {
        this.service = service;
        this.metrics = [];
        this.roles = [];
    }
    MetricsTabComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getMetrics()
            .do(function (metrics) {
            _this.metrics = _this.checkForAccess(metrics.data);
            _this.selectedMetric = _this.metrics[0].label;
            // this is to propagate to the metrics components to initialize
            // for the first time a query is selected...
            // this.service.setSelectedMetric(this.selectedMetric);
            console.log(_this.metrics);
        })
            .subscribe(function (event) {
            /*console.log(event);*/
        }, function (err) {
            console.log(err);
        });
    };
    MetricsTabComponent.prototype.onChange = function (event) {
        console.log(event);
        this.selectedMetric = event.value;
        this.service.setSelectedMetric(this.selectedMetric);
    };
    MetricsTabComponent.prototype.clear = function () {
        this.selectedMetric = null;
        // this.service.setMetric(this.selectedMetric);
    };
    // filter out the quality metrics that are not allowed
    // for the current logged on user
    MetricsTabComponent.prototype.checkForAccess = function (input) {
        return input;
    };
    MetricsTabComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'metrics',
            templateUrl: './metrics.component.html',
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], MetricsTabComponent);
    return MetricsTabComponent;
}());
exports.MetricsTabComponent = MetricsTabComponent;
//# sourceMappingURL=metrics.component.js.map