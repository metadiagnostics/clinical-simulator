"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
//our root app component
var core_1 = require("@angular/core");
var query_model_1 = require("../shared/query.model");
var constants_1 = require("../shared/constants");
var mediator_service_1 = require("../shared/mediator.service");
var IndividualsMetricsComponent = /** @class */ (function () {
    function IndividualsMetricsComponent(mediatorService) {
        this.mediatorService = mediatorService;
        this.model = null;
        this.active = false;
        this.type = constants_1.MetricSettings.INDIVIDUALS;
        this.secondaryQueryComponentType = '';
    }
    IndividualsMetricsComponent.prototype.subscribe = function () {
        var _this = this;
        this.secondaryQueryComponentTypeSubscription = this.mediatorService.secondaryQueryType$
            .subscribe(function (type) {
            if (1 || _this.active) {
                _this.secondaryQueryComponentType = type;
                console.log('in IndividualsMetricsComponent secondaryQueryType$ is changed to', type);
            }
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
        this.subscription = this.mediatorService.metricTypeSource$
            .subscribe(function (type) {
            _this.active = type === _this.type;
            console.log('Individual new metric type', type);
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
    };
    // if not active, do not react to the latest primary query results
    IndividualsMetricsComponent.prototype.ngOnInit = function () {
        console.log('Indi initiated');
        this.subscribe();
    };
    IndividualsMetricsComponent.prototype.ngOnDestroy = function () {
        console.log('Indi Destroyed');
        this.subscription.unsubscribe();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", query_model_1.QueryModel)
    ], IndividualsMetricsComponent.prototype, "model", void 0);
    IndividualsMetricsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'individuals-metric',
            styleUrls: ['./individuals.component.css'],
            templateUrl: './individuals.component.html'
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], IndividualsMetricsComponent);
    return IndividualsMetricsComponent;
}());
exports.IndividualsMetricsComponent = IndividualsMetricsComponent;
//# sourceMappingURL=individuals.component.js.map