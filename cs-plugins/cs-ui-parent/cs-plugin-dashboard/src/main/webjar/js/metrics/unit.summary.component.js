"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var query_model_1 = require("../shared/query.model");
var constants_1 = require("../shared/constants");
var mediator_service_1 = require("../shared/mediator.service");
var model_service_1 = require("../shared/model.service");
var UnitsSummaryMetricsComponent = /** @class */ (function () {
    function UnitsSummaryMetricsComponent(mediatorService, modelService) {
        this.mediatorService = mediatorService;
        this.modelService = modelService;
        this.model = null;
        this.secondaryQueryComponentType = '';
        this.active = false;
        this.type = constants_1.MetricSettings.UNIT_SUMMARY;
    }
    UnitsSummaryMetricsComponent.prototype.subscribe = function () {
        var _this = this;
        this.secondaryQueryComponentTypeSubscription = this.mediatorService.secondaryQueryType$
            .subscribe(function (type) {
            if (1 || _this.active) {
                _this.secondaryQueryComponentType = type;
                console.log('in Unit summary secondaryQueryType$ is changed to', type);
            }
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
        this.metricTypeSubscription = this.mediatorService.metricTypeSource$
            .subscribe(function (type) {
            console.log('in Unit summary subscription metricTypeSource$ is changed to', type);
            _this.active = type === _this.type;
            _this.modelService.resetModel();
            console.log('Unit Summary new metric type', type);
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
        this.modelSubscription = this.modelService.modelSource$
            .subscribe(function (model) {
            console.log('in Unit summary model: ', model);
            if (_this.active) {
                console.log('model intended for Unit summary');
                // TODO send the model to the grid
            }
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
    };
    UnitsSummaryMetricsComponent.prototype.ngOnInit = function () {
        console.log('Units Summary metric has initiated');
        this.subscribe();
    };
    UnitsSummaryMetricsComponent.prototype.ngOnDestroy = function () {
        this.metricTypeSubscription.unsubscribe();
        this.modelSubscription.unsubscribe();
        console.log('Unit summary destroyed');
    };
    UnitsSummaryMetricsComponent.prototype.onChange = function (event) {
        console.log('primary: ' + event.primary + ' secondary: ' + event.secondary);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", query_model_1.QueryModel)
    ], UnitsSummaryMetricsComponent.prototype, "model", void 0);
    UnitsSummaryMetricsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'unit-summary-metric',
            styleUrls: ['./unit.summary.component.css'],
            templateUrl: './unit.summary.component.html'
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService,
            model_service_1.ModelService])
    ], UnitsSummaryMetricsComponent);
    return UnitsSummaryMetricsComponent;
}());
exports.UnitsSummaryMetricsComponent = UnitsSummaryMetricsComponent;
//# sourceMappingURL=unit.summary.component.js.map