import { Component, Input, Output, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { QueryModel } from '../shared/query.model';
import { MetricSettings } from '../shared/constants';
import { MediatorService } from '../shared/mediator.service';
import { ModelService } from '../shared/model.service';

@Component({
               moduleId: module.id
               , selector: 'unit-summary-metric'
               , styleUrls: ['./unit.summary.component.css']
               , templateUrl: './unit.summary.component.html'
           })

export class UnitsSummaryMetricsComponent implements OnInit {
    @Input() model : QueryModel = null;
    private metricTypeSubscription : Subscription;
    private secondaryQueryComponentTypeSubscription : Subscription;
    private secondaryQueryComponentType : string = '';
    private modelSubscription : Subscription;
    private active: boolean = false;
    readonly type : string = MetricSettings.UNIT_SUMMARY;

    constructor(private mediatorService: MediatorService,
                private modelService: ModelService) {
    }

    subscribe() {
        this.secondaryQueryComponentTypeSubscription = this.mediatorService.secondaryQueryType$
            .subscribe(
                type => {
                    if (1 || this.active) {
                        this.secondaryQueryComponentType = type;
                        console.log('in Unit summary secondaryQueryType$ is changed to', type);
                    }
                },
                // TODO more fatal
                err => console.error(err)
            );
        this.metricTypeSubscription = this.mediatorService.metricTypeSource$
            .subscribe(
                type => {
                    console.log('in Unit summary subscription metricTypeSource$ is changed to', type);
                    this.active = type === this.type;
                    this.modelService.resetModel();
                    console.log('Unit Summary new metric type', type);
                },
                // TODO more fatal
                err => console.error(err)
            );

        this.modelSubscription = this.modelService.modelSource$
            .subscribe(
                model => {
                    console.log('in Unit summary model: ', model);
                    if (this.active) {
                        console.log('model intended for Unit summary');
                        // TODO send the model to the grid
                    }

                },
                // TODO more fatal
                err => console.error(err)
            );
    }

    ngOnInit() {
        console.log('Units Summary metric has initiated');
        this.subscribe();
    }

    ngOnDestroy() {
        this.metricTypeSubscription.unsubscribe();
        this.modelSubscription.unsubscribe();
        console.log('Unit summary destroyed');
    }

    onChange(event) {
        console.log('primary: ' + event.primary + ' secondary: ' + event.secondary);
    }
}