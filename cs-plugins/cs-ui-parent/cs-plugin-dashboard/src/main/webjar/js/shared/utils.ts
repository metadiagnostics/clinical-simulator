/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { TreeNode } from 'primeng/primeng';

export interface MetricsItem {
    label: string;
    value: string;
    id : number;
    roles: Array<string>
}

export class Locations {
    data: string = '';
    label: string = '';

    constructor(treeNode: TreeNode) {
        this.data = treeNode.data;
        this.label = treeNode.label;
    }
}

export class  TimeRangeForQuery {
    startTime: string = '';
    endTime: string = '';

    isEmpty(): boolean {
        return this.startTime === '' || this.endTime === '';
    }
}

export class TimeRange {
    constructor(public label: string, public name: string,
                public startTime: string, public endTime: string) {
    }
}



