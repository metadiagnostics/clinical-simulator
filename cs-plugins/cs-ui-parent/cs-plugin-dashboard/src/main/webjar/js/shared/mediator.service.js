"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var plugin_1 = require("../plugin");
var plugin = plugin_1.Plugin.plugin;
var configFile = 'webjars/cwf-dashboard/config.json';
var rolesFile = 'webjars/cwf-dashboard/roles.json';
var metricsFile = 'webjars/cwf-dashboard/metrics.json';
var unitsFile = 'webjars/cwf-dashboard/units.json';
var locationsFile = 'webjars/cwf-dashboard/locations.json';
var bundlesFile = 'webjars/cwf-dashboard/bundles.json';
var timeRangesFile = 'webjars/cwf-dashboard/time-ranges.json';
var MediatorService = /** @class */ (function () {
    function MediatorService(http) {
        this.http = http;
        // Observable  source
        this._metricTypeSource = new BehaviorSubject_1.BehaviorSubject(null);
        this._secondaryQueryType = new BehaviorSubject_1.BehaviorSubject(null);
        // Observable stream
        this.metricTypeSource$ = this._metricTypeSource.asObservable();
        this.secondaryQueryType$ = this._secondaryQueryType.asObservable();
        this.path = '';
        this.selectedMetric = '';
        if (!plugin) {
            configFile = './app/shared/config.json';
            rolesFile = './app/shared/roles.json';
            metricsFile = './app/shared/metrics.json';
            unitsFile = './app/shared/units.json';
            locationsFile = './app/shared/locations.json';
            bundlesFile = './app/shared/bundles.json';
            timeRangesFile = './app/shared/time-ranges.json';
        }
    }
    MediatorService.prototype.getTimeRanges = function () {
        return this.http.get(timeRangesFile)
            .toPromise()
            .then(function (res) { return res.json().data; });
    };
    MediatorService.prototype.getBundles = function () {
        return this.http.get(bundlesFile)
            .toPromise()
            .then(function (res) { return res.json().data; });
    };
    MediatorService.prototype.getLocations = function () {
        console.log('location service: ', locationsFile);
        return this.http.get(locationsFile)
            .toPromise()
            .then(function (res) { return res.json().data; });
    };
    MediatorService.prototype.getUnitsForLocations = function (locations) {
        console.log('looking units for locations: ', locations);
        return this.http.get(unitsFile)
            .toPromise()
            .then(function (res) { return res.json().data; });
    };
    MediatorService.prototype.getConfig = function () {
        console.log('reading config file from ' + configFile);
        return this.http.get(configFile)
            .map(function (res) {
            // TODO Should we stash these
            // console.log('configurations are: ' , res.text());
            return res.json();
        })
            .catch(function (error) { return Rx_1.Observable.throw(error.json().error ||
            'Failed to getTimeRanges grid configuration settings'); });
    };
    MediatorService.prototype.getUserRoles = function () {
        console.log('reading roles from ', rolesFile);
        return this.http.get(rolesFile)
            .map(function (res) {
            // console.log('roles are: ', res.text());
            return res.json();
        })
            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Failed to getTimeRanges roles settings'); });
    };
    MediatorService.prototype.getMetrics = function () {
        console.log('reading metrics from ', metricsFile);
        return this.http.get(metricsFile)
            .map(function (res) {
            // console.log('metrics are: ', res.text());
            console.log(res.json());
            return res.json();
        })
            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Failed to getTimeRanges metrics settings'); });
    };
    MediatorService.prototype.setServer = function (path) {
        this.path = path;
    };
    MediatorService.prototype.setSelectedMetric = function (metric) {
        // this.selectedMetric = metric;
        console.log('broadcast metric type is being set to: ', metric);
        this._metricTypeSource.next(metric);
    };
    MediatorService.prototype.setSecondaryQueryType = function (type) {
        console.log('broadcast secondary query type is being set to: ', type);
        this._secondaryQueryType.next(type);
    };
    MediatorService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
    ], MediatorService);
    return MediatorService;
    var _a;
}());
exports.MediatorService = MediatorService;
//
// import { Injectable } from '@angular/core';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
//
// @Injectable()
// export class MetricsSelectedService {
//
//     // Observable  source
//     private _metricTypeSource = new BehaviorSubject<string>(null);
//
//     // Observable stream
//     public metricTypeSource$ = this._metricTypeSource.asObservable();
//
//     setSelectedMetric(type: string) {
//         this._metricTypeSource.next(type);
//     }
// }
//# sourceMappingURL=mediator.service.js.map