"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Locations = /** @class */ (function () {
    function Locations(treeNode) {
        this.data = '';
        this.label = '';
        this.data = treeNode.data;
        this.label = treeNode.label;
    }
    return Locations;
}());
exports.Locations = Locations;
var TimeRangeForQuery = /** @class */ (function () {
    function TimeRangeForQuery() {
        this.startTime = '';
        this.endTime = '';
    }
    TimeRangeForQuery.prototype.isEmpty = function () {
        return this.startTime === '' || this.endTime === '';
    };
    return TimeRangeForQuery;
}());
exports.TimeRangeForQuery = TimeRangeForQuery;
var TimeRange = /** @class */ (function () {
    function TimeRange(label, name, startTime, endTime) {
        this.label = label;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    return TimeRange;
}());
exports.TimeRange = TimeRange;
//# sourceMappingURL=utils.js.map