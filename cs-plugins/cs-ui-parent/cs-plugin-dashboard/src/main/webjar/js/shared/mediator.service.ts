/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Plugin} from '../plugin';

let plugin = Plugin.plugin;

let configFile : string = 'webjars/cwf-dashboard/config.json';
let rolesFile         = 'webjars/cwf-dashboard/roles.json';
let metricsFile       = 'webjars/cwf-dashboard/metrics.json';
let unitsFile         = 'webjars/cwf-dashboard/units.json';
let locationsFile     = 'webjars/cwf-dashboard/locations.json';
let bundlesFile       = 'webjars/cwf-dashboard/bundles.json';
let timeRangesFile    = 'webjars/cwf-dashboard/time-ranges.json';


@Injectable()
export class MediatorService {

    // Observable  source
    private _metricTypeSource = new BehaviorSubject<string>(null);
    private _secondaryQueryType = new BehaviorSubject<string>(null);

    // Observable stream
    public metricTypeSource$ = this._metricTypeSource.asObservable();
    public secondaryQueryType$ = this._secondaryQueryType.asObservable();


    private path : string = '';
    private selectedMetric : string = '';

    constructor(private http: Http) {
        if (!plugin) {
            configFile = './app/shared/config.json';
            rolesFile = './app/shared/roles.json';
            metricsFile = './app/shared/metrics.json';
            unitsFile = './app/shared/units.json';
            locationsFile = './app/shared/locations.json';
            bundlesFile = './app/shared/bundles.json';
            timeRangesFile = './app/shared/time-ranges.json';
        }
    }

    getTimeRanges() {
        return this.http.get(timeRangesFile)
            .toPromise()
            .then(res => res.json().data);
    }

    getBundles() {
        return this.http.get(bundlesFile)
            .toPromise()
            .then(res => res.json().data);
    }

    getLocations()  : Promise <any[]> {
        console.log('location service: ', locationsFile);

        return this.http.get(locationsFile)
            .toPromise()
            .then(res =>  res.json().data);
    }

    getUnitsForLocations(locations: Array<string>) {
        console.log('looking units for locations: ', locations);
        return this.http.get(unitsFile)
            .toPromise()
            .then(res => res.json().data);
    }

    getConfig (): Observable<Array<string>> {
        console.log('reading config file from ' + configFile);
        return this.http.get(configFile)
            .map((res: Response) => {
                // TODO Should we stash these
                // console.log('configurations are: ' , res.text());
                return res.json();
            })
            .catch((error: any) => Observable.throw(error.json().error ||
                'Failed to getTimeRanges grid configuration settings'));

    }

    getUserRoles(): Observable<Array<string>> {
        console.log('reading roles from ', rolesFile);
        return this.http.get(rolesFile)
            .map((res: Response) => {
                // console.log('roles are: ', res.text());
                return res.json()
            })
            .catch((error: any) => Observable.throw(error.json().error || 'Failed to getTimeRanges roles settings'));

    }

    getMetrics(): Observable<Array<string>> {
        console.log('reading metrics from ', metricsFile);
        return this.http.get(metricsFile)
            .map((res: Response) => {
                // console.log('metrics are: ', res.text());
                console.log(res.json());
                return res.json()
            })
            .catch((error: any) => Observable.throw(error.json().error || 'Failed to getTimeRanges metrics settings'));

    }

    setServer(path) {
        this.path = path;
    }

    setSelectedMetric(metric) {
        // this.selectedMetric = metric;
        console.log('broadcast metric type is being set to: ', metric);
        this._metricTypeSource.next(metric);
    }


    setSecondaryQueryType(type: string) {
        console.log('broadcast secondary query type is being set to: ', type);
        this._secondaryQueryType.next(type);
    }
}


//
// import { Injectable } from '@angular/core';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
//
// @Injectable()
// export class MetricsSelectedService {
//
//     // Observable  source
//     private _metricTypeSource = new BehaviorSubject<string>(null);
//
//     // Observable stream
//     public metricTypeSource$ = this._metricTypeSource.asObservable();
//
//     setSelectedMetric(type: string) {
//         this._metricTypeSource.next(type);
//     }
// }