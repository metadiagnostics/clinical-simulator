/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { QueryModel } from './query.model';

@Injectable()
export class ModelService {

    // Observable source
    private _modelSource = new BehaviorSubject<QueryModel>(null);

    // Observable stream
    public modelSource$ = this._modelSource.asObservable();

    getModel(queryString) {
        let model : QueryModel = null;
        console.log('model service: ', model);
        this._modelSource.next(model);
    }

    resetModel() {
        console.log('ModelService : setting model to null');
        this._modelSource.next(null);
    }
}