/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import { MediatorService } from '../shared/mediator.service';

@Component({
               moduleId: module.id,
               selector: 'bundles',
               templateUrl: './bundles.component.html'
           })

export class BundlesComponent implements OnInit {
    bundles: Array<string> = [];
    selectedBundle: Array<string> = [];
    @Output() selectedBundle$ = new EventEmitter<Array<string>>();

    constructor(private service: MediatorService) {
    }

    ngOnInit() {
        this.service.getBundles()
            .then(res => {
                console.log('bundles: ', res);
                this.bundles = res;
            })
            .catch(e => {
                // TODO this should be fatal?
                console.log(e);
            });

    }

    onChange(event) {
        console.log('bundle selected: ', event);
        // this.selectedBundle = event;
        this.selectedBundle$.emit(this.selectedBundle);
    }
}