"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var utils_1 = require("../shared/utils");
var mediator_service_1 = require("../shared/mediator.service");
var model_service_1 = require("../shared/model.service");
var _ = require("lodash");
var QueryRunnerComponent = /** @class */ (function () {
    function QueryRunnerComponent(mediatorService, modelService) {
        this.mediatorService = mediatorService;
        this.modelService = modelService;
        // input parameters from child components
        this.locations = [];
        this.units = [];
        this.bundle = '';
        this.timeRangeForQuery = new utils_1.TimeRangeForQuery();
        this.label = 'Query';
        this.metricType = '';
        this.model = null;
        this.disabled = true;
    }
    QueryRunnerComponent.prototype.subscribe = function () {
        var _this = this;
        this.metricTypeSubscription = this.mediatorService.metricTypeSource$
            .subscribe(function (metricType) {
            console.log('------------query runner has a new metric type: ', metricType);
            console.log('metricTypeSource$ changed');
            _this.metricType = metricType;
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
        this.primaryModelSubscription = this.modelService.modelSource$
            .subscribe(function (model) {
            console.log('query runner received a model: ', model);
            _this.model = model;
        }, 
        // TODO more fatal
        function (err) { return console.error(err); });
    };
    QueryRunnerComponent.prototype.ngOnInit = function () {
        this.subscribe();
    };
    QueryRunnerComponent.prototype.ngOnDestroy = function () {
        this.metricTypeSubscription.unsubscribe();
        this.primaryModelSubscription.unsubscribe();
    };
    QueryRunnerComponent.prototype.run = function () {
        var queryString = this.composeQueryString();
        console.log('run with query string: ', queryString);
        this.modelService.getModel(queryString);
        // this.label = 'ddddd';
    };
    QueryRunnerComponent.prototype.unitChangeHandler = function (units) {
        this.units = units;
        console.log('selected units: ', this.units);
        this.figureOutDisableStatus();
    };
    QueryRunnerComponent.prototype.locationChangeHandler = function (locations) {
        var _this = this;
        console.log('Setting locations from: ', locations);
        this.locations = [];
        locations.forEach(function (location) {
            _this.locations.push(location.label);
        });
        console.log('locations are changed: ', this.locations);
        this.figureOutDisableStatus();
    };
    QueryRunnerComponent.prototype.bundleChangeHandler = function (bundle) {
        this.bundle = bundle;
        console.log('bundle are changed: ', this.bundle);
        this.figureOutDisableStatus();
    };
    QueryRunnerComponent.prototype.timeRangeChangeHandler = function (timeRangeForQuery) {
        this.timeRangeForQuery.startTime = timeRangeForQuery.startTime;
        this.timeRangeForQuery.endTime = timeRangeForQuery.endTime;
        console.log('time range is changed: ', this.timeRangeForQuery);
        this.figureOutDisableStatus();
    };
    QueryRunnerComponent.prototype.figureOutDisableStatus = function () {
        // console.log('timeRangeForQuery: ', this.timeRangeForQuery instanceof TimeRangeForQuery);
        this.disabled = this.timeRangeForQuery.isEmpty()
            || _.isEmpty(this.bundle)
            || this.locations.length === 0
            || this.units.length === 0;
        console.log('disabled: ', this.disabled);
    };
    QueryRunnerComponent.prototype.composeQueryString = function () {
        console.log("timeRange: " + this.timeRangeForQuery + "\n                    bundle: " + this.bundle + "\n                    locations: " + this.locations + "\n                    units: " + this.units + "\n                    metricType: " + this.metricType + "\n                    ");
        return '';
    };
    QueryRunnerComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'query-runner',
            templateUrl: './query.runner.component.html'
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService,
            model_service_1.ModelService])
    ], QueryRunnerComponent);
    return QueryRunnerComponent;
}());
exports.QueryRunnerComponent = QueryRunnerComponent;
//# sourceMappingURL=query.runner.components.js.map