/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MediatorService } from "../shared/mediator.service"
import{ Tree, TreeNode } from 'primeng/primeng';

@Component({
               moduleId: module.id
               , selector: 'location-tree'
               , templateUrl: './locations.component.html'
           })

export class LocationsComponent implements OnInit {
    @ViewChild('expandingTree')
    expandingTree: Tree;

    locations: Array<TreeNode> = [];
    selectedNodes: Array<TreeNode> = [];
    currentSelection: string = '';
    // sending output to query runner component
    // when selections change...
    @Output() selectedLocations$ = new EventEmitter<Array<TreeNode>>();

    constructor(private service: MediatorService) {
    }

    ngOnInit(): void {
        this.service.getLocations()
            .then(files => {
                      this.locations = files;
                      console.log(this.locations);
                  }
            )
            .catch(e => {
                // TODO this should be fatal?
                console.log(e);
            });
    }

    // Tree component events...
    private nodeSelect(event: any): void {
        // this.selectedNodes.forEach(sel => {
        //     console.log('selected: ', sel.label);
        // });
        this.selectedLocations$.emit(this.selectedNodes);
        console.log('emitting node selected', this.selectedNodes);
    }

    private nodeUnselect(event: any): void {
        this.selectedLocations$.emit(this.selectedNodes);
        console.log('emitting node unselected', this.selectedNodes);
    }

}

