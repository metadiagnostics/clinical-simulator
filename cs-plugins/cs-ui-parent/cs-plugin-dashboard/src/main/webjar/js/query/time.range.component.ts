/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MediatorService } from '../shared/mediator.service';
import { TimeRange, TimeRangeForQuery } from '../shared/utils';

@Component({
               moduleId: module.id,
               selector: 'time-range',
               templateUrl: './time.range.component.html',
           })

export class TimeRangesComponent implements OnInit {
    timeRanges: Array<TimeRange> = [];
    @Output() selectedTimeRange$ = new EventEmitter<Array<TimeRangeForQuery>>();


    constructor(private service: MediatorService) {
    }

    ngOnInit() {
        this.service.getTimeRanges()
            .then(res => {
                this.timeRanges = res;
                console.log('time ranges: ', this.timeRanges);
            })
            .catch(e => {
                // TODO this should be fatal?
                console.log(e);
            });
    }

    onChange(event) {
        console.log('time range being emitted: ', event);
        this.selectedTimeRange$.emit(event);
    }
}