/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, OnInit, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { MediatorService } from '../shared/mediator.service';
import { SelectItem } from 'primeng/primeng';

@Component({
               moduleId: module.id
               , selector: 'units'
               , templateUrl: './units.component.html'
           })

export class UnitsComponent implements OnInit {
    units: Array<SelectItem> = [];
    selectedUnits: Array<string> = [];

    @Input() locations;
    // sending output to query runner component
    @Output() selectedUnits$ = new EventEmitter<Array<string>>();

    constructor(private service: MediatorService) {
    }

    ngOnInit() {
        this.selectedUnits$.emit(this.selectedUnits);
        console.log('inputed locations: ', this.locations);
        this.service.getUnitsForLocations(this.locations)
            .then(res => {
                this.units = res;
                console.log('units: ', this.units);
            })
            .catch(e => {
                // TODO this should be fatal?
                console.log(e);
            });
    }

    // multi select drop down events...
    onChange(event) {
        console.log(event);
        console.log(this.selectedUnits);
        this.selectedUnits$.emit(this.selectedUnits);
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('new locations list are received', changes);
        this.service.getUnitsForLocations(changes.locations.currentValue)
            .then(res => {
                this.units = res;
                console.log('units: ', this.units);
            })
            .catch(e => {
                // TODO this should be fatal?
                console.log(e);
            });
    }
}