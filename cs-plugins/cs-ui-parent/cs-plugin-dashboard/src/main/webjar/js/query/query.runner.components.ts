/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { QueryModel } from '../shared/query.model';
import { TimeRangeForQuery } from '../shared/utils';
import { MediatorService } from '../shared/mediator.service';
import { ModelService } from '../shared/model.service';
import { Subscription } from 'rxjs/Subscription';
import _ = require('lodash');


@Component({
               moduleId: module.id,
               selector: 'query-runner',
               templateUrl: './query.runner.component.html'
           })

export class QueryRunnerComponent implements OnInit {

    // input parameters from child components
    private locations: Array<string> = [];
    private units: Array<string> = [];
    private bundle: string = '';
    private timeRangeForQuery: TimeRangeForQuery = new TimeRangeForQuery();
    private label: string = 'Query';
    private metricTypeSubscription : Subscription;
    private primaryModelSubscription : Subscription;
    private metricType: string = '';
    private model: QueryModel = null;


    private disabled: boolean = true;

    constructor(private mediatorService: MediatorService,
                private modelService: ModelService) {
    }

    subscribe() {
        this.metricTypeSubscription = this.mediatorService.metricTypeSource$
            .subscribe(
                metricType => {
                    console.log('------------query runner has a new metric type: ', metricType);
                    console.log('metricTypeSource$ changed');
                        this.metricType = metricType;
                },
                // TODO more fatal
                err => console.error(err)
            );

        this.primaryModelSubscription = this.modelService.modelSource$
            .subscribe(
                model => {
                    console.log('query runner received a model: ', model);
                    this.model = model;
                },
                // TODO more fatal
                err => console.error(err)
            );
    }

    ngOnInit() {
        this.subscribe();
    }

    ngOnDestroy() {
        this.metricTypeSubscription.unsubscribe();
        this.primaryModelSubscription.unsubscribe();
    }

    run() {
        let queryString = this.composeQueryString();
        console.log('run with query string: ', queryString);

        this.modelService.getModel(queryString);

        // this.label = 'ddddd';
    }

    unitChangeHandler(units): void {
        this.units = units;
        console.log('selected units: ', this.units);
        this.figureOutDisableStatus();
    }

    locationChangeHandler(locations): void {
        console.log('Setting locations from: ', locations);
        this.locations = [];

        locations.forEach(location => {
            this.locations.push(location.label);
        });

        console.log('locations are changed: ', this.locations);
        this.figureOutDisableStatus();
    }

    bundleChangeHandler(bundle): void {
        this.bundle = bundle;
        console.log('bundle are changed: ', this.bundle);
        this.figureOutDisableStatus();
    }

    timeRangeChangeHandler(timeRangeForQuery: any): void {
        this.timeRangeForQuery.startTime = timeRangeForQuery.startTime;
        this.timeRangeForQuery.endTime = timeRangeForQuery.endTime;

        console.log('time range is changed: ', this.timeRangeForQuery);
        this.figureOutDisableStatus();
    }

    figureOutDisableStatus(): void {
        // console.log('timeRangeForQuery: ', this.timeRangeForQuery instanceof TimeRangeForQuery);

        this.disabled = this.timeRangeForQuery.isEmpty()
            || _.isEmpty(this.bundle)
            || this.locations.length === 0
            || this.units.length === 0;

        console.log('disabled: ', this.disabled);
    }

    composeQueryString() {
        console.log(`timeRange: ${this.timeRangeForQuery}\n                    bundle: ${this.bundle}\n                    locations: ${this.locations}\n                    units: ${this.units}\n                    metricType: ${this.metricType}\n                    `);
        return '';
    }
}