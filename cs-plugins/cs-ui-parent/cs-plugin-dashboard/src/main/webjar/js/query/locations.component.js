"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mediator_service_1 = require("../shared/mediator.service");
var primeng_1 = require("primeng/primeng");
var LocationsComponent = /** @class */ (function () {
    function LocationsComponent(service) {
        this.service = service;
        this.locations = [];
        this.selectedNodes = [];
        this.currentSelection = '';
        // sending output to query runner component
        // when selections change...
        this.selectedLocations$ = new core_1.EventEmitter();
    }
    LocationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getLocations()
            .then(function (files) {
            _this.locations = files;
            console.log(_this.locations);
        })
            .catch(function (e) {
            // TODO this should be fatal?
            console.log(e);
        });
    };
    // Tree component events...
    LocationsComponent.prototype.nodeSelect = function (event) {
        // this.selectedNodes.forEach(sel => {
        //     console.log('selected: ', sel.label);
        // });
        this.selectedLocations$.emit(this.selectedNodes);
        console.log('emitting node selected', this.selectedNodes);
    };
    LocationsComponent.prototype.nodeUnselect = function (event) {
        this.selectedLocations$.emit(this.selectedNodes);
        console.log('emitting node unselected', this.selectedNodes);
    };
    __decorate([
        core_1.ViewChild('expandingTree'),
        __metadata("design:type", typeof (_a = typeof primeng_1.Tree !== "undefined" && primeng_1.Tree) === "function" && _a || Object)
    ], LocationsComponent.prototype, "expandingTree", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], LocationsComponent.prototype, "selectedLocations$", void 0);
    LocationsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'location-tree',
            templateUrl: './locations.component.html'
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], LocationsComponent);
    return LocationsComponent;
    var _a;
}());
exports.LocationsComponent = LocationsComponent;
//# sourceMappingURL=locations.component.js.map