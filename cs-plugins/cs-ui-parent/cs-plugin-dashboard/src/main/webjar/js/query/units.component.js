"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mediator_service_1 = require("../shared/mediator.service");
var UnitsComponent = /** @class */ (function () {
    function UnitsComponent(service) {
        this.service = service;
        this.units = [];
        this.selectedUnits = [];
        // sending output to query runner component
        this.selectedUnits$ = new core_1.EventEmitter();
    }
    UnitsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedUnits$.emit(this.selectedUnits);
        console.log('inputed locations: ', this.locations);
        this.service.getUnitsForLocations(this.locations)
            .then(function (res) {
            _this.units = res;
            console.log('units: ', _this.units);
        })
            .catch(function (e) {
            // TODO this should be fatal?
            console.log(e);
        });
    };
    // multi select drop down events...
    UnitsComponent.prototype.onChange = function (event) {
        console.log(event);
        console.log(this.selectedUnits);
        this.selectedUnits$.emit(this.selectedUnits);
    };
    UnitsComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        console.log('new locations list are received', changes);
        this.service.getUnitsForLocations(changes.locations.currentValue)
            .then(function (res) {
            _this.units = res;
            console.log('units: ', _this.units);
        })
            .catch(function (e) {
            // TODO this should be fatal?
            console.log(e);
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], UnitsComponent.prototype, "locations", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], UnitsComponent.prototype, "selectedUnits$", void 0);
    UnitsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'units',
            templateUrl: './units.component.html'
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], UnitsComponent);
    return UnitsComponent;
}());
exports.UnitsComponent = UnitsComponent;
//# sourceMappingURL=units.component.js.map