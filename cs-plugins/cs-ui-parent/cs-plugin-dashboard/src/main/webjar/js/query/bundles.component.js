"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mediator_service_1 = require("../shared/mediator.service");
var BundlesComponent = /** @class */ (function () {
    function BundlesComponent(service) {
        this.service = service;
        this.bundles = [];
        this.selectedBundle = [];
        this.selectedBundle$ = new core_1.EventEmitter();
    }
    BundlesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.service.getBundles()
            .then(function (res) {
            console.log('bundles: ', res);
            _this.bundles = res;
        })
            .catch(function (e) {
            // TODO this should be fatal?
            console.log(e);
        });
    };
    BundlesComponent.prototype.onChange = function (event) {
        console.log('bundle selected: ', event);
        // this.selectedBundle = event;
        this.selectedBundle$.emit(this.selectedBundle);
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], BundlesComponent.prototype, "selectedBundle$", void 0);
    BundlesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'bundles',
            templateUrl: './bundles.component.html'
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], BundlesComponent);
    return BundlesComponent;
}());
exports.BundlesComponent = BundlesComponent;
//# sourceMappingURL=bundles.component.js.map