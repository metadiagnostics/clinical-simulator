/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the 'License');
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { MediatorService } from '../shared/mediator.service';
import { GridOptions } from 'ag-grid/main';

let columnDefs = [
    {headerName: 'Measure Name', field: 'name',   cellStyle: {'text-align': 'left'}, width : 340},
    {headerName: 'Measure Value', field: 'value', cellStyle: {'text-align': 'left'}},
    {headerName: 'Goal', field: 'goal', cellStyle: {'text-align': 'left'}},
    {headerName: 'Status', field: 'status', cellStyle: {'text-align': 'left'}}
];

let rowData = [
    {name: 'Catheter Associated Blood Stream Infections', value: '0.5%', goal: '<1%', status: 'Pass', type: '1'},
    {name: 'Chronic Lung Disease', value: '5 %', goal: '<5%', status: 'Marginal', type: '2'},
    {name: 'Retinopathy of Prematurity', value: '1%', goal: '<2%', status: 'Pass', type: '3'},
    {name: 'Necrotizing Enterocolitis', value: '5%', goal: '<1%', status: 'Fail', type: '4'},
    {name: 'Grade III IVH', value: '5%', goal: '<10%', status: 'Pass', type: '5'},
    {name: 'Grade IV IVH', value: '5%', goal: '<5%', status: 'Marginal', type: '6'}
];

@Component({
               moduleId: module.id
               , selector: 'unit-summary-primary-grid'
               , templateUrl: './unit.summary.grid.component.html'
               , styleUrls: ['i-o-renderer.css']
               , encapsulation: ViewEncapsulation.None
           })


export class UnitSummaryPrimaryGridComponent implements OnInit {
    private gridOptions: GridOptions;

    constructor(private service: MediatorService) {
        this.gridOptions = {
            enableSorting: true,
            rowSelection: 'single',
            enableColResize: true,
            suppressMovableColumns: true,
            columnDefs: null,
            rowData: null,
            suppressCellSelection: true,
            onRowClicked($event) {
                let type = $event.data.type;
                // console.log('secondary query component is being set to: ', type);
                // lets broadcast the secondary query component type...
                service.setSecondaryQueryType('' + type);
            }
        }
    }

    ngOnInit() {
        this.gridOptions.columnDefs = columnDefs;
        this.gridOptions.rowData  = rowData;
    }
}
