"use strict";
/*
 *  Copyright 2016 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var moment = require("moment");
var configuration_settings_1 = require("../configuration.settings");
var GridSettings = /** @class */ (function () {
    function GridSettings(currentDate, type, inputModel) {
        this.currentDate = currentDate;
        this.type = type;
        this.rows = [];
        this.columns = [];
        this.generalSettings = [];
        var dataTypes = this.getTypes(inputModel);
        if (_.isEmpty(dataTypes) || !_.isArray(dataTypes)) {
            // console.log('bad model');
            return;
        }
        var columnsDetails = this.getColumnsFromModel(inputModel, dataTypes);
        if (_.isEmpty(columnsDetails) || !_.isArray(columnsDetails)) {
            // console.log('bad model');
            return;
        }
        this.rows = this.getRowsFromModel(inputModel, dataTypes, columnsDetails);
        var columnHeaders = this.headerize(columnsDetails, _.get(inputModel, 'type'));
        this.columns = columnHeaders;
        this.thisModel = inputModel;
    }
    GridSettings.prototype.headerize = function (columnsDetails, type) {
        var retVal = [];
        var value = {};
        // TODO this is incorrect the headerName
        // and field can not be the same
        value.headerName = type;
        if (value.headerName === configuration_settings_1.ConfigSettings.I_O) {
            value.headerName = 'I & O';
        }
        value.field = type;
        // end of TODO
        // console.log('headerName: ' + value.headerName + ' field is: ' + value.field);
        retVal.push(value);
        for (var key in columnsDetails) {
            value = {};
            if (columnsDetails[key].time !== undefined) {
                value.headerName = this.format(columnsDetails[key].timestamp);
                // console.log(value.headerName);
                // no field, no way to map to the model, i.e. no mapping to
                // getTimeRanges the value for cell, from the row that is
                value.field = columnsDetails[key].timestamp;
                // console.log('headerName: ' + value.headerName + ' field is: ' + value.field);
                retVal.push(value);
            }
        }
        return retVal;
    };
    GridSettings.prototype.getRows = function () {
        return this.rows;
    };
    GridSettings.prototype.getColumns = function () {
        return this.columns;
    };
    GridSettings.prototype.getTypeFromRow = function (row) {
        var dataTypes = this.getTypes(this.thisModel);
        return dataTypes[row];
    };
    GridSettings.prototype.format = function (timestamp) {
        if (!moment(timestamp, 'YYYYMMDDHHmmss').isValid()) {
            return '';
        }
        if (this.type === configuration_settings_1.ConfigSettings.I_O) {
            return moment(timestamp, "YYYYMMDDHHmmss").format('DD-MMM HH:mm');
        }
        var retVal;
        // remove the YYYY
        retVal = timestamp.substr(4, 2);
        // if necessary, remove the zero
        // console.log(retVal.substr(0,1));
        if (retVal.substr(0, 1) === '0') {
            retVal = retVal.substr(1, 1);
        }
        retVal += '/' + timestamp.substr(6, 2) + ' ';
        retVal += timestamp.substr(8, 2);
        retVal += ':';
        retVal += timestamp.substr(10, 2);
        // retVal += ':';
        // retVal += timestamp.substr(12, 2);
        return retVal;
    };
    GridSettings.prototype.getTypes = function (inputModel) {
        var _this = this;
        var dataTypes = _.get(inputModel, 'data');
        if (_.isEmpty(dataTypes) || !_.isArray(dataTypes)) {
            return [];
        }
        var rows = [];
        var loc = 0;
        // for (let dataType in dataTypes) {
        _.forEach(dataTypes, function (dataType) {
            // console.log('dataType : ' + dataType);
            var keys = Object.keys(dataType);
            var index = _this.getGeneralSettingsIndex(keys);
            if (index !== -1) {
                // console.log('saw general Settings');
                var key = 'data[' + loc + '].generalSettings';
                var setting = _.get(inputModel, key);
                _this.generalSettings.push(setting);
                // remove the key from the keys array
                keys.splice(index, 1);
            }
            else {
                _this.generalSettings.push({});
            }
            // console.log(keys);
            rows.push(keys);
            loc++;
        });
        return rows;
    };
    GridSettings.prototype.getGeneralSettingsIndex = function (keys) {
        var i = 0;
        while (i < keys.length) {
            // console.log(keys[i]);
            if (keys[i] === 'generalSettings') {
                return i;
            }
            i++;
        }
        return -1;
    };
    GridSettings.prototype.getColumnsFromModel = function (model, dataTypes) {
        var dataTypes = _.get(model, 'data');
        if (_.isEmpty(dataTypes) || !_.isArray(dataTypes)) {
            return [];
        }
        var columns = [];
        _.forEach(dataTypes, function (dataType) {
            var types = Object.keys(dataType);
            _.forEach(types, function (type) {
                var vitalDetails = _.get(dataType, type);
                _.forEach(vitalDetails, function (vDetail) {
                    columns.push(_.get(vDetail, 'timestamp'));
                });
            });
        });
        // dedupe the columns, and sort them
        // remove any duplicates
        columns = columns.filter(function (elem, index) {
            return index === columns.indexOf(elem);
        });
        columns.sort();
        var retVal = [];
        _.forEach(columns, function (column) {
            var columnValue = {};
            if (column !== undefined) {
                columnValue.timestamp = column;
                columnValue.time = column.substr(column.indexOf(' ') + 1, column.length);
                retVal.push(columnValue);
            }
        });
        var type = _.get(model, 'type');
        retVal.unshift(type);
        return retVal;
    };
    GridSettings.prototype.getRowsFromModel = function (inputModel, dataTypes, columnsDetails) {
        var retVal = [];
        var numberOfRows = _.get(inputModel, 'data').length;
        // we have to use old fashion loop since the order is very important
        for (var rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
            var row = {};
            // create all the keys based on columns details timestamp
            // can not have duplicate columns name
            // had to go with something unique. So timestamp
            for (var j = 0; j < columnsDetails.length; j++) {
                var columnName = columnsDetails[j].timestamp;
                if (columnName !== undefined) {
                    // console.log('column name: ', columnName);
                    row[columnName] = '';
                }
            }
            // lets adjust the dataType column names with the provenance
            // if specified...
            var dataType = dataTypes[rowIndex][0];
            // let's getTimeRanges a list of all the row keys
            var keys = _.keys(row);
            var length = keys.length;
            //console.log('we have keys: ' , length);
            var I_O_type = configuration_settings_1.ConfigSettings.I_O_TYPE;
            for (var k = 0; k < length; k++) {
                var currentKey = 'data[' + rowIndex + '].' + dataType + '[' + k + ']';
                // console.log('current key: ' , currentKey);
                var newField = _.get(inputModel, currentKey);
                // console.log('newField: ' , newField);
                if (newField) {
                    row[newField.timestamp] = newField.value;
                    row.I_O_type = newField.I_O_type;
                    // some fields were being mistakenly added
                    // row[newField.value] = newField.value;
                    console.log('row is: ', row);
                }
            }
            var type = _.get(inputModel, 'type');
            var generalSetting = this.generalSettings[rowIndex];
            if (!_.isEmpty(generalSetting.provenance)) {
                dataType += ' ( ' + generalSetting.provenance + ' )';
            }
            // console.log('row type: ', dataType);
            row[type] = dataType;
            // console.log(row);
            retVal.push(row);
        }
        return retVal;
    };
    return GridSettings;
}());
exports.GridSettings = GridSettings;
//# sourceMappingURL=grid-settings.js.map