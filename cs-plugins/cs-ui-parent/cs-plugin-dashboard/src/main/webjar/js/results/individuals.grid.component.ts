/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the 'License');
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { MediatorService } from '../shared/mediator.service';
import { GridOptions } from 'ag-grid/main';

let columnDefs = [
    {headerName: 'Patient Name', field: 'name',   cellStyle: {'text-align': 'left'}, width : 340},
    {headerName: 'NCABSI', field: 'ncabsi', cellStyle: {'text-align': 'left'}},
    {headerName: 'CLD', field: 'cld', cellStyle: {'text-align': 'left'}},
    {headerName: 'ROP', field: 'rop', cellStyle: {'text-align': 'left'}},
    {headerName: 'NEC', field: 'nec', cellStyle: {'text-align': 'left'}},
    {headerName: 'IVH', field: 'ivh', cellStyle: {'text-align': 'left'}}
];

let rowData = [
    {name: 'Baby Jones', ncabsi: '-', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "1"},
    {name: 'Baby Smith', ncabsi: '-', cld: 'fail', rop: '-', nec: '-', ivh: '-', type: "2"},
    {name: 'Baby Grant', ncabsi: 'Fail', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "3"},
    {name: 'Baby Kellt', ncabsi: '-', cld: 'Fail', rop: '-', nec: 'Fail', ivh: '-', type: "4"},
    {name: 'Baby Trump', ncabsi: '-', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "5"},
    {name: 'Baby Obama', ncabsi: '-', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "6"}
];

@Component({
               moduleId: module.id
               , selector: 'individuals-primary-grid'
               , templateUrl: './individuals.grid.component.html'
               , styleUrls: ['i-o-renderer.css']
               , encapsulation: ViewEncapsulation.None
           })


export class IndividualsPrimaryGridComponent implements OnInit {
    private gridOptions: GridOptions;

    constructor(private service: MediatorService) {
        this.gridOptions = {
            enableSorting: true,
            rowSelection: 'single',
            enableColResize: true,
            suppressMovableColumns: true,
            columnDefs: null,
            rowData: null,
            suppressCellSelection: true,
            onRowClicked($event) {
                let type = $event.data.type;
                // console.log('secondary query component is being set to: ', type);
                // lets broadcast the secondary query component type...
                service.setSecondaryQueryType('' + type);
            }
        }
    }

    ngOnInit() {
        this.gridOptions.columnDefs = columnDefs;
        this.gridOptions.rowData  = rowData;
    }
}
