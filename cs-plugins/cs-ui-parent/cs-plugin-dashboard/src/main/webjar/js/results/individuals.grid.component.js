"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the 'License');
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mediator_service_1 = require("../shared/mediator.service");
var columnDefs = [
    { headerName: 'Patient Name', field: 'name', cellStyle: { 'text-align': 'left' }, width: 340 },
    { headerName: 'NCABSI', field: 'ncabsi', cellStyle: { 'text-align': 'left' } },
    { headerName: 'CLD', field: 'cld', cellStyle: { 'text-align': 'left' } },
    { headerName: 'ROP', field: 'rop', cellStyle: { 'text-align': 'left' } },
    { headerName: 'NEC', field: 'nec', cellStyle: { 'text-align': 'left' } },
    { headerName: 'IVH', field: 'ivh', cellStyle: { 'text-align': 'left' } }
];
var rowData = [
    { name: 'Baby Jones', ncabsi: '-', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "1" },
    { name: 'Baby Smith', ncabsi: '-', cld: 'fail', rop: '-', nec: '-', ivh: '-', type: "2" },
    { name: 'Baby Grant', ncabsi: 'Fail', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "3" },
    { name: 'Baby Kellt', ncabsi: '-', cld: 'Fail', rop: '-', nec: 'Fail', ivh: '-', type: "4" },
    { name: 'Baby Trump', ncabsi: '-', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "5" },
    { name: 'Baby Obama', ncabsi: '-', cld: '-', rop: '-', nec: 'Fail', ivh: '-', type: "6" }
];
var IndividualsPrimaryGridComponent = /** @class */ (function () {
    function IndividualsPrimaryGridComponent(service) {
        this.service = service;
        this.gridOptions = {
            enableSorting: true,
            rowSelection: 'single',
            enableColResize: true,
            suppressMovableColumns: true,
            columnDefs: null,
            rowData: null,
            suppressCellSelection: true,
            onRowClicked: function ($event) {
                var type = $event.data.type;
                // console.log('secondary query component is being set to: ', type);
                // lets broadcast the secondary query component type...
                service.setSecondaryQueryType('' + type);
            }
        };
    }
    IndividualsPrimaryGridComponent.prototype.ngOnInit = function () {
        this.gridOptions.columnDefs = columnDefs;
        this.gridOptions.rowData = rowData;
    };
    IndividualsPrimaryGridComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'individuals-primary-grid',
            templateUrl: './individuals.grid.component.html',
            styleUrls: ['i-o-renderer.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], IndividualsPrimaryGridComponent);
    return IndividualsPrimaryGridComponent;
}());
exports.IndividualsPrimaryGridComponent = IndividualsPrimaryGridComponent;
//# sourceMappingURL=individuals.grid.component.js.map