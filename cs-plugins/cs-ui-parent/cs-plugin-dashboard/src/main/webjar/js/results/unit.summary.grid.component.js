"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the 'License');
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an 'AS IS' BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var mediator_service_1 = require("../shared/mediator.service");
var columnDefs = [
    { headerName: 'Measure Name', field: 'name', cellStyle: { 'text-align': 'left' }, width: 340 },
    { headerName: 'Measure Value', field: 'value', cellStyle: { 'text-align': 'left' } },
    { headerName: 'Goal', field: 'goal', cellStyle: { 'text-align': 'left' } },
    { headerName: 'Status', field: 'status', cellStyle: { 'text-align': 'left' } }
];
var rowData = [
    { name: 'Catheter Associated Blood Stream Infections', value: '0.5%', goal: '<1%', status: 'Pass', type: '1' },
    { name: 'Chronic Lung Disease', value: '5 %', goal: '<5%', status: 'Marginal', type: '2' },
    { name: 'Retinopathy of Prematurity', value: '1%', goal: '<2%', status: 'Pass', type: '3' },
    { name: 'Necrotizing Enterocolitis', value: '5%', goal: '<1%', status: 'Fail', type: '4' },
    { name: 'Grade III IVH', value: '5%', goal: '<10%', status: 'Pass', type: '5' },
    { name: 'Grade IV IVH', value: '5%', goal: '<5%', status: 'Marginal', type: '6' }
];
var UnitSummaryPrimaryGridComponent = /** @class */ (function () {
    function UnitSummaryPrimaryGridComponent(service) {
        this.service = service;
        this.gridOptions = {
            enableSorting: true,
            rowSelection: 'single',
            enableColResize: true,
            suppressMovableColumns: true,
            columnDefs: null,
            rowData: null,
            suppressCellSelection: true,
            onRowClicked: function ($event) {
                var type = $event.data.type;
                // console.log('secondary query component is being set to: ', type);
                // lets broadcast the secondary query component type...
                service.setSecondaryQueryType('' + type);
            }
        };
    }
    UnitSummaryPrimaryGridComponent.prototype.ngOnInit = function () {
        this.gridOptions.columnDefs = columnDefs;
        this.gridOptions.rowData = rowData;
    };
    UnitSummaryPrimaryGridComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'unit-summary-primary-grid',
            templateUrl: './unit.summary.grid.component.html',
            styleUrls: ['i-o-renderer.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [mediator_service_1.MediatorService])
    ], UnitSummaryPrimaryGridComponent);
    return UnitSummaryPrimaryGridComponent;
}());
exports.UnitSummaryPrimaryGridComponent = UnitSummaryPrimaryGridComponent;
//# sourceMappingURL=unit.summary.grid.component.js.map