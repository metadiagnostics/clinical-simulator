"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var ScenarioConfig = /** @class */ (function () {
    function ScenarioConfig() {
        this.scenarioListItems = [];
        this.uploadedMedia = {};
        this.deletedScenarios = [];
        this.importedScenarioName = '';
    }
    ScenarioConfig.prototype.reset = function (scenarioNames) {
        this.uploadedMedia = {};
        this.deletedScenarios = [];
        this.scenarioListItems = _.map([], _.clone);
        this.initialize(scenarioNames);
    };
    ScenarioConfig.prototype.getImportedFileDetails = function () {
        return this.uploadedMedia;
    };
    ScenarioConfig.prototype.clear = function () {
        this.scenarioListItems = [];
        this.selectedScenario = '';
        this.deletedScenarios = [];
        this.importedScenarioName = '';
    };
    ScenarioConfig.prototype.getDeletedScenarios = function () {
        return this.deletedScenarios;
    };
    ScenarioConfig.prototype.initialize = function (names) {
        this.scenarioListItems = [];
        var that = this;
        _.forEach(names, function (name) {
            var entry = {};
            entry.code = name;
            entry.name = name;
            that.scenarioListItems.push(entry);
        });
    };
    ScenarioConfig.prototype.getImportedScenarioName = function () {
        return this.importedScenarioName;
    };
    ScenarioConfig.prototype.delete = function () {
        var temp = _.map(this.scenarioListItems, _.clone);
        this.scenarioListItems = _.map(temp, _.clone);
        var nameToMatch = this.selectedScenario.name;
        // console.log(nameToMatch);
        this.scenarioListItems = _.remove(this.scenarioListItems, function (item) {
            return item.name !== nameToMatch;
        });
        this.deletedScenarios.push(nameToMatch);
        return true;
    };
    ScenarioConfig.prototype.import = function (file) {
        this.readContent(file);
        this.importedScenarioName = file.name;
    };
    ScenarioConfig.prototype.cacheImportedFile = function (name, data) {
        this.uploadedMedia.name = name;
        this.uploadedMedia.type = 'text';
        this.uploadedMedia.data = data;
    };
    ScenarioConfig.prototype.readContent = function (file) {
        var reader = new FileReader();
        var that = this;
        reader.onload = (function (reader) {
            return function (e) {
                var contents = reader.result;
                // console.log('lines read are: ', contents);
                that.cacheImportedFile(file.name, contents);
                that.addScenario(file.name);
            };
        })(reader);
        var result = reader.readAsText(file);
    };
    ScenarioConfig.prototype.addScenario = function (name) {
        var copyOfScenarioNames = [];
        var scenarioListItems = this.scenarioListItems;
        _.forEach(scenarioListItems, function (item) {
            copyOfScenarioNames.push(item);
        });
        copyOfScenarioNames.push({ name: name, code: name });
        // console.log(copyOfScenarioNames);
        // if we just simply add to the original list, the update does not show...
        this.scenarioListItems = _.map(copyOfScenarioNames, _.clone);
    };
    return ScenarioConfig;
}());
exports.ScenarioConfig = ScenarioConfig;
//# sourceMappingURL=scenario.config.js.map