/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as _ from 'lodash';

export interface ListItem {
    name: string;
    code: string;
}

export interface UploadedMedia {
    data: string;
    type: string;
    name: string;
}

export class ScenarioConfig {
    public scenarioListItems: Array<ListItem> = [];
    public selectedScenario: any;

    private uploadedMedia = {}  as UploadedMedia;
    private deletedScenarios : Array<string> = [];
    private importedScenarioName : string = '';

    public reset(scenarioNames: Array<string>) : void {
        this.uploadedMedia = {} as UploadedMedia;
        this.deletedScenarios = [];

        this.scenarioListItems = _.map([], _.clone);
        this.initialize(scenarioNames);

    }

    public getImportedFileDetails () : UploadedMedia {
        return this.uploadedMedia;
    }

    public clear() : void {
        this.scenarioListItems = [];
        this.selectedScenario = '';
        this.deletedScenarios = [];
        this.importedScenarioName = '';
    }

    public getDeletedScenarios() : Array<string> {
        return this.deletedScenarios;
    }

    public initialize(names: Array<string>) : void {
        this.scenarioListItems = [];
        let that = this;
        _.forEach(names, function(name ) {
            let entry = {} as ListItem;
            entry.code = name;
            entry.name = name;
            that.scenarioListItems.push(entry);
        });
    }

    public getImportedScenarioName() : string {
        return this.importedScenarioName;
    }

    public delete(): boolean {
        let temp : Array<string> = _.map(this.scenarioListItems, _.clone);
        this.scenarioListItems = _.map(temp, _.clone);
        let nameToMatch = this.selectedScenario.name;
        // console.log(nameToMatch);

        this.scenarioListItems = _.remove(this.scenarioListItems, function (item) {
            return item.name  !== nameToMatch;
        });

        this.deletedScenarios.push(nameToMatch);
        return true;
    }

    public import(file: File): void {
        this.readContent(file);
        this.importedScenarioName = file.name;
    }

    private cacheImportedFile(name: string, data: any) {
        this.uploadedMedia.name = name;
        this.uploadedMedia.type = 'text';
        this.uploadedMedia.data = data;
    }

    private readContent(file: File) : void {
        var reader = new FileReader();
        let that = this;
        reader.onload = (function(reader)
        {
            return function(e)
            {
                var contents = reader.result;
                // console.log('lines read are: ', contents);
                that.cacheImportedFile(file.name, contents);
                that.addScenario(file.name);
            }
        })(reader);

        let result = reader.readAsText(file);
    }

    private addScenario(name: string) : void {
        let copyOfScenarioNames : Array<ListItem> = [];

        let scenarioListItems = this.scenarioListItems;
        _.forEach(scenarioListItems, function(item ) {
            copyOfScenarioNames.push(item);
        });

        copyOfScenarioNames.push({name: name, code: name});
        // console.log(copyOfScenarioNames);

        // if we just simply add to the original list, the update does not show...
        this.scenarioListItems = _.map(copyOfScenarioNames, _.clone);
    }
}