"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var cs_plugin_common_1 = require("cs-plugin-common");
var _ = require("lodash");
var ScenarioTableComponent = /** @class */ (function () {
    function ScenarioTableComponent() {
        this.modelProcessor = null;
        this.grid = null;
        this.gridOptions = {};
        this.uuid = null;
        this.id = null;
        this.showContent = false;
        this.title = '';
        this.modality = false;
        this.width = 500;
        this.height = 300;
        this.columns = [];
    }
    ScenarioTableComponent.prototype.setTitle = function (title) {
        this.title = title;
    };
    ScenarioTableComponent.prototype.setWidth = function (width) {
        this.width = width;
    };
    ScenarioTableComponent.prototype.setHeight = function (height) {
        this.height = height;
    };
    ScenarioTableComponent.prototype.setModal = function (modality) {
        this.modality = modality;
    };
    ScenarioTableComponent.prototype.show = function (show) {
        if (show === void 0) { show = true; }
        this.showContent = show;
    };
    ScenarioTableComponent.prototype.initialize = function (modelProcessor, grid, gridOptions) {
        this.uuid = cs_plugin_common_1.Guid.newGuid();
        this.id = cs_plugin_common_1.Guid.newGuid();
        this.modelProcessor = modelProcessor;
        this.columns = modelProcessor.getColumns();
        this.grid = grid;
        _.assign(this.gridOptions, gridOptions);
        this.grid.initialize(this.gridOptions, this.uuid);
    };
    ScenarioTableComponent.prototype.process = function (model) {
        // console.log('ScenarioTableComponent process');
        this.modelProcessor.process(model);
        var rows = this.modelProcessor.getRows();
        var columns = this.modelProcessor.getColumns();
        this.grid.refreshGrid(this.gridOptions, rows, columns);
        if (!_.isEmpty(rows)) {
            this.grid.selectFirstRow(this.gridOptions);
        }
    };
    ScenarioTableComponent.prototype.setModel = function (model) {
        this.process(model);
    };
    ScenarioTableComponent.prototype.clear = function () {
        console.log('clear the grid');
        // TODO why is this not working...
        // Should clear the grid
        // asked the ag-grid tech support and they
        // say it works in their sample which is correct
        this.gridOptions.rowData = [];
        this.gridOptions.columnDefs = [];
        this.gridOptions.api.setRowData(this.gridOptions.rowData);
        this.gridOptions.api.setColumnDefs(this.gridOptions.columnDefs);
    };
    ScenarioTableComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'scenario-dialog',
            templateUrl: './scenario.table.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ScenarioTableComponent);
    return ScenarioTableComponent;
}());
exports.ScenarioTableComponent = ScenarioTableComponent;
//# sourceMappingURL=scenario.table.component.js.map