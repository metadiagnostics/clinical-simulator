/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridOptions } from 'ag-grid';
import { AgGridModule } from 'ag-grid-angular';

import { ButtonModule } from 'primeng/components/button/button';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ListboxModule } from 'primeng/components/listbox/listbox';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { SelectItem } from 'primeng/primeng';
import { TableModule, Guid, Utilities, IBackendService, ibackendProvider } from 'cs-plugin-common';
import { ScenarioInstancesModelProcessor } from './model/scenario.instances.model.processor';
import { ScenarioStepsModelProcessor } from './model/scenario.steps.model.processor';
import { ScenarioStepsGridRenderer } from './renderer/scenario.steps.grid.renderer';
import { ScenarioInstancesGridRenderer } from './renderer/scenario.instances.grid.renderer';
import { ScenarioConfig } from './util/scenario.config';
import { ScenarioTableComponent } from './scenario.table.component';


import * as _ from 'lodash';
const STOPPED : string  = 'STOPPED';

@Component({
    moduleId: module.id,
    selector: 'plugin',
    templateUrl: 'plugin.component.html',
    styleUrls: ['./plugin.component.css'],
    encapsulation: ViewEncapsulation.None

})
export class PluginComponent {
    @ViewChild('scenarioRunnerPlugin')
    private root;

    @ViewChild('scenarioStepsComp')
    private scenarioSteps;

    @ViewChild('ranScenarioComp')
    private ranScenarios;

    public scenario: SelectItem;
    private scenarios: SelectItem[] = [];

    private currentState: String = '';
    public showSettings: boolean = false;

    private uuid: any = '';
    private scenarioConfig: ScenarioConfig;
    private cleanUpInProgress : boolean = false;
    private errorMessage : string = '';


    constructor(private backendService: IBackendService) {
        this.uuid = Guid.newGuid();
        this.currentState = 'STOPPED';
        this.scenarioConfig = new ScenarioConfig();
    }

    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeCurrentScenarioSteps();
        this.initializeRanScenarios();
        if (Utilities.isRunningStandalone()) {

            this.scenarios = [
                {label: 'scenario1', value: 'scenario1'}
                , {label: 'scenario2', value: 'scenario2'}
                , {label: 'scenario3', value: 'scenario3'}
                , {label: 'scenario4', value: 'scenario4'}
                , {label: 'scenario5', value: 'scenario5'}
            ];
            this.setupListOfScenariosForConfigDialog();
        } else {
            this.requestScenarioList();
        }
    }

    public import($event): void {
        // we will handle only one file per import...

        let file = $event.target.files[0];

        if (file.name.length > 100) {
            this.errorMessage = 'File name cannot be more than 100 characters';
        } else {
            this.errorMessage = '';
            this.scenarioConfig.import(file);
        }
    }

    public showConfigurations(): void {
        this.errorMessage = '';
        this.showSettings = true;
    }

    public selectScenarioDeleteDisabled() : boolean {
        return _.isEmpty(this.scenarioConfig.selectedScenario);
    }

    public close(save: boolean): void {
        this.showSettings = false;

        if (save) {
            this.processTheConfigurationChanges();
        }

        this.scenarioConfig.reset(this.getScenarioNames());
    }

    public runDisabled(): boolean {
        if (this.cleanUpInProgress) {
            return true;
        }
        let retVal: boolean = _.isEmpty(this.scenario) || this.currentState === 'RUNNING';
        return retVal;
    }

    public pauseDisabled(): boolean {
        if (this.cleanUpInProgress) {
            return true;
        }
        let retVal: boolean = _.isEmpty(this.scenario) || this.currentState !== 'RUNNING';
        return retVal;
    }

    public stopDisabled(): boolean {
        if (this.cleanUpInProgress) {
            return true;
        }
        let retVal: boolean = _.isEmpty(this.scenario) || this.currentState !== 'RUNNING';
        return retVal;
    }

    public showScenarioSteps(): void {
        this.scenarioSteps.show();
    }

    public showScenarioRunInstances() : void {
        let params: any = {
            _callback: 'scenarioRunInstancesResponse'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('retrieveScenarioRunInstances', params);

    }

    // begin server commands
    public run(): void {
        console.log('run scenario' + this.scenario);
        this.clearSteps();
        let params: any = {
            scenario: this.scenario
            , _callback: 'runScenarioResponse'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('runScenario', params);
    }

    public pause(): void {
        let params: any = {
            _callback: 'pauseScenarioResponse'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('pauseScenario', params);
    }

    public stop(): void {
        let params: any = {
            _callback: 'stopScenarioResponse'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('stopScenario', params);
    }

    public deleteScenarioInstance() : void {

        let params: any = {
            _callback: 'deleteScenarioInstanceResponse'
            , id: 'c091cac4-68c9-4a1a-87dd-30d1209c2304'
            , uuid: this.uuid
        };

        // this.backendService.sendBackendEvent('deleteScenarioInstance', params);
    }

    private importScenario() : void {
        let fileDetails = this.scenarioConfig.getImportedFileDetails();
        if (_.isEmpty(fileDetails)) {
            return;
        }

        let params: any = {
            name: fileDetails.name
            , type: fileDetails.type
            , data: fileDetails.data
            , _callback: 'importScenarioResponse'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('importScenario', params);
    }

    private deleteScenario() : void {
        // one delete at a time
        let deleteScenarioRequests: Array<string>  = this.scenarioConfig.getDeletedScenarios();
        if (_.isEmpty(deleteScenarioRequests)) {
            return;
        }

        console.log('need to delete: ', deleteScenarioRequests[0]);
        let params: any = {
            scenario: deleteScenarioRequests[0]
            , _callback: 'deleteScenarioResponse'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('deleteScenario', params);
    }
    // end of server commands

    // start of fujion callbacks...
    public processScenarioListResponse(response: any): void {
        if (_.isEmpty(response.scenarios)) {
            console.error('got no scenarios from the server');
            return;
        }


        let scenarioNames = _.values(response.scenarios);

        if (!_.isEmpty(scenarioNames)) {
            for (let i = 0; i < scenarioNames.length; i++) {
                let entry: any = {};
                entry.value = entry.label = scenarioNames[i];
                this.scenarios.push(entry);
            }
            // this.scenario = this.scenariosListItem[0].value;
        }
        this.setupListOfScenariosForConfigDialog();
    }

    public runScenarioResponse(response: any): void {
        console.log(`Scenario ${this.scenario} is running`);
        this.currentState = 'RUNNING';
    }

    public pauseScenarioResponse(response: any): void {
        console.log(`Scenario ${this.scenario} is paused`);
        this.currentState = 'PAUSED';
    }

    public stopScenarioResponse(response: any): void {
        console.log(`Scenario ${this.scenario} is stopped`);
        this.currentState = 'STOPPED';
    }

    public stateChangeHandler(response: any): void {
        console.log(`Scenario ${this.scenario} state is changed to ${response.state}`);
        this.currentState = response.state;
    }

    public stepsUpdateHandler(response: any): void {
        this.scenarioSteps.setModel(response.data);
    }

    public importScenarioResponse(response: any) : void {
    }

    public scenarioRunInstancesResponse(response: any) : void {
        this.ranScenarios.setModel(response.data);
        this.ranScenarios.show();
    }

    public deleteScenarioResponse(response: any) :void {
    }
    // end of fujion callbacks...

    private setupListOfScenariosForConfigDialog() : void {
        let scenarios = this.scenarios;
        let names : Array<string> = [];
        _.forEach(scenarios, function(scenario) {
            names.push(scenario.label);
        });

        this.scenarioConfig.initialize(names);
    }

    private clearSteps(): void {
        this.scenarioSteps.clear();
    }

    private requestScenarioList(): void {
        let params: any = {
            _callback: 'processScenarioListResponse'
            , _stateChangecallback: 'stateChangeHandler'
            , _stepsCallback: 'stepsUpdateHandler'
            , endpoint: 'scenariosListItem'
            , uuid: this.uuid
        };

        this.backendService.sendBackendEvent('retrieveScenarioList', params);
    }

    private getScenarioNames() : Array<string> {
        let retVal : Array<string> = [];
        let scenarios = this.scenarios;
        _.forEach(scenarios, function(scenario) {
            retVal.push(scenario.label);
        });

        return retVal;
    }

    private processTheConfigurationChanges(): void {
        this.importScenario();
        this.deleteScenario();
        this.updateScenarioList();
    }

    private deleteScenarioByName(name) : void {
        this.scenarios = _.remove(this.scenarios, function (item) {
            return item.value  !== name;
        });
    }

    // this is called after an OK has been issued on the config change dialog
    private updateScenarioList() : void {
        // update the list of scenarios...
        let importedScenario: string = this.scenarioConfig.getImportedScenarioName();

        // do the adds
        if (!_.isEmpty(importedScenario)) {
            let item: any = {};
            item.label = item.value = importedScenario;
            this.scenarios.push(item);
        }

        // do the deletes
        let deletedScenarios : Array<string> = this.scenarioConfig.getDeletedScenarios();
        if (!_.isEmpty(deletedScenarios)) {
            let that = this;
            _.forEach(deletedScenarios, function(deletedScenario) {
                that.deleteScenarioByName(deletedScenario);
            });
        }
        this.scenarioConfig.clear();
    }

    private initializeCurrentScenarioSteps() : void {

        let gridOptions = {} as GridOptions;

        gridOptions.enableSorting = true;
        gridOptions.suppressColumnVirtualisation = true;
        gridOptions.rowSelection = 'none';
        gridOptions.suppressMovableColumns = false;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.enableColResize = true;
        gridOptions.pagination = true;
        gridOptions.paginationPageSize = 30;

        this.scenarioSteps.setTitle('Current Scenario Steps ...');
        this.scenarioSteps.setWidth(600);
        this.scenarioSteps.setHeight(500);
        this.scenarioSteps.setModal(false);
        this.scenarioSteps.initialize(
            new ScenarioStepsModelProcessor([
                                                { headerName: 'Index', field: 'index'},
                                                { headerName: 'Name', field: 'name'},
                                                { headerName: 'Time', field: 'time'}
                                            ])
            , new ScenarioStepsGridRenderer()
            , gridOptions);
    }

    private initializeRanScenarios() : void {
        let gridOptions = {} as GridOptions;

        gridOptions.onRowDoubleClicked = ($event) => {
            console.log($event);
            // this.scenarios.display(index);
        }

        gridOptions.onSelectionChanged = ($event) => {
            console.log($event);
            let selectedRows = gridOptions.api.getSelectedRows();
            selectedRows.forEach( function(selectedRow, index) {
                console.log(selectedRow);
                // this.scenarios.display(index);
            });

        }

        gridOptions.enableSorting = true;
        gridOptions.suppressColumnVirtualisation = true;
        gridOptions.rowSelection = 'single';
        gridOptions.suppressMovableColumns = false;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.enableColResize = true;

        this.ranScenarios.setTitle('Scenarios Instances ...');
        this.ranScenarios.setWidth(460);
        this.ranScenarios.setHeight(300);
        this.ranScenarios.setModal(true);
        this.ranScenarios.initialize(
            new ScenarioInstancesModelProcessor([
                                                    { headerName: 'Name', field: 'name'},
                                                    { headerName: 'Time', field: 'startTime'}
                                                    ])
            , new ScenarioInstancesGridRenderer()
            , gridOptions);
    }
}



let ngModule = {
    imports: [
        BrowserModule
        , CommonModule
        , HttpModule
        , FormsModule
        , BrowserAnimationsModule
        , TableModule
        , ButtonModule
        , DropdownModule
        , DialogModule
        , ListboxModule
        , TooltipModule
        , AgGridModule.withComponents(
            [])
    ],
    providers: [
        ibackendProvider
    ],
    declarations: [
        PluginComponent
        , ScenarioTableComponent
    ]
};
export {PluginComponent as AngularComponent, ngModule};

