"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var animations_1 = require("@angular/platform-browser/animations");
var ag_grid_angular_1 = require("ag-grid-angular");
var button_1 = require("primeng/components/button/button");
var dropdown_1 = require("primeng/components/dropdown/dropdown");
var dialog_1 = require("primeng/components/dialog/dialog");
var listbox_1 = require("primeng/components/listbox/listbox");
var tooltip_1 = require("primeng/components/tooltip/tooltip");
var cs_plugin_common_1 = require("cs-plugin-common");
var scenario_instances_model_processor_1 = require("./model/scenario.instances.model.processor");
var scenario_steps_model_processor_1 = require("./model/scenario.steps.model.processor");
var scenario_steps_grid_renderer_1 = require("./renderer/scenario.steps.grid.renderer");
var scenario_instances_grid_renderer_1 = require("./renderer/scenario.instances.grid.renderer");
var scenario_config_1 = require("./util/scenario.config");
var scenario_table_component_1 = require("./scenario.table.component");
var _ = require("lodash");
var STOPPED = 'STOPPED';
var PluginComponent = /** @class */ (function () {
    function PluginComponent(backendService) {
        this.backendService = backendService;
        this.scenarios = [];
        this.currentState = '';
        this.showSettings = false;
        this.uuid = '';
        this.cleanUpInProgress = false;
        this.errorMessage = '';
        this.uuid = cs_plugin_common_1.Guid.newGuid();
        this.currentState = 'STOPPED';
        this.scenarioConfig = new scenario_config_1.ScenarioConfig();
    }
    PluginComponent.prototype.ngOnInit = function () {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeCurrentScenarioSteps();
        this.initializeRanScenarios();
        if (cs_plugin_common_1.Utilities.isRunningStandalone()) {
            this.scenarios = [
                { label: 'scenario1', value: 'scenario1' },
                { label: 'scenario2', value: 'scenario2' },
                { label: 'scenario3', value: 'scenario3' },
                { label: 'scenario4', value: 'scenario4' },
                { label: 'scenario5', value: 'scenario5' }
            ];
            this.setupListOfScenariosForConfigDialog();
        }
        else {
            this.requestScenarioList();
        }
    };
    PluginComponent.prototype.import = function ($event) {
        // we will handle only one file per import...
        var file = $event.target.files[0];
        if (file.name.length > 100) {
            this.errorMessage = 'File name cannot be more than 100 characters';
        }
        else {
            this.errorMessage = '';
            this.scenarioConfig.import(file);
        }
    };
    PluginComponent.prototype.showConfigurations = function () {
        this.errorMessage = '';
        this.showSettings = true;
    };
    PluginComponent.prototype.selectScenarioDeleteDisabled = function () {
        return _.isEmpty(this.scenarioConfig.selectedScenario);
    };
    PluginComponent.prototype.close = function (save) {
        this.showSettings = false;
        if (save) {
            this.processTheConfigurationChanges();
        }
        this.scenarioConfig.reset(this.getScenarioNames());
    };
    PluginComponent.prototype.runDisabled = function () {
        if (this.cleanUpInProgress) {
            return true;
        }
        var retVal = _.isEmpty(this.scenario) || this.currentState === 'RUNNING';
        return retVal;
    };
    PluginComponent.prototype.pauseDisabled = function () {
        if (this.cleanUpInProgress) {
            return true;
        }
        var retVal = _.isEmpty(this.scenario) || this.currentState !== 'RUNNING';
        return retVal;
    };
    PluginComponent.prototype.stopDisabled = function () {
        if (this.cleanUpInProgress) {
            return true;
        }
        var retVal = _.isEmpty(this.scenario) || this.currentState !== 'RUNNING';
        return retVal;
    };
    PluginComponent.prototype.showScenarioSteps = function () {
        this.scenarioSteps.show();
    };
    PluginComponent.prototype.showScenarioRunInstances = function () {
        var params = {
            _callback: 'scenarioRunInstancesResponse',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('retrieveScenarioRunInstances', params);
    };
    // begin server commands
    PluginComponent.prototype.run = function () {
        console.log('run scenario' + this.scenario);
        this.clearSteps();
        var params = {
            scenario: this.scenario,
            _callback: 'runScenarioResponse',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('runScenario', params);
    };
    PluginComponent.prototype.pause = function () {
        var params = {
            _callback: 'pauseScenarioResponse',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('pauseScenario', params);
    };
    PluginComponent.prototype.stop = function () {
        var params = {
            _callback: 'stopScenarioResponse',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('stopScenario', params);
    };
    PluginComponent.prototype.deleteScenarioInstance = function () {
        var params = {
            _callback: 'deleteScenarioInstanceResponse',
            id: 'c091cac4-68c9-4a1a-87dd-30d1209c2304',
            uuid: this.uuid
        };
        // this.backendService.sendBackendEvent('deleteScenarioInstance', params);
    };
    PluginComponent.prototype.importScenario = function () {
        var fileDetails = this.scenarioConfig.getImportedFileDetails();
        if (_.isEmpty(fileDetails)) {
            return;
        }
        var params = {
            name: fileDetails.name,
            type: fileDetails.type,
            data: fileDetails.data,
            _callback: 'importScenarioResponse',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('importScenario', params);
    };
    PluginComponent.prototype.deleteScenario = function () {
        // one delete at a time
        var deleteScenarioRequests = this.scenarioConfig.getDeletedScenarios();
        if (_.isEmpty(deleteScenarioRequests)) {
            return;
        }
        console.log('need to delete: ', deleteScenarioRequests[0]);
        var params = {
            scenario: deleteScenarioRequests[0],
            _callback: 'deleteScenarioResponse',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('deleteScenario', params);
    };
    // end of server commands
    // start of fujion callbacks...
    PluginComponent.prototype.processScenarioListResponse = function (response) {
        if (_.isEmpty(response.scenarios)) {
            console.error('got no scenarios from the server');
            return;
        }
        var scenarioNames = _.values(response.scenarios);
        if (!_.isEmpty(scenarioNames)) {
            for (var i = 0; i < scenarioNames.length; i++) {
                var entry = {};
                entry.value = entry.label = scenarioNames[i];
                this.scenarios.push(entry);
            }
            // this.scenario = this.scenariosListItem[0].value;
        }
        this.setupListOfScenariosForConfigDialog();
    };
    PluginComponent.prototype.runScenarioResponse = function (response) {
        console.log("Scenario " + this.scenario + " is running");
        this.currentState = 'RUNNING';
    };
    PluginComponent.prototype.pauseScenarioResponse = function (response) {
        console.log("Scenario " + this.scenario + " is paused");
        this.currentState = 'PAUSED';
    };
    PluginComponent.prototype.stopScenarioResponse = function (response) {
        console.log("Scenario " + this.scenario + " is stopped");
        this.currentState = 'STOPPED';
    };
    PluginComponent.prototype.stateChangeHandler = function (response) {
        console.log("Scenario " + this.scenario + " state is changed to " + response.state);
        this.currentState = response.state;
    };
    PluginComponent.prototype.stepsUpdateHandler = function (response) {
        this.scenarioSteps.setModel(response.data);
    };
    PluginComponent.prototype.importScenarioResponse = function (response) {
    };
    PluginComponent.prototype.scenarioRunInstancesResponse = function (response) {
        this.ranScenarios.setModel(response.data);
        this.ranScenarios.show();
    };
    PluginComponent.prototype.deleteScenarioResponse = function (response) {
    };
    // end of fujion callbacks...
    PluginComponent.prototype.setupListOfScenariosForConfigDialog = function () {
        var scenarios = this.scenarios;
        var names = [];
        _.forEach(scenarios, function (scenario) {
            names.push(scenario.label);
        });
        this.scenarioConfig.initialize(names);
    };
    PluginComponent.prototype.clearSteps = function () {
        this.scenarioSteps.clear();
    };
    PluginComponent.prototype.requestScenarioList = function () {
        var params = {
            _callback: 'processScenarioListResponse',
            _stateChangecallback: 'stateChangeHandler',
            _stepsCallback: 'stepsUpdateHandler',
            endpoint: 'scenariosListItem',
            uuid: this.uuid
        };
        this.backendService.sendBackendEvent('retrieveScenarioList', params);
    };
    PluginComponent.prototype.getScenarioNames = function () {
        var retVal = [];
        var scenarios = this.scenarios;
        _.forEach(scenarios, function (scenario) {
            retVal.push(scenario.label);
        });
        return retVal;
    };
    PluginComponent.prototype.processTheConfigurationChanges = function () {
        this.importScenario();
        this.deleteScenario();
        this.updateScenarioList();
    };
    PluginComponent.prototype.deleteScenarioByName = function (name) {
        this.scenarios = _.remove(this.scenarios, function (item) {
            return item.value !== name;
        });
    };
    // this is called after an OK has been issued on the config change dialog
    PluginComponent.prototype.updateScenarioList = function () {
        // update the list of scenarios...
        var importedScenario = this.scenarioConfig.getImportedScenarioName();
        // do the adds
        if (!_.isEmpty(importedScenario)) {
            var item = {};
            item.label = item.value = importedScenario;
            this.scenarios.push(item);
        }
        // do the deletes
        var deletedScenarios = this.scenarioConfig.getDeletedScenarios();
        if (!_.isEmpty(deletedScenarios)) {
            var that_1 = this;
            _.forEach(deletedScenarios, function (deletedScenario) {
                that_1.deleteScenarioByName(deletedScenario);
            });
        }
        this.scenarioConfig.clear();
    };
    PluginComponent.prototype.initializeCurrentScenarioSteps = function () {
        var gridOptions = {};
        gridOptions.enableSorting = true;
        gridOptions.suppressColumnVirtualisation = true;
        gridOptions.rowSelection = 'none';
        gridOptions.suppressMovableColumns = false;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.enableColResize = true;
        gridOptions.pagination = true;
        gridOptions.paginationPageSize = 30;
        this.scenarioSteps.setTitle('Current Scenario Steps ...');
        this.scenarioSteps.setWidth(600);
        this.scenarioSteps.setHeight(500);
        this.scenarioSteps.setModal(false);
        this.scenarioSteps.initialize(new scenario_steps_model_processor_1.ScenarioStepsModelProcessor([
            { headerName: 'Index', field: 'index' },
            { headerName: 'Name', field: 'name' },
            { headerName: 'Time', field: 'time' }
        ]), new scenario_steps_grid_renderer_1.ScenarioStepsGridRenderer(), gridOptions);
    };
    PluginComponent.prototype.initializeRanScenarios = function () {
        var gridOptions = {};
        gridOptions.onRowDoubleClicked = function ($event) {
            console.log($event);
            // this.scenarios.display(index);
        };
        gridOptions.onSelectionChanged = function ($event) {
            console.log($event);
            var selectedRows = gridOptions.api.getSelectedRows();
            selectedRows.forEach(function (selectedRow, index) {
                console.log(selectedRow);
                // this.scenarios.display(index);
            });
        };
        gridOptions.enableSorting = true;
        gridOptions.suppressColumnVirtualisation = true;
        gridOptions.rowSelection = 'single';
        gridOptions.suppressMovableColumns = false;
        gridOptions.suppressCellSelection = true;
        gridOptions.suppressNoRowsOverlay = false;
        gridOptions.suppressLoadingOverlay = false;
        gridOptions.enableColResize = true;
        this.ranScenarios.setTitle('Scenarios Instances ...');
        this.ranScenarios.setWidth(460);
        this.ranScenarios.setHeight(300);
        this.ranScenarios.setModal(true);
        this.ranScenarios.initialize(new scenario_instances_model_processor_1.ScenarioInstancesModelProcessor([
            { headerName: 'Name', field: 'name' },
            { headerName: 'Time', field: 'startTime' }
        ]), new scenario_instances_grid_renderer_1.ScenarioInstancesGridRenderer(), gridOptions);
    };
    __decorate([
        core_1.ViewChild('scenarioRunnerPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('scenarioStepsComp'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "scenarioSteps", void 0);
    __decorate([
        core_1.ViewChild('ranScenarioComp'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "ranScenarios", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./plugin.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}());
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        common_1.CommonModule,
        http_1.HttpModule,
        forms_1.FormsModule,
        animations_1.BrowserAnimationsModule,
        cs_plugin_common_1.TableModule,
        button_1.ButtonModule,
        dropdown_1.DropdownModule,
        dialog_1.DialogModule,
        listbox_1.ListboxModule,
        tooltip_1.TooltipModule,
        ag_grid_angular_1.AgGridModule.withComponents([])
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider
    ],
    declarations: [
        PluginComponent,
        scenario_table_component_1.ScenarioTableComponent
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map