"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var ScenarioInstance = /** @class */ (function () {
    function ScenarioInstance() {
        this.name = '';
        this.startTime = '';
        this.id = '';
    }
    return ScenarioInstance;
}());
var ScenarioInstancesModelProcessor = /** @class */ (function () {
    function ScenarioInstancesModelProcessor(columns) {
        this.rows = [];
        this.columns = [];
        this.columns = columns;
    }
    ScenarioInstancesModelProcessor.prototype.process = function (model) {
        if (_.isEmpty(model)) {
            console.error('can not process an empty model');
            return;
        }
        this.rows = this.getRowsFromModel(model);
    };
    ScenarioInstancesModelProcessor.prototype.getRowsFromModel = function (model) {
        // console.log(model);
        var rows = [];
        if (!_.isArray(model)) {
            return [];
        }
        _.forEach(model, function (scenarioInstance) {
            var s = new ScenarioInstance();
            s.name = scenarioInstance.name;
            s.startTime = scenarioInstance.startTime;
            s.id = scenarioInstance.id;
            rows.push(s);
        });
        return rows;
    };
    ScenarioInstancesModelProcessor.prototype.getColumns = function () {
        return this.columns;
    };
    ScenarioInstancesModelProcessor.prototype.getRows = function () {
        return this.rows;
    };
    return ScenarioInstancesModelProcessor;
}());
exports.ScenarioInstancesModelProcessor = ScenarioInstancesModelProcessor;
//# sourceMappingURL=scenario.instances.model.processor.js.map