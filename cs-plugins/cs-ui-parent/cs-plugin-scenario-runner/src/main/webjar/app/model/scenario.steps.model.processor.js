"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var ScenarioStepsModelProcessor = /** @class */ (function () {
    function ScenarioStepsModelProcessor(columns) {
        this.rows = [];
        this.columns = [];
        this.columns = columns;
    }
    ScenarioStepsModelProcessor.prototype.process = function (model) {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }
        this.rows = this.rows.concat(this.getRowsFromModel(model));
    };
    // we get one entry per call
    // it lines up with the server setting
    ScenarioStepsModelProcessor.prototype.getRowsFromModel = function (model) {
        var rows = [];
        rows.push(model);
        return rows;
    };
    ScenarioStepsModelProcessor.prototype.getColumns = function () {
        return this.columns;
    };
    ScenarioStepsModelProcessor.prototype.getRows = function () {
        return this.rows;
    };
    return ScenarioStepsModelProcessor;
}());
exports.ScenarioStepsModelProcessor = ScenarioStepsModelProcessor;
//# sourceMappingURL=scenario.steps.model.processor.js.map