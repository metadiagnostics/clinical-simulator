/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import _ = require('lodash');

class ScenarioInstance {
    public name: string = '';
    public startTime: string = '';
    public id : string = '';

}

export class ScenarioInstancesModelProcessor {

    private rows: any = [];
    private columns: any = [];

    constructor(columns: any) {
        this.columns = columns;
    }

    public process(model: any) {
        if (_.isEmpty(model)) {
            console.error('can not process an empty model');
            return;
        }

        this.rows = this.getRowsFromModel(model);
    }

    protected getRowsFromModel(model) {
        // console.log(model);

        let rows = [];
        if (!_.isArray(model)) {
            return [];
        }

        _.forEach(model, scenarioInstance => {
            let s = new ScenarioInstance();
            s.name = scenarioInstance.name;
            s.startTime = scenarioInstance.startTime;
            s.id = scenarioInstance.id;
            rows.push(s);
        });


        return rows;
    }

    public getColumns(): any {
        return this.columns;
    }

    public getRows() : any {
        return this.rows;
    }
}

