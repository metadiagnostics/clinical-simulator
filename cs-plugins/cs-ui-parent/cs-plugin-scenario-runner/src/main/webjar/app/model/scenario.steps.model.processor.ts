/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import _ = require('lodash');

export class ScenarioStepsModelProcessor {

    private rows: any = [];
    private columns: any = [];

    constructor(columns: any) {
        this.columns = columns;
    }


    public process(model: any) {
        if (_.isNil(model)) {
            console.error('can not process an empty model');
            return;
        }

        this.rows = this.rows.concat(this.getRowsFromModel(model))
    }


    // we get one entry per call
    // it lines up with the server setting

    protected getRowsFromModel(model) {
        let rows = [];
        rows.push(model);

        return rows;
    }

    public getColumns(): any {
        return this.columns;
    }

    public getRows() : any {
        return this.rows;
    }
}

