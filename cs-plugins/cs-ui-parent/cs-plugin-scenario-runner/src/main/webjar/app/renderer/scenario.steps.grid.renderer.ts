/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import * as _ from 'lodash';
import { ScenarioGridRenderer } from './scenario.grid.renderer';

export class ScenarioStepsGridRenderer extends ScenarioGridRenderer {

    protected configureColumns(columns) : Array<any> {
        if ( _.isEmpty(columns)) {
            return [];
        }

        _.forEach(columns, function(column) {
            column.editable = false;
            column.headerClass = 'small-padding-left';
            column.cellClass = 'small-padding-left';
        });

        columns[0].width = 50;
        columns[1].width = 240;
        columns[2].width = 200;

        return columns;
    }
}