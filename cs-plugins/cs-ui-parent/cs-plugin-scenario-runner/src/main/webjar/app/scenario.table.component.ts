/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Component, ViewEncapsulation } from '@angular/core';
import { Grid, TableModelProcessor, Guid } from 'cs-plugin-common';
import * as _ from 'lodash';

@Component({
               moduleId: module.id,
               selector: 'scenario-dialog',
               templateUrl: './scenario.table.component.html',
               encapsulation: ViewEncapsulation.None
           })


export class ScenarioTableComponent {
    private modelProcessor: TableModelProcessor = null;
    private grid: Grid = null;

    public gridOptions: any = {};
    public uuid: any = null;
    public id: any = null;
    public showContent: boolean = false;
    public title: string = '';
    public modality: boolean = false;
    public width: number = 500;
    public height: number = 300;
    public columns: any = [];

    public setTitle(title: string): void {
        this.title = title;
    }

    public setWidth(width: number): void {
        this.width = width;
    }

    public setHeight(height: number): void {
        this.height = height;
    }

    public setModal(modality: boolean): void {
        this.modality = modality;
    }

    public show(show: boolean = true): void {
        this.showContent = show;
    }

    public initialize(modelProcessor: TableModelProcessor, grid: Grid, gridOptions: any) {
        this.uuid = Guid.newGuid();
        this.id = Guid.newGuid();
        this.modelProcessor = modelProcessor;
        this.columns = modelProcessor.getColumns();
        this.grid = grid;
        _.assign(this.gridOptions, gridOptions);

        this.grid.initialize(this.gridOptions, this.uuid);
    }

    private process(model: any): void {
        // console.log('ScenarioTableComponent process');
        this.modelProcessor.process(model);

        let rows = this.modelProcessor.getRows();
        let columns = this.modelProcessor.getColumns();

        this.grid.refreshGrid(this.gridOptions, rows, columns);
        if (!_.isEmpty(rows)) {
            this.grid.selectFirstRow(this.gridOptions);
        }
    }

    public setModel(model: any): void {
        this.process(model);
    }

    public clear(): void {
        console.log('clear the grid');

        // TODO why is this not working...
        // Should clear the grid
        // asked the ag-grid tech support and they
        // say it works in their sample which is correct
        this.gridOptions.rowData = [];
        this.gridOptions.columnDefs = [];
        this.gridOptions.api.setRowData(this.gridOptions.rowData);
        this.gridOptions.api.setColumnDefs(this.gridOptions.columnDefs);
    }
}