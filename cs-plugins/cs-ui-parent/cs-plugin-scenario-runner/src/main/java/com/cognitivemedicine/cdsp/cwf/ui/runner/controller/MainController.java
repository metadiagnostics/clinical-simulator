/** *****************************************************************************
 *
 * Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 ****************************************************************************** */
package com.cognitivemedicine.cdsp.cwf.ui.runner.controller;

import com.cognitivemedicine.cdsp.cwf.api.fhir.FhirConfiguratorFactory;
import com.cognitivemedicine.cdsp.cwf.ui.runner.config.ConfigUtilsConstants;
import com.cognitivemedicine.cdsp.cwf.ui.runner.store.ScenarioStore;
import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.cognitivemedicine.cdsp.fhir.client.config.FhirConfigurator;
import com.cognitivemedicine.cdsp.simulator.api.ScenarioInstanceEventListenerAdapter;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.CreateOrUpdateResourceStepInstance;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.simulator.dao.ScenarioHelper;
import com.cognitivemedicine.cs.simulator.dao.ScenarioRunInstanceDAO;
import com.cognitivemedicine.cs.simulator.dao.ScenarioRunInstance;
import com.cognitivemedicine.cs.simulator.pms.runtime.JMSService;
import com.cognitivemedicine.cs.simulator.pms.runtime.JMSServiceImpl;
import com.cognitivemedicine.cs.simulator.sf.runtime.SmartFormService;
import com.cognitivemedicine.cs.simulator.sf.runtime.SmartFormServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.carewebframework.shell.plugins.PluginController;
import org.carewebframework.ui.dialog.DialogUtil;
import org.carewebframework.ui.thread.ThreadEx;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.client.ExecutionContext;
import org.fujion.component.BaseComponent;
import org.fujion.event.Event;
import org.hl7.fhir.dstu3.model.Patient;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.hspconsortium.cwf.fhir.common.FhirUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 */
public class MainController extends PluginController {
    @WiredComponent
    private AngularComponent scenarioRunnerPlugin;

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    /**
     * This is the name of the variable this plugin will add to all the
     * scenarios. The value of this variable will be null if there is no
     * selected patient in the application or the id of the selected patient
     * (i.e. Patient/123) when there is one.
     */
    public final static String SIM_PATIENT_ID = "SIM_PATIENT_ID";

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(MainController.class);
    private long executedStepCount = 0;

    public ScenarioRunInstanceDAO getScenarioRunInstanceDAO() {
        return scenarioRunInstanceDAO;
    }

    public void setScenarioRunInstanceDAO(ScenarioRunInstanceDAO scenarioRunInstanceDAO) {
        this.scenarioRunInstanceDAO = scenarioRunInstanceDAO;
    }

    private enum State {
        RUNNING,
        STOPPED,
        PAUSED
    }

    private State state = State.STOPPED;
    private String stateChangeCallback = "";
    private String stepUpdateCall = "";
    private ScenarioStore scenarioStore;
    private ScenarioStore.ScenarioStoreEventListener scenarioStoreEventListener;
    private ScenarioInstance currentScenario;
    private String currentScenarioName = null;
    private Collection<String> scenarioNames = new ArrayList<String>();
    private List<ScenarioHelper.ExecutedStep> executedSteps = new ArrayList<>();
    private ClinicalSimulatorFhirServiceImpl clinicalSimulatorFhirService;
    private String scenarioStartTime = "";
    private ScenarioRunInstanceDAO scenarioRunInstanceDAO;

    public MainController() {
        super();
        changeState(State.STOPPED);
    }

    /**
     * ****************************************************************
     * Overridden Supertype Methods
     * ****************************************************************
     */
    @Override
    public void afterInitialized(BaseComponent comp) {
        super.afterInitialized(comp);

        loadScenarios();

        scenarioStoreEventListener = new ScenarioStore.ScenarioStoreEventListener() {
            @Override
            public void scenarioRegistered(ScenarioDefinition definition) {
                refresh();
            }

            @Override
            public void scenarioUnregistered(String id) {
                refresh();
            }

            private void refresh() {
                if (ExecutionContext.getPage().isDead()) {
                    //TODO: is there a better way to remove this listener when
                    //this controller is "destroyed"?
                    scenarioStore.removeEventListener(scenarioStoreEventListener);
                    return;
                }
            }

        };
        scenarioStore.addEventListener(scenarioStoreEventListener);
    }

    private void changeState(State newState) {
        state = newState;
        if (!stateChangeCallback.equals("")) {
            Map<String, Object> data = new HashMap<>();
            data.put("state", state);
            LOG.error("RxR called the state change callback with state: " + state);
            invoke(stateChangeCallback, data);
        }
    }

    private void changeStateAsync(String pageId, State newState) {
        ExecutionContext.invoke(pageId, () -> changeState(newState));
    }

    private boolean startNewScenario() {
        String pageId = ExecutionContext.getPage().getId();

        try {
            if (state != State.STOPPED) {
                DialogUtil.showError("There is a scenario running. Please stop it before starting a new one.", "Error");
                return false;
            }

            ScenarioDefinition definition = scenarioStore.getScenarioDefinition(currentScenarioName);
            if (definition == null) {
                DialogUtil.showError("There is no scenario registered with the name " + currentScenarioName, "Error");
                return false;
            }

            currentScenario = definition.createInstance(createScenarioContext());
            currentScenario.addEventListener(new ScenarioInstanceEventListenerAdapter() {

                @Override
                public void onScenarioRun() {
                    changeStateAsync(pageId, State.RUNNING);
                    scenarioStartTime = getCurrentDateTime();
                    executedSteps = new ArrayList<>();
                    resetExecutedStepCount();
                    LOG.error("RxR starting scenario: " + currentScenarioName + " at: " + scenarioStartTime);
                }

                @Override
                public void onScenarioPause() {
                    System.out.println("RxR pausing scenario: " + currentScenarioName );
                    changeStateAsync(pageId, State.PAUSED);
                }

                @Override
                public void onScenarioFinish() {
                    changeStateAsync(pageId, State.STOPPED);
                    System.out.println("RxR finish scenario: " + currentScenarioName);

                    currentScenario = null;
                    storeExecutedSteps();
                }

                @Override
                public void onScenarioStep(StepInstance step) {
//                    if (state != State.RUNNING) {
//                        return;
//                    }

                    if (getExecutedStepCount() == 0) {
                        changeStateAsync(pageId, State.RUNNING);
                    }

                    if (step instanceof CreateOrUpdateResourceStepInstance) {
                        String id = ((CreateOrUpdateResourceStepInstance) step).getResource().getIdElement().getIdPart();
                        String resourceType = ((CreateOrUpdateResourceStepInstance) step).getResource().getIdElement().getResourceType();

                        if (((CreateOrUpdateResourceStepInstance) step).getResource() instanceof Patient) {
                            ExecutionContext.invoke(pageId, () -> PatientContext.changePatient(((Patient) ((CreateOrUpdateResourceStepInstance) step).getResource())));
                        }

                        ScenarioHelper.ExecutedStep executedStep = new ScenarioHelper.ExecutedStep();

                        final String name = step.getStepDefinition().getName();
                        executedStep.setName(name);
                        executedStep.setDetail(name);
                        executedStep.setTime(getCurrentDateTime());
                        executedStep.setId(id);
                        executedStep.setResourceType(resourceType);
                        executedStep.setIndex(incrementAndGetExecutedStepCount());
                        executedSteps.add(executedStep);

                        if (!stepUpdateCall.equals("")) {
                            Map<String, Object> data = new HashMap<>();
//                            LOG.debug("RxR just processed a scenario step");

                            data.put("data", executedStep);
                            invoke(stepUpdateCall, data);
                        }
                    }
                }

                @Override
                public void onScenarioAbort(StepInstance step, Exception ex) {
                    LOG.error("Exception while running scenario", ex);
                    LOG.error("RxR abort scenario: " + currentScenarioName);

                    changeStateAsync(pageId, State.STOPPED);
                    storeExecutedSteps();

                    ExecutionContext.invoke(pageId, () -> {
                        DialogUtil.showError("Exception executing Simulation Step '" + step.getStepDefinition().getName() + "': " + ex.getMessage() + "\nThe Simulation was aborted.", "Error");
                    });
                }

                @Override
                public void onScenarioResume() {
                }
            });

            this.startBackgroundThread(new ThreadEx.IRunnable() {
                @Override
                public void run(ThreadEx thread) throws Exception {
                    currentScenario.run();
                }

                @Override
                public void abort() {
                }
            });

            return true;
        } catch (Exception e) {
            LOG.error("There was an error running the scenario", e);
            stopCurrentScenario();
            DialogUtil.showError(e.getMessage(), "Error");
            return false;
        }
    }

    private void storeExecutedSteps() {
        String steps = ScenarioHelper.getSteps(executedSteps);
        String endTime = getCurrentDateTime();
        this.resetExecutedStepCount();

        getScenarioRunInstanceDAO().saveOrUpdateScenarioSteps(
                currentScenarioName, scenarioStartTime, endTime, steps);
    }

    private boolean pauseCurrentScenario() {
        try {
            if (currentScenario == null) {
                return false;
            }

            currentScenario.pause();
            return true;
        } catch (Exception e) {
            DialogUtil.showError(e.getMessage(), "Error");
            return false;
        }
    }

    private boolean resumeCurrentScenario() {
        try {
            if (currentScenario == null) {
                return false;
            }
            currentScenario.resume();
            return true;
        } catch (Exception e) {
            DialogUtil.showError(e.getMessage(), "Error");
            return false;
        }
    }

    private boolean stopCurrentScenario() {
        try {
            changeStateAsync(ExecutionContext.getPage().getId(), State.STOPPED);
            currentScenario.stop();
            currentScenario = null;

//            this.storeExecutedSteps();
            return true;
        } catch (Exception e) {
            DialogUtil.showError(e.getMessage(), "Error");
            return false;
        }
    }

    private void publishCaseScenarioEvent(String eventName, String simulationId) {
        this.getEventManager().fireLocalEvent(eventName, new Event(eventName, null, simulationId));
    }

    private void loadScenarios() {
        List<String> definitionNames = scenarioStore.getScenarioNames();
        Collections.sort(definitionNames);

        definitionNames.forEach(n -> scenarioNames.add(n));
    }

    @EventHandler(value = "importScenario", target = "scenarioRunnerPlugin")
    private void importScenario$scenarioRunnerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String name = (String) params.get("name");
        String data = (String) params.get("data");
        Map<String, Object> response = new HashMap<>();
        response.put("status", "success");
        addMetadata(response, (String) params.get("uuid"), null);
        String callback = ((String) params.get("_callback"));

        try {
            scenarioStore.registerNewScenario(name, data);
            scenarioNames = scenarioStore.getScenarioNames();
            this.invoke(callback, response);
        } catch (Exception e) {
            log.error("Exception when registering new Scenario",e);
            DialogUtil.showError(e);
        }
    }

    @EventHandler(value = "deleteScenario", target = "scenarioRunnerPlugin")
    private void deleteScenario$scenarioRunnerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        String scenarioName = (String) params.get("scenario");

        scenarioStore.unregisterScenario(scenarioName);
        scenarioNames = scenarioStore.getScenarioNames();

        Map<String, Object> response = new HashMap<>();
        response.put("status", "success");
        addMetadata(response, (String) params.get("uuid"), null);
        String callback = ((String) params.get("_callback"));
        this.invoke(callback, response);
    }

    @EventHandler(value = "runScenario", target = "scenarioRunnerPlugin")
    private void runScenario$scenarioRunnerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        Map<String, Object> data = new HashMap<>();
        String scenarioName = (String) params.get("scenario");
        if (!scenarioNames.contains(scenarioName)) {
            DialogUtil.showError(scenarioName + " is not found in the list");
            return;
        }

        currentScenarioName = scenarioName;
        processRunScenarioRequest();
    }

    @EventHandler(value = "stopScenario", target = "scenarioRunnerPlugin")
    private void stopScenario$scenarioRunnerPlugin(Event event) {
        processStopScenarioRequest();
    }

    @EventHandler(value = "pauseScenario", target = "scenarioRunnerPlugin")
    private void pauseScenario$scenarioRunnerPlugin(Event event) {
        processPauseScenarioRequest();
    }

    @EventHandler(value = "retrieveScenarioList", target = "scenarioRunnerPlugin")
    private void retrieveScenarioList$scenarioRunnerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        Map<String, Object> data = new HashMap<>();
        data.put("scenarios", scenarioNames);
        addMetadata(data, (String) params.get("uuid"), null);
        String callback = ((String) params.get("_callback"));
        stateChangeCallback = ((String) params.get("_stateChangecallback"));
        stepUpdateCall = ((String) params.get("_stepsCallback"));
        this.invoke(callback, data);
    }

    @EventHandler(value = "deleteScenarioInstance", target = "scenarioRunnerPlugin")
    private void deleteScenarioInstance$scenarioRunnerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();

        String id = (String) params.get("id");
        deleteScenarioFHIRResources(id);

        Map<String, Object> data = new HashMap<>();
        addMetadata(data, (String) params.get("uuid"), null);
        String callback = ((String) params.get("_callback"));

        this.invoke(callback, data);
    }

    private void deleteScenarioFHIRResources(String id) {
        ScenarioRunInstance scenarioInstance = getScenarioRunInstanceDAO().getInstanceById(id);
        List<ScenarioHelper.ExecutedStep> ess = getScenarioRunInstanceDAO().getExecutedSteps(scenarioInstance);

        setUpFhirServiceSettings();

        clinicalSimulatorFhirService.deleteFHIRResources(ess);
        getScenarioRunInstanceDAO().deleteInstance(scenarioInstance);
    }

    @EventHandler(value = "retrieveScenarioRunInstances", target = "scenarioRunnerPlugin")
    private void retrieveScenarioRunInstances$scenarioRunnerPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>) event.getData();
        Map<String, Object> data = new HashMap<>();

        List<ScenarioRunInstance> results = getScenarioRunInstanceDAO().getRanScenarioInstances();
        redactTheSteps(results);
        data.put("data", results);
        addMetadata(data, (String) params.get("uuid"), null);
        String callback = ((String) params.get("_callback"));
        this.invoke(callback, data);
    }

    private void redactTheSteps(List<ScenarioRunInstance> scenarioRunInstances) {
        for (ScenarioRunInstance  scenarioRunInstance: scenarioRunInstances) {
            scenarioRunInstance.setSteps("");
        }
    }

    private void invoke(String functionName, Object... args) {
        if (this.scenarioRunnerPlugin != null) {
            this.scenarioRunnerPlugin.ngInvoke(functionName, args);
        }
    }

    private void addMetadata(Map<String, Object> data, String uuid, String message) {
        if (data != null) {
            Map<String, Object> metadata = new HashMap<>();
            metadata.put("_uuid", uuid);

            if (message != null) {
                metadata.put("message", message);
            }
            data.put("metadata", metadata);
        }
    }

    private String getCurrentDateTime() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }

    private void processRunScenarioRequest() {
        String pageId = ExecutionContext.getPage().getId();
        switch (state) {
            case RUNNING:
                changeStateAsync(pageId, State.PAUSED);
                if (!pauseCurrentScenario()) {
                    changeStateAsync(pageId, State.RUNNING);
                }
                break;
            case STOPPED:
                if (!startNewScenario()) {
                    changeStateAsync(pageId, State.STOPPED);
                }
                break;
            case PAUSED:
                changeStateAsync(pageId, State.RUNNING);
                if (!resumeCurrentScenario()) {
                    changeStateAsync(pageId, State.PAUSED);
                }
                break;
            default:
                break;
        }

    }

    private void processStopScenarioRequest() {
        stopCurrentScenario();
    }

    private void processPauseScenarioRequest() {
        if (state == State.PAUSED) {
            if (resumeCurrentScenario()) {
                changeStateAsync(ExecutionContext.getPage().getId(), State.RUNNING);
            }
            return;
        }

        if (pauseCurrentScenario()) {
            changeStateAsync(ExecutionContext.getPage().getId(), State.PAUSED);
        } else {
            changeStateAsync(ExecutionContext.getPage().getId(), State.STOPPED);
        }
    }

    public ScenarioStore getScenarioStore() {
        return scenarioStore;
    }

    public void setScenarioStore(ScenarioStore scenarioStore) {
        this.scenarioStore = scenarioStore;
    }

    private ScenarioContext createScenarioContext() {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilsConstants.CONTEXT_NAME);

        //FHIR
        setUpFhirServiceSettings();
        
        //SMART-FORMS
        String smartFormsServiceEndpoint = config.getString(ConfigUtilsConstants.KEY_CS_SMARTFORM_SERVICE_ENDPOINT);
        SmartFormService smartFormService = new SmartFormServiceImpl(smartFormsServiceEndpoint);
        
        //PMS
        String jmsEndpoint = config.getString(ConfigUtilsConstants.KEY_CS_JMS_ENDPOINT);
        String jmsTopic = config.getString(ConfigUtilsConstants.KEY_CS_JMS_TOPIC);
        String jmsDiscriminatorProperty = config.getString(ConfigUtilsConstants.KEY_CS_JMS_DISCRIMINATOR);

        JMSService jmsService = new JMSServiceImpl(jmsEndpoint, jmsTopic, jmsDiscriminatorProperty);

        ScenarioContext context = new ScenarioContext();
        new com.cognitivemedicine.cdsp.simulator.fhir.config.ContextConfigurator(clinicalSimulatorFhirService).configureContext(context);
        new com.cognitivemedicine.cs.simulator.sf.config.ContextConfigurator(smartFormService).configureContext(context);
        new com.cognitivemedicine.cs.simulator.pms.config.ContextConfigurator(jmsService).configureContext(context);

        if (PatientContext.getActivePatient() != null) {
            String idPath = FhirUtil.getResourceIdPath(PatientContext.getActivePatient(), true);
            context.addInitParameter(SIM_PATIENT_ID, "value/" + idPath);
        }

        return context;
    }

    private void setUpFhirServiceSettings() {

        if (this.clinicalSimulatorFhirService != null) {
            return;
        }

        FhirConfigurator configurator = new FhirConfiguratorFactory().createInstance();

        BaseService baseService = new BaseService(configurator);
        clinicalSimulatorFhirService = new ClinicalSimulatorFhirServiceImpl(baseService);
        clinicalSimulatorFhirService.setRootUrl(configurator.getRootUrl());
    }

    public long getExecutedStepCount() {
        return executedStepCount;
    }

    private void resetExecutedStepCount() {
        this.executedStepCount = 0;
    }
    
    private long incrementAndGetExecutedStepCount() {
        this.executedStepCount ++;
        return this.getExecutedStepCount();
    }
}

