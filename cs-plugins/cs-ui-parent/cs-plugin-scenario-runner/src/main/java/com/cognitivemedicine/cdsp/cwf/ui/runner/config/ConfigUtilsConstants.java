/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.cwf.ui.runner.config;

/**
 *
 * @author esteban
 */
public class ConfigUtilsConstants {
    public static final String CONTEXT_NAME = "cs.cwf.case.simulator";
    
    public static final String KEY_CS_SMARTFORM_SERVICE_ENDPOINT = "smartform.endpoint";
    
    public static final String KEY_CS_JMS_ENDPOINT = "jms.endpoint";
    public static final String KEY_CS_JMS_TOPIC = "jms.topic";
    public static final String KEY_CS_JMS_DISCRIMINATOR = "jms.discriminator";
}
