package com.cognitivemedicine.cdsp.cwf.ui.runner.controller;

import ca.uhn.fhir.context.FhirContext;
import com.cognitivemedicine.cdsp.fhir.client.BaseService;
import com.cognitivemedicine.cdsp.simulator.runtime.FHIRService;
import com.cognitivemedicine.cs.simulator.dao.ScenarioHelper;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.MedicationAdministration;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.ProcedureRequest;
import org.hl7.fhir.dstu3.model.AuditEvent;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hl7.fhir.dstu3.model.Immunization;
import org.hl7.fhir.dstu3.model.Specimen;
import org.hl7.fhir.dstu3.model.Consent;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class ClinicalSimulatorFhirServiceImpl implements FHIRService {
    private static final Logger LOG = LoggerFactory.getLogger(ClinicalSimulatorFhirServiceImpl.class);
    private final BaseService baseService;
    private static int NUMBER_OF_DELETION_ATTEMPTS = 5;
    private String rootURL = "";

    public void setRootUrl(String rootURL) {
        this.rootURL = rootURL;
        if (this.rootURL.length() > 0 && !this.rootURL.endsWith("/")) {
            this.rootURL += "/";
        }
    }

    public ClinicalSimulatorFhirServiceImpl(BaseService baseService) {
        this.baseService = baseService;
    }

    public FhirContext getFhirContext() {
        return this.baseService.getClient().getFhirContext();
    }

    protected void cacheStepResource(IBaseResource resource) {
    }

    public IBaseResource saveOrUpdate(IBaseResource resource) {
        this.cacheStepResource(resource);
        return this.baseService.createOrUpdateResource(resource);
    }

    public <T extends IBaseResource> List<T> searchResourceByIdentifier(String codeSystem, String code, Class<T> clazz) {
        return this.baseService.searchResourcesByIdentifier(codeSystem, code, clazz);
    }

    public <T extends IBaseResource> List<T> searchResourceByCoding(String codeSystem, String code, Class<T> clazz) {
        return this.baseService.searchResourcesByCoding(codeSystem, code, clazz);
    }

    public <T extends IBaseResource> T searchResourceById(IIdType id, Class<T> clazz) {
        return this.baseService.searchResourceById(id.getIdPart(), clazz);
    }

    public <T extends IBaseResource> List<T> searchResourceByIdentifierAndCode(String identifierSystem, String identifier,
                                      String codeSystem, String code, Class<T> clazz) {
        return null;
    }

    private boolean deleteFHIRResource(String type, String id) {
        try {
            String urlString = rootURL + type + "/" + id;
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            int responseCode = con.getResponseCode();
            LOG.debug("DELETE " + urlString + " finished with response code: " + responseCode);
            con.disconnect();
            
            return responseCode == 200;
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    public List<ScenarioHelper.ExecutedStep> deleteFHIRResources(List<ScenarioHelper.ExecutedStep> ess) {
        List<ScenarioHelper.ExecutedStep> failedResourceIds = new ArrayList<ScenarioHelper.ExecutedStep>(ess);
        Collections.copy(failedResourceIds, ess);

        int index = 0;
        do {
            for (ScenarioHelper.ExecutedStep es: ess) {

                if (failedResourceIds.contains(es)) {
                    if (!this.deleteFHIRResource(es.getResourceType(), es.getId())) {
                        LOG.error("FHIR Resource deletion for " + es.getResourceType() + " " + es.getId() + " failed");
                    } else {
                        failedResourceIds.remove(es);
                        LOG.debug("FHIR Resource deletion for " + es.getResourceType() + " " + es.getId() + " succeeded");
                    }
                }
            }
            LOG.debug("FHIR Resource deletion round: " +  index);
        } while (index++ < NUMBER_OF_DELETION_ATTEMPTS && failedResourceIds.size() > 0);

        LOG.debug("FHIR Resources deletion for has " + failedResourceIds.size() +   " resources that could not be deleted");
        return failedResourceIds;
    }
}

