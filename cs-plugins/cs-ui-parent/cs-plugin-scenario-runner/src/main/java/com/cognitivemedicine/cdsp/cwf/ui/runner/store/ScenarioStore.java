/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.cwf.ui.runner.store;

import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.serialization.ScenarioSerializer;
import com.cognitivemedicine.cdsp.simulator.serialization.ScenarioSerializerRegistry;
import com.cognitivemedicine.cs.simulator.dao.Scenario;
import com.cognitivemedicine.cs.simulator.dao.ScenarioDAO;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author esteban
 */
public class ScenarioStore {
    
    private ScenarioDAO scenarioDao;
    private ScenarioSerializerRegistry scenarioSerializerRegistry;

    public ScenarioStore(ScenarioDAO scenarioDao, ScenarioSerializerRegistry scenarioSerializerRegistry) {
        this.scenarioDao = scenarioDao;
        this.scenarioSerializerRegistry = scenarioSerializerRegistry;
    }
    
    public interface ScenarioStoreEventListener {
        void scenarioRegistered(ScenarioDefinition definition);
        void scenarioUnregistered(String id);
    }
    
    private final List<ScenarioStoreEventListener> listeners = Collections.synchronizedList(new ArrayList<>());
    
    public void addEventListener(ScenarioStoreEventListener listener){
        listeners.add(listener);
    }
    
    public void removeEventListener(ScenarioStoreEventListener listener){
        listeners.remove(listener);
    }
    
    public void registerNewScenario(String name, String contents) throws IOException {
        this.scenarioDao.addOrUpdateScenario(name, contents);

        ScenarioSerializer serializer = scenarioSerializerRegistry.getSerializerForFilename(name);

        if (serializer != null) {
            StringReader definitionReader = new StringReader(contents);

            ScenarioDefinition sd = serializer.deserializeScenarioDefinition(name, definitionReader);
            new ArrayList<>(listeners).forEach(l -> l.scenarioRegistered(sd));
        }
    }
    
    public void unregisterScenario(String id){
        if (id != null){
            this.scenarioDao.deleteScenario(id);
            new ArrayList<>(listeners).forEach(l -> l.scenarioUnregistered(id));
        }
    }

    public ScenarioDefinition getScenarioDefinition(String name){
        Scenario scenario = this.scenarioDao.getScenarioById(name);

        if (scenario != null){
            return this.convertScenarioToDefinition(scenario);
        }

        return null;
    }
    
    public List<String> getScenarioNames(){
       return this.scenarioDao.getScenarioNames();
    }

    private ScenarioDefinition convertScenarioToDefinition(Scenario scenario) {
        ScenarioSerializer serializer = scenarioSerializerRegistry.getSerializerForFilename(scenario.getId());

        if (serializer == null) {
            return null;
        }

        StringReader definitionReader = new StringReader(scenario.getDefinition());
        try {
            return serializer.deserializeScenarioDefinition(scenario.getId(), definitionReader);
        } catch (IOException io) {
            throw new IllegalStateException(io);
        }
    }
}
