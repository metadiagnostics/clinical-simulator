/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.plugin;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.cognitivemedicine.common.model.PatientSearchModel;
import com.cognitivemedicine.common.service.PatientSearchService;
import com.cognitivemedicine.common.util.Utilities;
import org.carewebframework.api.event.IGenericEvent;
import org.carewebframework.shell.elements.*;
import org.carewebframework.shell.plugins.PluginController;
import org.fujion.angular.AngularComponent;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.client.ExecutionContext;
import org.fujion.event.Event;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.springframework.beans.factory.annotation.Autowired;


public class PatientSearchController extends PluginController {

    @WiredComponent
    private AngularComponent patientSearchPlugin;

    @Autowired
    private PatientSearchService patientSearchService;

    private final IGenericEvent<?> patientChangeListener = (eventName, eventData) -> {
        ElementTabPane searchTab = this.getPlugin().getAncestor(ElementTabPane.class);
        ElementUI patientRecordTab = searchTab.getNextSibling(false);

        if (PatientContext.getActivePatient() == null) {
            this.getPlugin().setVisible(true);

            if (patientRecordTab != null) {
                this.togglePatientRecordTab(patientRecordTab, false);
            }
        } else {
            this.getPlugin().setVisible(false);

            if (patientRecordTab != null) {
                this.togglePatientRecordTab(patientRecordTab, true);
                patientRecordTab.bringToFront();
            }
        }
    };

    private void togglePatientRecordTab(ElementUI patientRecordTab, boolean visible) {
        Iterable<ElementTreeView> treeViewList = patientRecordTab.getChildren(ElementTreeView.class);

        ElementTreeView treeView = null;

        if (treeViewList != null) {
            for (ElementTreeView tv : treeViewList) {
                treeView = tv;
            }
        }

        if (treeView != null) {
            Iterable<ElementTreePane> paneList = treeView.getChildren(ElementTreePane.class);

            if (paneList != null) {
                for (ElementTreePane pane : paneList) {
                    Iterable<ElementPlugin> pluginList = pane.getChildren(ElementPlugin.class);

                    if (pluginList != null) {
                        for (ElementPlugin plugin: pluginList) {
                            if (plugin.isVisible() != visible) {
                                plugin.setVisible(visible);
                            }
                        }
                    }
                }
            }

        }
    }

    @Override
    public void onLoad(ElementPlugin plugin) {
        super.onLoad(plugin);
        getEventManager().subscribe(Utilities.PATIENT_CONTEXT_EVENT, this.patientChangeListener);

        // This works with PatientRecordPluginController to handle toggling tabs
        ElementTabPane searchTab = this.getPlugin().getAncestor(ElementTabPane.class);
        ElementUI patientRecordTab = searchTab.getNextSibling(false);

        if (patientRecordTab != null) {
            this.togglePatientRecordTab(patientRecordTab, false);
        }
    }

    @Override
    public void onUnload() {
        super.onUnload();
        getEventManager().unsubscribe(Utilities.PATIENT_CONTEXT_EVENT, this.patientChangeListener);
    }

    @EventHandler(value = "selectPatient", target = "patientSearchPlugin")
    private void selectPatient$patientSearchPlugin(Event event) {
        Map<String, Object> params = (Map<String, Object>)event.getData();
        String patientId = (String) params.get("id");

        String pageId = ExecutionContext.getPage().getId();
        ExecutionContext.invoke(pageId, () -> {
            PatientContext.changePatient(patientId);
        });
    }

    @EventHandler(value = "search", target = "patientSearchPlugin")
    private void search$patientSearchPlugin(Event event) throws UnsupportedEncodingException {
        Map<String, Object> params = (Map<String, Object>)event.getData();
        String requestId = (String) params.get("requestId");
        Map<String, Object> data = this.patientSearchService.findPatientByCriteria(new PatientSearchModel(params));

        String callback = (String) params.get("_callback");
        data.put("requestId", requestId);

        if (callback != null) {
            this.patientSearchPlugin.ngInvoke(callback, data);
        }
    }
}
