"use strict";
/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var animations_1 = require("@angular/platform-browser/animations");
var cs_plugin_common_1 = require("cs-plugin-common");
var button_1 = require("primeng/components/button/button");
var inputtext_1 = require("primeng/components/inputtext/inputtext");
var dropdown_1 = require("primeng/components/dropdown/dropdown");
var calendar_1 = require("primeng/components/calendar/calendar");
var ng2_split_pane_1 = require("ng2-split-pane/lib/ng2-split-pane");
var _ = require("lodash");
var moment = require("moment");
var PluginComponent = /** @class */ (function () {
    function PluginComponent(backendService) {
        this.backendService = backendService;
        this.selectedPatientId = null;
        this.formModel = {};
        this.genderOptions = null;
        this.currentRequestId = null;
        this.genderOptions = [
            { label: "Male", value: "male" },
            { label: "Female", value: "female" }
        ];
    }
    PluginComponent.prototype.ngOnInit = function () {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeTable();
    };
    PluginComponent.prototype.initializeTable = function () {
        var _this = this;
        var gridOptions = {
            onSelectionChanged: function ($event) {
                var selectedRows = $event.api.getSelectedRows();
                if (_.isArray(selectedRows) && !_.isEmpty(selectedRows)) {
                    _this.selectedPatientId = _.get(selectedRows, '[0].id', '');
                }
            },
            onRowDoubleClicked: function ($event) {
                _this.selectedPatientId = _.get($event, 'data.id');
                _this.selectPatient();
            },
            suppressNoRowsOverlay: false
        };
        var renderer = new cs_plugin_common_1.TableGrid();
        var modelProcessor = new cs_plugin_common_1.TableModelProcessor([{
                "field": "lastName",
                "headerName": "Last Name"
            }, {
                "field": "firstName",
                "headerName": "First Name"
            }, {
                "field": "middleName",
                "headerName": "Middle Name"
            }, {
                "field": "gender",
                "headerName": "Gender"
            }, {
                "field": "birthdate",
                "headerName": "Birth Date"
            }, {
                "field": "medicalRecordNumber",
                "headerName": "MR#"
            }, {
                "field": "city",
                "headerName": "City"
            }, {
                "field": "zip",
                "headerName": "Zip Code"
            }]);
        this.grid.initialize(modelProcessor, renderer, gridOptions);
    };
    PluginComponent.prototype.cancel = function () {
        this.currentRequestId = null;
        this.selectedPatientId = null;
        this.grid.setModel({ data: [] });
    };
    PluginComponent.prototype.clear = function () {
        this.resetForm();
    };
    PluginComponent.prototype.search = function () {
        if (!_.isEmpty(this.formModel)) {
            this.grid.showLoadingIndicator();
            this.selectedPatientId = null;
            this.requestData();
        }
    };
    PluginComponent.prototype.requestData = function () {
        var requestId = cs_plugin_common_1.Guid.newGuid();
        var params = {
            _uuid: this.grid.getUUID(),
            _callback: 'updateTable',
            type: 'PATIENTSEARCH',
            requestId: requestId
        };
        _.assign(params, this.formModel);
        if (!_.isUndefined(this.formModel.dob)) {
            params.dateOfBirth = moment(this.formModel.dob).format("YYYYMMDD");
        }
        this.currentRequestId = requestId;
        this.backendService.sendBackendEvent('search', params);
    };
    PluginComponent.prototype.updateTable = function (model) {
        var modelRequestId = _.get(model, 'requestId', '');
        if (modelRequestId === this.currentRequestId) {
            this.grid.setModel(model);
        }
    };
    PluginComponent.prototype.selectPatient = function () {
        var params = {
            _callback: 'onPatientSelected',
            id: this.selectedPatientId
        };
        this.backendService.sendBackendEvent('selectPatient', params);
        this.resetForm();
    };
    PluginComponent.prototype.resetForm = function () {
        this.grid.setModel({ data: [] });
        this.selectedPatientId = null;
        this.formModel = {};
    };
    __decorate([
        core_1.ViewChild('patientSearchPlugin'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "root", void 0);
    __decorate([
        core_1.ViewChild('patientGrid'),
        __metadata("design:type", Object)
    ], PluginComponent.prototype, "grid", void 0);
    PluginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'plugin',
            templateUrl: 'plugin.component.html',
            styleUrls: ['./css/plugin.component.css'],
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [cs_plugin_common_1.IBackendService])
    ], PluginComponent);
    return PluginComponent;
}());
exports.PluginComponent = PluginComponent;
exports.AngularComponent = PluginComponent;
var ngModule = {
    imports: [
        platform_browser_1.BrowserModule,
        http_1.HttpModule,
        forms_1.FormsModule,
        animations_1.BrowserAnimationsModule,
        button_1.ButtonModule,
        dropdown_1.DropdownModule,
        calendar_1.CalendarModule,
        inputtext_1.InputTextModule,
        cs_plugin_common_1.TableModule,
        ng2_split_pane_1.SplitPaneModule
    ],
    providers: [
        cs_plugin_common_1.ibackendProvider
    ],
    declarations: [
        PluginComponent
    ]
};
exports.ngModule = ngModule;
//# sourceMappingURL=plugin.component.js.map