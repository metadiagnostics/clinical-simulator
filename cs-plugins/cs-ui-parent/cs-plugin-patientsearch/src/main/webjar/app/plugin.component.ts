/*
 *  Copyright 2017 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import {FormsModule} from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    IBackendService, ibackendProvider,
    TableGrid, TableModelProcessor,
    TableModule, Guid
} from "cs-plugin-common";

import {ButtonModule} from "primeng/components/button/button";
import { InputTextModule } from "primeng/components/inputtext/inputtext";
import {DropdownModule} from 'primeng/components/dropdown/dropdown';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import * as _ from 'lodash';
import * as moment from 'moment';
import {SelectItem} from "primeng/components/common/selectitem";

@Component({
   moduleId   : module.id,
   selector   : 'plugin',
   templateUrl: 'plugin.component.html',
   styleUrls: ['./css/plugin.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class PluginComponent {
  
    @ViewChild('patientSearchPlugin')
    private root;

    @ViewChild('patientGrid')
    private grid;

    private selectedPatientId = null;

    private formModel: any = {};

    private genderOptions: SelectItem[] = null;

    private currentRequestId: string = null;

    constructor(private backendService: IBackendService){
        this.genderOptions = [
            { label: "Male", value: "male"},
            { label: "Female", value: "female"}
        ];
    }
    
    ngOnInit() {
        this.backendService.setRootNativeElement(this.root.nativeElement);
        this.initializeTable();
    }

    private initializeTable(): void {
        let gridOptions = {
            onSelectionChanged: ($event) => {
                let selectedRows = $event.api.getSelectedRows();
                if (_.isArray(selectedRows) && ! _.isEmpty(selectedRows)) {
                    this.selectedPatientId = _.get(selectedRows, '[0].id', '');
                }
            },
            onRowDoubleClicked: ($event) => {
                this.selectedPatientId = _.get($event, 'data.id');
                this.selectPatient();
            },
            suppressNoRowsOverlay: false
        };

        let renderer = new TableGrid();
        let modelProcessor = new TableModelProcessor([ {
            "field": "lastName",
            "headerName": "Last Name"
        }, {
            "field": "firstName",
            "headerName": "First Name"
        }, {
            "field": "middleName",
            "headerName": "Middle Name"
        }, {
            "field": "gender",
            "headerName": "Gender"
        }, {
            "field": "birthdate",
            "headerName": "Birth Date"
        }, {
            "field": "medicalRecordNumber",
            "headerName": "MR#"
        }, {
            "field": "city",
            "headerName": "City"
        }, {
            "field": "zip",
            "headerName": "Zip Code"
        }]);

        this.grid.initialize(modelProcessor, renderer, gridOptions);
    }

    private cancel() {
        this.currentRequestId = null;
        this.selectedPatientId = null;
        this.grid.setModel({ data: []});
    }

    private clear() {
        this.resetForm();
    }

    private search() {
        if (!_.isEmpty(this.formModel)) {
            this.grid.showLoadingIndicator();
            this.selectedPatientId = null;
            this.requestData();
        }
    }

    private requestData() {
        let requestId = Guid.newGuid();
        let params: any = {
            _uuid: this.grid.getUUID(),
            _callback: 'updateTable',
            type: 'PATIENTSEARCH',
            requestId: requestId
        };

        _.assign(params, this.formModel);

        if (!_.isUndefined(this.formModel.dob)) {
            params.dateOfBirth = moment(this.formModel.dob).format("YYYYMMDD");
        }

        this.currentRequestId = requestId;
        this.backendService.sendBackendEvent('search', params);
    }

    public updateTable(model): void {
        let modelRequestId = _.get(model, 'requestId', '');

        if (modelRequestId === this.currentRequestId) {
            this.grid.setModel(model);
        }
    }

    public selectPatient() {
        let params: any = {
            _callback: 'onPatientSelected',
            id: this.selectedPatientId
        };
        this.backendService.sendBackendEvent('selectPatient', params);
        this.resetForm();
    }

    private resetForm() {
        this.grid.setModel({ data: []});
        this.selectedPatientId = null;
        this.formModel = {};
    }
}

let ngModule = {
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        BrowserAnimationsModule,
        ButtonModule,
        DropdownModule,
        CalendarModule,
        InputTextModule,
        TableModule,
        SplitPaneModule
    ],
    providers: [
        ibackendProvider
    ],
    declarations: [
        PluginComponent
    ]
};
export {PluginComponent as AngularComponent, ngModule};

