/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data.entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import org.springframework.data.domain.Persistable;

/**
 *
 * @author duanedecouteau
 */
@Entity
@Table(name = "acknowledgements")
@XmlRootElement
public class Acknowledgements implements Persistable<String> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idacknowledgements")
    private Integer idacknowledgements;
    @Size(max = 45)
    @Column(name = "resource_type")
    private String resourceType;
    @Size(max = 45)
    @Column(name = "resource_id")
    private String resourceId;
    @Size(max = 80)
    @Column(name = "display_name")
    private String displayName;
    @Size(max = 80)
    @Column(name = "user_id")
    private String userId;
    @Column(name = "viewed")
    private Integer viewed;
    @Column(name = "date_time_viewed")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTimeViewed;
    @Size(max = 45)
    @Column(name = "code")
    private String code;
    @Size(max = 80)
    @Column(name = "code_system")
    private String codeSystem;
    @Column(name = "severity")
    private Integer severity;

    public Acknowledgements() {
    }

    public Acknowledgements(Integer idacknowledgements) {
        this.idacknowledgements = idacknowledgements;
    }

    @XmlElement
    public Integer getIdacknowledgements() {
        return idacknowledgements;
    }

    public void setIdacknowledgements(Integer idacknowledgements) {
        this.idacknowledgements = idacknowledgements;
    }

    @XmlElement
    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }
    
    @XmlElement
    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    @XmlElement
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @XmlElement
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @XmlElement
    public Integer getViewed() {
        return viewed;
    }

    public void setViewed(Integer viewed) {
        this.viewed = viewed;
    }

    @XmlElement
    public Date getDateTimeViewed() {
        return dateTimeViewed;
    }

    public void setDateTimeViewed(Date dateTimeViewed) {
        this.dateTimeViewed = dateTimeViewed;
    }

    @XmlElement
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement
    public String getCodeSystem() {
        return codeSystem;
    }

    public void setCodeSystem(String codeSystem) {
        this.codeSystem = codeSystem;
    }

    @XmlElement
    public Integer getSeverity() {
        return severity;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idacknowledgements != null ? idacknowledgements.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acknowledgements)) {
            return false;
        }
        Acknowledgements other = (Acknowledgements) object;
        if ((this.idacknowledgements == null && other.idacknowledgements != null) || (this.idacknowledgements != null && !this.idacknowledgements.equals(other.idacknowledgements))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements[ idacknowledgements=" + idacknowledgements + " ]";
    }

    @Override
    public String getId() {
         String res = null;
        if (idacknowledgements == null) {
            return null;
        }
        else {
            res = idacknowledgements.toString();
        }
        return res;
   }

    @Override
    public boolean isNew() {
        if (idacknowledgements != null) {
            return false;
        }
        return true;
   }
    
}
