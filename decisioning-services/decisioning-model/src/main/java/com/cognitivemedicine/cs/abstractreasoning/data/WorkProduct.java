/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Adherence;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author duanedecouteau
 */
@XmlRootElement
public class WorkProduct {
    private List<Acknowledgements> acks;
    private List<RiskFactors> riskFactors;
    private List<Adherence> processAdherence;
    private LineChartServiceModel lineChart;

    /**
     * @return the riskFactors
     */
    @XmlElement
    public List<RiskFactors> getRiskFactors() {
        return riskFactors;
    }

    /**
     * @param riskFactors the riskFactors to set
     */
    public void setRiskFactors(List<RiskFactors> riskFactors) {
        this.riskFactors = riskFactors;
    }

    /**
     * @return the processAdherence
     */
    @XmlElement
    public List<Adherence> getProcessAdherence() {
        return processAdherence;
    }

    /**
     * @param processAdherence the processAdherence to set
     */
    public void setProcessAdherence(List<Adherence> processAdherence) {
        this.processAdherence = processAdherence;
    }

    /**
     * @return the lineChart
     */
    @XmlElement
    public LineChartServiceModel getLineChart() {
        return lineChart;
    }

    /**
     * @param lineChart the lineChart to set
     */
    public void setLineChart(LineChartServiceModel lineChart) {
        this.lineChart = lineChart;
    }

    /**
     * @return the acks
     */
    public List<Acknowledgements> getAcks() {
        return acks;
    }

    /**
     * @param acks the acks to set
     */
    public void setAcks(List<Acknowledgements> acks) {
        this.acks = acks;
    }
    
    
    
}
