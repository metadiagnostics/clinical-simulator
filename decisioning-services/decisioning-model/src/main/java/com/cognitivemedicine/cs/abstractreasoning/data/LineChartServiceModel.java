package com.cognitivemedicine.cs.abstractreasoning.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LineChartServiceModel {
    private LineChartServiceDetail data;
    
    public LineChartServiceModel() {
        
    }

    @XmlElement
    public LineChartServiceDetail getData() {
        return data;
    }

    public void setData(LineChartServiceDetail data) {
        this.data = data;
    }
}
