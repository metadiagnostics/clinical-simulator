/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.quartz.JobDetail;
import org.quartz.Trigger;

/**
 *
 * @author duanedecouteau
 */
@XmlRootElement
public class JobDefinition {
    
    private JobDetail jobDetail;
    
    private Trigger jobTrigger;

    /**
     * @return the jobDetail
     */
    @XmlElement
    public JobDetail getJobDetail() {
        return jobDetail;
    }

    /**
     * @param jobDetail the jobDetail to set
     */
    public void setJobDetail(JobDetail jobDetail) {
        this.jobDetail = jobDetail;
    }

    /**
     * @return the jobTrigger
     */
    @XmlElement
    public Trigger getJobTrigger() {
        return jobTrigger;
    }

    /**
     * @param jobTrigger the jobTrigger to set
     */
    public void setJobTrigger(Trigger jobTrigger) {
        this.jobTrigger = jobTrigger;
    }
    
    
    
}
