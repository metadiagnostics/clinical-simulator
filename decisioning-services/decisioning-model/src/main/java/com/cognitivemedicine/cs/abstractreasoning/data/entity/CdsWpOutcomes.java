/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data.entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.domain.Persistable;

/**
 *
 * @author duanedecouteau
 */
@Entity
@Table(name = "cds_wp_outcomes")
@XmlRootElement
public class CdsWpOutcomes implements Persistable<String> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcds_wp_outcomes")
    private Integer idcdsWpOutcomes;
    @Size(max = 60)
    @Column(name = "uuid")
    private String uuid;
    @Size(max = 80)
    @Column(name = "patient_id")
    private String patientId;
    @Size(max = 80)
    @Column(name = "ktd_reference")
    private String ktdReference;
    @Column(name = "date_processed")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateProcessed;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "work_product")
    private String workProduct;
    @Size(max = 45)
    @Column(name = "code")
    private String code;
    @Size(max = 80)
    @Column(name = "code_system")
    private String codeSystem;
    @Size(max =80)
    @Column(name = "resource_id")
    private String resourceId;
    
    public CdsWpOutcomes() {
    }

    public CdsWpOutcomes(Integer idcdsWpOutcomes) {
        this.idcdsWpOutcomes = idcdsWpOutcomes;
    }

    @XmlElement
    public Integer getIdcdsWpOutcomes() {
        return idcdsWpOutcomes;
    }

    public void setIdcdsWpOutcomes(Integer idcdsWpOutcomes) {
        this.idcdsWpOutcomes = idcdsWpOutcomes;
    }

    @XmlElement
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @XmlElement
    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    @XmlElement
    public String getKtdReference() {
        return ktdReference;
    }

    public void setKtdReference(String ktdReference) {
        this.ktdReference = ktdReference;
    }

    @XmlElement
    public Date getDateProcessed() {
        return dateProcessed;
    }

    public void setDateProcessed(Date dateProcessed) {
        this.dateProcessed = dateProcessed;
    }

    @XmlElement
    public String getWorkProduct() {
        return workProduct;
    }

    public void setWorkProduct(String workProduct) {
        this.workProduct = workProduct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcdsWpOutcomes != null ? idcdsWpOutcomes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CdsWpOutcomes)) {
            return false;
        }
        CdsWpOutcomes other = (CdsWpOutcomes) object;
        if ((this.idcdsWpOutcomes == null && other.idcdsWpOutcomes != null) || (this.idcdsWpOutcomes != null && !this.idcdsWpOutcomes.equals(other.idcdsWpOutcomes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes[ idcdsWpOutcomes=" + idcdsWpOutcomes + " ]";
    }

    @Override
    public String getId() {
        String res = null;
        if (idcdsWpOutcomes == null) {
            return null;
        }
        else {
            res = idcdsWpOutcomes.toString();
        }
        return res;
    }

    @Override
    public boolean isNew() {
        if (idcdsWpOutcomes != null) {
            return false;
        }
        return true;
    }

    /**
     * @return the code
     */
    @XmlElement
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the codeSystem
     */
    @XmlElement
    public String getCodeSystem() {
        return codeSystem;
    }

    /**
     * @param codeSystem the codeSystem to set
     */
    public void setCodeSystem(String codeSystem) {
        this.codeSystem = codeSystem;
    }

    /**
     * @return the resourceId
     */
    @XmlElement
    public String getResourceId() {
        return resourceId;
    }

    /**
     * @param resourceId the resourceId to set
     */
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }
    
}
