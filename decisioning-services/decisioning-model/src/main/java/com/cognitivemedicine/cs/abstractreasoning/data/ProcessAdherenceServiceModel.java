package com.cognitivemedicine.cs.abstractreasoning.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProcessAdherenceServiceModel {
    private String label;
    private String status;
    private List<Criteria> criteria = new ArrayList();
    
    public ProcessAdherenceServiceModel() {
        
    }

    public ProcessAdherenceServiceModel(String label, String status, List<Criteria> criteria) {
        this.label = label;
        this.status = status;
        this.criteria = criteria;
    }

    @XmlElement
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @XmlElement
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlElement
    public List<Criteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<Criteria> criteria) {
        this.criteria = criteria;
    }
}
