/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data;

import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsTask;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duanedecouteau
 */
@XmlRootElement
public class CdsTaskList {
    
    private List<CdsTask> cdstasks;

    /**
     * @return the cdstasks
     */

    @XmlElement(name = "cdsTask")
    public List<CdsTask> getCdstasks() {
        return cdstasks;
    }

    /**
     * @param cdstasks the cdstasks to set
     */
    public void setCdstasks(List<CdsTask> cdstasks) {
        this.cdstasks = cdstasks;
    }

    
}
