/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data.entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.domain.Persistable;

/**
 *
 * @author duanedecouteau
 */
@Entity
@Table(name = "alert_flags")
@XmlRootElement
public class AlertFlags implements Persistable<String> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idalert_flags")
    private Integer idalertFlags;
    @Size(max = 45)
    @Column(name = "code")
    private String code;
    @Size(max = 80)
    @Column(name = "code_system")
    private String codeSystem;
    @Size(max = 80)
    @Column(name = "display_name")
    private String displayName;
    @Column(name = "severity")
    private Integer severity;
    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    @Size(max = 80)
    @Column(name = "ktd_reference")
    private String ktdReference;
    @Size(max = 80)
    @Column(name = "patient_id")
    private String patientId;

    public AlertFlags() {
    }


    public AlertFlags(Integer idalertFlags) {
        this.idalertFlags = idalertFlags;
    }

    @XmlElement
    public Integer getIdalertFlags() {
        return idalertFlags;
    }

    public void setIdalertFlags(Integer idalertFlags) {
        this.idalertFlags = idalertFlags;
    }
    
    @XmlElement
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement    
    public String getCodeSystem() {
        return codeSystem;
    }

    public void setCodeSystem(String codeSystem) {
        this.codeSystem = codeSystem;
    }

    @XmlElement    
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @XmlElement    
    public Integer getSeverity() {
        return severity;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    @XmlElement    
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @XmlElement    
    public String getKtdReference() {
        return ktdReference;
    }

    public void setKtdReference(String ktdReference) {
        this.ktdReference = ktdReference;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idalertFlags != null ? idalertFlags.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlertFlags)) {
            return false;
        }
        AlertFlags other = (AlertFlags) object;
        if ((this.idalertFlags == null && other.idalertFlags != null) || (this.idalertFlags != null && !this.idalertFlags.equals(other.idalertFlags))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cognitivemedicine.cs.abstractreasoning.data.entity.AlertFlags[ idalertFlags=" + idalertFlags + " ]";
    }

    @Override
    public String getId() {
         String res = null;
        if (idalertFlags == null) {
            return null;
        }
        else {
            res = idalertFlags.toString();
        }
        return res;
   }

    @Override
    public boolean isNew() {
       if (idalertFlags != null) {
            return false;
        }
        return true;
     }

    /**
     * @return the patientId
     */
    @XmlElement
    public String getPatientId() {
        return patientId;
    }

    /**
     * @param patientId the patientId to set
     */
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }
    
}
