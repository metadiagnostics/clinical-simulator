/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.domain.Persistable;

/**
 *
 * @author duanedecouteau
 */
@Entity
@Table(name = "adherence")
@XmlRootElement

public class Adherence implements Persistable<String> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idadherence")
    private Integer idadherence;
    @Column(name = "resource_type")
    private String resourceType;
    @Column(name = "resource_id")
    private String resourceId;
    @Column(name = "display_name")
    private String displayName;
    @Column(name = "adherence_value")
    private String adherenceValue;
    @Lob
    @Column(name = "reasoning")
    private String reasoning;
    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    @Column(name = "classification")
    private String classification;

    public Adherence() {
    }

    public Adherence(Integer idadherence) {
        this.idadherence = idadherence;
    }

    @XmlElement
    public Integer getIdadherence() {
        return idadherence;
    }

    public void setIdadherence(Integer idadherence) {
        this.idadherence = idadherence;
    }

    @XmlElement
    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @XmlElement
    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    @XmlElement
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @XmlElement
    public String getAdherenceValue() {
        return adherenceValue;
    }

    public void setAdherenceValue(String adherenceValue) {
        this.adherenceValue = adherenceValue;
    }

    @XmlElement
    public String getReasoning() {
        return reasoning;
    }

    public void setReasoning(String reasoning) {
        this.reasoning = reasoning;
    }

    @XmlElement
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @XmlElement
    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idadherence != null ? idadherence.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adherence)) {
            return false;
        }
        Adherence other = (Adherence) object;
        if ((this.idadherence == null && other.idadherence != null) || (this.idadherence != null && !this.idadherence.equals(other.idadherence))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cognitivemedicine.common.cds.model.Adherence[ idadherence=" + idadherence + " ]";
    }

    @Override
    public String getId() {
        String res = null;
        if (idadherence == null) {
            return null;
        }
        else {
            res = idadherence.toString();
        }
        return res;
   }

    @Override
    public boolean isNew() {
       if (idadherence != null) {
            return false;
        }
        return true;
    }
    
}
