package com.cognitivemedicine.cs.abstractreasoning.data;

import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NECWorkProductModel {
    private List<RiskFactors> riskFactors;
    private List<ProcessAdherenceServiceModel> processAdherence;
    private LineChartServiceModel lineChartModel;
    
    public NECWorkProductModel() {
        
    }

    @XmlElement
    public List<RiskFactors> getRiskFactors() {
        return riskFactors;
    }

    public void setRiskFactors(List<RiskFactors> riskFactors) {
        this.riskFactors = riskFactors;
    }

    @XmlElement
    public List<ProcessAdherenceServiceModel> getProcessAdherence() {
        return processAdherence;
    }

    public void setProcessAdherence(List<ProcessAdherenceServiceModel> processAdherence) {
        this.processAdherence = processAdherence;
    }
    
    @XmlElement
    public LineChartServiceModel getLineChartModel() {
        return lineChartModel;
    }

    public void setLineChartModel(LineChartServiceModel lineChartModel) {
        this.lineChartModel = lineChartModel;
    }
}
