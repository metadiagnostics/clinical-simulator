package com.cognitivemedicine.cs.abstractreasoning.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LineChartServiceDetail {
    private String title;
    private List<LineChartServicePoint> data = new ArrayList();

    public LineChartServiceDetail() {
        this.data = new ArrayList<>();
    }

    @XmlElement
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addEntry(String x, String y) {
        this.data.add(new LineChartServicePoint(x,y));
    }

    @XmlElement
    public List<LineChartServicePoint> getData() {
        return this.data;
    }

    public void setData(List<LineChartServicePoint> data) {
        this.data = data;
    }
}