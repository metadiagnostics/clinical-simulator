/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.data.entity;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.domain.Persistable;

/**
 *
 * @author duanedecouteau
 */
@Entity
@Table(name = "cds_task")
@XmlRootElement
public class CdsTask implements Persistable<String> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcds_task")
    private Integer idcdsTask;
    @Size(max = 60)
    @Column(name = "uuid")
    private String uuid;
    @Size(max = 45)
    @Column(name = "patient_id")
    private String patientId;
    @Size(max = 80)
    @Column(name = "reasoning_topic")
    private String reasoningTopic;
    @Column(name = "time_placed_in_queue")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timePlacedInQueue;
    @Column(name = "time_acquired_for_processing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeAcquiredForProcessing;
    @Column(name = "time_completed")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeCompleted;
    @Column(name = "time_persisted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timePersisted;
    @Column(name = "total_processing_time")
    private Integer totalProcessingTime;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "work_product")
    private String workProduct;
    @Size(max = 45)
    @Column(name = "requesting_process")
    private String requestingProcess;

    public CdsTask() {
    }

    public CdsTask(Integer idcdsTask) {
        this.idcdsTask = idcdsTask;
    }

    @XmlElement
    public Integer getIdcdsTask() {
        return idcdsTask;
    }

    public void setIdcdsTask(Integer idcdsTask) {
        this.idcdsTask = idcdsTask;
    }

    @XmlElement
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPatientId() {
        return patientId;
    }

    @XmlElement
    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getReasoningTopic() {
        return reasoningTopic;
    }

    public void setReasoningTopic(String reasoningTopic) {
        this.reasoningTopic = reasoningTopic;
    }

    @XmlElement
    public Date getTimePlacedInQueue() {
        return timePlacedInQueue;
    }

    public void setTimePlacedInQueue(Date timePlacedInQueue) {
        this.timePlacedInQueue = timePlacedInQueue;
    }

    @XmlElement
    public Date getTimeAcquiredForProcessing() {
        return timeAcquiredForProcessing;
    }

    public void setTimeAcquiredForProcessing(Date timeAcquiredForProcessing) {
        this.timeAcquiredForProcessing = timeAcquiredForProcessing;
    }

    @XmlElement
    public Date getTimeCompleted() {
        return timeCompleted;
    }

    public void setTimeCompleted(Date timeCompleted) {
        this.timeCompleted = timeCompleted;
    }

    @XmlElement
    public Date getTimePersisted() {
        return timePersisted;
    }

    public void setTimePersisted(Date timePersisted) {
        this.timePersisted = timePersisted;
    }

    @XmlElement
    public Integer getTotalProcessingTime() {
        return totalProcessingTime;
    }

    public void setTotalProcessingTime(Integer totalProcessingTime) {
        this.totalProcessingTime = totalProcessingTime;
    }

    @XmlElement
    public String getWorkProduct() {
        return workProduct;
    }

    public void setWorkProduct(String workProduct) {
        this.workProduct = workProduct;
    }

    @XmlElement
    public String getRequestingProcess() {
        return requestingProcess;
    }

    public void setRequestingProcess(String requestingProcess) {
        this.requestingProcess = requestingProcess;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcdsTask != null ? idcdsTask.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CdsTask)) {
            return false;
        }
        CdsTask other = (CdsTask) object;
        if ((this.idcdsTask == null && other.idcdsTask != null) || (this.idcdsTask != null && !this.idcdsTask.equals(other.idcdsTask))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsTask[ idcdsTask=" + idcdsTask + " ]";
    }

    @Override
    public String getId() {
        String res = null;
        if (idcdsTask == null) {
            return null;
        }
        else {
            res = idcdsTask.toString();
        }
        return res;
    }

    @Override
    public boolean isNew() {
        if (idcdsTask != null) {
            return false;
        }
        return true;
   }
    
}
