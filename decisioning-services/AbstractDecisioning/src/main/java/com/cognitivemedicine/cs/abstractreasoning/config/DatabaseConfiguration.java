package com.cognitivemedicine.cs.abstractreasoning.config;

import com.cognitivemedicine.config.utils.ConfigUtils;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackages = "com.cognitivemedicine.cs.abstractreasoning.repository")
public class DatabaseConfiguration {
    @Bean(name = "dataSource", destroyMethod = "close")
    public DataSource dataSource() {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        String host = config.getString(ConfigUtilConstants.KEY_MYSQL_HOST);
        String port = config.getString(ConfigUtilConstants.KEY_MYSQL_PORT);
        String dbName = config.getString(ConfigUtilConstants.KEY_MYSQL_DBNAME);
        String username = config.getString(ConfigUtilConstants.KEY_MYSQL_USERNAME);
        String password = config.getString(ConfigUtilConstants.KEY_MYSQL_PASSWORD);

        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");

        String url = "jdbc:mysql://" + host + ":" + port + "/" + dbName + "?createDatabaseIfNotExist=true";
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("select 1;");
        return dataSource;
    }

    @Bean (name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource);
        bean.setJpaVendorAdapter(jpaVendorAdapter);
        bean.setPackagesToScan("com.cognitivemedicine.cs.abstractreasoning.data.entity");
        return bean;
    }

    @Bean( name = "jpaVendorAdapter")
    public HibernateJpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setShowSql(true);
        adapter.setGenerateDdl(true);
        adapter.setDatabase(Database.MYSQL);
        return adapter;
    }

    @Bean(name = "transactionManager")
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager factory = new JpaTransactionManager();
        return factory;
    }

    @Autowired
    private DataSource dataSource;

    @Autowired
    private HibernateJpaVendorAdapter jpaVendorAdapter;
}
