/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognitivemedicine.cs.abstractreasoning.scheduler;

import com.cognitivemedicine.cs.abstractreasoning.scheduler.jobs.UofAJob;
import org.quartz.JobDetail;
import static org.quartz.JobBuilder.newJob;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author duanedecouteau
 */
public class UofAScheduler {
    private Scheduler scheduler;
    
    public UofAScheduler() {
        //have web service to start scheduler
    }
    
    public void run() throws Exception {
        //for testing purposes
        scheduler = getScheduler();
        scheduler.start();
        //create job description
        JobDetail job = newJob(UofAJob.class).withIdentity("job1", "group1").build();
        //create job trigger
        Trigger trigger = newTrigger().withIdentity("trigger1", "group1").startNow().withSchedule(simpleSchedule().withIntervalInSeconds(10).repeatForever()).build();
        
        scheduler.scheduleJob(job, trigger);
    }
    
    
    
    public static void main(String[] args) {
        //for testing
        try {
            new UofAScheduler();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the scheduler
     */
    public Scheduler getScheduler() {
        if (scheduler == null) {
            try {
                scheduler = StdSchedulerFactory.getDefaultScheduler();
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
        }
        return scheduler;
    }

    /**
     * @param scheduler the scheduler to set
     */
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
    
}
