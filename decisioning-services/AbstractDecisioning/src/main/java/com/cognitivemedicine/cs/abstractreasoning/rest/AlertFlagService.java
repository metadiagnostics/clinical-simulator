/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.rest;


import com.cognitivemedicine.cs.abstractreasoning.repository.ReasoningReferenceRepository;
import com.cognitivemedicine.cs.abstractreasoning.config.ServiceConfigurations;
import com.cognitivemedicine.cs.abstractreasoning.data.AlertFlagsList;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.AlertFlags;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.annotations.Api;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author duanedecouteau
 */
@Api
@Component
@Path("/alertflag")
public class AlertFlagService {
    
    public static final Logger LOGGER = Logger.getLogger(AlertFlagService.class.getName());
    
    @Autowired
    private ReasoningReferenceRepository referenceRepo;
    
    @Autowired
    private ServiceConfigurations serviceConfigurations;

    
    @GET
    @Path("/query")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public AlertFlagsList getAlertFlags() {
            AlertFlagsList res = new AlertFlagsList();
            List<AlertFlags> results = referenceRepo.findAll();
            res.setAlertFlags(results);
            return res;
    }
    
    @GET
    @Path("/queryByCodeAndCodeSystem")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public AlertFlags getAlertFlagByCodeAndCodeSystem(@QueryParam("code") String code, @QueryParam("codeSystem") String codeSystem) {
        AlertFlags result = referenceRepo.findByCodeAndCodeSystem(code, codeSystem);
        return result;
    }
    
    @GET
    @Path("/queryByPatientId")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public AlertFlagsList getAlertFlagByPatientId(@QueryParam("patientId") String patientId) {
        List<AlertFlags> list = referenceRepo.findByPatientId(patientId);
        AlertFlagsList res = new AlertFlagsList();
        res.setAlertFlags(list);
        return res;
    }
    
    
    @POST
    @Path("/addAlertflag")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public AlertFlags addNewAlertFlag(String content) {
        AlertFlags res = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            AlertFlags entity = mapper.readValue(content, AlertFlags.class);
            res = referenceRepo.saveAndFlush(entity);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Add Alert Flags Failed "+ex.getMessage() , content));            
        }
        return res;
    }
    
    @DELETE
    @Path("/deleteAlertflag")
    public Response deleteAlertFlag(@QueryParam("id") String id) {
        
        if (!referenceRepo.exists(id)) {
            throw new WebApplicationException("'"+id+"' does exist.", 404);
        }
        
        referenceRepo.delete(id);
        
        return Response.ok().build();
    }
    
}
