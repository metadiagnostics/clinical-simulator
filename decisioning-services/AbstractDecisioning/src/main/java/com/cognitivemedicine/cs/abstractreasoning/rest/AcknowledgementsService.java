/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.rest;


import com.cognitivemedicine.cs.abstractreasoning.repository.AcknowledgementsRepository;
import com.cognitivemedicine.cs.abstractreasoning.config.ServiceConfigurations;
import com.cognitivemedicine.cs.abstractreasoning.data.AcknowledgementsList;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author duanedecouteau
 */
@Api
@Component
@Path("/acknowledgements")
public class AcknowledgementsService {
    
    public static final Logger LOGGER = Logger.getLogger(AcknowledgementsService.class.getName());
        
    @Autowired
    private AcknowledgementsRepository acknowledgementsRepo;
    
    @Autowired
    private ServiceConfigurations serviceConfigurations;
            
    @GET
    @Path("/queryByUserAndResourceTypeAndResourceId")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Acknowledgements getAcknowledgementsByUserIdAndResourceTypeAndResourceId(@QueryParam("userId") String userId, @QueryParam("resourceType") String resourceType, @QueryParam("resourceId") String resourceId) {

        Acknowledgements res = null;
        try {
            res = acknowledgementsRepo.findByUserIdAndResourceTypeAndResourceId(userId, resourceType, resourceId);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Find Acknowledgement by Resource ID %s. Failed "+ex.getMessage() , resourceId));
        }        
        return res;
    }
    
    @GET
    @Path("/queryByUserId")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public AcknowledgementsList getAcknowledgementsByUserId(@QueryParam("userId") String userId) {
        AcknowledgementsList res = new AcknowledgementsList();
        try {
            List<Acknowledgements> results = acknowledgementsRepo.findByUserId(userId);
            res.setAcknowledgements(results);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Find Acknowledgement by User ID %s. Failed "+ex.getMessage() , userId));
        }
        
        return res;        
    }
    
    @POST
    @Path("/addAcknowledgements")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Acknowledgements addAcknowledgements(String content) {
        Acknowledgements result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Acknowledgements ackm = mapper.readValue(content, Acknowledgements.class);
            Acknowledgements res = acknowledgementsRepo.findByUserIdAndResourceTypeAndResourceId(ackm.getUserId(), ackm.getResourceType(), ackm.getResourceId());
            try {
                if (res.getIdacknowledgements() != null && res.getIdacknowledgements().intValue() > 0) {
                    LOGGER.log(Level.INFO, String.format("Add Acknowledgement Failed Record Exists", content));
                }
            } 
            catch (Exception ex2) {
                //service return null on find so add
                result = acknowledgementsRepo.saveAndFlush(ackm);
                LOGGER.log(Level.INFO, String.format("Add Acknowledgement Success Id = "+result.getIdacknowledgements()));  
            }
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Add Acknowledgement Failed "+ex.getMessage() , content));  
            ex.printStackTrace();
        }
        return result;
    }  
}
