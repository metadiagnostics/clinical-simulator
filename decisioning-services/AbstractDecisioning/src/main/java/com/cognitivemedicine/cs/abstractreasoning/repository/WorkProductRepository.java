/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.repository;

import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author duanedecouteau
 */
public interface WorkProductRepository extends JpaRepository<CdsWpOutcomes, String> {
    
    @Query("SELECT c from CdsWpOutcomes c where c.patientId = :patientId")
    List<CdsWpOutcomes> findByPatientId(@Param("patientId") String patientId);
    @Query("select c from CdsWpOutcomes c where c.patientId = :patientId and c.ktdReference = :ktdReference")
    CdsWpOutcomes findByPatientIdAndKTDType(@Param("patientId") String patientId, @Param("ktdReference") String ktdReference);
    @Query("select c from CdsWpOutcomes c where c.patientId = :patientId and c.code = :code and c.codeSystem = :codeSystem")
    CdsWpOutcomes findByPatientIdAndCodeAndCodeSystem(@Param("patientId") String patientId, @Param("code") String code, @Param("codeSystem") String codeSystem);
    @Query("Select c from CdsWpOutcomes c where c.resourceId = :resourceId")
    CdsWpOutcomes findByResourceId(@Param("resourceId") String resourceId);
    @Query("Select c from CdsWpOutcomes c where c.patientId = :patientId and c.ktdReference = :ktdReference")
    CdsWpOutcomes findByPatientIdAndKtdReference(@Param("patientId") String patientId, @Param("ktdReference") String ktdReference);

}
