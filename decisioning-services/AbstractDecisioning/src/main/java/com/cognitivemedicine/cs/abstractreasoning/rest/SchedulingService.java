/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.rest;

import com.cognitivemedicine.cs.abstractreasoning.data.JobDefinition;
import com.cognitivemedicine.cs.abstractreasoning.scheduler.UofAScheduler;
import java.util.logging.Logger;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Component;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.cognitivemedicine.cs.abstractreasoning.scheduler.jobs.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
/**
 *
 * @author duanedecouteau
 */
@Api
@Component
@Path("/scheduler")
public class SchedulingService {
    public static final Logger LOGGER = Logger.getLogger(SchedulingService.class.getName());
    private UofAScheduler scheduler = new UofAScheduler();
    
    @POST
    @Path("/start")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response startScheduler() {
        try {
            if (scheduler.getScheduler().isShutdown()) {
                scheduler = new UofAScheduler();
            }
            scheduler.getScheduler().start();
            loadStandardJobs();
        }
        catch(Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler start failure "+ex.getMessage()));
            return Response.serverError().build();
        }
        return Response.ok().build();
    }
    
    @POST
    @Path("/shutdown")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response shutdownScheduler() {
        try {
            scheduler.getScheduler().shutdown();
        }
        catch(Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler shutdown failure "+ex.getMessage()));
            return Response.serverError().build();
        }        
        return Response.ok().build();
    }
    
    
    @POST
    @Path("/pause")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response pauseScheduler() {
        try {
            scheduler.getScheduler().pauseAll();
        }
        catch(Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler Pause All Jobs Failed "+ex.getMessage()));
            return Response.serverError().build();
        }                
        return Response.ok().build();
    }
    
    @POST
    @Path("/resume")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response resumeScheduler() {
        try {
            scheduler.getScheduler().resumeAll();
        }
        catch(Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler Resume All Jobs Failed "+ex.getMessage()));
            return Response.serverError().build();
        }                
        return Response.ok().build();
    }
    
    
    @GET
    @Path("/status")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<JobExecutionContext> statusScheduler() {
        List<JobExecutionContext> res = new ArrayList();
        try {
            res = scheduler.getScheduler().getCurrentlyExecutingJobs();
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler Check Status Failed "+ex.getMessage()));            
        }
        
        return res;
    }
    
    @POST
    @Path("/submitJob")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response submitJob(String content) {
                
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            JobDefinition def = mapper.readValue(content, JobDefinition.class);
            JobDetail jd = def.getJobDetail();
            Trigger trgr = def.getJobTrigger();
             
            scheduler.getScheduler().scheduleJob(jd, trgr);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler Check Status Failed "+ex.getMessage())); 
            return Response.serverError().build();
        }
        return Response.ok().build();
    }
    
    private void loadStandardJobs() {
        try {
            
            if (scheduler.getScheduler().isStarted()) {
                JobDetail job = newJob(UofAJob.class).withIdentity("job1", "group1").build();
                //create job trigger
                Trigger trigger = newTrigger().withIdentity("trigger1", "group1").startNow().withSchedule(simpleSchedule().withIntervalInHours(1).repeatForever()).build();

                scheduler.getScheduler().scheduleJob(job, trigger); 
            }
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Scheduler Check Status Failed "+ex.getMessage()));             
        }
    }
    
}
