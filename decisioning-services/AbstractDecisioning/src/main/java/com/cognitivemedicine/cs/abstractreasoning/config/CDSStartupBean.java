/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.config;

import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsTask;
import com.cognitivemedicine.cs.abstractreasoning.repository.ReasoningTaskRepository;
import com.cognitivemedicine.cs.abstractreasoning.repository.WorkProductRepository;
import com.cognitivemedicine.cs.abstractreasoning.service.CDSTaskService;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.WebApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author duanedecouteau
 */
@Component
public class CDSStartupBean {
    private final static Logger LOGGER = Logger.getLogger(CDSStartupBean.class.getName());    
    
    @Autowired
    private ServiceConfigurations serviceConfiguration;
    
    @Autowired
    private ReasoningTaskRepository taskRepo;
    
    @Autowired
    private WorkProductRepository workProductRepo;
    
    @Autowired 
    private LinkedBlockingQueue<String> taskQueue;
    
    private List<CDSTaskService> cdsTaskThreads;
     
    @PostConstruct
    public void start() {
        
        try {
            cdsTaskThreads = new ArrayList<>();
            for (int i=0; i < serviceConfiguration.getNumberOfCDSTaskThreads(); i++) {
                CDSTaskService cdsTaskServiceThread = new CDSTaskService(taskRepo, workProductRepo, taskQueue);
                cdsTaskServiceThread.setName("CDS Task Thread-"+i);
                cdsTaskThreads.add(cdsTaskServiceThread);
                cdsTaskServiceThread.start();
            }
            //load the queue with whatever remaining from previous (perhaps interrupted) run.
            {
                List<CdsTask> cdsTasks = taskRepo.findAll();
                for (CdsTask cdsTask: cdsTasks)
                    taskQueue.add(cdsTask.getUuid().toString());
            }



        }
        catch (Exception e)
        {
            LOGGER.log(Level.INFO,
                    String.format("Error occurred: %s %s", e.getClass().getCanonicalName(), e.getMessage()));
            throw new WebApplicationException(e);
        }
        
    }
    
    @PreDestroy
    public void stop() {


        for (CDSTaskService cdsServiceThread : cdsTaskThreads) {
            if (cdsServiceThread != null && cdsServiceThread.isAlive())
                cdsServiceThread.interrupt();
        }

    }    
    
}
