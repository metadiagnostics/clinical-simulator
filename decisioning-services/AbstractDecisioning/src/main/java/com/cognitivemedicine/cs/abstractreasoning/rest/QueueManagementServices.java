/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.rest;

import com.cognitivemedicine.cs.abstractreasoning.data.CdsTaskList;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsTask;
import com.cognitivemedicine.cs.abstractreasoning.repository.ReasoningTaskRepository;
import static com.cognitivemedicine.cs.abstractreasoning.rest.AbstractReasoningService.LOGGER;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author duanedecouteau
 */
@Api
@Component
@Path("/queue")
public class QueueManagementServices {
        public static final Logger LOGGER = Logger.getLogger(QueueManagementServices.class.getName());
        
        @Autowired
        private ReasoningTaskRepository taskRepo;
        
        @Autowired
        private LinkedBlockingQueue<String> taskQueue;
        
        @GET
        @Path("/getAllTasks")
        @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
        public CdsTaskList getAllTasks() {
            CdsTaskList res = new CdsTaskList();
            try {
                List<CdsTask> results = taskRepo.findAll();
                res.setCdstasks(results);
            }
            catch (Exception ex) {
                LOGGER.log(Level.INFO, String.format("Failed to get ALL Task From Queue. "));                
            }
            
            return res;
        }
        
        @GET
        @Path("/getTask")
        @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
        public CdsTask getTask(@QueryParam("uuid") String uuid) {
            CdsTask res = null;
            try {
                res = taskRepo.findByUuid(uuid);
                        
            }
            catch (Exception ex) {
                LOGGER.log(Level.INFO, String.format("Failed to get Tasks From Queue. ", uuid));                                
            }
            
            return res;
        }

        @GET
        @Path("/getTasksByPatient")
        @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
        public CdsTaskList getTaskByPatient(@QueryParam("patientId") String patientId) {
            CdsTaskList res = new CdsTaskList();
            try {
                List<CdsTask> result = taskRepo.findByPatientId(patientId);
                res.setCdstasks(result);
            }
            catch (Exception ex) {
                LOGGER.log(Level.INFO, String.format("Failed to get Tasks From Queue. ", patientId));                                                
            }
            
            return res;
        }
        
        @POST
        @Path("/addTask")
        @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
        public Response addTask(@QueryParam("uuid") String uuid, String content) {
            CdsTask res = taskRepo.findByUuid(uuid);
            if (res != null) {
                LOGGER.log(Level.INFO, String.format("Task Add Request ID %s. Request rejected. " , uuid));
                throw new WebApplicationException
                    ("This request ID already exists.", Response.status(Response.Status.CONFLICT)
                    .build());
            }
            else {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    CdsTask task = mapper.readValue(content, CdsTask.class);
                    CdsTask taskSaved = taskRepo.saveAndFlush(res);
                    taskQueue.add(uuid);
                }
                catch (Exception ex) {
                    LOGGER.log(Level.INFO, String.format("Task Add Request ID %s. Request rejected. " , uuid));
                }
            }
            
            return Response.ok().build();
        }
        
        @POST
        @Path("/updateTask")
        @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
        public Response updateTask(@QueryParam("uuid") String uuid, String content) {
            CdsTask task = taskRepo.findByUuid(uuid);
            if (task == null) {
                LOGGER.log(Level.INFO, String.format("Task Update Request ID %s. Request rejected. " , uuid));
                throw new WebApplicationException
                    ("This request ID does not exist.", Response.status(Response.Status.CONFLICT)
                    .build());                
            }
            else {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    CdsTask tempTask = mapper.readValue(content, CdsTask.class);
                    task.setTimeAcquiredForProcessing(tempTask.getTimeAcquiredForProcessing());
                    task.setTimeCompleted(tempTask.getTimeCompleted());
                    task.setTotalProcessingTime(tempTask.getTotalProcessingTime());
                    task.setWorkProduct(tempTask.getWorkProduct());
                    task = taskRepo.saveAndFlush(task);
                }
                catch (Exception ex) {
                    LOGGER.log(Level.INFO, String.format("Task Update Request ID %s. Request rejected. " , uuid));
                    throw new WebApplicationException
                        ("DB Task Update Failed.", Response.status(Response.Status.CONFLICT)
                        .build());                     
                }
                
            }
            return Response.ok().build();
        }
        
        @POST
        @Path("/reloadTasks")
        @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
        public Response reloadTasks() {
            try {
                taskQueue.clear();
                List<CdsTask> tasks = taskRepo.findAll();
                Iterator iter = tasks.iterator();
                while (iter.hasNext()) {
                    CdsTask t = (CdsTask)iter.next();
                    taskQueue.add(t.getUuid());
                }
            }
            catch(Exception ex) {
                    LOGGER.log(Level.INFO, String.format("Task Reload Request rejected. "));
                    throw new WebApplicationException
                        ("Task Reload Failed.", Response.status(Response.Status.CONFLICT)
                        .build());                                    
            }
            return Response.ok().build();
        }
        
        @DELETE
        @Path("/deleteTask")
        public Response deleteTask(@QueryParam("uuid") String uuid) {
            try {
                CdsTask task = taskRepo.findByUuid(uuid);
                taskRepo.delete(task);
                taskQueue.remove(uuid);
            }
            catch (Exception ex) {
                    LOGGER.log(Level.INFO, String.format("Task Reload Request rejected. "));
                    throw new WebApplicationException
                        ("Task Reload Failed.", Response.status(Response.Status.CONFLICT)
                        .build());                                                    
            }
            
            return Response.ok().build();
        }
        
}
