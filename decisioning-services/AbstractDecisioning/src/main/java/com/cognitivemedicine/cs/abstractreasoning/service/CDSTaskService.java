/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.service;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author duanedecouteau
 */
public class CDSTaskService extends Thread {
    private final static Logger LOGGER = Logger.getLogger(CDSTaskService.class.getName()); 
    
    private JpaRepository taskRepo;
    private JpaRepository workProductRepo;
    
    private LinkedBlockingQueue<String> taskQueue;
    
    private boolean isShutdown = false;
    
    public CDSTaskService(JpaRepository taskRepo, JpaRepository workProductRepo, LinkedBlockingQueue<String> taskQueue) {
        this.taskQueue = taskQueue;
        this.taskRepo = taskRepo;
        this.workProductRepo = workProductRepo;
        
    }
    
    public void shutdown() {
        
    }
    
    @Override
    public void run() {
        
    }
    
    
}
