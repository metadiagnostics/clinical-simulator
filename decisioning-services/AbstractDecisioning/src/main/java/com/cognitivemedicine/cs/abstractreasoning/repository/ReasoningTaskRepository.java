/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.repository;

import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsTask;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
/**
 *
 * @author duanedecouteau
 */
public interface ReasoningTaskRepository extends JpaRepository<CdsTask, String> {
    
    List<CdsTask> findAll();
    @Query("SELECT c from CdsTask c where c.uuid = :uuid")
    CdsTask findByUuid(@Param("uuid") String uuid);
    @Query("SELECT c from CdsTask c where c.patientId = :patientId")
    List<CdsTask> findByPatientId(@Param("patientId") String patientId);
}
