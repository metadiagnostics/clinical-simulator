package com.cognitivemedicine.cs.abstractreasoning.config;

public class ConfigUtilConstants {
    public static final String CONTEXT_NAME = "abstract.decisioning.server.config";
    public static final String KEY_MYSQL_HOST = "mysql.host";
    public static final String KEY_MYSQL_PORT = "mysql.port";
    public static final String KEY_MYSQL_DBNAME = "mysql.dbname";
    public static final String KEY_MYSQL_USERNAME = "mysql.username";
    public static final String KEY_MYSQL_PASSWORD = "mysql.password";
    public static final String KEY_JMS_QUEUE = "cdsp.jms.url";
}
