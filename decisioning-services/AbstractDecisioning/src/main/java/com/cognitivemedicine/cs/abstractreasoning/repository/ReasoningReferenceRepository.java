/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.repository;

import com.cognitivemedicine.cs.abstractreasoning.data.entity.AlertFlags;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author duanedecouteau
 */
public interface ReasoningReferenceRepository extends JpaRepository<AlertFlags, String> {
    
    public List<AlertFlags> findAll();
    
    @Query("Select a from AlertFlags a where a.code = :code and a.codeSystem = :codeSystem")
    public AlertFlags findByCodeAndCodeSystem(@Param("code") String code, @Param("codeSystem") String codeSystem);
    @Query("Select a from AlertFlags a where a.patientId = :patientId")
    public List<AlertFlags> findByPatientId(@Param("patientId") String patientId);
    
}
