/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.rest;


import com.cognitivemedicine.cs.abstractreasoning.repository.WorkProductRepository;
import com.cognitivemedicine.cs.abstractreasoning.repository.ReasoningTaskRepository;
import com.cognitivemedicine.cs.abstractreasoning.config.ServiceConfigurations;
import com.cognitivemedicine.cs.abstractreasoning.data.ActiveQueue;
import com.cognitivemedicine.cs.abstractreasoning.data.CdsWpOutcomesList;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author duanedecouteau
 */
@Api
@Component
@Path("/reason")
public class AbstractReasoningService {
    
    public static final Logger LOGGER = Logger.getLogger(AbstractReasoningService.class.getName());
        
    @Autowired
    private ReasoningTaskRepository taskRepo;
    
    @Autowired 
    private WorkProductRepository workProductRepo;
        
    @Autowired
    private ServiceConfigurations serviceConfigurations;
    
    @Autowired
    private LinkedBlockingQueue<String> taskQueue;
            
    @GET
    @Path("/findWorkProductByPatientId")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CdsWpOutcomesList getAllWorkProductsByPatientId(@QueryParam("patientId") String patientId) {
        CdsWpOutcomesList res = new CdsWpOutcomesList();
        try {
           List<CdsWpOutcomes> result = workProductRepo.findByPatientId(patientId);
           res.setCdswpoutcomes(result);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Get CDS Work Product By PatientId Failed "+ex.getMessage() , patientId));              
        }
        return res;
    }
    
    @GET
    @Path("/findWorkProductByPatientAndKTD")
    @Produces({MediaType.APPLICATION_XML})
    public CdsWpOutcomes getWorkProductByPatientIdAndKTDType(@QueryParam("patientId") String patientId, @QueryParam("ktdReference") String ktdReference) {
        CdsWpOutcomes res = null;
        try {
            res = workProductRepo.findByPatientIdAndKTDType(patientId, ktdReference);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Get CDS Work Product By PatientId and KTD Type Failed "+ex.getMessage() , patientId));           
        }
        
        return res;
        
    }
    
    @GET
    @Path("/findWorkProductByPatientAndCoding")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CdsWpOutcomes getWorkProductByPatientIdAndCoding(@QueryParam("patientId") String patientId, @QueryParam("code") String code, @QueryParam("codeSystem") String codeSystem) {
        CdsWpOutcomes res = null;
        try {
            res = workProductRepo.findByPatientIdAndCodeAndCodeSystem(patientId, code, codeSystem);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Get CDS Work Product By PatientId and code codesystem Failed "+ex.getMessage() , patientId));           
            
        }
        return res;
    }
    
    @POST
    @Path("/cdsworkproduct")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addWorkProduct(String content) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            CdsWpOutcomes wp = mapper.readValue(content, CdsWpOutcomes.class);
            long cnt = workProductRepo.count();
            Long lg = new Long(cnt);
            wp.setIdcdsWpOutcomes(lg.intValue() + 1);
            workProductRepo.saveAndFlush(wp);
        }
        catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("POST Work Product Failed "+ex.getMessage() , content));
            return Response.serverError().build();
        }
        return Response.ok().build();
    }
    
    @GET
    @Path("/findWorkProductByResourceId")
    @Consumes({MediaType.APPLICATION_XML, MediaType.TEXT_XML})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public CdsWpOutcomes getWorkProductByResourceId(@QueryParam("resourceId") String resourceId) {
        CdsWpOutcomes res = null;
        try {
            res = workProductRepo.findByResourceId(resourceId);
        }
        catch(Exception ex) {
            LOGGER.log(Level.INFO, String.format("Get CDS Work Product By Resource Failed "+ex.getMessage() , resourceId));                          
        }
        return res;
    }
    
    @DELETE
    @Path("/deleteByPatientIdAndKTDRef")
    public Response deleteByPatientIdAndKTDRef(@QueryParam("patientId") String patientId, @QueryParam("ktdReference") String ktdReference) {
        try {
            CdsWpOutcomes wp = workProductRepo.findByPatientIdAndKtdReference(patientId, ktdReference);
            workProductRepo.delete(wp);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return Response.serverError().build();
        }
        return Response.ok().build();
    }
    
    
}
