/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognitivemedicine.cs.abstractreasoning.scheduler.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.cognitivemedicine.cdsp.cwf.jms.JMSServiceFactory;
import com.cognitivemedicine.cs.abstractreasoning.scheduler.utils.PublisherHelper;
import com.cognitivemedicine.config.utils.ConfigUtils;
import com.cognitivemedicine.cs.abstractreasoning.config.ConfigUtilConstants;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


/**
 *
 * @author duanedecouteau
 */
public class UofAJob implements Job {
    
    private PublisherHelper publisher;
    private String topic = "CASE_ALL";
    private String data = "KTD-NEC-";
    private String url;
    
    public UofAJob() {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        url = config.getString(ConfigUtilConstants.KEY_JMS_QUEUE);
    }
    
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        String discriminator = JMSServiceFactory.ContextKeys.JMS_CASE_EVENT_DISCRIMINATOR_VALUE;
        if (this.publisher == null) {
            this.publisher = new PublisherHelper(url, topic, "DSSRequestId");
        }
        sendMessage(topic, getDataDateTimeString());
        System.out.println("UofAJob message sent");
    }
    
    public void sendMessage(String topic, String data){
        publisher.publishString(topic, data);
    }

    private String getDataDateTimeString() {
        String dateRes = data;
        
        try {
            ZonedDateTime local = ZonedDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
            dateRes = dateRes + local.format(formatter);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return dateRes;
    }
    
}
