/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.staging.data;

import com.cognitivemedicine.cs.abstractreasoning.data.Assessment;
import com.cognitivemedicine.cs.abstractreasoning.data.Criteria;
import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.LineChartServiceDetail;
import com.cognitivemedicine.cs.abstractreasoning.data.LineChartServiceModel;
import com.cognitivemedicine.cs.abstractreasoning.data.ProcessAdherenceServiceModel;
import com.cognitivemedicine.cs.abstractreasoning.data.ProblemType;
import com.cognitivemedicine.cs.abstractreasoning.data.RiskFactors;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author duanedecouteau
 */
public class MockData {
    
    public MockData() {
        
    }
    
    public NECWorkProductModel getDetailData() {
        NECWorkProductModel model = new NECWorkProductModel();
        try {
        List<RiskFactors> riskFactors = new ArrayList(); 

        RiskFactors riskFactorOne = new RiskFactors("Steroid Exposure", "High");
        RiskFactors riskFactorTwo = new RiskFactors("Multiple Transfusions", "Moderate");
        RiskFactors riskFactorThree = new RiskFactors("PDA", "Minimal");
        
        riskFactors.add(riskFactorOne);
        riskFactors.add(riskFactorTwo);
        riskFactors.add(riskFactorThree);
        
        model.setRiskFactors(riskFactors);
        

//        GridModel riskFactorGridModel = new GridModel();
//        GridRow riskFactorOne = new GridRow("Steroid Exposure");
//        riskFactorOne.addCell(new GridCell(ProblemType.CONTRIBUTION, "High"));
//        GridRow riskFactorTwo = new GridRow("Multiple Transfusions");
//        riskFactorTwo.addCell(new GridCell(ProblemType.CONTRIBUTION, "Moderate"));
//        GridRow riskFactorThree = new GridRow("PDA");
//        riskFactorThree.addCell(new GridCell(ProblemType.CONTRIBUTION, "Minimal"));
//        riskFactorGridModel.addRow(riskFactorOne);
//        riskFactorGridModel.addRow(riskFactorTwo);
//        riskFactorGridModel.addRow(riskFactorThree);
//
//        riskFactors.put("data", riskFactorGridModel.convertToSerializeableModel());
//        riskFactors.put("type", ProblemType.FACTOR_NAME);
//        model.setRiskFactors(riskFactors);

        
        List<ProcessAdherenceServiceModel> processAdherenceList = new ArrayList<>();
//        Map<String, Object> criteriaOne = new HashMap<>();
//        GridModel paOne = new GridModel();
//        GridRow rowOne = new GridRow("20171130");
//        rowOne.addCell(new GridCell(ProblemType.NAME, "Being fed human milk"));
//        rowOne.addCell(new GridCell(ProblemType.ASSESSMENT, "Compliant"));
//        paOne.addRow(rowOne);
//        criteriaOne.put("data", paOne.convertToSerializeableModel());
//        criteriaOne.put("type", ProblemType.DATE_TIME);
//        criteriaOne.put("dataType", "date");
//
//        processAdherenceList.add(new ProcessAdherenceServiceModel("Human Milk", "ok", criteriaOne));
        
        Criteria criteriaOne = new Criteria();
        Assessment assessment1 = new Assessment();
        assessment1.setCompliancy("Compliant");
        assessment1.setDate(new SimpleDateFormat("yyyyMMdd").parse("20171130"));
        assessment1.setName("Being fed human milk");
        criteriaOne.getData().add(assessment1);
        criteriaOne.setDate(new Date());
        criteriaOne.setType(ProblemType.DATE_TIME);
        
        List<Criteria> criteriaList = new ArrayList();
        criteriaList.add(criteriaOne);
        ProcessAdherenceServiceModel pModelOne = new ProcessAdherenceServiceModel("Human Milk", "ok", criteriaList);
        processAdherenceList.add(pModelOne);
        

//        Map<String, Object> criteriaTwo = new HashMap<>();
//        GridModel paTwo = new GridModel();
//        GridRow rowTwo = new GridRow("20171129");
//        rowTwo.addCell(new GridCell(ProblemType.NAME, "Clean teeth"));
//        rowTwo.addCell(new GridCell(ProblemType.ASSESSMENT, "Partially Compliant"));
//        paTwo.addRow(rowTwo);
//        criteriaTwo.put("data", paTwo.convertToSerializeableModel());
//        criteriaTwo.put("type", ProblemType.DATE_TIME);
//        criteriaTwo.put("dataType", "date");
//        processAdherenceList.add(new ProcessAdherenceServiceModel("Oral Care", "caution", criteriaTwo));

        Criteria criteriaTwo = new Criteria();
        Assessment assessment2 = new Assessment();
        assessment2.setCompliancy("Partially Compliant");
        assessment2.setDate(new SimpleDateFormat("yyyyMMdd").parse("20171129"));
        assessment2.setName("Clean Teeth");
        criteriaTwo.getData().add(assessment2);
        criteriaTwo.setDate(new Date());
        criteriaTwo.setType(ProblemType.DATE_TIME);
        List<Criteria> criteriaList2 = new ArrayList();
        criteriaList2.add(criteriaTwo);
        ProcessAdherenceServiceModel pModelTwo = new ProcessAdherenceServiceModel("Oral Care", "caution", criteriaList2);
        processAdherenceList.add(pModelTwo);
        

//        Map<String, Object> criteriaThree = new HashMap<>();
//        GridModel paThree = new GridModel();
//        GridRow rowThree = new GridRow("20171030");
//        rowThree.addCell(new GridCell(ProblemType.NAME, "H2-Blocker Prescribing"));
//        rowThree.addCell(new GridCell(ProblemType.ASSESSMENT, "Non-Compliant"));
//        paThree.addRow(rowThree);
//
//        GridRow rowFour = new GridRow("20171030");
//        rowFour.addCell(new GridCell(ProblemType.NAME, "Antibiotic Duration"));
//        rowFour.addCell(new GridCell(ProblemType.ASSESSMENT, "Compliant"));
//        paThree.addRow(rowFour);
//
//        criteriaThree.put("data", paThree.convertToSerializeableModel());
//        criteriaThree.put("type", ProblemType.DATE_TIME);
//        criteriaThree.put("dataType", "date");
//        processAdherenceList.add(new ProcessAdherenceServiceModel("Medication Stewardship", "fail", criteriaThree));

        Criteria criteriaThree = new Criteria();
        Assessment assessment3 = new Assessment();
        assessment3.setCompliancy("Non-Compliant");
        assessment3.setDate(new SimpleDateFormat("yyyyMMdd").parse("20171030"));
        assessment3.setName("H2-Blocker Prescribing");
        Assessment assessment4 = new Assessment();
        assessment4.setCompliancy("Compliant");
        assessment4.setDate(new SimpleDateFormat("yyyyMMdd").parse("20171030"));
        assessment4.setName("Antibiotic Duration");
        criteriaThree.getData().add(assessment3);
        criteriaThree.getData().add(assessment4);
        criteriaThree.setDate(new Date());
        criteriaThree.setType(ProblemType.DATE_TIME);
        List<Criteria> criteriaList3 = new ArrayList();
        criteriaList3.add(criteriaThree);
        ProcessAdherenceServiceModel pModelThree = new ProcessAdherenceServiceModel("Medication Stewardship", "fail", criteriaList3);
        processAdherenceList.add(pModelThree);

//        Map<String, Object> criteriaFour = new HashMap<>();
//        GridModel paFour = new GridModel();
//        GridRow rowFive = new GridRow("20171130");
//        rowFive.addCell(new GridCell(ProblemType.NAME, "Feeding criteria 1"));
//        rowFive.addCell(new GridCell(ProblemType.ASSESSMENT, "Compliant"));
//        paFour.addRow(rowFive);
//
//        GridRow rowSix = new GridRow("20171130");
//        rowSix.addCell(new GridCell(ProblemType.NAME, "Feeding criteria 2"));
//        rowSix.addCell(new GridCell(ProblemType.ASSESSMENT, "Compliant"));
//        paFour.addRow(rowSix);
//
//        criteriaFour.put("data", paFour.convertToSerializeableModel());
//        criteriaFour.put("type", ProblemType.DATE_TIME);
//        criteriaFour.put("dataType", "date");
//        processAdherenceList.add(new ProcessAdherenceServiceModel("Feeding Protocol", "ok", criteriaFour));

        Criteria criteriaFour = new Criteria();
        Assessment assessment5 = new Assessment();
        assessment5.setCompliancy("Compliant");
        assessment5.setDate(new SimpleDateFormat("yyyyMMdd").parse("20171130"));
        assessment5.setName("Feeding criteria 1");
        Assessment assessment6 = new Assessment();
        assessment6.setCompliancy("Compliant");
        assessment6.setDate(new SimpleDateFormat("yyyyMMdd").parse("20171130"));
        assessment6.setName("Feeding criteria 2");
        criteriaFour.getData().add(assessment5);
        criteriaFour.getData().add(assessment6);
        criteriaFour.setDate(new Date());
        criteriaFour.setType(ProblemType.DATE_TIME);
        List<Criteria> criteriaList4 = new ArrayList();
        criteriaList4.add(criteriaFour);
        ProcessAdherenceServiceModel pModelFour = new ProcessAdherenceServiceModel("Feeding Protocol", "ok", criteriaList4);
        processAdherenceList.add(pModelFour);

        model.setProcessAdherence(processAdherenceList);

        LineChartServiceModel gutCheckNECModel = new LineChartServiceModel();
        LineChartServiceDetail detail = new LineChartServiceDetail();
        detail.setTitle("GutCheckNEC Trend");
        detail.addEntry("10", "10");
        detail.addEntry("11", "11");
        detail.addEntry("12", "12");
        gutCheckNECModel.setData(detail);

        model.setLineChartModel(gutCheckNECModel);
        
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return model;
    }
    
}
