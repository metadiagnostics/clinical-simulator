/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractreasoning.staging;

import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractdecisioning.client.api.RestClient;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.cognitivemedicine.cs.abstractreasoning.staging.data.MockData;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author duanedecouteau
 */
public class Loader {
    private MockData mockdata = new MockData();
    private RestClient client;
    
    public void loadTestCdsOutcomes() {
        CdsWpOutcomes outcome = new CdsWpOutcomes();
        outcome.setCode("2707005");
        outcome.setCodeSystem("http://snomed.info/sct");
        outcome.setDateProcessed(new Date());
        outcome.setKtdReference("NEC");
        outcome.setPatientId("PatientId-1234");
        outcome.setResourceId("VHA-EHTAC-Condition-008");
        UUID uuid = UUID.randomUUID();
        outcome.setUuid(uuid.toString());
        //get work product as string
        try {
            NECWorkProductModel model = mockdata.getDetailData();
            ObjectMapper mapper = new ObjectMapper();
            String res = mapper.writeValueAsString(model);
            outcome.setWorkProduct(res);
            client = new RestClient();
            client.addCdsWpOutcome(outcome);
            
        }
        catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public void addAcknowledgementsTest() {
        Acknowledgements ack = new Acknowledgements();
        ack.setUserId("sam");
        ack.setResourceId("VHA-EHTAC-Condition-008");
        ack.setResourceType("Condition");
        ack.setViewed(new Integer(1));
        try {
            client.addAcknowledgement("sam", ack);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
        
    public static void main(String[] args) throws IOException{
        Loader loader = new Loader();
        try {
            loader.loadTestCdsOutcomes();
            loader.addAcknowledgementsTest();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}
