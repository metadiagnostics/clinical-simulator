/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.abstractdecisioning.client.api;

import com.cognitivemedicine.cs.abstractreasoning.data.AlertFlagsList;
import com.cognitivemedicine.cs.abstractreasoning.data.NECWorkProductModel;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.Acknowledgements;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.AlertFlags;
import com.cognitivemedicine.cs.abstractreasoning.data.entity.CdsWpOutcomes;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.fasterxml.jackson.jaxrs.xml.JacksonXMLProvider;
import java.util.Date;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author duanedecouteau for initial rest services testing for fujion gui
 */
public class RestClient {

    public static final Logger LOGGER = Logger.getLogger(RestClient.class.getName());

    private String baseURL = "http://localhost:8080/AbstractDecisioning/services";
    private String base;
    private Client client;
    private Date runDateTime;

    public RestClient() {

    }

    public RestClient(String baseURL) {
        this.baseURL = baseURL;
    }

    private Client getClient(){
        if (client == null) {
            client = ClientBuilder.newClient(new ClientConfig().register( 
                new JacksonJsonProvider().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            ).register(
                new JacksonXMLProvider().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            ));
        }
        return client;
    }
    
    public NECWorkProductModel retrieveDetailData(String resourceId) {
        NECWorkProductModel res = null;
        try {
            client = getClient();
            WebTarget target = client.target(baseURL);

            System.out.println("GET " + baseURL + "/reason/findWorkProductByResourceId?resourceId=" + resourceId);
            Response response = target.path("reason").path("findWorkProductByResourceId").queryParam("resourceId", resourceId).request(MediaType.APPLICATION_XML).get();

            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Failed Work Product Lookup. ", resourceId));
            } else {
                CdsWpOutcomes outcomes = response.readEntity(CdsWpOutcomes.class);

                String wp = outcomes.getWorkProduct();

                System.out.println(wp);

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                res = mapper.readValue(wp, NECWorkProductModel.class);

            }
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Failed Work Product Lookup. ", resourceId));
            ex.printStackTrace();
        }

        return res;
    }

    public CdsWpOutcomes getDecisioningOutcomeByPatientIdAndKTDType(String patientId, String ktdReference) {
        CdsWpOutcomes outcome = null;
        try {

            client = getClient();
            WebTarget target = client.target(baseURL);

            Response response = target.path("reason").path("findWorkProductByPatientAndKTD")
                .queryParam("patientId", patientId)
                .queryParam("ktdReference", ktdReference)
                .request(MediaType.APPLICATION_XML).get();
            

            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Failed Work Product Lookup. "));
            } else {
                outcome = response.readEntity(CdsWpOutcomes.class);
            }
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Failed Work Product Lookup. "));
            ex.printStackTrace();
        }

        return outcome;
    }

    public CdsWpOutcomes getDecisioningOutcomeByPatientIdAndCoding(String patientId, String code, String codeSystem) {
        CdsWpOutcomes outcome = null;
        try {

            client = getClient();
            WebTarget target = client.target(baseURL);

            Response response = target.path("reason").path("findWorkProductByPatientAndCoding")
                .queryParam("patientId", patientId)
                .queryParam("code", code)
                .queryParam("codeSystem", codeSystem)
                .request(MediaType.APPLICATION_XML).get();

            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Failed Work Product Lookup. "));
            } else {
                outcome = response.readEntity(CdsWpOutcomes.class);
            }
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Failed Work Product Lookup. "));
            ex.printStackTrace();
        }

        return outcome;
    }

    public List<AlertFlags> getAlertFlagsByPatientId(String patientId) {
        List<AlertFlags> aList = new ArrayList();
        try {
            client = getClient();
            WebTarget target = client.target(baseURL);

            Response response = target.path("alertflag").path("queryByPatientId")
                .queryParam("patientId", patientId)
                .request(MediaType.APPLICATION_JSON).get();

            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Alert Flag Failed Lookup. "));
            } else {
                AlertFlagsList res = response.readEntity(AlertFlagsList.class);
                aList = res.getAlertFlags();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return aList;
    }

    public List<Acknowledgements> getAcknowledgements(String userId, String resourceId) {
        List<Acknowledgements> aList = new ArrayList();
        try {
            client = getClient();
            WebTarget target = client.target(baseURL);

            Response response = target.path("acknowledgements").path("queryByUserAndResourceTypeAndResourceId")
                .queryParam("userId", userId)
                .queryParam("resourceType", "Condition")
                .queryParam("resourceId", resourceId)
                .request(MediaType.APPLICATION_XML).get();

            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Acknowledgments Failed Lookup. "));
            } else {
                Acknowledgements res = response.readEntity(Acknowledgements.class);
                aList.add(res);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return aList;
    }

    public void addAcknowledgement(String userId, Acknowledgements acknowledgements) {
        try {
            //make sure it's current userId acknowledging
            acknowledgements.setUserId(userId);
            client = getClient();
            WebTarget target = client.target(baseURL);

            Response response = target.path("acknowledgements").path("addAcknowledgements")
                .request(MediaType.APPLICATION_JSON).post(Entity.json(acknowledgements));
            
            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Failed Acknowledgement Add. ", userId));
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Failed Acknowledgement Add. ", userId));
        }

    }

    public void addCdsWpOutcome(CdsWpOutcomes cdswpoutcomes) {
        try {

            
            WebTarget target = client.target(baseURL);

            Response response = target.path("reason").path("cdsworkproduct")
                .request(MediaType.APPLICATION_JSON).post(Entity.json(cdswpoutcomes));
            
            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Failed CDS Workproduct Outcome Add. "));
                throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
            }

        } catch (Exception ex) {
            LOGGER.log(Level.INFO, String.format("Failed CDS Workproduct Outcome Add. "));
            ex.printStackTrace();
        }

    }

    public void deleteCdsWpOutcome(String patientId, String ktdReference) {
        try {
            client = getClient();
            WebTarget target = client.target(baseURL);

            Response response = target.path("reason").path("deleteByPatientIdAndKTDRef")
                .queryParam("patientId", patientId)
                .queryParam("ktdReference", ktdReference)
                .request(MediaType.APPLICATION_JSON).delete();

            if (response.getStatus() != 200) {
                LOGGER.log(Level.INFO, String.format("Work Product Delete Failed. "));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
        try {

            RestClient client = new RestClient();
            String search = "/Condition/condition-1234";
            String resourceId = new String(search.getBytes(), "UTF-8");
            NECWorkProductModel model = client.retrieveDetailData(resourceId);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
