/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cdsp.cs.setup;

import com.cognitivemedicine.cdsp.cwf.ui.runner.store.ScenarioStore;
import com.cognitivemedicine.cdsp.simulator.serialization.ScenarioSerializerRegistry;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import com.cognitivemedicine.csv.util.UofAScenarioSerializer;

/**
 *
 * @author esteban
 */
public class CaseScenarioSetup {
    
    private final ScenarioStore store;
    private final ScenarioSerializerRegistry scenarioSerializerRegistry;
    private final List<Resource> scenarioDefinitions = new ArrayList<>();

    public CaseScenarioSetup(ScenarioStore store, ScenarioSerializerRegistry scenarioSerializerRegistry, List<Resource> scenarioDefinitions){
        this.store = store;
        this.scenarioDefinitions.addAll(scenarioDefinitions);
        this.scenarioSerializerRegistry = scenarioSerializerRegistry;
        this.scenarioSerializerRegistry.registerSerializer(new UofAScenarioSerializer());
        Collections.sort(scenarioDefinitions, (d1, d2) -> d1.getFilename().compareTo(d2.getFilename()));
    }
    
    @PostConstruct
    public void loadScenarios() throws IOException{

        for (Resource scenarioResource : scenarioDefinitions) {

            InputStreamReader reader = new InputStreamReader(scenarioResource.getInputStream());
            String content = IOUtils.toString(reader);
            reader.close();

            String fileName = scenarioResource.getFilename();
            store.registerNewScenario(fileName, content);
        }
    }
    
}
