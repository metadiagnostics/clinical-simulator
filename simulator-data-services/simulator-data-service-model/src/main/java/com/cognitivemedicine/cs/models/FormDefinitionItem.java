package com.cognitivemedicine.cs.models;

import java.util.List;

public class FormDefinitionItem {
    private String question;
    private String label;
    private String units;
    private String description;
    private boolean required;
    private String hint;
    private String format;
    private String initial;
    private String calculation;
    private List<FormDefinitionItemChoice> options;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getCalculation() {
        return calculation;
    }

    public void setCalculation(String calculation) {
        this.calculation = calculation;
    }

    public List<FormDefinitionItemChoice> getOptions() {
        return options;
    }

    public void setOptions(List<FormDefinitionItemChoice> options) {
        this.options = options;
    }
}
