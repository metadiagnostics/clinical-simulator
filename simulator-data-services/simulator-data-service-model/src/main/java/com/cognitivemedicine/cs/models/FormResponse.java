package com.cognitivemedicine.cs.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "form-response")
public class FormResponse {

    @Id
    private String id;
    private String smartformType;
    private String smartformVersion;
    private String patientId;
    private String title;
    private String location;
    private String documentType;
    private String createdDateTime;
    private String lastUpdateDateTime;
    private String authorName;
    private String specialty;
    private String status;
    private String noteType;
    private List<FormResponseItem> responses;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSmartformType() {
        return smartformType;
    }

    public void setSmartformType(String smartformType) {
        this.smartformType = smartformType;
    }

    public String getSmartformVersion() {
        return smartformVersion;
    }

    public void setSmartformVersion(String smartformVersion) {
        this.smartformVersion = smartformVersion;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    public void setLastUpdateDateTime(String lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public List<FormResponseItem> getResponses() {
        return responses;
    }

    public void setResponses(List<FormResponseItem> responses) {
        this.responses = responses;
    }
}
