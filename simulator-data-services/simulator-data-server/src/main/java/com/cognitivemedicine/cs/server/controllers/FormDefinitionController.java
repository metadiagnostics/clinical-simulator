package com.cognitivemedicine.cs.server.controllers;

import com.cognitivemedicine.cs.models.FormDefinition;
import com.cognitivemedicine.cs.server.repositories.FormDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api")
@RestController
public class FormDefinitionController {

    @Autowired
    private FormDefinitionRepository formDefinitionRepository;

    @GetMapping(value = "/formdefinition")
    public ResponseEntity<FormDefinition> getFormDefinitionByType(String type, String version) {
        FormDefinition definition = this.formDefinitionRepository.findByTypeAndVersion(type, version);
        if(definition == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(definition, HttpStatus.OK);
    }

    @PostMapping(value = "/formdefinition")
    public ResponseEntity<String> add(@RequestBody FormDefinition formDefinition) {
        FormDefinition savedDefinition = this.formDefinitionRepository.save(formDefinition);

        if (savedDefinition == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(savedDefinition.getId(), HttpStatus.CREATED);
    }

    @PutMapping(value = "/formdefinition")
    public ResponseEntity<String> update(@RequestBody FormDefinition formDefinition) {
        if (this.formDefinitionRepository.exists(formDefinition.getId())) {
            FormDefinition savedDefinition = this.formDefinitionRepository.save(formDefinition);
            return new ResponseEntity<>(savedDefinition.getId(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/formdefinition/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        if (this.formDefinitionRepository.exists(id)) {
            this.formDefinitionRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
