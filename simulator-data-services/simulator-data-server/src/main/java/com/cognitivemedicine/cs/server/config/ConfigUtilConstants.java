package com.cognitivemedicine.cs.server.config;

public class ConfigUtilConstants {
    public static final String CONTEXT_NAME = "simulator.data.server.config";
    public static final String KEY_SDS_MONGO_HOST = "mongo.host";
    public static final String KEY_SDS_MONGO_PORT = "mongo.port";
    public static final String KEY_SDS_MONGO_DB_NAME = "mongo.dbname";
}
