package com.cognitivemedicine.cs.server.config;

import com.cognitivemedicine.config.utils.ConfigUtils;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories("com.cognitivemedicine.cs.server.repositories")
public class MongoConfiguration extends AbstractMongoConfiguration {
    @Override
    protected String getDatabaseName() {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        String dbName = config.getString(ConfigUtilConstants.KEY_SDS_MONGO_DB_NAME, null);
        return dbName;
    }

    @Override
    public Mongo mongo() throws Exception {
        ConfigUtils config = ConfigUtils.getInstance(ConfigUtilConstants.CONTEXT_NAME);
        String hostName = config.getString(ConfigUtilConstants.KEY_SDS_MONGO_HOST, null);
        int port = config.getInt(ConfigUtilConstants.KEY_SDS_MONGO_PORT);
        return new MongoClient(hostName, port);
    }
}
