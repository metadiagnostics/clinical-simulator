package com.cognitivemedicine.cs.server.controllers;

import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.cs.server.repositories.FormResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RequestMapping("/api")
@RestController
public class FormResponseController {
    private DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");

    @Autowired
    private FormResponseRepository formResponseRepository;

    @GetMapping(value = "/formresponse")
    public ResponseEntity<List<FormResponse>> getFormResponsesByCriteria(String patientId, String formType, String formVersion) {
        List<FormResponse> responses = this.formResponseRepository.findByPatientIdAndSmartformTypeAndSmartformVersion(patientId, formType, formVersion);

        return new ResponseEntity<>(responses, HttpStatus.OK);
    }

    @GetMapping(value = "/formresponse/{id}")
    public ResponseEntity<FormResponse> getFormResponseById(@PathVariable("id") String id) {
        FormResponse response = this.formResponseRepository.findById(id);

        if (response == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/formresponse")
    public ResponseEntity<String> add(@RequestBody FormResponse formResponse) {
        Date today = new Date();
        if (formResponse.getCreatedDateTime() == null) {
            formResponse.setCreatedDateTime(this.dateFormat.format(today));
        }

        if (formResponse.getLastUpdateDateTime() == null) {
            formResponse.setLastUpdateDateTime(this.dateFormat.format(today));
        }

        FormResponse savedResponse = this.formResponseRepository.save(formResponse);

        if (savedResponse == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(savedResponse.getId(), HttpStatus.CREATED);
    }

    @PutMapping(value = "/formresponse")
    public ResponseEntity<String> update(@RequestBody FormResponse formResponse) {
        if (this.formResponseRepository.exists(formResponse.getId())) {
            Date today = new Date();
            formResponse.setLastUpdateDateTime(this.dateFormat.format(today));
            FormResponse savedResponse = this.formResponseRepository.save(formResponse);
            return new ResponseEntity<>(savedResponse.getId(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/formresponse/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        if (this.formResponseRepository.exists(id)) {
            this.formResponseRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/formresponse/{smartformType}/{smartformVersion:.+}")
    public ResponseEntity<String> delete(@PathVariable("smartformType") String smartformType, @PathVariable("smartformVersion") String smartformVersion) {
        this.formResponseRepository.deleteAllBySmartformTypeAndSmartformVersion(smartformType, smartformVersion);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
