package com.cognitivemedicine.cs.server.repositories;

import com.cognitivemedicine.cs.models.FormResponse;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormResponseRepository extends MongoRepository<FormResponse, String> {
    List<FormResponse> findByPatientIdAndSmartformTypeAndSmartformVersion(String patientId, String smartform_type, String smartform_version);
    FormResponse findById(String id);
    void deleteAllBySmartformTypeAndSmartformVersion(String smartformType, String smartformVersion);
}
