package com.cognitivemedicine.cs.server.repositories;

import com.cognitivemedicine.cs.models.FormDefinition;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormDefinitionRepository extends MongoRepository<FormDefinition, String> {

    FormDefinition findByTypeAndVersion(String type, String version);
}
