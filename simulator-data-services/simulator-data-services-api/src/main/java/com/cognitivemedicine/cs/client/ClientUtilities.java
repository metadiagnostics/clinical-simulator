package com.cognitivemedicine.cs.client;

import com.cognitivemedicine.cs.models.FormResponse;
import com.cognitivemedicine.cs.models.FormResponseItem;

public class ClientUtilities {
    public static int calculateGutCheckNecScore(FormResponse response) {
        int score = 0;
        for (FormResponseItem item : response.getResponses()) {
            score += item.getValue();
        }

        return score;
    }
}
