package com.cognitivemedicine.cs.client;

import com.cognitivemedicine.cs.models.FormDefinition;
import com.cognitivemedicine.cs.models.FormResponse;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import javax.ws.rs.core.MediaType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;

public class DataServiceRestClient {

    public static final Logger LOGGER = Logger.getLogger(DataServiceRestClient.class.getName());
    private String baseURL;
    private Client client;
    private DateFormat lastUpdatedFormat = new SimpleDateFormat("yyyyMMddHHmm");

    public DataServiceRestClient() {
        this("http://localhost:8080/simulator-data-server/api");
    }

    public DataServiceRestClient(String baseURL) {
        this.baseURL = baseURL;
        this.client = ClientBuilder.newClient(new ClientConfig().register( 
                new JacksonJsonProvider()
            ));
    }

    public FormDefinition retrieveFormDefinition(String type, String version) {
        FormDefinition definition = null;

        WebTarget target = client.target(baseURL);
        
        Response response = target.path("formdefinition")
            .queryParam("type", type)
            .queryParam("version", version)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .get();
            
        if (response.getStatus() == 200) {
            definition = response.readEntity(FormDefinition.class);
        } else {
            LOGGER.log(Level.INFO, String.format("Failed to find form definition for type", type));
        }

        return definition;
    }

    public FormResponse[] retrieveFormResponses(String patientId, String formType, String formVersion) {
        FormResponse[] responses = new FormResponse[0];

        WebTarget target = client.target(baseURL);
        
        Response response = target.path("formresponse")
            .queryParam("patientId", patientId)
            .queryParam("formType", formType)
            .queryParam("formVersion", formVersion)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .get();

        if (response.getStatus() == 200) {
            responses = response.readEntity(FormResponse[].class);
        } else {
            LOGGER.log(Level.INFO, String.format("Could not find form responses for patientId", patientId));
        }

        return responses;
    }

    public FormResponse[] retrieveFormResponses(String patientId, String formType, String formVersion, Date startDate, Date endDate) {
        FormResponse[] responses = this.retrieveFormResponses(patientId, formType, formVersion);

        List<FormResponse> filteredResponses = new ArrayList<>();

        if (startDate != null && endDate != null) {
            for (FormResponse response : responses) {
                if (response.getLastUpdateDateTime() != null) {
                    try {
                        Date lastUpdatedDate = this.lastUpdatedFormat.parse(response.getLastUpdateDateTime());

                        if (lastUpdatedDate.before(endDate) && lastUpdatedDate.after(startDate)) {
                            filteredResponses.add(response);
                        }
                    } catch (ParseException pe) {
                        // Skip this date if we can't parse it
                        pe.printStackTrace();
                    }
                }
            }
        }

        return filteredResponses.toArray(new FormResponse[filteredResponses.size()]);
    }

    public FormResponse retrieveFormResponseById(String responseId) {
        FormResponse response = null;

        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formresponse")
            .path(responseId)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .get();

        if (clientResponse.getStatus() == 200) {
            response = clientResponse.readEntity(FormResponse.class);
        }

        return response;
    }

    public String addFormDefinition(String definitionJson) {
        String response = null;
        
        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formdefinition")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .post(Entity.json(definitionJson));

        if(clientResponse.getStatus() == 201) {
            response = clientResponse.readEntity(String.class);
        }

        return response;
    }

    public String updateFormDefinition(String definitionJson) {
        String response = null;
        
        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formdefinition")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .put(Entity.json(definitionJson));

        if (clientResponse.getStatus() == 200) {
            response = clientResponse.readEntity(String.class);
        }

        return response;
    }

    public String addFormResponse(String responseJson) {
        String response = null;
        
        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formresponse")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .post(Entity.json(responseJson));

        if(clientResponse.getStatus() == 201) {
            response = clientResponse.readEntity(String.class);
        }

        return response;
    }

    public String updateFormResponse(String definitionJson) {
        String response = null;
        
        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formresponse")
            .request(MediaType.APPLICATION_JSON_TYPE)
            .header("Content-Type", MediaType.APPLICATION_JSON)
            .put(Entity.json(definitionJson));

        if (clientResponse.getStatus() == 200) {
            response = clientResponse.readEntity(String.class);
        }

        return response;
    }

    public boolean deleteFormResponse(String id) {

        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formresponse")
            .path(id)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .delete();
        
        if (clientResponse.getStatus() == 200) {
            return true;
        }

        return false;
    }

    public boolean deleteFormResponses(String smartformType, String smartformVersion) {

        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formresponse")
            .path(smartformType)
            .path(smartformVersion)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .delete();
        
        if (clientResponse.getStatus() == 200) {
            return true;
        }

        return false;
    }

    public boolean deleteFormDefinition(String id) {
        WebTarget target = client.target(baseURL);
        
        Response clientResponse = target.path("formdefinition")
            .path(id)
            .request(MediaType.APPLICATION_JSON_TYPE)
            .delete();

        if (clientResponse.getStatus() == 200) {
            return true;
        }

        return false;
    }

}
