/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.instance;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.instance.AbstractStepInstance;
import com.cognitivemedicine.cs.simulator.sf.config.ContextRegistryConstants;
import com.cognitivemedicine.cs.simulator.sf.definition.CreateResponsesStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.runtime.SmartFormService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class CreateResponsesStepInstance extends AbstractStepInstance {
    
    private final static Logger LOG = LoggerFactory.getLogger(CreateResponsesStepInstance.class);

    private final CreateResponsesStepDefinition definition;

    public CreateResponsesStepInstance(CreateResponsesStepDefinition definition) {
        super(definition);
        this.definition = definition;
    }

    @Override
    public void execute(ScenarioContext context) {
        
        LOG.debug("-- [START] CREATE SMART-FORM RESPONSES: " + definition);
        
        SmartFormService smartFormService = context.getContextService(ContextRegistryConstants.SMART_FORM_SERVICE_KEY);
        
        if (smartFormService == null){
            throw new IllegalStateException("No Smart-Form service was found in the context. Make sure the context was correctly configured");
        }
        
        //Replace any paramter the Form definition may have
        String finalFormDefitinon = context.getParameterResolver().resolveJsonResource(
            definition.getResponsesDefinition().getTemplate().getDefinition(), 
            definition.getResponsesDefinition().getParameters()
        );
        
        smartFormService.saveResponses(finalFormDefitinon);
        
        LOG.debug("-- [END] CREATE SMART-FORM RESPONSES");
    }
}
