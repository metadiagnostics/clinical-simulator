/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.config;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cs.simulator.sf.runtime.SmartFormService;

/**
 * Class used to configure a {@link ScenarioContext} with all the services
 * required by this module.
 * 
 * @author esteban
 */
public class ContextConfigurator {
    
    private final SmartFormService smartFormService;

    public ContextConfigurator(SmartFormService smartFormService) {
        this.smartFormService = smartFormService;
    }
    
    public void configureContext(ScenarioContext context){
        context.registerContextService(ContextRegistryConstants.SMART_FORM_SERVICE_KEY, smartFormService);
    }
    
}
