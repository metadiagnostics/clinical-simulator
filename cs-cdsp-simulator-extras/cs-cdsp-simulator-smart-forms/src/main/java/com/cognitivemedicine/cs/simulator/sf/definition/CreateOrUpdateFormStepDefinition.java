/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.definition;

import com.cognitivemedicine.cdsp.simulator.definition.AbstractStepDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;
import com.cognitivemedicine.cs.simulator.sf.instance.CreateOrUpdateFormStepInstance;

/**
 * This is the definition of a {@link CreateOrUpdateFormStepInstance} Step.
 * See the documentation in {@link CreateOrUpdateFormStepInstance} to see
 * how this step behaves on runtime.
 * 
 * @author esteban
 */
public class CreateOrUpdateFormStepDefinition extends AbstractStepDefinition {

    private FormInstanceDefinition formDefinition;
    
    public CreateOrUpdateFormStepDefinition() {
    }

    public CreateOrUpdateFormStepDefinition(FormInstanceDefinition resourceDefinition) {
        this.formDefinition = resourceDefinition;
    }

    @Override
    public StepInstance createInstance() {
        return new CreateOrUpdateFormStepInstance(this);
    }

    public FormInstanceDefinition getFormDefinition() {
        return formDefinition;
    }

    public void setFormDefinition(FormInstanceDefinition formDefinition) {
        this.formDefinition = formDefinition;
    }

    @Override
    public String toString() {
        return "CreateOrUpdateFormStepDefinition{" + "formDefinition=" + formDefinition + '}';
    }
    
}
