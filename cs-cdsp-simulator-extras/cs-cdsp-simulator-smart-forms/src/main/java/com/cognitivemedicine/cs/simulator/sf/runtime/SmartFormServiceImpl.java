/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.runtime;

import com.cognitivemedicine.cs.client.DataServiceRestClient;
import com.cognitivemedicine.cs.models.FormDefinition;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class SmartFormServiceImpl implements SmartFormService{
    
    private final static Logger LOG = LoggerFactory.getLogger(SmartFormServiceImpl.class);
    
    private final DataServiceRestClient client;

    public SmartFormServiceImpl(String url) {
        client = new DataServiceRestClient(url);
    }
    
    @Override
    public void saveOrUpdateForm(String formDefinition) {
        
        JsonElement parsed = new JsonParser().parse(formDefinition);
        String type = parsed.getAsJsonObject().get("type").getAsString();
        String version = parsed.getAsJsonObject().get("version").getAsString();
        
        LOG.debug("Looking for an existing Form definition of type '"+type+"'");
        FormDefinition existingDefinition = client.retrieveFormDefinition(type, version);
        if (existingDefinition != null){
            LOG.debug("Existing Form found. Updating it");
            
            //copy the original id to the new definition
            parsed.getAsJsonObject().addProperty("id", existingDefinition.getId());
            
            //serialize the definition
            formDefinition = new Gson().toJson(parsed);
            
            client.updateFormDefinition(formDefinition);
        } else {
            LOG.debug("Existing Form not found. Creating a new one");
            client.addFormDefinition(formDefinition);
        }
    }

    @Override
    public void clearResponses(String formType, String formVersion) {
        client.deleteFormResponses(formType, formVersion);
    }

    @Override
    public void saveResponses(String responses) {
        LOG.debug("Persisting new Resposnes");
        client.addFormResponse(responses);
    }

}
