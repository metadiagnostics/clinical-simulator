/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.definition;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class represents a Form Responses Definition Instance.
 * The word 'instance' here means a {@link ResponsesDefinition} with a specific
 * values for its parameters.
 * 
 * @author esteban
 */
public class ResponsesInstanceDefinition {
    
    /**
     * The id of the instance definition in the scenario
     */
    private String id;
    
    private ResponsesDefinition template;
    private Map<String, String> parameters = new HashMap<>();
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResponsesDefinition getTemplate() {
        return template;
    }

    public void setTemplate(ResponsesDefinition template) {
        this.template = template;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
    
    public void addParameter(String key, String value) {
        this.parameters.put(key, value);
    }

    @Override
    public String toString() {
        return "ResponsesInstanceDefinition{" + "id=" + id + ", parameters=[" + parameters.entrySet().stream().map(e -> "{"+e.getKey()+" -> "+e.getValue()+"}").collect(Collectors.joining(", ")) + "]}";
    }
    
}
