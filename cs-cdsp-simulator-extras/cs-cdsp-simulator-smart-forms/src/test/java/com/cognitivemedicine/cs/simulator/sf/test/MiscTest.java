/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.test;

import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.serialization.YamlScenarioSerializer;
import com.cognitivemedicine.cs.simulator.sf.definition.CreateOrUpdateFormStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.CreateResponsesStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.FormDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.FormInstanceDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.ResponsesDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.ResponsesInstanceDefinition;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class MiscTest {

    private final YamlScenarioSerializer serializer = new YamlScenarioSerializer();
    
    @Test
    public void serializeSampleScenario() throws IOException {


        FormDefinition formDefinition = new FormDefinition();
        formDefinition.setId("form-1");
        formDefinition.setDefinition(IOUtils.toString(new InputStreamReader(
                    CreateOrUpdateFormStepInstanceTest.class.getResourceAsStream("/forms/form-1.json")
                )
            )
        );
        
        FormInstanceDefinition formInstanceDefinition = new FormInstanceDefinition();
        formInstanceDefinition.setId("form-instance-1");
        formInstanceDefinition.setParameters(new HashMap<>());
        formInstanceDefinition.setTemplate(formDefinition);
        
        CreateOrUpdateFormStepDefinition definitionStep = new CreateOrUpdateFormStepDefinition(formInstanceDefinition);
        definitionStep.setName("Create or Update Form Definition");
        
        ResponsesDefinition responsesDefinition = new ResponsesDefinition();
        responsesDefinition.setId("responses-1");
        responsesDefinition.setDefinition(IOUtils.toString(new InputStreamReader(
            CreateOrUpdateFormStepInstanceTest.class.getResourceAsStream("/forms/responses-1.json")
        )));

        ResponsesInstanceDefinition responsesInstanceDefinition = new ResponsesInstanceDefinition();
        responsesInstanceDefinition.setId("responses-instance-1");
        responsesInstanceDefinition.setParameters(new HashMap<>());
        responsesInstanceDefinition.setTemplate(responsesDefinition);

        CreateResponsesStepDefinition responsesStep = new CreateResponsesStepDefinition(responsesInstanceDefinition);
        responsesStep.setName("Create Form Responses");

        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        scenarioDefinition.setName("Smart-Form sample for PatientId-1234");
        scenarioDefinition.addStep(definitionStep);
        scenarioDefinition.addStep(responsesStep);

        String dump = serializer.serializeScenarioDefinition(scenarioDefinition);

        System.out.println("\ndump = \n" + dump);
        System.out.println("\n");

    }
}
