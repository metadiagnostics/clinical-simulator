/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.runtime;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author esteban
 */
public class MockSmartFormServiceImpl implements SmartFormService {
    
    private Map<String, String> formDefinitionByType = new HashMap<>();
    private Map<String, List<String>> formResponsesByType = new HashMap<>();

    @Override
    public void saveOrUpdateForm(String formDefinition) {
        
        JsonElement parsed = new JsonParser().parse(formDefinition);
        
        String type = parsed.getAsJsonObject().get("type").getAsString();
        String version = parsed.getAsJsonObject().get("version").getAsString();
        
        formDefinitionByType.put(type, formDefinition);
        
        this.clearResponses(type, version);
        
    }

    @Override
    public void clearResponses(String formType, String formVersion) {
        formResponsesByType.remove(formType);
    }

    @Override
    public void saveResponses(String responses) {
        JsonElement parsed = new JsonParser().parse(responses);
        
        String smartFormType = parsed.getAsJsonObject().get("smartformType").getAsString();
        
        formResponsesByType.computeIfAbsent(smartFormType, t -> new ArrayList<>())
            .add(responses);
    }

    public Map<String, String> getFormDefinitionByType() {
        return formDefinitionByType;
    }

    public Map<String, List<String>> getFormResponsesByType() {
        return formResponsesByType;
    }

}
