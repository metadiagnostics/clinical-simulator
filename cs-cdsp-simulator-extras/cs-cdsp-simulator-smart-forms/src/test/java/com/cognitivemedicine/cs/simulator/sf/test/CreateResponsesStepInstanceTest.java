/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.sf.test;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.serialization.YamlScenarioSerializer;
import com.cognitivemedicine.cs.simulator.sf.config.ContextConfigurator;
import com.cognitivemedicine.cs.simulator.sf.definition.CreateResponsesStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.ResponsesDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.ResponsesInstanceDefinition;
import com.cognitivemedicine.cs.simulator.sf.runtime.MockSmartFormServiceImpl;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class CreateResponsesStepInstanceTest {
    
    private final MockSmartFormServiceImpl smartFormService = new MockSmartFormServiceImpl();
    private final YamlScenarioSerializer serializer = new YamlScenarioSerializer();
    
    @Test
    public void doTest() throws IOException{
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(smartFormService).configureContext(ctx);
        
        
        ResponsesDefinition definition = new ResponsesDefinition();
        definition.setId("responses-1");
        definition.setDefinition(IOUtils.toString(new InputStreamReader(
                    CreateOrUpdateFormStepInstanceTest.class.getResourceAsStream("/forms/responses-1.json")
                )
            )
        );
        
        ResponsesInstanceDefinition instanceDefinition = new ResponsesInstanceDefinition();
        instanceDefinition.setId("responses-instance-1");
        instanceDefinition.setParameters(new HashMap<>());
        instanceDefinition.setTemplate(definition);
        
        CreateResponsesStepDefinition step1 = new CreateResponsesStepDefinition(instanceDefinition);
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        scenarioDefinition.addStep(step1);
        
        String dump = serializer.serializeScenarioDefinition(scenarioDefinition);
        
        //System.out.println("\ndump = \n" + dump);
        //System.out.println("\n");
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        scenarioInstance.run();
        
        assertThat(smartFormService.getFormResponsesByType().get("gutchecknec"), not(nullValue()));
        assertThat(smartFormService.getFormResponsesByType().get("gutchecknec").size(), is(1));
    }
    
    @Test
    public void doTestWithParameters() throws IOException{
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(smartFormService).configureContext(ctx);
        
        
        ResponsesDefinition definition = new ResponsesDefinition();
        definition.setId("responses-1");
        definition.setDefinition(IOUtils.toString(new InputStreamReader(
                    CreateOrUpdateFormStepInstanceTest.class.getResourceAsStream("/forms/responses-2.json")
                )
            )
        );
        
        Map<String, String> params = new HashMap<>();
        params.put("patientId", "value/Patient-123");
        
        ResponsesInstanceDefinition instanceDefinition = new ResponsesInstanceDefinition();
        instanceDefinition.setId("responses-instance-1");
        instanceDefinition.setParameters(params);
        instanceDefinition.setTemplate(definition);
        
        CreateResponsesStepDefinition step1 = new CreateResponsesStepDefinition(instanceDefinition);
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        scenarioDefinition.addStep(step1);
        
        String dump = serializer.serializeScenarioDefinition(scenarioDefinition);
        
        //System.out.println("\ndump = \n" + dump);
        //System.out.println("\n");
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        scenarioInstance.run();
        
        assertThat(smartFormService.getFormResponsesByType().get("gutchecknec"), not(nullValue()));
        assertThat(smartFormService.getFormResponsesByType().get("gutchecknec").size(), is(1));
        
        JsonObject responses = new JsonParser().parse(smartFormService.getFormResponsesByType().get("gutchecknec").get(0)).getAsJsonObject();
        
        assertThat(responses.get("patientId").getAsString(), is("Patient-123"));
        
    }
    
}
