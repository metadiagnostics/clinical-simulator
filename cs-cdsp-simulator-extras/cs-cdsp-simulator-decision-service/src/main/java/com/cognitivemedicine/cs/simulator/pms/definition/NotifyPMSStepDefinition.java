/*
 * Copyright 2017 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.pms.definition;

import com.cognitivemedicine.cdsp.simulator.definition.AbstractStepDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;
import com.cognitivemedicine.cs.simulator.pms.instance.NotifyPMSStepInstance;

/**
 * This is the definition of a {@link NotifyPMSStepInstance} Step.
 * See the documentation in {@link NotifyPMSStepInstance} to see
 * how this step behaves on runtime.
 * 
 * @author esteban
 */
public class NotifyPMSStepDefinition extends AbstractStepDefinition {

    private String topic;
    private String data;
    private String dataSuffix;
    
    
    public NotifyPMSStepDefinition() {
    }

    public NotifyPMSStepDefinition(String topic, String data) {
        this(topic, data, "");
    }

    public NotifyPMSStepDefinition(String topic, String data, String dataSuffix) {
        this.topic = topic;
        this.data = data;
        this.dataSuffix = dataSuffix;
    }
    
    @Override
    public StepInstance createInstance() {
        return new NotifyPMSStepInstance(this);
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataSuffix() {
        return dataSuffix;
    }

    public void setDataSuffix(String dataSuffix) {
        this.dataSuffix = dataSuffix;
    }

    
    @Override
    public String toString() {
        return "NotifyPMSStepDefinition{" + "topic=" + topic + ", data=" + data+dataSuffix + '}';
    }
    
}
