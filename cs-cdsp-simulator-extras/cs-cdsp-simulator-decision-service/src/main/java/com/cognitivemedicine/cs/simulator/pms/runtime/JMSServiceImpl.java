/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.pms.runtime;

import com.cognitivemedicine.cdsp.cwf.jms.JMSServiceFactory;
import com.cognitivemedicine.cdsp.messaging.jms.JMSMessagePublisher;
import com.sun.messaging.ConnectionConfiguration;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author esteban
 */
public class JMSServiceImpl implements JMSService {

    private final static Logger LOG = LoggerFactory.getLogger(JMSServiceImpl.class);

    private final String discriminatorProperty;
    private final JMSMessagePublisher jmsProducer;

    public JMSServiceImpl() {
        // JMS Stuff
        JMSServiceFactory jmsHelper = new JMSServiceFactory();
        this.discriminatorProperty
            = jmsHelper.getConfigUtils().getString(JMSServiceFactory.ContextKeys.JMS_CASE_EVENT_DISCRIMINATOR_VALUE);
        this.jmsProducer = jmsHelper.getCaseStateMessagePublisher();
    }

    public JMSServiceImpl(String jmsConnect, String topic, String discriminatorProperty) {

        this.discriminatorProperty = discriminatorProperty;
        this.jmsProducer
            = new JMSMessagePublisher(createConnectionFactory(jmsConnect), topic, discriminatorProperty);
    }

    @Override
    public void publishString(String topic, String data) {
        this.jmsProducer.publish(data, topic);
    }

    private ConnectionFactory createConnectionFactory(String imqAddressList) {
        try {
            com.sun.messaging.ConnectionFactory connectionFactory = new com.sun.messaging.ConnectionFactory();
            connectionFactory.setProperty(ConnectionConfiguration.imqAddressList, imqAddressList);
            connectionFactory.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
            return connectionFactory;
        } catch (JMSException e) {
            throw new IllegalStateException("There was an error configure JMS channel, it will not be configured", e);
        }
    }
}
