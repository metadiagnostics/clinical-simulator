/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.pms.instance;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.instance.AbstractStepInstance;
import com.cognitivemedicine.cs.simulator.pms.config.ContextRegistryConstants;
import com.cognitivemedicine.cs.simulator.pms.definition.NotifyPMSStepDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cognitivemedicine.cs.simulator.pms.runtime.JMSService;

/**
 *
 * @author esteban
 */
public class NotifyPMSStepInstance extends AbstractStepInstance {
    
    private final static Logger LOG = LoggerFactory.getLogger(NotifyPMSStepInstance.class);

    private final NotifyPMSStepDefinition definition;

    public NotifyPMSStepInstance(NotifyPMSStepDefinition definition) {
        super(definition);
        this.definition = definition;
    }

    @Override
    public void execute(ScenarioContext context) {
        
        LOG.debug("-- [START] NOTIFY PMS: " + definition);
        
        JMSService jmsService = context.getContextService(ContextRegistryConstants.JMS_SERVICE_KEY);
        
        if (jmsService == null){
            throw new IllegalStateException("No JMS service was found in the context. Make sure the context was correctly configured");
        }
        
        String data = definition.getData() == null ? "" : context.getParameterResolver().resolveParameterValue(definition.getData());
        String dataSuffix = definition.getDataSuffix() == null ? "" : context.getParameterResolver().resolveParameterValue(definition.getDataSuffix());
        
        LOG.debug("Data to be sent to topic '"+definition.getTopic()+"': '"+data+dataSuffix+"'");
        
        jmsService.publishString(definition.getTopic(), data+dataSuffix);
        
        LOG.debug("-- [END] NOTIFY PMS");
    }
}
