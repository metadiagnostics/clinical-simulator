/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.pms.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author esteban
 */
public class MockJMSServiceImpl implements JMSService{
    
    private final Map<String, List<String>> store = new HashMap<>();

    @Override
    public void publishString(String topic, String data) {
        store.computeIfAbsent(topic, k -> new ArrayList<>()).add(data);
    }
    
    public List<String> getPublishedData(String topic){
        return store.getOrDefault(topic, Collections.EMPTY_LIST);
    }
    
}
