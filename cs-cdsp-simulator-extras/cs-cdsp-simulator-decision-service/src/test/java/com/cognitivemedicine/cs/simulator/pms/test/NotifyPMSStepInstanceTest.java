/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.pms.test;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.serialization.YamlScenarioSerializer;
import com.cognitivemedicine.cs.simulator.pms.config.ContextConfigurator;
import com.cognitivemedicine.cs.simulator.pms.definition.NotifyPMSStepDefinition;
import com.cognitivemedicine.cs.simulator.pms.runtime.MockJMSServiceImpl;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.Locale;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class NotifyPMSStepInstanceTest {
    
    private final MockJMSServiceImpl jmsService = new MockJMSServiceImpl();
    private final YamlScenarioSerializer serializer = new YamlScenarioSerializer();
    
    @Test
    public void doTest(){
        String topic = "ALL";
        String data = "Some-String";
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(jmsService).configureContext(ctx);
        
        
        NotifyPMSStepDefinition step1 = new NotifyPMSStepDefinition();
        step1.setTopic(topic);
        step1.setData("value/"+data);
        
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        scenarioDefinition.addStep(step1);
        
        String dump = serializer.serializeScenarioDefinition(scenarioDefinition);
        
        System.out.println("\ndump = \n" + dump);
        System.out.println("\n");
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        scenarioInstance.run();
        
        assertThat(jmsService.getPublishedData(topic).size(), is(1));
        assertThat(jmsService.getPublishedData(topic).get(0), is(data));
        
    }
    
    @Test
    public void doVariableParameterTest(){
        
        String topic = "ALL";
        String data = "value/KTD-NEC-";
        String dataSuffix = "datetime/N";
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(jmsService).configureContext(ctx);
        
        
        NotifyPMSStepDefinition step1 = new NotifyPMSStepDefinition();
        step1.setTopic(topic);
        step1.setData(data);
        step1.setDataSuffix(dataSuffix);
        
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        scenarioDefinition.addStep(step1);
        
        String dump = serializer.serializeScenarioDefinition(scenarioDefinition);
        
        System.out.println("\ndump = \n" + dump);
        System.out.println("\n");
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        scenarioInstance.run();
        
        assertThat(jmsService.getPublishedData(topic).size(), is(1));
        
    }
    
}
