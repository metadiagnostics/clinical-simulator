/*
 * Copyright 2018 Cognitive Medicine Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.cs.simulator.pms.test;

import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cs.simulator.pms.config.ContextConfigurator;
import com.cognitivemedicine.cs.simulator.pms.definition.NotifyPMSStepDefinition;
import com.cognitivemedicine.cs.simulator.pms.runtime.JMSService;
import com.cognitivemedicine.cs.simulator.pms.runtime.JMSServiceImpl;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author esteban
 */
public class NotifyPMSStepInstanceIntegrationTest {
    
    private final JMSService jmsService = new JMSServiceImpl("localhost:7676", "JMS_CHANNEL_RESPONSE", "DSSRequestId");
    
    @Test
    @Ignore("Only for dev")
    public void doVariableParameterTest(){
        
        String topic = "CASE_ALL";
        String data = "value/KTD-NEC-";
        String dataSuffix = "value/"+DateTimeFormatter.ISO_INSTANT.format(Instant.now());
        
        ScenarioContext ctx = new ScenarioContext();
        new ContextConfigurator(jmsService).configureContext(ctx);
        
        
        NotifyPMSStepDefinition step1 = new NotifyPMSStepDefinition();
        step1.setTopic(topic);
        step1.setData(data);
        step1.setDataSuffix(dataSuffix);
        
        
        ScenarioDefinition scenarioDefinition = new ScenarioDefinition();
        scenarioDefinition.addStep(step1);
        
        ScenarioInstance scenarioInstance = scenarioDefinition.createInstance(ctx);
        scenarioInstance.run();
        
    }
    
}
