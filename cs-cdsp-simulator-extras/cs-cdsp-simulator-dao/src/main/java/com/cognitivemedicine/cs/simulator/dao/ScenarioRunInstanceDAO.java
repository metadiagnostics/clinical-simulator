/** *****************************************************************************
 *
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 ****************************************************************************** */
package com.cognitivemedicine.cs.simulator.dao;

import org.carewebframework.hibernate.core.AbstractDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScenarioRunInstanceDAO extends AbstractDAO<ScenarioRunInstance> {

    private static final Logger LOG = LoggerFactory.getLogger(ScenarioRunInstanceDAO.class);

    public ScenarioRunInstanceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public boolean saveOrUpdateScenarioSteps(String name, String startTime,
                                             String endTime, String steps) {
        if (steps.equals("")) {
            LOG.error("Query " + name + " did not contain any executed steps");
            return false;
        }

        ScenarioRunInstance scenarioRunInstance = new ScenarioRunInstance(name, startTime, endTime, steps);
        LOG.debug("save the scenarioRunInstance");
        this.saveOrUpdate(scenarioRunInstance);

        return true;
    }

    public List<ScenarioRunInstance> getRanScenarioInstances() {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        List<ScenarioRunInstance> retVal = new ArrayList<>();

        String hql = "FROM com.cognitivemedicine.cs.simulator.dao.ScenarioRunInstance";

        try {
            Query query = session.createQuery(hql);
            retVal.addAll(query.list());
            tx.commit();
        } catch(Exception e) {
            tx.rollback();
            throw e;
        }

        return retVal;
    }


    public ScenarioRunInstance getInstanceById(String id) {
        return this.get(ScenarioRunInstance.class, id);
    }

    public List<ScenarioHelper.ExecutedStep> getExecutedSteps(ScenarioRunInstance scenarioInstance) {
        String steps = scenarioInstance.getSteps();
        List<ScenarioHelper.ExecutedStep> retVal = new ArrayList<>();

        String stepsDetails [] = steps.split(ScenarioHelper.STEP__DELIMITER);
        for (String temp: stepsDetails) {
            LOG.debug("step detail: " + temp);
            ScenarioHelper.ExecutedStep step = new ScenarioHelper.ExecutedStep(temp);
            retVal.add(step);
        }

        Collections.reverse(retVal);
        return retVal;
    }

    public void deleteInstance(ScenarioRunInstance scenarioInstance) {
        this.delete(scenarioInstance);
    }
}
