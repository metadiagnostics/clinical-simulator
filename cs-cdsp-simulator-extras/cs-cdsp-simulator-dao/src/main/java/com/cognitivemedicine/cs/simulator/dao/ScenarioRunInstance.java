/** *****************************************************************************
 *
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 ****************************************************************************** */
package com.cognitivemedicine.cs.simulator.dao;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "CWF_SCENARIO_INSTANCES")
public class ScenarioRunInstance {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id = "";

    @Column(name = "name")
    private String name = "";

    @Column(name = "starttime")
    private String starttime = "";

    @Column(name = "endtime")
    private String endtime = "";

    @Lob
    @Column(name = "steps")
    private String steps = "";

    public ScenarioRunInstance() {
    }

    public ScenarioRunInstance(String name, String startTime, String endTime, String steps) {
        this.setId(UUID.randomUUID().toString());
        this.setName(name);
        this.setStartTime(startTime);
        this.setEndtime(endTime);
        this.setSteps(steps);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getStartTime() {
        return starttime;
    }

    public void setStartTime(String startTime) {
        this.starttime = startTime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
