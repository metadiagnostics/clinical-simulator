/** *****************************************************************************
 *
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 ****************************************************************************** */

package com.cognitivemedicine.cs.simulator.dao;

import java.util.List;

public class ScenarioHelper {
    public static final String STEP_DETAIL_DELIMITER = "XXX";
    public static final String STEP__DELIMITER = ",";

    public static String getSteps(List<ExecutedStep> executedSteps) {
        String retVal = "";

        for (ScenarioHelper.ExecutedStep step : executedSteps) {
            retVal += step;
            retVal += STEP__DELIMITER;
        }

//        System.out.println(" RxR GET STEPS: " + STEP_DETAIL_DELIMITER + retVal);

        return retVal;
    }

    public static class ExecutedStep {
        private String name = "";
        private String id = "";
        private String time = "";
        private String resourceType = "";
        private String detail = "";
        private long index = 0;

        public ExecutedStep() {

        }

        public ExecutedStep(String step) {
            String stepDetail []  = step.split(STEP_DETAIL_DELIMITER);
            this.id = stepDetail[0];
            this.resourceType = stepDetail[1];
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String toString() {
            String retVal = "";
            retVal += getId() + STEP_DETAIL_DELIMITER;
            retVal += getResourceType() + STEP_DETAIL_DELIMITER;
            retVal += getName() + STEP_DETAIL_DELIMITER;
            retVal += getTime() + STEP_DETAIL_DELIMITER;
            retVal += getIndex() + STEP_DETAIL_DELIMITER;

            return retVal;
        }

        public void setIndex(long index) {
            this.index = index;
        }

        public long getIndex() {
            return index;
        }
    }
}
