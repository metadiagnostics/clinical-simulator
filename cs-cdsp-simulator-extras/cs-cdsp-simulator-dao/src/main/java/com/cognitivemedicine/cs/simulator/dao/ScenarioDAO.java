/** *****************************************************************************
 *
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 ****************************************************************************** */
package com.cognitivemedicine.cs.simulator.dao;

import org.carewebframework.hibernate.core.AbstractDAO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ScenarioDAO extends AbstractDAO<Scenario> {
    private static final String HQL_SELECT = "FROM com.cognitivemedicine.cs.simulator.dao.Scenario";

    public ScenarioDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public void addOrUpdateScenario(String id, String definition) {
        Scenario scenario = new Scenario(id, definition);
        this.saveOrUpdate(scenario);
    }

    public List<Scenario> getScenarios() {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        List<Scenario> list = new ArrayList<>();

        try {
            Query query = session.createQuery(HQL_SELECT);
            list.addAll(query.list());
            tx.commit();
        } catch(Exception e) {
            tx.rollback();
            throw e;
        }

        return list;
    }

    public List<String> getScenarioNames() {
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        List<String> names = new ArrayList<>();

        try {
            Query query = session.createQuery("select id from com.cognitivemedicine.cs.simulator.dao.Scenario");
            names.addAll(query.list());
            tx.commit();
        } catch(Exception e) {
            tx.rollback();
            throw e;
        }

        return names;
    }

    public Scenario getScenarioById(String id) {
        return this.get(Scenario.class, id);
    }

    public void deleteScenario(String id) {
        Scenario s = new Scenario();
        s.setId(id);
        this.delete(s);
    }
}
