/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing;

import com.cognitivemedicine.csv.util.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import com.cognitivemedicine.csv.util.Misc;
import org.hl7.fhir.dstu3.model.BaseResource;

/**
 *
 * @author esteban
 */
public abstract class BaseSectionParser implements SectionParser {
    
    private final List<BaseResource> resources = new ArrayList<>();

    @Override
    public void parse(BufferedReader reader) {
        try {
            String text = Misc.stringifyReader(reader);
            parseText(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parseText(String text) throws IllegalStateException {
        boolean headersParsed = false;
        boolean inSection = false;
        String[] lines;

        try {
            lines = text.split(Constants.LINE_DELIMITER_REGEX);
        } catch (PatternSyntaxException e) {
            throw new IllegalStateException("Exception parsing lines", e);
        }

        if (lines.length == 0) {
            throw new IllegalStateException("Exception: no lines were found", new Exception());
        }

        // start processing the lines passed in splitted by carriage return
        for (String line : lines) {

            //First let's find the section
            if (!inSection) {
                if (line.split(Constants.FIELD_DELIMITER).length > 0 &&
                        this.getSectionDelimiter().equals(line.split(Constants.FIELD_DELIMITER)[0].trim())) {
                    inSection = true;
                }
                continue;
            }

            if (!headersParsed) {
                parseHeader(line);
                headersParsed = true;
            } else if (isEmptyCSVLine(line)) {
                return;
            } else {
                parseBody(line);
            }
        }
    }

    @Override
    public List<BaseResource> getResources() {
        return this.resources;
    }
    
    protected void addResource(BaseResource resource){
        this.resources.add(resource);
    }
    
    protected abstract void parseHeader(String line);
    protected abstract void parseBody(String line);
    
    private boolean isEmptyCSVLine(String line){
        return ("".equals(line.trim()) || line.trim().matches("^,+$"));
    }
    
}
