/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.util;

import org.hl7.fhir.dstu3.model.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.hl7.fhir.exceptions.FHIRException;

public class FhirUtil {

    public static MedicationRequest buildMedicationRequest(Patient patient, String id, CodeableConcept cc, String value, Date timestamp) {
        MedicationRequest req = new MedicationRequest();
        req.setId(id);
        req.setSubject(buildReference(patient));
        req.setMedication(cc);

        if (value != null) {
            try {
                req.setStatus(MedicationRequest.MedicationRequestStatus.fromCode(value.toLowerCase()));
            } catch (FHIRException ex) {
            }
        }
        
        req.setAuthoredOn(timestamp);
        
        req.addDosageInstruction(createDosage(value, timestamp, timestamp, 3, Timing.UnitsOfTime.D, 10, "cc"));
        
        return req;
    }

    public static List<BaseResource> buildMedicationAdministration(Patient patient, String id, CodeableConcept cc, double doseValue, Date timestamp, MedicationRequest request) {
        
        List<BaseResource> results = new ArrayList<>();
        
        MedicationAdministration medAdmin = new MedicationAdministration();
        medAdmin.setId(id);
        medAdmin.setSubject(buildReference(patient));
        
        MedicationAdministration.MedicationAdministrationDosageComponent dosage = new MedicationAdministration.MedicationAdministrationDosageComponent();
        SimpleQuantity doseQuantity = new SimpleQuantity();
        doseQuantity.setValue(doseValue);
        dosage.setDose(doseQuantity);
        medAdmin.setDosage(dosage);
        
        medAdmin.setEffective(new DateTimeType(timestamp));
        medAdmin.setMedication(cc);

        if (request != null) {
            medAdmin.setPrescription(buildReference(request));
        } else {
            //create a mock prescription
            MedicationRequest mReq = buildMedicationRequest(patient, UUID.randomUUID().toString(), cc, MedicationRequest.MedicationRequestStatus.ACTIVE.toCode(), timestamp);
            medAdmin.setPrescription(buildReference(mReq));
            results.add(mReq);
            
        }

        results.add(medAdmin);
        
        return results;
    }
    
    public static Dosage createDosage(String text, Date start, Date end, int frequency, Timing.UnitsOfTime periodUnit, double doseValue, String doseUnit){
        return new Dosage()
            .setText(text)
            .setTiming(new Timing()
                .setRepeat(new Timing.TimingRepeatComponent()
                    .setBounds(new Period()
                        .setStart(start)
                        .setEnd(end)
                    )
                    .setFrequency(frequency)
                    .setPeriodUnit(periodUnit)
                )
            )
            .setDose(new SimpleQuantity()
                .setValue(doseValue)
                .setUnit(doseUnit)
            );
    }

    public static Observation buildObservation(Patient patient, String id, CodeableConcept codeableConcept, double value, String unit, String category) {
        Quantity quantity = new SimpleQuantity();
        quantity.setValue(value);
        quantity.setUnit(unit);
        quantity.setCode(unit);
        return buildObservation(patient, id, codeableConcept, quantity, category);
    }

    public static Observation buildObservation(Patient patient, String id, CodeableConcept codeableConcept, String value, String category) {
        return buildObservation(patient, id, codeableConcept, new StringType(value), category);
    }

    public static Observation buildObservation(Patient patient, String id, CodeableConcept codeableConcept, Type value, String category) {
        Observation obs = new Observation();
        obs.setId(id);
        obs.setSubject(buildReference(patient));
        obs.setCode(codeableConcept);
        obs.setValue(value);
        CodeableConcept categoryConcept = FhirUtil.buildCodeableConcept(category, category);
        obs.addCategory(categoryConcept);
        return obs;
    }

    public static NutritionOrder buildNutritionOrder(Patient patient, String id, String codeSystem, String code,
        NutritionOrder.NutritionOrderStatus status, Date timestamp) {
        NutritionOrder order = new NutritionOrder();
        order.setId(id);
        order.setPatient(buildReference(patient));
        order.setStatus(status);
        order.setDateTime(timestamp);

        NutritionOrder.NutritionOrderOralDietComponent diet = new NutritionOrder.NutritionOrderOralDietComponent();
        diet.addType(buildCodeableConcept(codeSystem, code, ""));
        order.setOralDiet(diet);

        return order;
    }

    public static List<BaseResource> buildDiagnosticReport(Patient patient, ProcedureRequest request, CodeableConcept resultCode, Date date,
        Practitioner practitioner, Organization providerOrganization, Organization deptOrganization,
        CodeableConcept categoryCode, String status, Observation resultObservation) {
        DiagnosticReport report = new DiagnosticReport();
        report.setSubject(buildReference(patient));

        if (request != null) {
            List<Resource> containedResources = new ArrayList<>();
            containedResources.add(request);
            report.setContained(containedResources);
            report.addBasedOn(buildReference(request));
        }

        List<DiagnosticReport.DiagnosticReportPerformerComponent> performers = new ArrayList<>();

        if (deptOrganization != null) {
            DiagnosticReport.DiagnosticReportPerformerComponent deptPerformer = new DiagnosticReport.DiagnosticReportPerformerComponent();
            deptPerformer.setActor(buildReference(deptOrganization));
            performers.add(deptPerformer);
        }

        if (providerOrganization != null) {
            DiagnosticReport.DiagnosticReportPerformerComponent providerOrgPerformer = new DiagnosticReport.DiagnosticReportPerformerComponent();
            providerOrgPerformer.setActor(buildReference(providerOrganization));
            performers.add(providerOrgPerformer);
        }

        if (practitioner != null) {
            DiagnosticReport.DiagnosticReportPerformerComponent providerPerformer = new DiagnosticReport.DiagnosticReportPerformerComponent();
            providerPerformer.setActor(buildReference(practitioner));
            performers.add(providerPerformer);
        }

        if (resultObservation != null) {
            List<Reference> resultReferenceList = new ArrayList<>();
            resultReferenceList.add(buildReference(resultObservation));

            report.setResult(resultReferenceList);
        }

        report.setEffective(new DateTimeType(date));
        report.setIssued(date);
        report.setPerformer(performers);
        report.setCategory(categoryCode);
        report.setCode(resultCode);

        if (status != null) {
            report.setStatus(DiagnosticReport.DiagnosticReportStatus.valueOf(status));
        }

        List<BaseResource> finalResourceList = new ArrayList<>();

        if (resultObservation != null) {
            finalResourceList.add(resultObservation);
        }

        finalResourceList.add(report);
        return finalResourceList;
    }

    public static ProcedureRequest buildProcedureRequest(Patient patient, String id, Practitioner practitioner, CodeableConcept codeableConcept, Date date) {
        ProcedureRequest pr = new ProcedureRequest();
        pr.setId(id);
        pr.setSubject(buildReference(patient));
        pr.setCode(codeableConcept);
        pr.setAuthoredOn(date);

        if (practitioner != null) {
            pr.setPerformer(buildReference(practitioner));
        }

        return pr;
    }

    public static Organization buildOrganization(String id, String name, CodeableConcept organizationCode) {
        List<CodeableConcept> types = new ArrayList<>();
        types.add(organizationCode);

        Organization org = new Organization();
        org.setId(id);
        org.setName(name);
        org.setType(types);
        return org;
    }

    public static Practitioner buildPractitioner(String id, String firstName, String lastName) {
        Practitioner p = new Practitioner();
        p.setId(id);

        List<HumanName> names = buildName(firstName, lastName);
        names.get(0).setText(lastName + "," + firstName);
        p.setName(names);

        return p;
    }

    public static PractitionerRole buildPractitionerRole(String id, Practitioner practitioner, Organization organization) {
        PractitionerRole role = new PractitionerRole();
        role.setId(id);
        role.setPractitioner(buildReference(practitioner));
        role.setOrganization(buildReference(organization));
        return role;
    }

    public static Condition buildCondition(Patient patient, CodeableConcept codeableConcept, Date date, Practitioner practitioner, CodeableConcept severity) {
        Condition condition = new Condition();
        condition.setCode(codeableConcept);
        condition.setAssertedDateElement(new DateTimeType(date));
        condition.setOnset(new DateTimeType(date));
        condition.setSubject(buildReference(patient));
        if (practitioner != null) {
            condition.setAsserter(buildReference(practitioner));
        }
        condition.setSeverity(severity);

        return condition;
    }

    public static CodeableConcept buildCodeableConcept(String code, String display) {
        return buildCodeableConcept("http://snomed.info/sct", code, display);
    }

    public static CodeableConcept buildCodeableConcept(String codeSystem, String code, String display) {
        CodeableConcept codeableConcept = new CodeableConcept();
        List<Coding> codings = new ArrayList<>();
        codings.add(new Coding(codeSystem, code, display));
        codeableConcept.setCoding(codings);
        codeableConcept.setText(display);
        return codeableConcept;
    }

    public static List<HumanName> buildName(String firstName, String lastName) {
        // this is a sick way to set a name :(
        List<HumanName> names = new ArrayList<HumanName>();
        HumanName humanName = new HumanName();
        humanName.setFamily(lastName);
        List<StringType> given = new ArrayList<StringType>();
        given.add(new StringType(firstName));
        humanName.setGiven(given);
        names.add(humanName);
        return names;
    }

    public static Reference buildReference(Resource resource) {
        Reference reference = new Reference(resource.getResourceType().name() + "/" + resource.getIdElement().getIdPart());
        reference.setResource(resource);
        return reference;
    }

    public static Date convertCSVTimeStampToDate(String time) {
        String expectedPattern = "MM/dd/yy HH:mm";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);

        try {
            return formatter.parse(time);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Exception parsing date: " + time, e);
        }
    }

}
