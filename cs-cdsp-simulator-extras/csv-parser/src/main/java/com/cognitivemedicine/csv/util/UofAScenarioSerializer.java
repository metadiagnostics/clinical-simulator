/*
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.util;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.cognitivemedicine.cdsp.simulator.definition.*;
import com.cognitivemedicine.cdsp.simulator.serialization.ScenarioSerializer;
import com.cognitivemedicine.cdsp.simulator.serialization.YamlScenarioSerializer;
import com.cognitivemedicine.cs.simulator.pms.definition.NotifyPMSStepDefinition;
import com.cognitivemedicine.cs.simulator.sf.definition.*;
import com.cognitivemedicine.csv.processing.CodeableConceptSectionParser;
import com.cognitivemedicine.csv.processing.FHIRResourceSectionParser;
import com.cognitivemedicine.csv.processing.Parser;
import com.cognitivemedicine.csv.processing.PatientSectionParser;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.BaseResource;
import org.hl7.fhir.dstu3.model.Patient;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 *
 * @author esteban
 */

public class UofAScenarioSerializer implements ScenarioSerializer{
    private static final String ALLOWED_REGEX = ".*\\.uofacsv";

    private final FhirContext fhirContext;
    private final IParser fhirParser;

    private boolean addFormsSteps;
    private boolean addNotifyPMSStep;

    public UofAScenarioSerializer() {
        this.fhirContext = FhirContext.forDstu3();
        this.fhirParser = this.fhirContext.newXmlParser();
    }

    @Override
    public boolean accepts(String filename) {
        return org.apache.commons.lang.StringUtils.isNotBlank(filename)
                && filename.matches(ALLOWED_REGEX);
    }

    public ScenarioDefinition toScenarioDefinition(List<BaseResource> resources){
        List<StepDefinition> steps = this.toStepDefinitions(resources);

        ScenarioDefinition scenario = new ScenarioDefinition();
        scenario.setSteps(steps);

        return scenario;
    }

    public List<StepDefinition> toStepDefinitions(List<BaseResource> resources){

        List<StepDefinition> steps = new ArrayList<>();

        String patientId = null;
        for (BaseResource resource : resources) {
            steps.add(this.createStepDefinition(resource));

            if (resource instanceof Patient){
                patientId = ((Patient)resource).getIdElement().getIdPart();
            }
        }

        if (addFormsSteps){
            try{
                steps.add(this.createFormDefinitionStep());
                steps.add(this.createFormResponseStep(patientId));
            } catch (IOException e){
                throw new RuntimeException("Exception creating CreateResponsesStepDefinition or CreateOrUpdateFormStepDefinition", e);
            }
        }

        if (addNotifyPMSStep){
            try {
                //TODO: take the latest date from the last step.
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2018-02-12");
                steps.add(this.createNotifyPMSStep(date));
            } catch (ParseException ex) {
                throw new RuntimeException("Exception parsing date", ex);
            }

        }

        return steps;
    }

    public String toYaml(List<BaseResource> resources){
        return this.toYaml(this.toScenarioDefinition(resources));
    }

    public String toYaml(ScenarioDefinition scenario){
        YamlScenarioSerializer serializer = new YamlScenarioSerializer();
        return serializer.serializeScenarioDefinition(scenario);
    }

    public void setAddFormsSteps(boolean addFormsSteps) {
        this.addFormsSteps = addFormsSteps;
    }

    public void setAddNotifyPMSStep(boolean addNotifyPMSStep) {
        this.addNotifyPMSStep = addNotifyPMSStep;
    }

    private StepDefinition createStepDefinition(BaseResource r){

        ResourceTemplateDefinition template = new ResourceTemplateDefinition();
        template.setId(UUID.randomUUID().toString());
        template.setDefinition(fhirParser.encodeResourceToString(r));

        ResourceInstanceDefinition instance = new ResourceInstanceDefinition();
        //instance.setId(id); TODO
        instance.setTemplate(template);

        CreateOrUpdateResourceStepDefinition stepDefinition = new CreateOrUpdateResourceStepDefinition(instance);
        stepDefinition.setName("Create Resource "+r.getClass().getSimpleName());

        return stepDefinition;

    }

    private CreateOrUpdateFormStepDefinition createFormDefinitionStep() throws IOException {
        FormDefinition definition = new FormDefinition();
        definition.setId("definition-1");
        definition.setDefinition(IOUtils.toString(new InputStreamReader(
                UofAScenarioSerializer.class.getResourceAsStream("/forms/definition-1.json")
        )));

        FormInstanceDefinition instanceDefinition = new FormInstanceDefinition();
        instanceDefinition.setId("definition-instance-1");
        instanceDefinition.setParameters(new HashMap<>());
        instanceDefinition.setTemplate(definition);

        CreateOrUpdateFormStepDefinition stepDefinition = new CreateOrUpdateFormStepDefinition(instanceDefinition);
        stepDefinition.setName("Create NEC Form");

        return stepDefinition;
    }

    private CreateResponsesStepDefinition createFormResponseStep(String patientId) throws IOException{
        ResponsesDefinition definition = new ResponsesDefinition();
        definition.setId("responses-1");
        definition.setDefinition(IOUtils.toString(new InputStreamReader(
                        UofAScenarioSerializer.class.getResourceAsStream("/forms/response-1.json")
                )
                )
        );

        Map<String, String> params = new HashMap<>();
        params.put("patientId", "value/"+patientId);

        ResponsesInstanceDefinition instanceDefinition = new ResponsesInstanceDefinition();
        instanceDefinition.setId("responses-instance-1");
        instanceDefinition.setParameters(params);
        instanceDefinition.setTemplate(definition);

        CreateResponsesStepDefinition stepDefinition = new CreateResponsesStepDefinition(instanceDefinition);
        stepDefinition.setName("Create Form Response ("+patientId+")");

        return stepDefinition;
    }

    private NotifyPMSStepDefinition createNotifyPMSStep(Date date){
        String topic = "CASE_ALL";
        String data = "value/KTD-NEC-";
        String dataSuffix = "value/"+DateTimeFormatter.ISO_INSTANT.format(date.toInstant());

        NotifyPMSStepDefinition step = new NotifyPMSStepDefinition();
        step.setTopic(topic);
        step.setData(data);
        step.setDataSuffix(dataSuffix);

        step.setName("Start Reasoning");

        return step;
    }

    @Override
    public String serializeScenarioDefinition(ScenarioDefinition scenarioDefinition) {
        return null;
    }

    public ScenarioDefinition deserializeScenarioDefinition(String name, File definitionFile) throws IOException {
        String definition = FileUtils.readFileToString(definitionFile, Charset.forName("UTF-8"));
        return deserializeScenarioDefinition(name, definition);
    }

    @Override
    public ScenarioDefinition deserializeScenarioDefinition(String name, Reader destinationReader) throws IOException {
        return deserializeScenarioDefinition(name, IOUtils.toString(destinationReader));
    }

    public ScenarioDefinition deserializeScenarioDefinition(String name, String definition) throws IOException {

        PatientSectionParser patientSectionParser = new PatientSectionParser();
        CodeableConceptSectionParser ccSectionParser = new CodeableConceptSectionParser();
        FHIRResourceSectionParser fhirResourceSectionParser = new FHIRResourceSectionParser(patientSectionParser, ccSectionParser);

        Parser parser = new Parser();
        parser.setSectionParsers(patientSectionParser, ccSectionParser, fhirResourceSectionParser);
        ScenarioDefinition scenarioDefinition = null;
        try {
            parser.parseText(definition);
            List<BaseResource> fhirResources = parser.getFHIRResources();

            UofAScenarioSerializer serializer = new UofAScenarioSerializer();
            scenarioDefinition = serializer.toScenarioDefinition(fhirResources);

            if (!StringUtils.isBlank(name)) {
                scenarioDefinition.setName(name);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException("unable to parse file content");
        }

        return scenarioDefinition;
    }
}
