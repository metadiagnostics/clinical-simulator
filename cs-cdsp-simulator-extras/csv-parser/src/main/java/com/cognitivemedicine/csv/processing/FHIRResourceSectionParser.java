/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.processing;

import com.cognitivemedicine.csv.processing.type.*;
import com.cognitivemedicine.csv.util.Constants;
import com.cognitivemedicine.csv.util.FhirUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.util.UuidUtil;
import org.hl7.fhir.dstu3.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FHIRResourceSectionParser extends BaseSectionParser {

    private final PatientSectionParser patientParser;
    private final CodeableConceptSectionParser codeableConceptsParser;

    private String[] headers = null;

    private final Map<Integer, Map<String, FhirTypeProcessor>> typeProcessorsByCol = new HashMap<>();

    public FHIRResourceSectionParser(PatientSectionParser patientParser, CodeableConceptSectionParser codeableConceptsParser) {
        this.patientParser = patientParser;
        this.codeableConceptsParser = codeableConceptsParser;
    }

    @Override
    public String getSectionDelimiter() {
        return Constants.FHIR_RESOURCE_DELIMITER_LINE;
    }

    @Override
    protected void parseHeader(String line) {
        this.headers = line.split(Constants.FIELD_DELIMITER);
    }

    @Override
    protected void parseBody(String line) {
        String[] parts = line.split(Constants.FIELD_DELIMITER);
        Date timestamp = null;

        if (headers.length < parts.length) {
            throw new IllegalArgumentException("The number of elements in the line is higher than the number of elements in the header: " + parts.length + "!=" + headers.length + "\n. Line: " + line);
        }

        Patient patient = this.patientParser.getPatient();

        if (patient == null) {
            throw new IllegalStateException("No patient was found in parser");
        }

        for (int i = 0; i < parts.length; i++) {

            // there are some headers that are null, skip those
            if (headers[i].equals("")) {
                continue;
            }

            // if there is skipable value in the cell, skip
            if (this.isSkippableValue(parts[i])) {
                continue;
            }

            if (Constants.FHIR_RESOURCE_DATE_TIME.equals(headers[i])) {
                timestamp = FhirUtil.convertCSVTimeStampToDate(parts[i]);
            } else {

                List<BaseResource> resources = createFHIRResources(i, patient, headers[i], parts[i].trim(), timestamp);
                if (resources == null) {
                    // TODO log something and continue
                    // Something went wrong, skip and process the next one
                    continue;
                }

                for (BaseResource resource : resources) {
                    this.addResource(resource);
                }
            }
        }
    }

    private List<BaseResource> createFHIRResources(int colIndex, Patient patient, String display, String value, Date dateParameter) {

        List<BaseResource> resources = new ArrayList<>();

        CodeableConceptSectionParser.CodeableConceptEntry cce = this.codeableConceptsParser.getCodeableConceptEntry(display);

        if (value == null || cce == null || cce.getCodeableConcept() == null) {
            throw new IllegalArgumentException("No CodeableConcept definition found for header " + display);
        }

        CodeableConcept codeableConcept = cce.getCodeableConcept();

        String resourceType = cce.getResourceType();
        String type = cce.getType();
        String category = cce.getCategory();
        String units = cce.getUnits();

        //Allow pre-processing according to the 'type' of col.
        BaseResource openResource = null;
        FhirTypeProcessor.PreProcessResult preProcessResult = null;
        FhirTypeProcessor processor = getProcessorForType(colIndex, type);
        if (processor != null) {
            preProcessResult = processor.preProcess(colIndex, resourceType, codeableConcept, value);
            codeableConcept = preProcessResult.getCodeableConcept();
            openResource = preProcessResult.getOpenResource();
            resourceType = preProcessResult.getResourceType();
            value = preProcessResult.getValue();
        }

        try {
            switch (resourceType) {
                case Constants.FHIR_OBSERVATION:
                    resources.add(this.createObservation(patient, codeableConcept, dateParameter, category, value, units));
                    break;
                case Constants.FHIR_MEDICATION_REQUEST:
                    resources.add(this.createMedicationRequest(patient, codeableConcept, dateParameter, value));
                    break;
                case Constants.FHIR_MEDICATION_ADMINISTRATION:
                    resources.addAll(this.createMedicationAdministration(patient, (MedicationRequest) openResource, codeableConcept, dateParameter, value));
                    break;
                case Constants.FHIR_STD_NUTRITION_ORDER:
                    resources.add(this.createNutritionOrder(patient, codeableConcept, dateParameter, value));
                    break;
                case Constants.FHIR_CONDITION:
                    resources.add(this.createCondition(patient, codeableConcept, dateParameter, value, patientParser.getPractitioner(), FhirUtil.buildCodeableConcept("24484000", "Severe")));
                    break;
                case Constants.FHIR_PROCEDURE_REQUEST:
                    resources.add(this.createProcedureRequest(patient, codeableConcept, dateParameter, value));
                    break;
                case Constants.FHIR_DIAGNOSTIC_REPORT:
                    resources.addAll(this.createDiagnosticReport(patient, this.patientParser.getPractitioner(),
                            this.patientParser.getDeptOrganization(), this.patientParser.getProviderOrganization(), (ProcedureRequest) openResource, codeableConcept, dateParameter, category, value, units));
                    break;
                case Constants.FHIR_DIAGNOSTIC_REPORT_PANEL:
                    if (!StringUtils.isEmpty(cce.getChildren()) && !StringUtils.isEmpty(value)) {
                        String[] children = cce.getChildren().split("/");
                        String[] values = value.split("/");

                        resources.addAll(this.createDiagnosticReportPanel(patient, this.patientParser.getPractitioner(),
                                this.patientParser.getDeptOrganization(), this.patientParser.getProviderOrganization(), (ProcedureRequest) openResource, codeableConcept, dateParameter, category, children, values));
                    }

                    break;
                default:
                    throw new IllegalArgumentException("We don't know how to handle a Resource of type " + cce.getResourceType());
            }
        } catch (Exception e) {
            throw new IllegalStateException("Exception parsing resource", e);
        }

        if (processor != null) {
            processor.postProcess(colIndex, resources.isEmpty() ? null : resources.get(resources.size()-1), preProcessResult);
        }

        return resources;
    }

    private FhirTypeProcessor getProcessorForType(int colIndex, String type) {
        if (StringUtils.isBlank(type)) {
            return null;
        }
        Map<String, FhirTypeProcessor> processorsForCol = typeProcessorsByCol.computeIfAbsent(colIndex, i -> new HashMap<>());
        return processorsForCol.computeIfAbsent(type, t -> {
            switch (t) {
                case Constants.CC_TYPE_COMPOSITE:
                    return new CompositeTypeProcessor();
                case Constants.CC_TYPE_DYNAMIC_CODE:
                    return new DynamicCodeTypeProcessor();
                case Constants.CC_TYPE_MED_REQ_ADMIN:
                    return new MedicationRequestAdministrationTypeProcessor();
                default:
                    throw new IllegalArgumentException("Unssuported FHIR type: " + t);
            }
        });
    }

    private Condition createCondition(Patient patient, CodeableConcept codeableConcept, Date dateParameter, String value, Practitioner practitioner, CodeableConcept severity) {
        return FhirUtil.buildCondition(patient, codeableConcept, dateParameter, practitioner, severity);
    }

    private NutritionOrder createNutritionOrder(Patient patient, CodeableConcept codeableConcept, Date date, String value) {
        NutritionOrder nutritionOrder = FhirUtil.buildNutritionOrder(patient,
            UuidUtil.getTimeBasedUuid().toString(),
            codeableConcept.getCodingFirstRep().getCode(),
            codeableConcept.getCodingFirstRep().getDisplay(),
            NutritionOrder.NutritionOrderStatus.ACTIVE, date);
        return nutritionOrder;
    }

    private MedicationRequest createMedicationRequest(Patient patient, CodeableConcept codeableConcept, Date date, String value) {
        return FhirUtil.buildMedicationRequest(
            patient,
            UuidUtil.getTimeBasedUuid().toString(),
            codeableConcept,
            value,
            date
        );
    }

    private List<BaseResource> createMedicationAdministration(Patient patient, MedicationRequest medicationRequest, CodeableConcept codeableConcept, Date date, String value) {

        double dose = 0;
        try {
            if ("#VALUE!".equals(value)){  //TODO: this is just a workaround. Fix the values in the csv.
                value = "100"; 
            }
            dose = Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Unable to convert Medication Administration's dosage to double: " + value);
        }

        // TODO we really dont need this codeableConcept.getCodingFirstRep().getSystem()
        return FhirUtil.buildMedicationAdministration(
            patient,
            UuidUtil.getTimeBasedUuid().toString(),
            codeableConcept,
            dose,
            date,
            medicationRequest);

    }

    private Observation createObservation(Patient patient, CodeableConcept codeableConcept, Date date,
                                          String category, String value, String units) {

        String val = value;
        String unit = "";
        
        if (value.contains(Constants.FHIR_OBSERVATION_VALUE_UNIT_DELIMITER)){
            String[] parts = value.split(Constants.FHIR_OBSERVATION_VALUE_UNIT_DELIMITER);
            val = parts[0];
            unit = parts[1];
        } else if (units != null) {
            unit = units;
        }
        
        try {
            double doubleValue = Double.parseDouble(val);
            return FhirUtil.buildObservation(patient,
                UuidUtil.getTimeBasedUuid().toString(),
                codeableConcept,
                doubleValue,
                unit, category)
                .setIssued(date)
                .setEffective(new DateTimeType(date));
        } catch (NumberFormatException e) {
            return FhirUtil.buildObservation(patient,
                UuidUtil.getTimeBasedUuid().toString(),
                codeableConcept,
                val, category)
                .setIssued(date)
                .setEffective(new DateTimeType(date));
        }

    }

    private ProcedureRequest createProcedureRequest(Patient patient, CodeableConcept codeableConcept, Date date, String value) {
        return FhirUtil.buildProcedureRequest(patient, UuidUtil.getTimeBasedUuid().toString(), null, codeableConcept, date);
    }

    private List<BaseResource> createDiagnosticReport(Patient patient, Practitioner practitioner, Organization deptOrganization, Organization providerOrganization, ProcedureRequest request, CodeableConcept codeableConcept,
                                                      Date date, String category, String value, String units) {

        CodeableConcept categoryCode = FhirUtil.buildCodeableConcept(category, category);
        Observation resultObservation = this.createObservation(patient, codeableConcept, date, category, value, units);

        String status = "FINAL";
        return FhirUtil.buildDiagnosticReport(patient, request, codeableConcept, date, practitioner, providerOrganization, deptOrganization, categoryCode, status, resultObservation);
    }

    private List<BaseResource> createDiagnosticReportPanel(Patient patient, Practitioner practitioner, Organization deptOrganization, Organization providerOrganization, ProcedureRequest request, CodeableConcept codeableConcept,
                                                           Date date, String category, String[] children, String[] values) {
        List<BaseResource> finalResourceList = new ArrayList<>();
        CodeableConcept panelCategoryCode = FhirUtil.buildCodeableConcept(category, category);
        String status = "FINAL";

        List<BaseResource> panelList = FhirUtil.buildDiagnosticReport(patient, request, codeableConcept, date, practitioner, providerOrganization, deptOrganization, panelCategoryCode, status, null);
        DiagnosticReport panel = (DiagnosticReport) panelList.get(0);
        int currentValueIndex = 0;

        for (int i = 0; i < children.length; i++) {

            if (this.isSkippableValue(values[i])) {
                continue;
            }

            CodeableConceptSectionParser.CodeableConceptEntry childConceptEntry = this.codeableConceptsParser.getCodeableConceptEntry(children[i]);

            if (childConceptEntry.getResourceType().equals(Constants.FHIR_DIAGNOSTIC_REPORT)) {
                Observation result = this.createObservation(patient, childConceptEntry.getCodeableConcept(), date, childConceptEntry.getCategory(), values[currentValueIndex], childConceptEntry.getUnits());

                String id = Integer.valueOf(i).toString();
                result.setId(id);
                panel.addContained(result);

                Reference reference = new Reference("#" + id);
                panel.addResult(reference);
                currentValueIndex++;
            } else if (childConceptEntry.getResourceType().equals(Constants.FHIR_DIAGNOSTIC_REPORT_PANEL)) {
                if (childConceptEntry.getChildren() != null) {
                    String[] panelChildren = childConceptEntry.getChildren().split("/");
                    CodeableConcept subPanelCategoryCode = FhirUtil.buildCodeableConcept(childConceptEntry.getCategory(), childConceptEntry.getCategory());

                    List<BaseResource> subPanelList = FhirUtil.buildDiagnosticReport(patient, request, childConceptEntry.getCodeableConcept(), date, practitioner, providerOrganization, deptOrganization, subPanelCategoryCode, status, null);
                    DiagnosticReport subPanel = (DiagnosticReport) subPanelList.get(0);
                    String subPanelId = Integer.valueOf(i).toString();
                    subPanel.setId(subPanelId);

                    for (int c = 0; c < panelChildren.length; c++) {
                        CodeableConceptSectionParser.CodeableConceptEntry subPanelChildConceptEntry = this.codeableConceptsParser.getCodeableConceptEntry(panelChildren[c]);
                        Observation subPanelResult = this.createObservation(patient, subPanelChildConceptEntry.getCodeableConcept(), date, subPanelChildConceptEntry.getCategory(), values[currentValueIndex], subPanelChildConceptEntry.getUnits());

                        String subResultId = subPanelId + Integer.valueOf(c).toString();
                        subPanelResult.setId(subResultId);
                        panel.addContained(subPanelResult);

                        Reference resultReference = new Reference("#" + subResultId);
                        subPanel.addResult(resultReference);
                        currentValueIndex++;
                    }

                    Reference reference = new Reference("#" + subPanelId);
                    panel.addResult(reference);
                    panel.addContained(subPanel);
                }
            }
        }

        finalResourceList.add(panel);
        return finalResourceList;
    }

    private boolean isSkippableValue(String value) {
        return (value == null
                || value.trim().equals("")
                || value.trim().equals("0")
                || value.trim().equals("0.0")
                || value.trim().equals("0.00"));
    }
}
