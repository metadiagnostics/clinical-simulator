/* 
 * Copyright 2018 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cognitivemedicine.csv.test.processing;

import com.cognitivemedicine.csv.processing.CodeableConceptSectionParser;
import java.io.BufferedReader;
import java.io.StringReader;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

public class CodeableConceptSectionParserTest {

    @Before
    public void doBefore() {
    }

    @Test
    public void Valid() {
        CodeableConceptSectionParser parser = new CodeableConceptSectionParser();
        
        String csv = ""
            + "RESOURCE DEFINTIONS,\n"
            + "name,code system,code,resource type,type,category,units\n"
            + "Daily Weight,http//snomed.info/sct,27113001,Observation,,,g\n"
            + "Colostrum,http//snomed.info/sct,53875002,Medication Administration,,,cc\n"
            + ",,,,,";

        parser.parse(new BufferedReader(new StringReader(csv)));
        
        CodeableConcept cc1 = parser.getCodeableConcept("Daily Weight");
        assertThat(cc1, not(nullValue()));
        assertThat(cc1.getText(), is("Daily Weight (g)"));
        assertThat(cc1.getCodingFirstRep(), not(nullValue()));
        assertThat(cc1.getCodingFirstRep().getSystem(), is("http//snomed.info/sct"));
        assertThat(cc1.getCodingFirstRep().getCode(), is("27113001"));
        assertThat(cc1.getCodingFirstRep().getDisplay(), is("Daily Weight (g)"));
        
        String cc1ResourceType = parser.getCodeableConceptEntry("Daily Weight").getResourceType();
        assertThat(cc1ResourceType, is("Observation"));
        
        
        CodeableConcept cc2 = parser.getCodeableConcept("Colostrum");
        assertThat(cc2, not(nullValue()));
        assertThat(cc2.getText(), is("Colostrum (cc)"));
        assertThat(cc2.getCodingFirstRep(), not(nullValue()));
        assertThat(cc2.getCodingFirstRep().getSystem(), is("http//snomed.info/sct"));
        assertThat(cc2.getCodingFirstRep().getCode(), is("53875002"));
        assertThat(cc2.getCodingFirstRep().getDisplay(), is("Colostrum (cc)"));
        
        String cc2Type = parser.getCodeableConceptEntry("Colostrum").getResourceType();
        assertThat(cc2Type, is("Medication Administration"));
        
        
    }
}
