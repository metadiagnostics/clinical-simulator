# THIS REPO IS DEPRECATED. A NEW VERSION OF IT IS AVAILABLE HERE: https://bitbucket.org/cogmedsys/clinical-sim/ # 

# README #

This is the Clinical Simulator Main Repository. 

The Clinical Simulator is a suite of applications aimed to simulate a clinical
EHR that can be used for educational purposes, or as a place to prototype and
test new features and/or external systems interacting with an EHR.

This repo contains git submodules.

To clone this repo, run the following command:

    git clone --recursive https://bitbucket.org/cogmedsys/clinical-simulator

if one forgets to used the  --recursive option when cloning, run 

    git submodule update --init


### What is this repository for? ###

This is the repo containing the complete fusion based EMR application.
It is comprised on many modules, including all of the plugins.

### How do I get set up? ###

cd clinical-simulator, the root of the repo, run 

    mvn clean install -DskipTests
    cp ./simulator/cs-webapp/target/cs-webapp##0.0.1-SNAPSHOT.war $TOMCAT_HOME/webapps/cs-webapp.war
    restart tomcat

This repository contains multiple typescript-based modules. In order to build (tsc)
these repositories, you can use the **tsc** profile:

    mvn clean install -DskipTests -Ptsc

### Contribution guidelines ###


### Who do I talk to? ###
